<?php

return [

    
    'system_name' => 'ICOP',
    'system_company' => 'Masamart',
    'email-no-reply' => 'no-reply@icop.my',


    //setting disable 
    'disable' => 'true', //true = can & false = canot

    //currency
    'currency' => 'RM',

    //Percentage rejected = 60.00 %
    'percentage_rejected' => '60',

    //Max Eligibility 0.6
    'max_eligibility' => '0.6',

    //Recommended sum covered
    'recommended_sum_covered' => '500.00',

];
