<?php

return [

    
    'ic' => 'Please provide only Malaysian IC Number',
    'vehicle' => 'Please provide Vehicle Number',
    'postcode' => 'Please provide Postcode Malaysia Area',

    'email' => 'Please enter Email Address',


    /* Age Quote */
    'full_name' => 'Please enter Full Name as per NRIC',
    'phone' => 'Please enter without "-" e.g 016XXXX',
    'marital_status' => 'Please select Marital Status',
    'region' => 'Please select Region',
    'address_1' => 'Please enter Address Line 1', 
    'address_2' => 'Please enter Address Line 2', 


    /* Financing */
    'nric' => 'Please provide a copy of Nric. Maximum of 6MB with jpg, jpeg or png format',
    'pay_slip' => 'Please provide a copy of Payslip. Maximum of 6MB with jpg, jpeg or png format',
    'bpa_179' => 'Please provide a copy of BPA 179 Document. Maximum of 6MB with pdf format',


];
