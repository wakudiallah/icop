<?php

namespace Laravel\Fortify\Http\Controllers;

use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Password;
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\RequestPasswordResetLinkViewResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use App\Notifications\CustomResetPasswordNotification;
use Laravel\Fortify\Fortify;
use App\Models\User;
use Mail;

class PasswordResetLinkController extends Controller
{
    /**
     * Show the reset password link request view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Laravel\Fortify\Contracts\RequestPasswordResetLinkViewResponse
     */
    public function create(Request $request): RequestPasswordResetLinkViewResponse
    {
        return app(RequestPasswordResetLinkViewResponse::class);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Support\Responsable
     */
    
     public function storexx(Request $request)
     {
 
        
        $request->validate([Fortify::email() => 'required|email']);
     
         $status = $this->broker()->sendResetLink(
             $request->only(Fortify::email())
         );


         
     
         if ($status === Password::RESET_LINK_SENT) {
             $user = User::where('email', $request->email)->first();


             $token = $user->createToken('Custom Reset Password');

            // Prepare the data to be sent to the email template
            $data = [
                'token' => $token,
                'user' => $user,
            ];
            
             


            Mail::send('email.test', $data, function ($message) use ($user) {
                $message->from('wakudiallah@netxpert.com.my', 'Icop'); // Specify the sender address and name here
                $message->to($user->email)->subject('Reset Password');
                // Add any additional configuration for the email if needed
            });


         }

      
     
         return $status == Password::RESET_LINK_SENT
                     ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                     : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);

     }
    
    



    
     public function store(Request $request): Responsable
    {
       
        
        $request->validate([Fortify::email() => 'required|email']);
        
        
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = $this->broker()->sendResetLink(
            $request->only(Fortify::email())
        );


        return $status == Password::RESET_LINK_SENT
                    ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                    : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);

                    
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(): PasswordBroker
    {
        return Password::broker(config('fortify.passwords'));
    }


    public function sendResetLinkEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
                    ? back()->with(['status' => __($status)])
                    : back()->withErrors(['email' => __($status)]);
    }

}
