@extends('templates.apps-icop.template')

@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3>{{ __('messages.moto')}} </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb" style="background: white !important;">


                        <div class="section-title">

                             <!-- <h1 class="wow fadeInUp" data-wow-delay="0.6s">Hello, I am <strong>Stimulus</strong> currently York city.</h1> -->

                             
                        <div id="contact-form">

                            <h1 align="center">404 | Not Found</h1>
                            <p align="center">Sorry, your page Not Found. Please refresh and try again.</p>
                            <!-- <a href="{{ url()->previous() }}">Go Back</a> -->
                            
                            @section('code', '404')
                            @section('message', __('Not Found'))
                            
                            
                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

<style>

     .red-required {
     color: red;
     font-size: 12pt;
     }

     .invalid-feedback {
          display: none;
          color: red;
     }

     .wow .error-container {
          margin-top: -15px;
          margin-bottom: 10px;
          color: red !important;
          font-size: 10pt;
          font-style: italic;  /* Added to make the font italic */
          
     }
 

     .is-invalid {
          border-color: red !important;
     }
     .is-invalid ~ .invalid-feedback {
          display: block;
     }

 </style>


@endpush

