<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamInsurancePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_insurance_policies', function (Blueprint $table) {
            $table->id();
            $table->integer('insurance_company_id')->nullable();
            $table->longtext('product_disclosure', 150)->nullable();
            $table->string('disclosure_published', 50)->nullable();
            $table->longtext('policy_wording', 150)->nullable();
            $table->string('policy_published', 50)->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_insurance_policies');
    }
}
