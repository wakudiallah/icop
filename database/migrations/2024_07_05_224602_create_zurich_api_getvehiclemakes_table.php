<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiGetvehiclemakesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_getvehiclemakes', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('sdfMakeID', 100)->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_getvehiclemakes');
    }
}
