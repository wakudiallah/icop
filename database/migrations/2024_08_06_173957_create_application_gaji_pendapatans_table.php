<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationGajiPendapatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_gaji_pendapatans', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('gaji_pokok', 8, 2)->nullable();
            $table->decimal('imbuhan_tetap_perumahan', 8, 2)->nullable();
            $table->decimal('imbuhan_tetap_khidmat_awam', 8, 2)->nullable();
            $table->decimal('bantuan_sara_hidup', 8, 2)->nullable();
            $table->decimal('bayaran_insentif_tugas_khas', 8, 2)->nullable();
            $table->decimal('perkhidmatan_kritikal', 8, 2)->nullable();
            $table->decimal('elaun_tetap_lain_lain', 8, 2)->nullable();
            $table->decimal('insentif', 8, 2)->nullable();
            $table->decimal('penanggungan_kerja', 8, 2)->nullable();
            $table->decimal('jumlah_pendapatan', 8, 2)->nullable();
            $table->decimal('percentage', 8, 2)->nullable();
            $table->decimal('max_eligibility', 8, 2)->nullable();
            $table->decimal('available_deduction', 8, 2)->nullable();
            $table->decimal('jml_bersih', 8, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_gaji_pendapatans');
    }
}
