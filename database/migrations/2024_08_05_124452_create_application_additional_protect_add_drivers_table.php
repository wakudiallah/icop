<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationAdditionalProtectAddDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_additional_protect_add_drivers', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('driver_nric')->nullable();
            $table->string('code_relation')->nullable();
            $table->string('relationship')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_additional_protect_add_drivers');
    }
}
