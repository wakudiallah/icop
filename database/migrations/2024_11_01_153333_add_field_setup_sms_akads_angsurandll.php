<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSetupSmsAkadsAngsurandll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->string('text_angsuran')->before('text_caj_perkhidmatan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->dropColumn('text_angsuran');
        });
    }
}
