<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceBasicPremiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_basic_premia', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('success')->nullable();
            $table->string('errorMsg')->nullable();
            $table->string('quotationNo')->nullable();
            $table->decimal('totBasicNetAmt', 8,2)->nullable();
            $table->decimal('stampDutyAmt', 8,2)->nullable();
            $table->decimal('gst_Amt', 8,2)->nullable();
            $table->decimal('gst_Pct', 8,2)->nullable();
            $table->decimal('rebatePct', 8,2)->nullable();
            $table->decimal('rebateAmt', 8,2)->nullable();
            $table->decimal('totMtrPrem', 8,2)->nullable();
            $table->decimal('ttlPayablePremium', 8,2)->nullable();
            $table->decimal('ncdAmt', 8,2)->nullable();
            $table->decimal('ncd_Pct', 8,2)->nullable();
            $table->timestamp('quote_Exp_Date')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_basic_premia');
    }
}
