<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichVehicleUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_vehicle_uses', function (Blueprint $table) {
            $table->id();
            $table->string('vehicle_use_code', 15)->nullable();
            $table->string('vehicle_use_description', 100)->nullable();
            $table->string('vehicle_class_code', 100)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_vehicle_uses');
    }
}
