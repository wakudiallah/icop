<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiGetVehicleInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_get_vehicle_infos', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('reg_no')->nullable();
            $table->string('vehRegNo')->nullable();
            $table->string('vehClass')->nullable();
            $table->string('vehMake')->nullable();
            $table->string('vehMakeDesc')->nullable();
            $table->year('vehMakeYear')->nullable();
            $table->string('vehUse')->nullable();
            $table->string('vehEngineNo')->nullable();
            $table->string('vehChassisNo')->nullable();
            $table->date('polEffDate')->nullable();
            $table->date('polExpDate')->nullable();
            $table->string('builtType')->nullable();
            $table->decimal('ncdPct', 5, 2)->nullable();
            $table->date('nxtNCDEffDate')->nullable();
            $table->string('ncdStatus')->nullable();
            $table->string('ismNCDRespCode')->nullable();
            $table->string('ismVIXRespCode')->nullable();
            $table->date('nxtPolEffDate')->nullable();
            $table->date('nxtPolExpDate')->nullable();
            $table->date('curEnqDate')->nullable();
            $table->string('vehVIXModelCode')->nullable();
            $table->string('vixNVIC')->nullable();
            $table->string('preInsCode')->nullable();
            $table->string('vixPreInsCode')->nullable();
            $table->string('vixVehClass')->nullable();
            $table->string('vixCoverType')->nullable();
            $table->string('vixPreInsDesc')->nullable();
            $table->string('coverType')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_get_vehicle_infos');
    }
}
