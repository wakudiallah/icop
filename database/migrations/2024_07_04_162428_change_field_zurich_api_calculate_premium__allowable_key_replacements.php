<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldZurichApiCalculatePremiumAllowableKeyReplacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_calculate_premium__allowable_key_replacements', function (Blueprint $table) {
            //
            $table->string('key_Replacement_SI', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_calculate_premium__allowable_key_replacements', function (Blueprint $table) {
            //
            $table->string('key_Replacement_SI', 255)->nullable(false)->change();
        });
    }
}
