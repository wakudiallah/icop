<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamStatusApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_status_applications', function (Blueprint $table) {
            $table->id();
            $table->string('code_status_app', 100)->nullable();
            $table->string('status_app', 100)->nullable();
            $table->string('label', 100)->nullable();
            $table->tinyinteger('status')->dafault(1);
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_status_applications');
    }
}
