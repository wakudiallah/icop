<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldZurichApiCalculatePremiumAllowableVoluntaryExcesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_calculate_premium__allowable_voluntary_excesses', function (Blueprint $table) {
            //
            $table->string('voluntary_Excess', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_calculate_premium__allowable_voluntary_excesses', function (Blueprint $table) {
            //
            $table->string('voluntary_Excess', 255)->nullable(false)->change();
        });
    }
}
