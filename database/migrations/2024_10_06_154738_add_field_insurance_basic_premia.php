<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInsuranceBasicPremia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_basic_premia', function (Blueprint $table) {
            //
            $table->decimal('final_payable', 8,2)->after('ttlPayablePremium')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_basic_premia', function (Blueprint $table) {
            //
            $table->dropColumn('final_payable');
        });
    }
}
