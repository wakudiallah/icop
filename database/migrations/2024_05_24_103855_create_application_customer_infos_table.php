<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationCustomerInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_customer_infos', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->text('address1')->nullable();
            $table->text('address2')->nullable();
            $table->string('postcode')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_customer_infos');
    }
}
