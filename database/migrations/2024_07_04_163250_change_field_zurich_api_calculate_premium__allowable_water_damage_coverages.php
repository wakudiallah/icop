<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldZurichApiCalculatePremiumAllowableWaterDamageCoverages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_calculate_premium__allowable_water_damage_coverages', function (Blueprint $table) {
            //
            $table->string('water_Damage_SI', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_calculate_premium__allowable_water_damage_coverages', function (Blueprint $table) {
            //
            $table->string('water_Damage_SI', 255)->nullable(false)->change();
        });
    }
}
