<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldAnnualNcdAmmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_calculate_premium_premium_details', function (Blueprint $table) {
            //
            //annual_NCD_Amount
            $table->float('annual_NCD_Amount', 18,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_calculate_premium_premium_details', function (Blueprint $table) {
            //
            $table->decimal('annual_NCD_Amount', 18,2)->nullable()->change();
        });
    }
}
