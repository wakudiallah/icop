<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowableExtCvrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_ext_cvrs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('ext_Cvr_Code')->nullable();
            $table->string('ext_Cvr_Desc')->nullable();
            $table->string('applicable_Ind')->nullable();
            $table->string('compulsory_Ind')->nullable();
            $table->string('recommended_Ind')->nullable();
            $table->string('endorsement_Ind')->nullable();
            $table->string('eC_Input_Type')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_ext_cvrs');
    }
}
