<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldMaritalStatusInApplicationCustomerInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            //
            $table->char('marital_status', 2)->nullable()->after('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            //
            $table->dropColumn('marital_status');
        });
    }
}
