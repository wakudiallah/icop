<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiIssuepoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_issuepolicies', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('coverNoteNo', 100)->nullable();
            $table->text('ncdMsg', 10, 2)->nullable();
            $table->text('coverNoteError')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_issuepolicies');
    }
}
