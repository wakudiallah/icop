<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiGetvehiclemodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_getvehiclemodels', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('sdfVehModelID')->nullable();
            $table->string('description')->nullable(); 
            $table->string('misDes')->nullable();
            $table->string('bodyType')->nullable(); 
            $table->string('capacityFrom')->nullable();
            $table->string('capacityTo')->nullable(); 
            $table->string('uom')->nullable();
            $table->string('ism')->nullable();
            $table->decimal('wScreenSI', 10, 2)->nullable(); 
            $table->string('noofPassenger')->nullable();
            $table->string('make')->nullable(); 
            $table->string('nvic')->nullable();
            $table->string('variant')->nullable(); 
            $table->decimal('marketValue', 10, 2)->nullable(); 
            $table->string('vehTransType')->nullable();
            $table->string('vehFuelType')->nullable(); 
            $table->string('vehVariantDesc')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_getvehiclemodels');
    }
}
