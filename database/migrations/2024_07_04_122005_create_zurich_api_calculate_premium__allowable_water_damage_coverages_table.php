<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowableWaterDamageCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_water_damage_coverages', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('water_Damage_SI', 10, 2)->nullable(); // Adjust precision and scale as per your needs
            $table->char('water_Damage_SI_Ind', 1)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_water_damage_coverages');
    }
}
