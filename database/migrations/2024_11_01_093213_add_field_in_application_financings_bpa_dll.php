<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInApplicationFinancingsBpaDll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_financings', function (Blueprint $table) {
            //
            $table->decimal('after_cal_monthly_installment', 8,2)->after('monthly_installment')->nullable();
            $table->decimal('processing_fee', 8,2)->after('after_cal_monthly_installment')->nullable();
            $table->decimal('bpa_charges', 8,2)->after('processing_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_financings', function (Blueprint $table) {
            //
            $table->dropColumn('after_cal_monthly_installment');
            $table->dropColumn('processing_fee');
            $table->dropColumn('bpa_charges');
        });
    }
}
