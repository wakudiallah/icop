<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichAllowablePacUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_allowable_pac_units', function (Blueprint $table) {
            $table->id();
            $table->string('no', 100)->nullable();
            $table->string('pac_unit', 100)->nullable();
            $table->timestamps();
            $table->SoftDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_allowable_pac_units');
    }
}
