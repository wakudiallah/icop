<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamInsurancePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_insurance_promos', function (Blueprint $table) {
            $table->id();
            $table->integer('insurance_product_id')->nullable();
            $table->longtext('promo')->nullable();
            $table->longtext('promo_english')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_insurance_promos');
    }
}
