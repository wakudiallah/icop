<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiPostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_postcodes', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('stateCode', 20)->nullable();
            $table->string('stateName', 200)->nullable();
            $table->string('regioncode', 10)->nullable();
            $table->string('countryCode', 100)->nullable();
            $table->string('countryName', 200)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_postcodes');
    }
}
