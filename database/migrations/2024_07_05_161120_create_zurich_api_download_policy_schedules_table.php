<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiDownloadPolicySchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_download_policy_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->text('emailStatus')->nullable();
            $table->string('fileUrl', 100)->nullable();
            $table->string('fileByte', 100)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_download_policy_schedules');
    }
}
