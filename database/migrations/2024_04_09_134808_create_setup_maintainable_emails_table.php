<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupMaintainableEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_maintainable_emails', function (Blueprint $table) {
            $table->id();
            $table->integer('email_for_id')->nullable();
            $table->integer('email_system_id')->nullable();
            $table->string('greeting', 200)->nullable();
            $table->text('text_email')->nullable();
            $table->string('closing', 255)->nullable();
            $table->tinyinteger('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_maintainable_emails');
    }
}
