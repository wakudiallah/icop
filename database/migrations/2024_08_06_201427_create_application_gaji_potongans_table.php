<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationGajiPotongansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_gaji_potongans', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('pcb', 8, 2)->nullable();
            $table->decimal('perkeso', 8, 2)->nullable();
            $table->decimal('cukai_pendapatan', 8, 2)->nullable();
            $table->decimal('pinjaman_perumahan', 8, 2)->nullable();
            $table->decimal('zakat', 8, 2)->nullable();
            $table->decimal('lemabaga_tabung_haji', 8, 2)->nullable();
            $table->decimal('pembiayaan_perumahan_lain_lain', 8, 2)->nullable();
            $table->decimal('angkasa', 8, 2)->nullable();
            $table->decimal('koperasi', 8, 2)->nullable();
            $table->decimal('piutang_gaji_elaun', 8, 2)->nullable();
            $table->decimal('angkasa_bukan_pinjaman', 8, 2)->nullable();
            $table->decimal('yir', 8, 2)->nullable();
            $table->decimal('yir2', 8, 2)->nullable();
            $table->decimal('jumlah_potongan', 8, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_gaji_potongans');
    }
}
