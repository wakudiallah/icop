<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichNationalityAndCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_nationality_and_codes', function (Blueprint $table) {
            $table->id();
            $table->string('no', 100)->nullable();
            $table->string('code', 100)->nullable();
            $table->string('nationality', 300)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_nationality_and_codes');
    }
}
