<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumPremiumDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium_premium_details', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->text('excessType')->nullable();
            $table->text('volExcessTypeApplied')->nullable();
            $table->text('excessAmt')->nullable();
            $table->text('volExcessAmt')->nullable();
            $table->text('totExcessAmt')->nullable();
            $table->text('package_Applicable')->nullable();
            $table->text('package_Prem')->nullable();
            $table->text('package_Selected')->nullable();

            $table->text('vehicle_Model_Code_Out')->nullable();

            $table->text('annual_BasePremAdjustment')->nullable();
            $table->text('annual_Schedule_Premium')->nullable();
            $table->text('annual_Loading_Amount')->nullable();
            $table->text('annual_Tuition_Purposes_Amount')->nullable();
            $table->text('annual_Tariff_Premium')->nullable();
            $table->text('annual_All_Riders')->nullable();
            $table->text('annual_NCD_Amount')->nullable();
            $table->text('annual_Voluntary_Excess_Amount')->nullable();
            $table->text('annual_Total_Extra_Cover_Premium')->nullable();
            $table->text('annual_Renewal_Bonus_Amount')->nullable();
            $table->text('basicAnnualPrem')->nullable();
            $table->text('annual_Act_BasePremAdjustment')->nullable();
            $table->text('annual_Act_Schedule_Premium')->nullable();
            $table->text('annual_Act_Loading_Amount')->nullable();
            $table->text('annual_Act_Tuition_Purposes_Amount')->nullable();
            $table->text('annual_Act_Tariff_Premium')->nullable();
            $table->text('annual_Act_All_Riders')->nullable();
            $table->text('annual_Act_NCD_Amount')->nullable();
            $table->text('annual_Act_Voluntary_Excess_Amount')->nullable();
            $table->text('annual_Act_Total_Extra_Cover_Premium')->nullable();
            $table->text('annual_Act_Renewal_Bonus_Amount')->nullable();
            $table->text('annual_Act_Premium')->nullable();
            $table->text('basicNetAct')->nullable();
            $table->text('actPrem')->nullable();
            $table->text('basicNetNonAct')->nullable();
            $table->text('nonActPrem')->nullable();
            $table->text('commAmt')->nullable();
            $table->text('commPct')->nullable();
            $table->text('gstOnCommAmt')->nullable();
            $table->text('nettPrem')->nullable();
            $table->text('basic_Premium_Adjustment')->nullable();
            $table->text('basic_Premium_Adjustment_Pct')->nullable();
            $table->text('schedule_Premium_0')->nullable();
            $table->text('basicPrem')->nullable();
            $table->text('trailerPremium')->nullable();
            $table->text('loadAmt')->nullable();
            $table->text('trailer_Loading_Amount')->nullable();
            $table->text('loadPct')->nullable();
            $table->text('trlLoadingPer')->nullable();
            $table->text('tuitionLoadAmt')->nullable();
            $table->text('trailerTuitionLoadAmt')->nullable();
            $table->text('tuitionLoadPct')->nullable();
            $table->text('trailerTuitionLoadPer')->nullable();
            $table->text('allRiderAmt')->nullable();
            $table->text('ncdAmt')->nullable();
            $table->text('volExcessDiscAmt')->nullable();
            $table->text('volExcessDiscPct')->nullable();
            $table->text('totBasicNetAmt')->nullable();
            $table->text('totExtCoverPrem')->nullable();
            $table->text('rnwBonusAmt')->nullable();
            $table->text('rnwBonusPct')->nullable();
            $table->text('basic_Premium')->nullable();
            $table->text('rebateAmt')->nullable();
            $table->text('rebatePct')->nullable();
            $table->text('grossPrem')->nullable();
            $table->text('gsT_Amt')->nullable();
            $table->text('gsT_Pct')->nullable();
            $table->text('ssT_Amt')->nullable();
            $table->text('ssT_Pct')->nullable();
            $table->text('stampDutyAmt')->nullable();
            $table->text('totMtrPrem')->nullable();
            $table->text('ptV_Amt')->nullable();
            $table->text('ttlPayablePremium')->nullable();
            $table->text('paC_Max_Additional_Vehicle')->nullable();
            $table->text('paC_MultiProductAmt')->nullable();
            $table->text('paC_MultiProductPct')->nullable();
            $table->text('paC_RebateAmt')->nullable();
            $table->text('paC_RebatePct')->nullable();
            $table->text('paC_GSTAmt')->nullable();
            $table->text('paC_GSTPct')->nullable();
            $table->text('paC_StampDuty')->nullable();
            $table->text('paC_TotPrem')->nullable();
            $table->text('paC_CommAmt')->nullable();
            $table->text('paC_CommPct')->nullable();
            $table->text('paC_GstOnCommAmt')->nullable();
            $table->text('paC_SumInsured')->nullable();
            $table->text('paC_GrossPrem')->nullable();
            $table->text('paC_NettPrem')->nullable();
            $table->text('paC_MinPrem')->nullable();
            $table->text('paC_Prem')->nullable();
            $table->text('paC_AddPrem')->nullable();
            $table->text('paC_AddVehPrem')->nullable();
            $table->text('pacExtraCover')->nullable();
            $table->text('renewal_Cap_Ind')->nullable();
            $table->text('basic_Without_Ren_Cap')->nullable();
            $table->text('tariff_Premium_Pure')->nullable();
            $table->text('tariff_Premium')->nullable();
            $table->text('detariff_Premium_Adjustment', 10, 3)->nullable();
            $table->text('uncapped_Detariff_Premium')->nullable();
            $table->text('loading_Group')->nullable();
            $table->text('detariff_Lower_Bound', 10, 3)->nullable();
            $table->text('detariff_Upper_Bound', 10, 3)->nullable();
            $table->text('chosen_Vehicle_SI_Min_Deviation')->nullable();
            $table->text('chosen_Vehicle_SI_Max_Deviation')->nullable();
            $table->text('chosen_Vehicle_SI_Lower_Bound')->nullable();
            $table->text('chosen_Vehicle_SI_Upper_Bound')->nullable();
            $table->text('loading_Overwrite_Ind')->nullable();
            $table->text('piaM_Blacklist_Veh_No_Ind')->nullable();
            $table->text('comp_BL_Veh_No_Ind')->nullable();
            $table->text('comp_BL_Client_Ind')->nullable();
            $table->text('abI_Check_Ind')->nullable();
            $table->text('allowable_Motor_Rebate_Ind')->nullable();
            $table->text('allowable_PAC_Rebate_Ind')->nullable();
            $table->text('calculate_Method')->nullable();
            $table->text('motor_Min_Prem_Ind')->nullable();
            $table->text('abI_Adopt_Ind')->nullable();
            $table->text('motor_Levy_Per')->nullable();
            $table->text('motor_Levy_Amount')->nullable();
            $table->text('recommended_Vehicle_SI_Type')->nullable();
            $table->text('manual_Benchmark_SI_Display')->nullable();
            $table->text('manual_Benchmark_SI_Text')->nullable();
            $table->text('manual_Benchmark_SI_Editable')->nullable();
            $table->text('vehicle_Use_Code')->nullable();
            $table->text('aV_SI_Value')->nullable();
            $table->text('rec_SI_Value')->nullable();
            $table->text('rec_SI_Text')->nullable();
            $table->text('veh_SI_Editable')->nullable();
            $table->text('quote_Exp_Date')->nullable();
            $table->text('vehicle_Class')->nullable();
            $table->text('aV_SI_Lower_Bound')->nullable();
            $table->text('aV_SI_Upper_Bound')->nullable();
            $table->text('mV_SI_Lower_Bound')->nullable();
            $table->text('mV_SI_Upper_Bound')->nullable();
            $table->text('isM_Cover_Type')->nullable();
            $table->text('ncD_Class')->nullable();
            $table->text('ncD_Applicable_Ind')->nullable();
            $table->text('ddlAgreeValue')->nullable();
            $table->text('rec_Vehicle_SI')->nullable();
            $table->text('paC_SI_Per_Seat')->nullable();
            $table->text('total_ZDA_Premium')->nullable();
            $table->text('zdA_Add_On')->nullable();
            $table->text('paC_Type_Offered')->nullable();
            $table->text('paC_Desc')->nullable();
            $table->text('paC_Max_Units')->nullable();
            $table->text('paC_Min_Units')->nullable();
            $table->text('rec_PAC_Min_Units')->nullable();
            $table->text('paC_Compulsory_Ind')->nullable();
            $table->text('paC_Type_Applicable')->nullable();
            $table->text('wakalah_Commission_Mtr_Pct')->nullable();
            $table->text('wakalah_Commission_Mtr_Amt')->nullable();
            $table->text('wakalah_Expense_Mtr_Pct')->nullable();
            $table->text('wakalah_Expense_Mtr_Amt')->nullable();
            $table->text('wakalah_Fee_Mtr_Pct')->nullable();
            $table->text('wakalah_Fee_Mtr_Amt')->nullable();
            $table->text('wakalah_Commission_PA_Pct')->nullable();
            $table->text('wakalah_Commission_PA_Amt')->nullable();
            $table->text('wakalah_Expense_PA_Pct')->nullable();
            $table->text('wakalah_Expense_PA_Amt')->nullable();
            $table->text('wakalah_Fee_PA_Pct')->nullable();
            $table->text('wakalah_Fee_PA_Amt')->nullable();
            $table->text('referralData')->nullable();
            $table->text('errorDetails')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__premium_details');
    }
}
