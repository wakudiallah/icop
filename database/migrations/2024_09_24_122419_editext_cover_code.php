<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditextCoverCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_extra_covers', function (Blueprint $table) {
            //
            $table->string('extCoverCode')->nullable()->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_extra_covers', function (Blueprint $table) {
            //
            $table->integer('extCoverCode')->nullable();
        });
    }
}
