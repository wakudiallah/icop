<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationSmsAkadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('application_sms_akads', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->boolean('send_status')->nullable();
            $table->timestamp('send_time')->nullable(); // Change timestamps to timestamp
            $table->text('message_send')->nullable();
            $table->boolean('reply_status')->nullable();
            $table->timestamp('reply_time')->nullable(); // Change timestamps to timestamp
            $table->text('message_reply')->nullable();
            $table->timestamps(); // This will create created_at and updated_at columns
            $table->softDeletes();
        });
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_sms_akads');
    }
}
