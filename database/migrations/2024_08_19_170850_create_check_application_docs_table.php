<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckApplicationDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_application_docs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->boolean('check_nirc')->nullable();
            $table->boolean('check_payslip')->nullable();
            $table->boolean('check_bpa179')->nullable();
            $table->boolean('check_eligibility_calculation')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_application_docs');
    }
}
