<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerApiAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_api_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('partner_code', 20)->nullable();
            $table->string('partner')->nullable();
            $table->string('userName')->nullable();
            $table->string('password')->nullable();
            $table->longtext('token')->nullable();
            $table->timestamp('expired')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_api_accounts');
    }
}
