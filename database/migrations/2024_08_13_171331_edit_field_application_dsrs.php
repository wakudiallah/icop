<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditFieldApplicationDsrs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_dsrs', function (Blueprint $table) {
            //dsr_baru_persen
            $table->decimal('dsr_baru_persen', 18,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_dsrs', function (Blueprint $table) {
            //
            $table->decimal('dsr_baru_persen', 18,2)->nullable()->change();
        });
    }
}
