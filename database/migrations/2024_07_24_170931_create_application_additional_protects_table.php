<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationAdditionalProtectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_additional_protects', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('total_payable', 8, 2)->nullable();
            $table->boolean('windscreen_protect')->nullable();
            $table->decimal('tot_windscreen_protect', 8, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_additional_protects');
    }
}
