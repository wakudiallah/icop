<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldFullnamePhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            $table->string('fullname')->after('uuid')->nullable();
            $table->string('phone')->after('fullname')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            $table->dropColumn('fullname');
            $table->dropColumn('phone');
        });
    }
}
