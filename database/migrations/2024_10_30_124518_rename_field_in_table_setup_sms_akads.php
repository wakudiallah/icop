<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameFieldInTableSetupSmsAkads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->renameColumn('text_basic_premium', 'text_basic_contribution');
            $table->renameColumn('text_rebate', 'text_caj_perkhidmatan');
            $table->dropColumn('text_add_on');
            $table->dropColumn('text_sst');
            $table->dropColumn('text_dutty_stamp');
            $table->dropColumn('text_payable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->renameColumn('text_basic_contribution', 'text_basic_premium');
            $table->renameColumn('text_caj_perkhidmatan', 'text_rebate');
            $table->text('text_add_on')->nullable();
            $table->text('text_sst')->nullable();
            $table->text('text_dutty_stamp')->nullable();
            $table->text('text_payable')->nullable();

        });
    }
}
