<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowablePaExtendedCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_pa_extended_coverages', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable()->nullable();
            $table->string('paC_Ext_Cvr_Code')->nullable();
            $table->string('paC_Ext_Cvr_Desc')->nullable();
            $table->char('ind', 1)->nullable();
            $table->integer('min_Units')->nullable();
            $table->integer('max_Units')->nullable();
            $table->char('additional_Insured_Person_Ind', 1)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_pa_extended_coverages');
    }
}
