<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumExtraCoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium_extra_cover_data', function (Blueprint $table) {
            $table->id();
            $table->string('extCoverCode')->nullable();
            $table->decimal('extCoverSumInsured', 10, 2)->nullable();
            $table->decimal('extCoverPrem', 10, 2)->nullable();
            $table->string('compulsory_Ind')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium_extra_cover_data');
    }
}
