<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowableCartAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_cart_amounts', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('cart_amount', 10, 2); // Adjust precision and scale as needed
            $table->enum('cart_amount_ind', ['Y', 'N']);
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_cart_amounts');
    }
}
