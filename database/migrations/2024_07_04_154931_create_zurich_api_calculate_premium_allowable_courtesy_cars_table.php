<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowableCourtesyCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium_allowable_courtesy_cars', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('courtesy_Car_Days')->nullable();
            $table->string('courtesy_Car_Days_Ind')->nullable();
            $table->timestamps();
            $table->SoftDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium_allowable_courtesy_cars');
    }
}
