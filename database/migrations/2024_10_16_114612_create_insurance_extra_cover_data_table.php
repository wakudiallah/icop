<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceExtraCoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_extra_cover_data', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('extCovCode')->nullable();
            $table->string('unitDay')->nullable();
            $table->string('unitAmount')->nullable();
            $table->string('effDate')->nullable();
            $table->string('expDate')->nullable();
            $table->decimal('sumInsured', 8, 2)->nullable();
            $table->string('noOfUnit')->nullable();
            $table->string('extCovDesc')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_extra_cover_data');
    }
}
