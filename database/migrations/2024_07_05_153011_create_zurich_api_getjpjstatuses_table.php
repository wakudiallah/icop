<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiGetjpjstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_getjpjstatuses', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('vehRegNo', 100)->nullable();
            $table->string('status')->nullable();
            $table->dateTime('dateTime');
            $table->string('jpjStatusError')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_getjpjstatuses');
    }
}
