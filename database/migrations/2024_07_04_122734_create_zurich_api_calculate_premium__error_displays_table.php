<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumErrorDisplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__error_displays', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('error_Code')->nullable();
            $table->string('error_Desc')->nullable();
            $table->string('severity')->nullable();
            $table->text('remarks')->nullable();
            $table->char('warning_Ind', 1)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__error_displays');
    }
}
