<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupZurichCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_zurich_carts', function (Blueprint $table) {
            $table->id();
            $table->integer('cart_day_id')->nullable();
            $table->integer('cart_amount_id')->nullable();
            $table->string('desc')->nullable();
            $table->boolean('status')->defaul(1)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_zurich_carts');
    }
}
