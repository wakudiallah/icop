<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceExtraCoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_extra_covers', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->integer('extCoverCode')->nullable();
            $table->decimal('extCoverSumInsured', 8,2)->nullable();
            $table->decimal('extCoverPrem', 8,2)->nullable();
            $table->string('compulsory_Ind')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_extra_covers');
    }
}
