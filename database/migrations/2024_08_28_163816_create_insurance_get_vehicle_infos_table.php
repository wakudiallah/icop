<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceGetVehicleInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     
    public function up()
    {
        Schema::create('insurance_get_vehicle_infos', function (Blueprint $table) {
            $table->id();
            $table->string('eId')->nullable();
            $table->string('uuid')->nullable();
            $table->string('vehRegNo')->nullable();
            $table->string('chasisNo')->nullable();
            $table->string('engineNo')->nullable();
            $table->string('yearMake')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('variant')->nullable();
            $table->string('series')->nullable();
            $table->string('vehCapacity')->nullable();
            $table->string('uom')->nullable();
            $table->string('ncd')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_get_vehicle_infos');
    }
}
