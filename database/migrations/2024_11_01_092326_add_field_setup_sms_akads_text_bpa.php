<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSetupSmsAkadsTextBpa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->string('text_bpa_charges')->after('text_caj_perkhidmatan')->nullable();
            $table->string('text_jumlah_pembiayaan')->after('text_bpa_charges')->nullable();
            $table->string('text_bayaran_balik')->after('text_jumlah_pembiayaan')->nullable();
            $table->string('text_tempah_bayaran')->after('text_bayaran_balik')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->dropColumn('text_bpa_charges');
            $table->dropColumn('text_jumlah_pembiayaan');
            $table->dropColumn('text_bayaran_balik');
            $table->dropColumn('text_tempah_bayaran');
        });
    }
}
