<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePraaplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praaplications', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('vehicle')->nullable();
            $table->string('ic', 30)->nullable();
            $table->string('ic2', 30)->nullable();
            $table->integer('ehailing')->nullable();
            $table->string('email', 100)->nullable();
            $table->string('whatsapp', 100)->nullable();
            $table->string('postcode', 10)->nullable();
            $table->string('type_ins', 30)->nullable();
            $table->string('pay_status', 30)->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praaplications');
    }
}
