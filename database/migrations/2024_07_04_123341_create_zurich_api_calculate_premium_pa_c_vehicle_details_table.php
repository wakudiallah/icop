<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumPaCVehicleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium_pa_c_vehicle_details', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('vehicle_Number')->nullable();
            $table->integer('no_of_Seats')->nullable();
            $table->boolean('is_Policyholder')->nullable();
            $table->decimal('paC_SI', 10, 2)->nullable(); // Adjust precision and scale as per your needs
            $table->decimal('paC_Annual_Basic_Premium', 10, 2)->nullable(); // Adjust precision and scale as per your needs
            $table->decimal('paC_Annual_Additional_Seats_Premium', 10, 2)->nullable(); // Adjust precision and scale as per your needs
            $table->string('paC_Benefit_Schedule_Code')->nullable();
            $table->string('paC_Medical_Plan_Entry_Code')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium_pa_c_vehicle_details');
    }
}
