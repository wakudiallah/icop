<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInApplicationCustomerInfosAgeDll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            //dob
            $table->date('dob')->nullable()->after('marital_status');
            $table->integer('age')->nullable()->after('dob');
            $table->char('gender', 2)->nullable()->after('age');
             

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_customer_infos', function (Blueprint $table) {
            //
            $table->dropColumn('dob');
            $table->dropColumn('age');
            $table->dropColumn('gender');
        });
    }
}
