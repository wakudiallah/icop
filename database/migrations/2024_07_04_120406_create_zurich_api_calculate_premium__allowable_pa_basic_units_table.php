<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowablePaBasicUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_pa_basic_units', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('pA_Basic_Unit')->nullable();
            $table->char('pA_Basic_Unit_Ind', 1)->nullable();
            $table->decimal('pA_Basic_SI', 10, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_pa_basic_units');
    }
}
