<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDatatypeZurichApiGetjpjstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_getjpjstatuses', function (Blueprint $table) {
            //
            $table->string('dateTime')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_getjpjstatuses', function (Blueprint $table) {
            //
            $table->string('dateTime')->nullable()->change();
        });
    }
}
