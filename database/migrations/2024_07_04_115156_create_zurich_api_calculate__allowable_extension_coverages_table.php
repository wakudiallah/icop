<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculateAllowableExtensionCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate__allowable_extension_coverages', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('ext_Cvr_Code', 2);
            $table->string('ext_Cvr_Desc');
            $table->char('applicable_Ind', 1)->nullable();
            $table->char('compulsory_Ind', 1)->nullable();
            $table->char('recommended_Ind', 1)->nullable();
            $table->char('endorsement_Ind', 1)->nullable();
            $table->string('eC_Input_Type')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate__allowable_extension_coverages');
    }
}
