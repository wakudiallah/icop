<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumAllowableVoluntaryExcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium__allowable_voluntary_excesses', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('voluntary_Excess', 10, 2)->nullable(); // Adjust precision and scale as per your needs
            $table->char('voluntary_Excess_Ind', 1)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium__allowable_voluntary_excesses');
    }
}
