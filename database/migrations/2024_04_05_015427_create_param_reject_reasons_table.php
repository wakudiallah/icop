<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamRejectReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_reject_reasons', function (Blueprint $table) {
            $table->id();
            $table->text('reject_reason')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_reject_reasons');
    }
}
