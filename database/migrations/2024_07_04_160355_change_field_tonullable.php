<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldTonullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_calculate_premium__allowable_cart_amounts', function (Blueprint $table) {
            //
            $table->decimal('cart_amount', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_calculate_premium__allowable_cart_amounts', function (Blueprint $table) {
            //
            $table->string('cart_amount')->nullable(false)->change();
        });
    }
}
