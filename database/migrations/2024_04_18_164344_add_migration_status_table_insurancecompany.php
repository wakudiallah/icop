<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMigrationStatusTableInsurancecompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('param_insurance_companies', function (Blueprint $table) {
            //
            $table->tinyinteger('status')->after('logo')->dafault(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('param_insurance_companies', function (Blueprint $table) {
            //
            $table->dropColumn('status');
        });
    }
}
