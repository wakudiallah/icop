<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegisNoInTablePraaplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('praaplications', function (Blueprint $table) {
            $table->string('reg_no', 11)->after('uuid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('praaplications', function (Blueprint $table) {
            //
            $table->dropColumn('reg_no');
        });
    }
}
