<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichAllowableKeyReplacementSumInsuredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_allowable_key_replacement_sum_insureds', function (Blueprint $table) {
            $table->id();
            $table->string('no', 100)->nullable();
            $table->string('key_replacement_sum_insured')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_allowable_key_replacement_sum_insureds');
    }
}
