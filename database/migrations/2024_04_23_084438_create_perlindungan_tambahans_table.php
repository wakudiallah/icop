<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerlindunganTambahansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perlindungan_tambahans', function (Blueprint $table) {
            $table->id();
            $table->tinyinteger('perlindungan_cermin')->dafault(0);
            $table->tinyinteger('pemandu_tambahan')->dafault(0);
            $table->tinyinteger('perlindungan_tambahan_khas')->dafault(0);
            $table->tinyinteger('perlindungan_kemalangan_peribadi')->dafault(0);
            $table->double('premium_dibayar', 8, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perlindungan_tambahans');
    }
}
