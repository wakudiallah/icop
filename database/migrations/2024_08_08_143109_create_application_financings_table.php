<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationFinancingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_financings', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->decimal('total_payable', 8, 2)->nullable();
            $table->integer('deduc_schema')->nullable();
            $table->decimal('monthly_installment', 8, 2)->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_financings');
    }
}
