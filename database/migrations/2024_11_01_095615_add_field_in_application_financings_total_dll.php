<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInApplicationFinancingsTotalDll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_financings', function (Blueprint $table) {
            //
            $table->decimal('total_financing', 8,2)->after('monthly_installment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_financings', function (Blueprint $table) {
            //
            $table->dropColumn('total_financing');
        });
    }
}
