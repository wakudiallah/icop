<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceBasicDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_basic_details', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('transType', 10)->nullable();
            $table->string('vehicleNo', 20)->nullable();
            $table->string('productCode', 10)->nullable();
            $table->string('coverType', 10)->nullable();
            $table->string('ciCode', 10)->nullable();
            $table->date('effDate')->nullable();
            $table->date('expDate')->nullable();
            $table->char('reconInd', 10)->nullable();
            $table->integer('yearOfMake')->nullable();
            $table->string('make', 10)->nullable();
            $table->string('model', 20)->nullable();
            $table->string('capacity', 10)->nullable();
            $table->string('uom', 5)->nullable();
            $table->string('engineNo', 50)->nullable();
            $table->string('chassisNo', 50)->nullable();
            $table->string('logBookNo', 50)->nullable();
            $table->char('regLoc', 10)->nullable();
            $table->char('regionCode', 10)->nullable();
            $table->integer('noOfPassenger')->nullable();
            $table->integer('noOfDrivers')->nullable();
            $table->char('insIndicator', 10)->nullable();
            $table->string('name', 200)->nullable();
            $table->string('insNationality', 50)->nullable();
            $table->string('newIC', 20)->nullable();
            $table->string('othersID', 50)->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->integer('age')->nullable();
            $table->char('gender', 10)->nullable();
            $table->char('maritalSts', 10)->nullable();
            $table->string('occupation', 10)->nullable();
            $table->string('mobileNo', 20)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('postCode', 10)->nullable();
            $table->string('state', 10)->nullable();
            $table->string('country', 10)->nullable();
            $table->decimal('sumInsured', 15, 2)->nullable();
            $table->decimal('abisi', 15, 2)->nullable();
            $table->string('chosen_SI_Type', 10)->nullable();
            $table->char('avInd', 10)->nullable();
            $table->char('pacInd', 10)->nullable();
            $table->integer('pacUnit')->nullable();
            $table->char('all_Driver_Ind', 10)->nullable();
            $table->timestamps(); 
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_basic_details');
    }
}
