<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSizeInTableDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_documents', function (Blueprint $table) {
            $table->text('sz_ic')->after('nric')->nullable();
            $table->string('ext_ic')->after('sz_ic')->nullable();


            $table->text('sz_pay')->after('payslip')->nullable();
            $table->string('ext_pay')->after('sz_pay')->nullable();


            $table->text('sz_bpa')->after('bpa179')->nullable();
            $table->string('ext_bpa')->after('sz_bpa')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_documents', function (Blueprint $table) {
            $table->dropColumn('sz_ic');
            $table->dropColumn('ext_ic');
            $table->dropColumn('sz_pay');
            $table->dropColumn('ext_pay');
            $table->dropColumn('sz_bpa');
            $table->dropColumn('ext_bpa');
        });
    }
}
