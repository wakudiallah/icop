<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichProductCodeCoverTypeCiCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_product_code_cover_type_ci_codes', function (Blueprint $table) {
            $table->id();
            $table->string('no', 100)->nullable();
            $table->string('product_code', 100)->nullable();
            $table->string('cover_type', 200)->nullable();
            $table->string('ci_code', 200)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_product_code_cover_type_ci_codes');
    }
}
