<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamRoadtaxlkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_roadtaxlkms', function (Blueprint $table) {
            $table->id();
            $table->integer('region')->nullable();
            $table->integer('ownership')->nullable();
            $table->integer('start')->nullable();
            $table->integer('end')->nullable();
            $table->decimal('kadar_flat', 8,2)->nullable();
            $table->decimal('kadar_asas', 8,2)->nullable();
            $table->decimal('kadar_progressif', 8,2)->nullable();
            $table->tinyinteger('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_roadtaxlkms');
    }
}
