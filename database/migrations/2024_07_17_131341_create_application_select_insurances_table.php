<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationSelectInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_select_insurances', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('insurance_id')->nullable();
            $table->decimal('basic_premium', 8, 2)->nullable();
            $table->decimal('sst', 8, 2)->nullable();
            $table->decimal('duti_setem', 8, 2)->nullable();
            $table->decimal('payable', 8, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_select_insurances');
    }
}
