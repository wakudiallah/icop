<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSetupSmsAkads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->string('text_basic_premium')->after('opening')->nullable();
            $table->string('text_rebate')->after('text_basic_premium')->nullable();
            $table->string('text_add_on')->after('text_rebate')->nullable();
            $table->string('text_sst')->after('text_add_on')->nullable();
            $table->string('text_dutty_stamp')->after('text_sst')->nullable();
            $table->string('text_payable')->after('text_dutty_stamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup_sms_akads', function (Blueprint $table) {
            //
            $table->dropColumn('text_basic_premium');
            $table->dropColumn('text_rebate');
            $table->dropColumn('text_add_on');
            $table->dropColumn('text_sst');
            $table->dropColumn('text_dutty_stamp');
            $table->dropColumn('text_payable');
        });
    }
}
