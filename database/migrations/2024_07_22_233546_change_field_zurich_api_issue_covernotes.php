<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldZurichApiIssueCovernotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zurich_api_issue_covernotes', function (Blueprint $table) {
            //
            $table->text('coverNoteNo')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zurich_api_issue_covernotes', function (Blueprint $table) {
            //
            $table->text('coverNoteNo')->nullable()->change();
        });
    }
}
