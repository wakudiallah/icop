<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiGetVehicleInfoVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_get_vehicle_info_variants', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('reg_no')->nullable();
            $table->string('nvic')->nullable();
            $table->string('vehModelCode')->nullable();
            $table->string('vehModel')->nullable();
            $table->integer('vehSeat')->nullable();
            $table->string('vehTransType')->nullable();
            $table->decimal('marketValue', 10, 2)->nullable();
            $table->integer('capacity')->nullable();
            $table->string('vehFuelType')->nullable();
            $table->string('variant')->nullable();
            $table->string('vehBody')->nullable();
            $table->string('uom')->nullable();
            $table->string('vehVariantDesc')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_get_vehicle_info_variants');
    }
}
