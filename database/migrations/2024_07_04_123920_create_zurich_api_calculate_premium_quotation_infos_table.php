<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZurichApiCalculatePremiumQuotationInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zurich_api_calculate_premium_quotation_infos', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('quotationNo')->nullable();
            $table->text('ncdMsg')->nullable();
            $table->decimal('ncdPct', 5, 2)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zurich_api_calculate_premium_quotation_infos');
    }
}
