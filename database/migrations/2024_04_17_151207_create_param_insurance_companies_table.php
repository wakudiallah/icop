<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamInsuranceCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_insurance_companies', function (Blueprint $table) {
            $table->id();
            $table->string('insurance_product', 100)->nullable();
            $table->string('company', 150)->nullable();
            $table->string('register_company', 150)->nullable();
            $table->longtext('logo')->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_insurance_companies');
    }
}
