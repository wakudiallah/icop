<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParamGstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_gsts', function (Blueprint $table) {
            $table->id();
            $table->double('gst', 8, 2)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->SoftDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_gsts');
    }
}
