<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichNationalityAndCode;

class ZurichNationalityAndCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $nationalities = [
            ['no' => '1', 'code' => 'ACI', 'nationality' => 'ASHMORE AND CARTIER ISLANDS'],
            ['no' => '2', 'code' => 'AFG', 'nationality' => 'AFGHANISTAN'],
            ['no' => '3', 'code' => 'AGL', 'nationality' => 'ANGOLA'],
            ['no' => '4', 'code' => 'ALB', 'nationality' => 'ALBANIA'],
            ['no' => '5', 'code' => 'AMS', 'nationality' => 'AMERICAN SAMOA'],
            ['no' => '6', 'code' => 'ANC', 'nationality' => 'ANTARCTICA'],
            ['no' => '7', 'code' => "AND", 'nationality' => 'ANDORRA'],
            ['no' => '8', 'code' => 'ANG', 'nationality' => 'ANGUILLA'],
            ['no' => '9', 'code' => 'ANT', 'nationality' => 'ANTIGUA & BARBUDA'],
            ['no' => '10', 'code' => 'ARG', 'nationality' => 'ARGENTINA'],
            ['no' => '11', 'code' => 'ARM', 'nationality' => 'ARMENIA'],
            ['no' => '12', 'code' => 'ARO', 'nationality' => 'ARCTIC OCEAN'],
            ['no' => '13', 'code' => 'ARU', 'nationality' => 'ARUBIA'],
            ['no' => '14', 'code' => 'ATO', 'nationality' => 'ATLANTIC OCEAN'],
            ['no' => '15', 'code' => 'AUS', 'nationality' => 'AUSTRALIA'],
            ['no' => '16', 'code' => 'AUT', 'nationality' => 'AUSTRIA'],
            ['no' => '17', 'code' => 'AZE', 'nationality' => 'AZERBAIJAN'],
            ['no' => '18', 'code' => 'BAH', 'nationality' => 'BAHRAIN'],
            ['no' => '19', 'code' => 'BAN', 'nationality' => 'BANGLADESH'],
            ['no' => '20', 'code' => 'BAR', 'nationality' => 'BARBADOS'],
            ['no' => '21', 'code' => 'BARC', 'nationality' => 'BARCELONA ESPANA'],
            ['no' => '22', 'code' => 'BDI', 'nationality' => 'BASSAS DA INDIA'],
            ['no' => '23', 'code' => 'BEL', 'nationality' => 'BELARUS'],
            ['no' => '24', 'code' => 'BEN', 'nationality' => 'BENIN'],
            ['no' => '25', 'code' => 'BER', 'nationality' => 'BERMUDA'],
            ['no' => '26', 'code' => 'BHS', 'nationality' => 'BAHAMAS'],
            ['no' => '27', 'code' => 'BHU', 'nationality' => 'BHUTAN'],
            ['no' => '28', 'code' => 'BIT', 'nationality' => 'BRITISH INDIAN OCEAN TERRITORY'],
            ['no' => '29', 'code' => 'BIZ', 'nationality' => 'BELIZE'],
            ['no' => '30', 'code' => 'BKI', 'nationality' => 'BAKER ISLAND'],
            ['no' => '31', 'code' => 'BLG', 'nationality' => 'BELGIUM'],
            ['no' => '32', 'code' => 'BOL', 'nationality' => 'BOLIVIA'],
            ['no' => '33', 'code' => 'BOS', 'nationality' => 'BOSNIA AND HERZEGOVINA'],
            ['no' => '34', 'code' => 'BOT', 'nationality' => 'BOTSWANA'],
            ['no' => '35', 'code' => 'BOU', 'nationality' => 'BOUVET ISLAND'],
            ['no' => '36', 'code' => 'BRA', 'nationality' => 'BRAZIL'],
            ['no' => '37', 'code' => 'BRU', 'nationality' => 'BRUNEI'],
            ['no' => '38', 'code' => 'BUD', 'nationality' => 'BURUNDI'],
            ['no' => '39', 'code' => 'BUL', 'nationality' => 'BULGARIA'],
            ['no' => '40', 'code' => 'BUR', 'nationality' => 'BURKINA FASO'],
            ['no' => '41', 'code' => 'BVI', 'nationality' => 'BRITISH VIRGIN ISLANDS'],
            ['no' => '42', 'code' => 'CAM', 'nationality' => 'CAMBODIA'],
            ['no' => '43', 'code' => 'CAN', 'nationality' => 'CANADA'],
            ['no' => '44', 'code' => 'CAP', 'nationality' => 'CAPE VERDE'],
            ['no' => '45', 'code' => 'CAY', 'nationality' => 'CAYMAN ISLANDS'],
            ['no' => '46', 'code' => 'CEN', 'nationality' => 'CENTRAL AFRICAN REPUBLIC'],
            ['no' => '47', 'code' => 'CHA', 'nationality' => 'CHAD'],
            ['no' => '48', 'code' => 'CHI', 'nationality' => 'CHINA'],
            ['no' => '49', 'code' => 'CHL', 'nationality' => 'CHILE'],
            ['no' => '50', 'code' => 'CKI', 'nationality' => 'COOK ISLANDS'],
            ['no' => '51', 'code' => 'CLI', 'nationality' => 'CLIPPERTON ISLAND'],
            ['no' => '52', 'code' => 'CMR', 'nationality' => 'CAMEROON'],
            ['no' => '53', 'code' => 'COC', 'nationality' => 'COCOS (KEELING) ISLANDS'],
            ['no' => '54', 'code' => 'COD', 'nationality' => 'CONGO DEMOCRATIC REPUBLIC'],
            ['no' => '55', 'code' => 'COL', 'nationality' => 'COLOMBIA'],
            ['no' => '56', 'code' => 'COM', 'nationality' => 'COMOROS'],
            ['no' => '57', 'code' => 'COR', 'nationality' => 'CONGO REPUBLIC'],
            ['no' => '58', 'code' => 'COS', 'nationality' => 'COSTA RICA'],
            ['no' => '59', 'code' => 'COT', 'nationality' => 'COTE D\'IVOIRE'],
            ['no' => '60', 'code' => 'CRI', 'nationality' => 'CHRISTMAS ISLAND'],
            ['no' => '61', 'code' => 'CRO', 'nationality' => 'CROATIA'],
            ['no' => '62', 'code' => 'CSI', 'nationality' => 'CORAL SEA ISLANDS'],
            ['no' => '63', 'code' => 'CUB', 'nationality' => 'CUBA'],
            ['no' => '64', 'code' => 'CYP', 'nationality' => 'CYPRUS'],
            ['no' => '65', 'code' => 'CZE', 'nationality' => 'CZECH REPUBLIC'],
            ['no' => '66', 'code' => 'DEN', 'nationality' => 'DENMARK'],
            ['no' => '67', 'code' => 'DJI', 'nationality' => 'DJIBOUTI'],
            ['no' => '68', 'code' => 'DMN', 'nationality' => 'DOMINICA'],
            ['no' => '69', 'code' => 'DOM', 'nationality' => 'DOMINICAN REPUBLIC'],
            ['no' => '70', 'code' => 'ECU', 'nationality' => 'ECUADOR'],
            ['no' => '71', 'code' => 'EGY', 'nationality' => 'EGYPT'],
            ['no' => '72', 'code' => 'ELS', 'nationality' => 'EL SALVADOR']
        ];

        foreach ($nationalities as $nationality) {
            ZurichNationalityAndCode::updateOrCreate(
                ['no' => $nationality['no']],
                ['code' => $nationality['code'], 'nationality' => $nationality['nationality']]
            );
        }

    }
}
