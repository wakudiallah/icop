<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamRegionlkm;

class ParamRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (!ParamRegionlkm::where('region', 'Penisular Malaysia')->exists()) {
            // Create the user only if it doesn't exist
            ParamRegionlkm::create([
                'region'    => 'Penisular Malaysia',
                'status' => '1',
            ]);
        }

        if (!ParamRegionlkm::where('region', 'Sabah / Sarawak')->exists()) {
            // Create the user only if it doesn't exist
            ParamRegionlkm::create([
                'region'    => 'Sabah / Sarawak',
                'status' => '1',
            ]);
        }

        if (!ParamRegionlkm::where('region', 'Pulau Pangkor & Langkawi')->exists()) {
            // Create the user only if it doesn't exist
            ParamRegionlkm::create([
                'region'    => 'Pulau Pangkor & Langkawi',
                'status' => '1',
            ]);
        }

        if (!ParamRegionlkm::where('region', 'Labuan')->exists()) {
            // Create the user only if it doesn't exist
            ParamRegionlkm::create([
                'region'    => 'Labuan',
                'status' => '1',
            ]);
        }


    }


}
