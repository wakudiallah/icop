<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichVoluntaryExcess;

class ZurichVoluntaryExcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $voluntaryExcesses = [
            ['code' => '00', 'desc' => '0'],
            ['code' => '02', 'desc' => '500'],
            ['code' => '03', 'desc' => '1,000'],
            ['code' => '04', 'desc' => '2,000'],
            ['code' => '05', 'desc' => '3,000'],
            ['code' => '06', 'desc' => '5,000'],
            ['code' => '07', 'desc' => '10,000'],
        ];

        foreach ($voluntaryExcesses as $excess) {
            if (!ZurichVoluntaryExcess::where('code', $excess['code'])->exists()) {
                ZurichVoluntaryExcess::create($excess);
            }
        }
        
    }
}
