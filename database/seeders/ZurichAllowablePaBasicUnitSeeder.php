<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowablePaBasicUnit;

class ZurichAllowablePaBasicUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        if (!ZurichAllowablePaBasicUnit::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePaBasicUnit::create([
                'no'    => '1',
                'pa_basic_unit'    => '1'
            ]);
        }

        if (!ZurichAllowablePaBasicUnit::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePaBasicUnit::create([
                'no'    => '2',
                'pa_basic_unit'    => ''
            ]);
        }

        if (!ZurichAllowablePaBasicUnit::where('no', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePaBasicUnit::create([
                'no'    => '3',
                'pa_basic_unit'    => '3'
            ]);
        }

        if (!ZurichAllowablePaBasicUnit::where('no', '4')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePaBasicUnit::create([
                'no'    => '4',
                'pa_basic_unit'    => '4'
            ]);
        }

        if (!ZurichAllowablePaBasicUnit::where('no', '5')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePaBasicUnit::create([
                'no'    => '5',
                'pa_basic_unit'    => '5'
            ]);
        }


    }
}
