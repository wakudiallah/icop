<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamInsurancePromo;

class ParamInsurancePromoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $transactionTypes = [
            ['insurance_product_id' => '1', 'promo' => 'Termasuk 24-Jam Khidmat Tunda sebanyak RM 200 setiap tahun', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Termasuk Pemandu Tambahan yang dinamakan', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Termasuk 24-Jam Bantuan Tuntutan', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Proses Kelulusan dan Pembayaran Tuntutan yang senang dan pantas', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Takaful', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Kelulusan Segera bagi Not Perlindungan', 'status' => '1'],
            ['insurance_product_id' => '1', 'promo' => 'Sesuai bagi Kegunaan E-hailing', 'status' => '1'],
        ];

        foreach ($transactionTypes as $transactionType) {
            if (!ParamInsurancePromo::where('promo', $transactionType['promo'])->exists()) {
                ParamInsurancePromo::create($transactionType);
            }
        }
    }
}
