<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichState;

class ZurichStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $states = [
            ['code' => '01', 'desc' => 'PERLIS INDERA KAYANGAN'],
            ['code' => '02', 'desc' => 'KEDAH DARUL AMAN'],
            ['code' => '03', 'desc' => 'PULAU PINANG'],
            ['code' => '04', 'desc' => 'PERAK DARUL RIDZUAN'],
            ['code' => '05', 'desc' => 'SELANGOR DARUL EHSAN'],
            ['code' => '06', 'desc' => 'WILAYAH PERSEKUTUAN'],
            ['code' => '07', 'desc' => 'JOHOR DARUL TAKZIM'],
            ['code' => '08', 'desc' => 'MELAKA'],
            ['code' => '09', 'desc' => 'NEGERI SEMBILAN DARUL KHUSUS'],
            ['code' => '10', 'desc' => 'PAHANG DARUL MAKMUR'],
            ['code' => '11', 'desc' => 'TERENGGANU DARUL IMAN'],
            ['code' => '12', 'desc' => 'KELANTAN DARUL NAIM'],
            ['code' => '13', 'desc' => 'SABAH'],
            ['code' => '14', 'desc' => 'SARAWAK'],
            ['code' => '15', 'desc' => 'LABUAN'],
            ['code' => '16', 'desc' => 'PUTRAJAYA'],
        ];

        foreach ($states as $state) {
            if (!ZurichState::where('code', $state['code'])->exists()) {
                ZurichState::create($state);
            }
        }
    }
}
