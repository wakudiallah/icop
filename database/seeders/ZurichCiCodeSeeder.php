<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichCiCode;

class ZurichCiCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $ciCodes = [
            ['code' => 'MX1', 'desc' => 'Private Use Only'],
            ['code' => 'MX12', 'desc' => 'Private Use Only - Insured Not Driving'],
            ['code' => 'MX4', 'desc' => 'Company Use Only'],
            ['code' => 'MY3', 'desc' => 'Motorcycle Only - Private/Company'],
        ];

        foreach ($ciCodes as $ciCode) {
            if (!ZurichCiCode::where('code', $ciCode['code'])->exists()) {
                ZurichCiCode::create($ciCode);
            }
        }
        
    }
}
