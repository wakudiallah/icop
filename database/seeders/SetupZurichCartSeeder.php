<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SetupZurichCart;

class SetupZurichCartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (!SetupZurichCart::where('desc', '7 Day - 50 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '7 Day - 50 Cart Amount',
                'cart_day_id' => 1,
                'cart_amount_id' => 1,
                'status' => 1
            ]);
        }

        if (!SetupZurichCart::where('desc', '7 Day - 100 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '7 Day - 100 Cart Amount',
                'cart_day_id' => 1,
                'cart_amount_id' => 2,
                'status' => 1
            ]);
        }

        if (!SetupZurichCart::where('desc', '7 Day - 200 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '7 Day - 200 Cart Amount',
                'cart_day_id' => 1,
                'cart_amount_id' => 3,
                'status' => 1
            ]);
        }


        if (!SetupZurichCart::where('desc', '14 Day - 50 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '14 Day - 50 Cart Amount',
                'cart_day_id' => 2,
                'cart_amount_id' => 1,
                'status' => 1
            ]);
        }



        if (!SetupZurichCart::where('desc', '14 Day - 100 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '14 Day - 100 Cart Amount',
                'cart_day_id' => 2,
                'cart_amount_id' => 2,
                'status' => 1
            ]);
        }

        if (!SetupZurichCart::where('desc', '14 Day - 200 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '14 Day - 200 Cart Amount',
                'cart_day_id' => 2,
                'cart_amount_id' => 3,
                'status' => 1
            ]);
        }



        if (!SetupZurichCart::where('desc', '21 Day - 50 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '21 Day - 50 Cart Amount',
                'cart_day_id' => 3,
                'cart_amount_id' => 1,
                'status' => 1
            ]);
        }

        if (!SetupZurichCart::where('desc', '21 Day - 100 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '21 Day - 100 Cart Amount',
                'cart_day_id' => 3,
                'cart_amount_id' => 2,
                'status' => 1
            ]);
        }

        if (!SetupZurichCart::where('desc', '21 Day - 200 Cart Amount')->exists()) {
            // Create the user only if it doesn't exist
            SetupZurichCart::create([
                'desc'    => '21 Day - 200 Cart Amount',
                'cart_day_id' => 3,
                'cart_amount_id' => 3,
                'status' => 1
            ]);
        }




    }
}
