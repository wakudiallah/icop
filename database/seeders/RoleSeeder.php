<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if (!Role::where('name', 'Super Admin')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'id'            => '2',
                'name'          => 'Super Admin',
                'guard_name'    => 'web',
            ]);
        }

        if (!Role::where('name', 'Agent')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'id'            => '3',
                'name'          => 'Agent',
                'guard_name'    => 'web',
            ]);
        }

        if (!Role::where('name', 'Admin')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'id'            => '4',
                'name'          => 'Admin',
                'guard_name'    => 'web',
            ]);
        }

        if (!Role::where('name', 'Company Pembiayaan')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'id'            => '5',
                'name'          => 'Company Pembiayaan',
                'guard_name'    => 'web',
            ]);
        }

        
        if (!Role::where('name', 'Customer')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'id'            => '6',
                'name'          => 'Customer',
                'guard_name'    => 'web',
            ]);
        }


    }
}
