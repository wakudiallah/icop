<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichExcess;


class ZurichExcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $excesses = [
            ['code' => 'EXS01', 'desc' => 'EXCESS ALL CLAIMS'],
            ['code' => 'EXS02', 'desc' => 'EXCESS - DAMAGE CLAIM (PCAR)'],
            ['code' => 'EXS03', 'desc' => 'EXCESS THEFT CLAIMS'],
            ['code' => 'EXS04', 'desc' => 'EXCESS - DAMAGE CLAIMS (MCYCLE)'],
            ['code' => 'EXS05', 'desc' => 'EXCESS - VOLUNTARY'],
        ];

        foreach ($excesses as $excess) {
            if (!ZurichExcess::where('code', $excess['code'])->exists()) {
                ZurichExcess::create($excess);
            }
        }
        
    }
}
