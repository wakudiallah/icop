<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichType;

class ZurichTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            ['code' => 'C', 'desc' => 'Conditional'],
            ['code' => 'M', 'desc' => 'Mandatory'],
            ['code' => 'O', 'desc' => 'Optional'],
        ];

        foreach ($types as $type) {
            if (!ZurichType::where('code', $type['code'])->exists()) {
                ZurichType::create($type);
            }
        }
    }
}
