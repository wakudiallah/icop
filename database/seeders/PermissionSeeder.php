<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!Permission::where('name', 'param-branch-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-branch-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-branch-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-branch-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-branch-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-branch-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-branch-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-branch-delete',
                'guard_name'    => 'web',
            ]);
        }



        if (!Permission::where('name', 'param-insurance-company-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-company-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-insurance-company-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-company-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-insurance-company-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-company-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-insurance-company-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-company-delete',
                'guard_name'    => 'web',
            ]);
        }
        




        if (!Permission::where('name', 'param-insurance-policy-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-policy-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-insurance-policy-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-policy-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-insurance-policy-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-policy-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-insurance-policy-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-policy-delete',
                'guard_name'    => 'web',
            ]);
        }



        if (!Permission::where('name', 'param-insurance-promo-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-promo-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-insurance-promo-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-promo-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-insurance-promo-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-promo-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-insurance-promo-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-insurance-promo-delete',
                'guard_name'    => 'web',
            ]);
        }


        

        if (!Permission::where('name', 'param-protection-type-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-protection-type-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-protection-type-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-protection-type-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-protection-type-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-protection-type-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-protection-type-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-protection-type-delete',
                'guard_name'    => 'web',
            ]);
        }





        if (!Permission::where('name', 'param-status-app-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-status-app-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-status-app-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-status-app-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-status-app-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-status-app-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-status-app-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-status-app-delete',
                'guard_name'    => 'web',
            ]);
        }

                
        
        if (!Permission::where('name', 'param-ehailing-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ehailing-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-ehailing-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ehailing-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-ehailing-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ehailing-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-ehailing-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ehailing-delete',
                'guard_name'    => 'web',
            ]);
        }





        if (!Permission::where('name', 'param-employment-status-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-status-list',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-employment-status-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-status-create',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-employment-status-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-status-edit',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-employment-status-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-status-delete',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-employment-type-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-type-list',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-employment-type-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-type-create',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-employment-type-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-type-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-employment-type-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-employment-type-delete',
                'guard_name'    => 'web',
            ]);
        }



        if (!Permission::where('name', 'param-occupation-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-occupation-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-occupation-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-occupation-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-occupation-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-occupation-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-occupation-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-occupation-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'param-payment-mode-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-payment-mode-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-payment-mode-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-payment-mode-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-payment-mode-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-payment-mode-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-payment-mode-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-payment-mode-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'param-reject-reason-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-reject-reason-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-reject-reason-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-reject-reason-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-reject-reason-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-reject-reason-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-reject-reason-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-reject-reason-delete',
                'guard_name'    => 'web',
            ]);
        }





        if (!Permission::where('name', 'param-bank-name-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-bank-name-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-bank-name-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-bank-name-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-bank-name-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-bank-name-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-bank-name-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-bank-name-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'param-takaful-provider-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-takaful-provider-list',
                'guard_name'    => 'web',
            ]);
        }



        if (!Permission::where('name', 'param-takaful-provider-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-takaful-provider-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-takaful-provider-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-takaful-provider-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-takaful-provider-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-takaful-provider-delete',
                'guard_name'    => 'web',
            ]);
        }





        if (!Permission::where('name', 'param-manfaat-tambahan-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-manfaat-tambahan-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-manfaat-tambahan-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-manfaat-tambahan-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-manfaat-tambahan-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-manfaat-tambahan-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-manfaat-tambahan-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-manfaat-tambahan-delete',
                'guard_name'    => 'web',
            ]);
        }
   
        


        if (!Permission::where('name', 'param-interest-rate-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-interest-rate-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-interest-rate-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-interest-rate-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-interest-rate-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-interest-rate-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-interest-rate-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-interest-rate-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'param-sst-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-sst-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-sst-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-sst-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-sst-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-sst-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-sst-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-sst-delete',
                'guard_name'    => 'web',
            ]);
        }
   



        if (!Permission::where('name', 'param-gst-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-gst-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-gst-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-gst-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-gst-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-gst-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-gst-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-gst-delete',
                'guard_name'    => 'web',
            ]);
        }     



        if (!Permission::where('name', 'param-state-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-state-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-state-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-state-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-state-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-state-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-state-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-state-delete',
                'guard_name'    => 'web',
            ]);
        }           




        if (!Permission::where('name', 'param-postcode-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-postcode-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-postcode-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-postcode-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-postcode-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-postcode-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-postcode-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-postcode-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'param-duti-setem-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-duti-setem-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-duti-setem-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-duti-setem-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-duti-setem-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-duti-setem-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-duti-setem-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-duti-setem-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'role-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'role-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'role-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'role-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'role-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'role-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'role-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'role-delete',
                'guard_name'    => 'web',
            ]);
        } 





        if (!Permission::where('name', 'user-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'user-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'user-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'user-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'user-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'user-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'user-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'user-delete',
                'guard_name'    => 'web',
            ]);
        } 





        if (!Permission::where('name', 'permission-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'permission-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'permission-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'permission-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'permission-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'permission-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'permission-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'permission-delete',
                'guard_name'    => 'web',
            ]);
        } 





        if (!Permission::where('name', 'audit-trail-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'audit-trail-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'audit-trail-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'audit-trail-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'audit-trail-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'audit-trail-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'audit-trail-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'audit-trail-delete',
                'guard_name'    => 'web',
            ]);
        } 





        if (!Permission::where('name', 'praapplication-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'praapplication-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'praapplication-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'praapplication-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'praapplication-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'praapplication-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'praapplication-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'praapplication-delete',
                'guard_name'    => 'web',
            ]);
        } 







        if (!Permission::where('name', 'param-type-insurance-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-type-insurance-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-type-insurance-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-type-insurance-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-type-insurance-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-type-insurance-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-type-insurance-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-type-insurance-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'param-email-system-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-system-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-email-system-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-system-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-email-system-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-system-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-email-system-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-system-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'param-email-for-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-for-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-email-for-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-for-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-email-for-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-for-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-email-for-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-email-for-delete',
                'guard_name'    => 'web',
            ]);
        }  




        if (!Permission::where('name', 'param-region-lkm-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-region-lkm-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-region-lkm-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-region-lkm-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-region-lkm-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-region-lkm-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-region-lkm-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-region-lkm-delete',
                'guard_name'    => 'web',
            ]);
        }  




        if (!Permission::where('name', 'param-ownership-lkm-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ownership-lkm-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-ownership-lkm-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ownership-lkm-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-ownership-lkm-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ownership-lkm-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-ownership-lkm-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-ownership-lkm-delete',
                'guard_name'    => 'web',
            ]);
        } 

   
        


        if (!Permission::where('name', 'param-roadtax-lkm-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-roadtax-lkm-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'param-roadtax-lkm-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-roadtax-lkm-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'param-roadtax-lkm-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-roadtax-lkm-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'param-roadtax-lkm-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'param-roadtax-lkm-delete',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'calculate-roadtax-view')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'calculate-roadtax-view',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'setup-maintainable-emails-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-maintainable-emails-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-maintainable-emails-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-maintainable-emails-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-maintainable-emails-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-maintainable-emails-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-maintainable-emails-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-maintainable-emails-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'setup-monthly-installment-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-monthly-installment-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-monthly-installment-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-monthly-installment-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-monthly-installment-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-monthly-installment-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-monthly-installment-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-monthly-installment-delete',
                'guard_name'    => 'web',
            ]);
        } 




        if (!Permission::where('name', 'setup-document-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-document-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-document-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-document-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-document-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-document-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-document-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-document-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'setup-smsakad-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-smsakad-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-smsakad-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-smsakad-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-smsakad-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-smsakad-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-smsakad-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-smsakad-delete',
                'guard_name'    => 'web',
            ]);
        }






        if (!Permission::where('name', 'setup-bpacharge-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-bpacharge-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-bpacharge-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-bpacharge-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-bpacharge-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-bpacharge-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-bpacharge-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-bpacharge-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'setup-zurichcart-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-zurichcart-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'setup-zurichcart-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-zurichcart-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'setup-zurichcart-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-zurichcart-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'setup-zurichcart-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'setup-zurichcart-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'app-processing-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-processing-list',
                'guard_name'    => 'web',
            ]);
        }


        if (!Permission::where('name', 'app-processing-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-processing-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'app-processing-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-processing-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'app-processing-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-processing-delete',
                'guard_name'    => 'web',
            ]);
        }




        if (!Permission::where('name', 'app-smsakad-list')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-smsakad-list',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'app-smsakad-create')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-smsakad-create',
                'guard_name'    => 'web',
            ]);
        }
        
        
        if (!Permission::where('name', 'app-smsakad-edit')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-smsakad-edit',
                'guard_name'    => 'web',
            ]);
        }

        if (!Permission::where('name', 'app-smsakad-delete')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'app-smsakad-delete',
                'guard_name'    => 'web',
            ]);
        }



        //Little Permission
        //Dashboard

        if (!Permission::where('name', 'dashboard')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'dashboard',
                'guard_name'    => 'web',
            ]);
        } 

        if (!Permission::where('name', 'dashboard-customer')->exists()) {
            // Create the user only if it doesn't exist
            Permission::create([
                'name'          => 'dashboard-customer',
                'guard_name'    => 'web',
            ]);
        } 

    }
}
