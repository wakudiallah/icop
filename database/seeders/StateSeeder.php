<?php

namespace Database\Seeders;
use App\Models\State;

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create( [
            'id'=>1,
            'state_code'=>'J',
            'state'=>'Johor',
            'alias_name'=>'JOHOR',
            'clrt_name'=>'JOHOR'
            ] );
                        
            State::create( [
            'id'=>2,
            'state_code'=>'K',
            'state'=>'Kedah',
            'alias_name'=>'KEDAH',
            'clrt_name'=>'KEDAH'
            ] );
                        
            State::create( [
            'id'=>3,
            'state_code'=>'D',
            'state'=>'Kelantan',
            'alias_name'=>'KELANTAN',
            'clrt_name'=>'KELANTAN'
            ] );
                        
            State::create( [
            'id'=>4,
            'state_code'=>'W',
            'state'=>'Wilayah Persekutuan Kuala Lumpur',
            'alias_name'=>'WP KUALA LUMPUR',
            'clrt_name'=>'W.P KUALA LUMPUR'
            ] );
                        
            State::create( [
            'id'=>5,
            'state_code'=>'L',
            'state'=>'Wilayah Persekutuan Labuan',
            'alias_name'=>'WP LABUAN',
            'clrt_name'=>'W.P LABUAN'
            ] );
                        
            State::create( [
            'id'=>6,
            'state_code'=>'M',
            'state'=>'Melaka',
            'alias_name'=>'MELAKA',
            'clrt_name'=>'MELAKA'
            ] );
                        
            State::create( [
            'id'=>7,
            'state_code'=>'N',
            'state'=>'Negeri Sembilan',
            'alias_name'=>'NEGERI SEMBILAN',
            'clrt_name'=>'NEGERI SEMBILAN'
            ] );
                        
            State::create( [
            'id'=>8,
            'state_code'=>'C',
            'state'=>'Pahang',
            'alias_name'=>'PAHANG',
            'clrt_name'=>'PAHANG'
            ] );
                        
            State::create( [
            'id'=>9,
            'state_code'=>'O',
            'state'=>'Wilayah Persekutuan Putra Jaya',
            'alias_name'=>'WP PUTRA JAYA',
            'clrt_name'=>'W.P PUTRAJAYA'
            ] );
                        
            State::create( [
            'id'=>10,
            'state_code'=>'R',
            'state'=>'Perlis',
            'alias_name'=>'PERLIS',
            'clrt_name'=>'PERLIS'
            ] );
                        
            State::create( [
            'id'=>11,
            'state_code'=>'P',
            'state'=>'Pulau Pinang',
            'alias_name'=>'PULAU PINANG',
            'clrt_name'=>'PULAU PINANG'
            ] );
                        
            State::create( [
            'id'=>12,
            'state_code'=>'A',
            'state'=>'Perak',
            'alias_name'=>'PERAK',
            'clrt_name'=>'PERAK'
            ] );
                        
            State::create( [
            'id'=>13,
            'state_code'=>'S',
            'state'=>'Sabah',
            'alias_name'=>'SABAH',
            'clrt_name'=>'SABAH\r\n'
            ] );
                        
            State::create( [
            'id'=>14,
            'state_code'=>'B',
            'State'=>'Selangor',
            'alias_name'=>'SELANGOR',
            'clrt_name'=>'SELANGOR'
            ] );
                        
            State::create( [
            'id'=>15,
            'state_code'=>'Q',
            'state'=>'Sarawak',
            'alias_name'=>'SARAWAK',
            'clrt_name'=>'SARAWAK'
            ] );
                        
            State::create( [
            'id'=>16,
            'state_code'=>'T',
            'state'=>'Terengganu',
            'alias_name'=>'TERENGGANU',
            'clrt_name'=>'TERENGGANU'
            ] );
    }
}
