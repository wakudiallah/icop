<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SetupBpaCharge;

class SetupBpaChargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        if (!SetupBpaCharge::where('setup_bpa_charges', '2')->exists()) {
            // Create the user only if it doesn't exist
            SetupBpaCharge::create([
                'setup_bpa_charges'    => '2',
                'status' => 1,
            ]);
        }


    }
}
