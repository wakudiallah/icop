<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowableCourtesyCarDay;


class ZurichAllowableCourtesyCarDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        if (!ZurichAllowableCourtesyCarDay::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCourtesyCarDay::create([
                'no'    => '1',
                'courtesy_car_day'    => '3'
            ]);
        }

        if (!ZurichAllowableCourtesyCarDay::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCourtesyCarDay::create([
                'no'    => '2',
                'courtesy_car_day'    => '5'
            ]);
        }

        if (!ZurichAllowableCourtesyCarDay::where('no', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCourtesyCarDay::create([
                'no'    => '3',
                'courtesy_car_day'    => '10'
            ]);
        }


    }
}
