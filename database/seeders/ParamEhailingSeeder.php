<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamEhailing;

class ParamEhailingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ehailing
        //status

               
        if (!ParamEhailing::where('ehailing', 'Yes')->exists()) {
            // Create the user only if it doesn't exist
            ParamEhailing::create([
                'ehailing'          => 'Yes',
                'status'    => 1,
            ]);
        }

        if (!ParamEhailing::where('ehailing', 'No')->exists()) {
            // Create the user only if it doesn't exist
            ParamEhailing::create([
                'ehailing'          => 'No',
                'status'    => 1,
            ]);
        }
    }
}
