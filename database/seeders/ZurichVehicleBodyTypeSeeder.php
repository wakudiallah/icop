<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichVehicleBodyType;

class ZurichVehicleBodyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $bodyTypes = [
            ['no' => '1', 'desc' => 'FOUR WHEEL DRIVE'],
            ['no' => '2', 'desc' => 'AEROBACK'],
            ['no' => '3', 'desc' => 'AGRICULTURAL'],
            ['no' => '4', 'desc' => 'AMBULANCE'],
            ['no' => '5', 'desc' => 'ALUMINIUM WITH ALUMINIUM ROOF'],
            ['no' => '6', 'desc' => 'BULLDOZER'],
            ['no' => '7', 'desc' => 'BACKHOE LOADER'],
            ['no' => '8', 'desc' => 'BUS'],
            ['no' => '9', 'desc' => 'BOX VAN'],
            ['no' => '10', 'desc' => 'CREW/DCAB PICKUP'],
            ['no' => '11', 'desc' => 'CASE'],
            ['no' => '12', 'desc' => 'CABRIOLET'],
            ['no' => '13', 'desc' => 'C/CHAS'],
            ['no' => '14', 'desc' => 'CONCRETE MIXER'],
            ['no' => '15', 'desc' => 'COMEL'],
            ['no' => '16', 'desc' => 'COUPE'],
            ['no' => '17', 'desc' => 'COMPACTOR'],
            ['no' => '18', 'desc' => 'CARGO'],
            ['no' => '19', 'desc' => 'MOBILE CRANE'],
            ['no' => '20', 'desc' => 'CARAVAN'],
            ['no' => '21', 'desc' => 'CATERPILLAR'],
            ['no' => '22', 'desc' => '2D CONVERTIBLE'],
            ['no' => '23', 'desc' => 'CANVAS TOP'],
            ['no' => '24', 'desc' => 'MOTORCYCLE'],
            ['no' => '25', 'desc' => 'DUAL CAB PICKUP'],
            ['no' => '26', 'desc' => '4D DOUBLE CAB PICK UP'],
            ['no' => '27', 'desc' => '3D HATCHBACK'],
            ['no' => '28', 'desc' => 'DUMPER TIPPER'],
            ['no' => '29', 'desc' => 'EXCAVATOR'],
            ['no' => '30', 'desc' => '4D CONVERTIBLE'],
            ['no' => '31', 'desc' => '4D HATCHBACK'],
            ['no' => '32', 'desc' => '4D COUPE'],
            ['no' => '33', 'desc' => '4D SEDAN'],
            ['no' => '34', 'desc' => '5D HATCHBACK'],
            ['no' => '35', 'desc' => '4D VAN'],
            ['no' => '36', 'desc' => '4D WAGON'],
            ['no' => '37', 'desc' => 'FORKLIFT'],
            ['no' => '38', 'desc' => 'FIRE BRIGADE'],
            ['no' => '39', 'desc' => 'GRAB LOADER'],
            ['no' => '40', 'desc' => 'HARD TOP'],
            ['no' => '41', 'desc' => 'HEARSES'],
            ['no' => '42', 'desc' => 'HATCHBACK'],
            ['no' => '43', 'desc' => 'HYDRAULIC'],
            ['no' => '44', 'desc' => 'JEEP'],
            ['no' => '45', 'desc' => 'LIFTBACK'],
            ['no' => '46', 'desc' => 'LIMOUSINE'],
            ['no' => '47', 'desc' => 'LORRY'],
            ['no' => '48', 'desc' => 'LUTON VAN'],
            ['no' => '49', 'desc' => 'LORRY WITH CRANE'],
            ['no' => '50', 'desc' => 'MOBILE AERIAL PLATFORM'],
            ['no' => '51', 'desc' => 'MOBILE SHOPS AND CANTEENS'],
            ['no' => '52', 'desc' => 'MOBILE PLANT'],
            ['no' => '53', 'desc' => 'MOBILE CONCRETE PUMP'],
            ['no' => '54', 'desc' => 'MPV'],
            ['no' => '55', 'desc' => 'MOTORCYCLE SIDE CAR'],
            ['no' => '56', 'desc' => 'MOTOR GRADER'],
            ['no' => '57', 'desc' => 'MOTOR TRADE'],
            ['no' => '58', 'desc' => 'JEEP'],
            ['no' => '59', 'desc' => 'NAKED'],
            ['no' => '60', 'desc' => 'PICK-UP'],
            ['no' => '61', 'desc' => 'PANEL VAN'],
            ['no' => '62', 'desc' => 'PRIME MOVER'],
            ['no' => '63', 'desc' => 'REFRIGERATOR'],
            ['no' => '64', 'desc' => 'REFRIGERATED BOX'],
            ['no' => '65', 'desc' => 'ROAD PAVER'],
            ['no' => '66', 'desc' => 'ROAD ROLLER'],
            ['no' => '67', 'desc' => 'ROAD SWEEPER'],
            ['no' => '68', 'desc' => 'STEEL WITH ALUMINIUM ROOF'],
            ['no' => '69', 'desc' => '4D SINGLE CAB PICK-UP'],
            ['no' => '70', 'desc' => 'MOTORCYCLE SIDE-CAR'],
            ['no' => '71', 'desc' => 'SEDAN'],
            ['no' => '72', 'desc' => 'SOFTTOP'],
            ['no' => '73', 'desc' => '2D SINGLE CAB PICK UP'],
            ['no' => '74', 'desc' => 'SKY MASTER / SKYLIFT'],
            ['no' => '75', 'desc' => 'SALOON'],
            ['no' => '76', 'desc' => 'SEMI TRAILER LOW LOADER'],
            ['no' => '77', 'desc' => 'SPORT'],
            ['no' => '78', 'desc' => 'SEMI PANEL VAN'],
            ['no' => '79', 'desc' => 'STEEL TIPPER'],
            ['no' => '80', 'desc' => 'STEEL TRAY'],
            ['no' => '81', 'desc' => 'STATIONWAGON'],
            ['no' => '82', 'desc' => 'SPORT UTILITY VEHICLE'],
            ['no' => '83', 'desc' => 'SHOVEL'],
            ['no' => '84', 'desc' => 'STATIONWAGON'],
            ['no' => '85', 'desc' => 'TAXI'],
            ['no' => '86', 'desc' => '2D ROADSTER'],
            ['no' => '87', 'desc' => '3D VAN'],
            ['no' => '88', 'desc' => 'TANKER'],
            ['no' => '89', 'desc' => 'TRUCK'],
            ['no' => '90', 'desc' => 'TRAILER'],
            ['no' => '91', 'desc' => 'TRACTOR'],
            ['no' => '92', 'desc' => 'TOWING TRUCK'],
            ['no' => '93', 'desc' => 'VAN'],
            ['no' => '94', 'desc' => 'WOODEN CARGO'],
            ['no' => '95', 'desc' => 'WOODEN TIPPER'],
            ['no' => '96', 'desc' => 'WOOD TRAY'],
            ['no' => '97', 'desc' => 'WINDOW VAN'],
            ['no' => '98', 'desc' => 'WAGON'],
            ['no' => '99', 'desc' => 'WHEEL / CRAWLER'],
            ['no' => '100', 'desc' => 'WOOD WITH ALUMINIUM ROOF'],
        
        ];

        foreach ($bodyTypes as $bodyType) {
            ZurichVehicleBodyType::create([
                'no' => $bodyType['no'],
                'desc' => $bodyType['desc'],
            ]);
        }

    }
}
