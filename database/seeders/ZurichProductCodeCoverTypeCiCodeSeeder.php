<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichProductCodeCoverTypeCiCode;

class ZurichProductCodeCoverTypeCiCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        if (!ZurichProductCodeCoverTypeCiCode::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '1',
                'product_code'    => 'PZ01',
                'cover_type'    => 'V-CO',
                'ci_code'    => 'MX12, MX1, MX4'
            ]);
        }

        if (!ZurichProductCodeCoverTypeCiCode::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '2',
                'product_code'    => 'PZ01',
                'cover_type'    => 'V-FT',
                'ci_code'    => 'MX12, MX1, MX4'
            ]);
        }

        if (!ZurichProductCodeCoverTypeCiCode::where('no', '')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '3',
                'product_code'    => 'PC01',
                'cover_type'    => 'V-TP',
                'ci_code'    => 'MX12, MX1, MX4'
            ]);
        }

        if (!ZurichProductCodeCoverTypeCiCode::where('no', '4')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '4',
                'product_code'    => 'MZ01',
                'cover_type'    => 'V-CO',
                'ci_code'    => 'MY3'
            ]);
        }


        if (!ZurichProductCodeCoverTypeCiCode::where('no', '5')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '5',
                'product_code'    => 'MC01',
                'cover_type'    => 'V-FT',
                'ci_code'    => 'MY3'
            ]);
        }


        if (!ZurichProductCodeCoverTypeCiCode::where('no', '6')->exists()) {
            // Create the user only if it doesn't exist
            ZurichProductCodeCoverTypeCiCode::create([
                'no'    => '6',
                'product_code'    => 'MC01',
                'cover_type'    => 'V-TP',
                'ci_code'    => 'MY3'
            ]);
        }


    }
}
