<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamRoadtaxlkm;

class ParamRoadtaxlkmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roadtaxData = [
            // Region 1, ownership 1
            ['region' => 1, 'ownership' => 1, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 55, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 70, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 90, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 200, 'kadar_progressif' => 0.4, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 280, 'kadar_progressif' => 0.5, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 380, 'kadar_progressif' => 1, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 880, 'kadar_progressif' => 2.5, 'status' => 1],
            ['region' => 1, 'ownership' => 1, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 2130, 'kadar_progressif' => 4.5, 'status' => 1],
            
            // Region 1, ownership 2
            ['region' => 1, 'ownership' => 2, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 110, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 140, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 180, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 400, 'kadar_progressif' => 0.8, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 560, 'kadar_progressif' => 1, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 760, 'kadar_progressif' => 3, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 2260, 'kadar_progressif' => 7.5, 'status' => 1],
            ['region' => 1, 'ownership' => 2, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 6010, 'kadar_progressif' => 13.5, 'status' => 1],
            
            // Region 1, ownership 4
            ['region' => 1, 'ownership' => 4, 'start' => 0,    'end' => 150,   'kadar_flat' => 2, 'status' => 1],
            ['region' => 1, 'ownership' => 4, 'start' => 151,  'end' => 200,   'kadar_flat' => 30, 'status' => 1],
            ['region' => 1, 'ownership' => 4, 'start' => 201,  'end' => 250,   'kadar_flat' => 50, 'status' => 1],
            ['region' => 1, 'ownership' => 4, 'start' => 251,  'end' => 500,   'kadar_flat' => 100, 'status' => 1],
            ['region' => 1, 'ownership' => 4, 'start' => 501,  'end' => 800,   'kadar_flat' => 250, 'status' => 1],
            ['region' => 1, 'ownership' => 4, 'start' => 801,  'end' => null,  'kadar_flat' => 350, 'status' => 1],
            
            // Other cases
            ['region' => 1, 'ownership' => 3, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 85, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 100, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 120, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 1601, 'end' => 1800,  'kadar_flat' => 300, 'kadar_progressif' => 0.3, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 1801, 'end' => 2000,  'kadar_flat' => 360, 'kadar_progressif' => 0.4, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 2001, 'end' => 2500,  'kadar_flat' => 440, 'kadar_progressif' => 0.8, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 2501, 'end' => 3000,  'kadar_flat' => 840, 'kadar_progressif' => 1.6, 'status' => 1],
            ['region' => 1, 'ownership' => 3, 'start' => 3001, 'end' => null,  'kadar_flat' => 1640, 'kadar_progressif' => 1.6, 'status' => 1],

            //Region 2, ownership 1 or ownership 2
            ['region' => 2, 'ownership' => 1, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 44, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 56, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 72, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 160, 'kadar_progressif' => 0.32, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 224, 'kadar_progressif' => 0.25, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 274, 'kadar_progressif' => 0.5, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 524, 'kadar_progressif' => 1, 'status' => 1],
            ['region' => 2, 'ownership' => 1, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 1024, 'kadar_progressif' => 1.35, 'status' => 1],

            ['region' => 2, 'ownership' => 2, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 44, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 56, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 72, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 160, 'kadar_progressif' => 0.32, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 224, 'kadar_progressif' => 0.25, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 274, 'kadar_progressif' => 0.5, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 524, 'kadar_progressif' => 1, 'status' => 1],
            ['region' => 2, 'ownership' => 2, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 1024, 'kadar_progressif' => 1.35, 'status' => 1],

            // Region 2, ownership 4
            ['region' => 2, 'ownership' => 4, 'start' => 0,    'end' => 150,   'kadar_flat' => 2, 'status' => 1],
            ['region' => 2, 'ownership' => 4, 'start' => 151,  'end' => 200,   'kadar_flat' => 30, 'status' => 1],
            ['region' => 2, 'ownership' => 4, 'start' => 201,  'end' => 250,   'kadar_flat' => 50, 'status' => 1],
            ['region' => 2, 'ownership' => 4, 'start' => 251,  'end' => 500,   'kadar_flat' => 100, 'status' => 1],
            ['region' => 2, 'ownership' => 4, 'start' => 501,  'end' => 800,   'kadar_flat' => 250, 'status' => 1],
            ['region' => 2, 'ownership' => 4, 'start' => 801,  'end' => null,  'kadar_flat' => 350, 'status' => 1],

            // Region 2, Other ownerships
            ['region' => 2, 'ownership' => 3, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 42.5, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 50, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 60, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_flat' => 165, 'kadar_progressif' => 0.17, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_flat' => 199, 'kadar_progressif' => 0.22, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_flat' => 243, 'kadar_progressif' => 0.44, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_flat' => 463, 'kadar_progressif' => 0.88, 'status' => 1],
            ['region' => 2, 'ownership' => 3, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_flat' => 903, 'kadar_progressif' => 1.2, 'status' => 1],

            ['region' => 3, 'ownership' => 1, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 27.50, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 35, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 45, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 100, 'kadar_progressif' => 0.2, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 140, 'kadar_progressif' => 0.25, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 190, 'kadar_progressif' => 0.5, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 440, 'kadar_progressif' => 0.75, 'status' => 1],
            ['region' => 3, 'ownership' => 1, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 1065, 'kadar_progressif' => 2.25, 'status' => 1],

            // Region 3, ownership 2
            ['region' => 3, 'ownership' => 2, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 55, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 70, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 90, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 200, 'kadar_progressif' => 0.4, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 280, 'kadar_progressif' => 0.5, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 380, 'kadar_progressif' => 1.5, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 1100, 'kadar_progressif' => 3.75, 'status' => 1],
            ['region' => 3, 'ownership' => 2, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 3005, 'kadar_progressif' => 6.75, 'status' => 1],

            // Region 3, ownership 4
            ['region' => 3, 'ownership' => 4, 'start' => 0,    'end' => 150,   'kadar_flat' => 2, 'status' => 1],
            ['region' => 3, 'ownership' => 4, 'start' => 151,  'end' => 200,   'kadar_flat' => 30, 'status' => 1],
            ['region' => 3, 'ownership' => 4, 'start' => 201,  'end' => 250,   'kadar_flat' => 50, 'status' => 1],
            ['region' => 3, 'ownership' => 4, 'start' => 251,  'end' => 500,   'kadar_flat' => 100, 'status' => 1],
            ['region' => 3, 'ownership' => 4, 'start' => 501,  'end' => 800,   'kadar_flat' => 250, 'status' => 1],
            ['region' => 3, 'ownership' => 4, 'start' => 801,  'end' => null,  'kadar_flat' => 350, 'status' => 1],

            // Region 3, Other ownerships
            ['region' => 3, 'ownership' => 3, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 42.50, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 50, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 70, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 150, 'kadar_progressif' => 0.15, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 180, 'kadar_progressif' => 0.2, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 220, 'kadar_progressif' => 0.4, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 420, 'kadar_progressif' => 0.8, 'status' => 1],
            ['region' => 3, 'ownership' => 3, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 820, 'kadar_progressif' => 0.8, 'status' => 1],


            ['region' => 4, 'ownership' => 1, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 22, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 28, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 36, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_asas' => 80,  'kadar_progressif' => 0.16, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_asas' => 112, 'kadar_progressif' => 0.13, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_asas' => 137, 'kadar_progressif' => 0.25, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_asas' => 262, 'kadar_progressif' => 0.50, 'status' => 1],
            ['region' => 4, 'ownership' => 1, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_asas' => 512, 'kadar_progressif' => 0.68, 'status' => 1],

            // Region 4, ownership 2 (same ranges as ownership 1)
            ['region' => 4, 'ownership' => 2, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 22, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 28, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 36, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_flat' => 80,  'kadar_progressif' => 0.16, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_flat' => 112, 'kadar_progressif' => 0.13, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_flat' => 137, 'kadar_progressif' => 0.25, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_flat' => 262, 'kadar_progressif' => 0.50, 'status' => 1],
            ['region' => 4, 'ownership' => 2, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_flat' => 512, 'kadar_progressif' => 0.68, 'status' => 1],

            // Region 4, ownership 4 (different ranges)
            ['region' => 4, 'ownership' => 4, 'start' => 0,    'end' => 150,   'kadar_flat' => 2, 'status' => 1],
            ['region' => 4, 'ownership' => 4, 'start' => 151,  'end' => 200,   'kadar_flat' => 30, 'status' => 1],
            ['region' => 4, 'ownership' => 4, 'start' => 201,  'end' => 250,   'kadar_flat' => 50, 'status' => 1],
            ['region' => 4, 'ownership' => 4, 'start' => 251,  'end' => 500,   'kadar_flat' => 100, 'status' => 1],
            ['region' => 4, 'ownership' => 4, 'start' => 501,  'end' => 800,   'kadar_flat' => 250, 'status' => 1],
            ['region' => 4, 'ownership' => 4, 'start' => 801,  'end' => null,  'kadar_flat' => 350, 'status' => 1],

            // Region 4, Default ownership
            ['region' => 4, 'ownership' => 3, 'start' => 0,    'end' => 1000,  'kadar_flat' => 20, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 1001, 'end' => 1200,  'kadar_flat' => 21.25, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 1201, 'end' => 1400,  'kadar_flat' => 25, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 1401, 'end' => 1600,  'kadar_flat' => 30, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 1601, 'end' => 1800,  'kadar_flat' => null, 'kadar_flat' => 82,  'kadar_progressif' => 0.89, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 1801, 'end' => 2000,  'kadar_flat' => null, 'kadar_flat' => 99,  'kadar_progressif' => 0.61, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 2001, 'end' => 2500,  'kadar_flat' => null, 'kadar_flat' => 121, 'kadar_progressif' => 0.72, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 2501, 'end' => 3000,  'kadar_flat' => null, 'kadar_flat' => 231, 'kadar_progressif' => 0.94, 'status' => 1],
            ['region' => 4, 'ownership' => 3, 'start' => 3001, 'end' => null,  'kadar_flat' => null, 'kadar_flat' => 451, 'kadar_progressif' => 1.10, 'status' => 1],
        ];

        foreach ($roadtaxData as $data) {
            ParamRoadtaxlkm::create($data);
        }


    }
}
