<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\ZurichReferral;

class ZurichReferralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $referrals = [
            ['code' => 'R01', 'referral_message' => 'Referred due to Body Type and Vehicle Age []', 'referral_variable' => 'Body Type and Vehicle Age'],
            ['code' => 'R02', 'referral_message' => 'Referred due to Insured Age []', 'referral_variable' => 'Insured Age'],
            ['code' => 'R03', 'referral_message' => 'Referred due to Vehicle Sum Insured []', 'referral_variable' => 'Vehicle Sum Insured'],
            ['code' => 'R04', 'referral_message' => 'Referred due to Transaction Type []', 'referral_variable' => 'Transaction Type'],
            ['code' => 'R05', 'referral_message' => 'Referred due to Vehicle Model []', 'referral_variable' => 'Vehicle Model'],
            ['code' => 'R06', 'referral_message' => 'Referred due to Vehicle Capacity []', 'referral_variable' => 'Vehicle Capacity'],
            ['code' => 'R07', 'referral_message' => 'Referred due to Vehicle Age []', 'referral_variable' => 'Vehicle Age'],
            ['code' => 'R08', 'referral_message' => 'Referred due to Post Claim Indicator []', 'referral_variable' => 'Post Claim Indicator'],
            ['code' => 'R09', 'referral_message' => 'Referred due to NCD Response []', 'referral_variable' => 'NCD Response'],
            ['code' => 'R10', 'referral_message' => 'Referred due to Vehicle Model, Vehicle Age and Region []', 'referral_variable' => 'Vehicle Model, Vehicle Age and Region'],
            ['code' => 'R11', 'referral_message' => 'Referred due to Change of ABI Market Value', 'referral_variable' => 'Vehicle Sum Insured Deviation'],
            ['code' => 'R12', 'referral_message' => 'Referred due to Sum Insured []', 'referral_variable' => 'Minimum Sum Insured'],
            ['code' => 'R13', 'referral_message' => '[] is blacklisted', 'referral_variable' => 'Insured Blacklist'],
            ['code' => 'R14', 'referral_message' => 'Vehicle registration blacklisted by []', 'referral_variable' => 'Vehicle Blacklist'],
        ];

        foreach ($referrals as $referral) {
            if (!ZurichReferral::where('code', $referral['code'])->exists()) {
                ZurichReferral::create($referral);
            }
        }

    }
}
