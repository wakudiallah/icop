<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichProductCode;

class ZurichProductCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $productCodes = [
            ['code' => 'PC01', 'desc' => 'PRIVATE CAR FOR PRIVATE USE'],
            ['code' => 'PC02', 'desc' => 'PRIVATE CAR FOR TUITION PURPOSE'],
            ['code' => 'PZ01', 'desc' => 'PRIVATE CAR – Z-DRIVER'],
            ['code' => 'MZ01', 'desc' => 'MOTORCYCLE – Z-RIDER'],
        ];

        foreach ($productCodes as $productCode) {
            if (!ZurichProductCode::where('code', $productCode['code'])->exists()) {
                ZurichProductCode::create($productCode);
            }
        }
    }
}
