<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichCoverType;

class ZurichCoverTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $coverTypes = [
            ['code' => 'V-CO', 'desc' => 'MOTOR COMPREHENSIVE'],
            ['code' => 'V-FT', 'desc' => 'MOTOR THIRD PARTY FIRE AND THEFT'],
            ['code' => 'V-TP', 'desc' => 'MOTOR THIRD PARTY'],
        ];

        foreach ($coverTypes as $coverType) {
            if (!ZurichCoverType::where('code', $coverType['code'])->exists()) {
                ZurichCoverType::create($coverType);
            }
        }
        
    }
}
