<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowableCartDay;


class ZurichAllowableCartDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        if (!ZurichAllowableCartDay::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartDay::create([
                'no'    => '1',
                'cart_day'    => '7'
            ]);
        }

        if (!ZurichAllowableCartDay::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartDay::create([
                'no'    => '2',
                'cart_day'    => '14'
            ]);
        }


        if (!ZurichAllowableCartDay::where('no', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartDay::create([
                'no'    => '3',
                'cart_day'    => '21'
            ]);
        }


    }
}
