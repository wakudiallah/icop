<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichError;

class ZurichErrorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $errors = [
            ['code' => '1a', 'desc' => 'Mandatory fields missing'],
            ['code' => '1b', 'desc' => 'Mandatory fields missing'],
            ['code' => '2', 'desc' => 'Extra Cover Conflict'],
            ['code' => '3', 'desc' => 'PA Extra Cover Conflict'],
            ['code' => '4', 'desc' => 'Private Car, Third Party Only'],
            ['code' => '5', 'desc' => 'Maximum Effective Date'],
            ['code' => '6', 'desc' => 'Maximum Vehicle Age'],
            ['code' => '7', 'desc' => 'Extra Cover Dependency'],
            ['code' => '8', 'desc' => 'Maximum Period of Cover'],
            ['code' => '9', 'desc' => 'No PAC Selected'],
            ['code' => '10', 'desc' => 'Minimum Sum Insured'],
            ['code' => '11', 'desc' => 'Third Party Cover is NOT Allowed'],
        ];

        foreach ($errors as $error) {
            if (!ZurichError::where('code', $error['code'])->exists()) {
                ZurichError::create($error);
            }
        }
    }
}
