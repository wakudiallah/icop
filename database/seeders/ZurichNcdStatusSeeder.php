<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichNcdStatus;

class ZurichNcdStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ncdStatuses = [
            ['code' => '000', 'desc' => 'OK for successful Enquiry'],
            ['code' => '001', 'desc' => 'NCD confirmation is unavailable as confirmation already been taken earlier'],
            ['code' => '002', 'desc' => 'OK for successful enquiry without market value'],
            ['code' => '003', 'desc' => 'Block the issuance, vehicle is Total Loss/Stolen'],
            ['code' => '004', 'desc' => 'Block the issuance because renew the certificate/policy no earlier than 2 months prior to expiry'],
            ['code' => '005', 'desc' => 'Previous policy has already expired'],
            ['code' => '006', 'desc' => 'We are not able to proceed with an online renewal of your policy due to active claims affecting your NCD status'],
            ['code' => '008', 'desc' => 'Error in ID No1 or ID No2 or ID does not match'],
            ['code' => '077', 'desc' => 'Data not found'],
            ['code' => '078', 'desc' => 'Allow to issue the covernote and display the message based on ISM response code to user'],
        ];

        foreach ($ncdStatuses as $ncdStatus) {
            if (!ZurichNcdStatus::where('code', $ncdStatus['code'])->exists()) {
                ZurichNcdStatus::create($ncdStatus);
            }
        }
    }
}

