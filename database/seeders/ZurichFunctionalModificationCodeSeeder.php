<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichFunctionalModificationCode;


class ZurichFunctionalModificationCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //code  desc


        if (!ZurichFunctionalModificationCode::where('code', '0')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '0',
                'desc'    => 'None'
            ]);
        }

        if (!ZurichFunctionalModificationCode::where('code', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '2',
                'desc'    => 'Sunroof, Moonroof & Canvas top'
            ]);
        }

        if (!ZurichFunctionalModificationCode::where('code', '4')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '4',
                'desc'    => 'Satellite Navigation System, Car Phone Kit'
            ]);
        }

        if (!ZurichFunctionalModificationCode::where('code', '8')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '8',
                'desc'    => 'Parking Sensors'
            ]);
        }

        if (!ZurichFunctionalModificationCode::where('code', '16')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '16',
                'desc'    => 'Accessibility Modifications'
            ]);
        }

        if (!ZurichFunctionalModificationCode::where('code', '32')->exists()) {
            // Create the user only if it doesn't exist
            ZurichFunctionalModificationCode::create([
                'code'    => '32',
                'desc'    => 'LPG Conversion'
            ]);
        }


    }
}
