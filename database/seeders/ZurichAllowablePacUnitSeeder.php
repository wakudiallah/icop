<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowablePacUnit;

class ZurichAllowablePacUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if (!ZurichAllowablePacUnit::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePacUnit::create([
                'no'    => '1',
                'pac_unit'    => '1'
            ]);
        }


        if (!ZurichAllowablePacUnit::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePacUnit::create([
                'no'    => '2',
                'pac_unit'    => '2'
            ]);
        }


        if (!ZurichAllowablePacUnit::where('no', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePacUnit::create([
                'no'    => '3',
                'pac_unit'    => '3'
            ]);
        }

        if (!ZurichAllowablePacUnit::where('no', '4')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePacUnit::create([
                'no'    => '4',
                'pac_unit'    => '4'
            ]);
        }


        if (!ZurichAllowablePacUnit::where('no', '5')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowablePacUnit::create([
                'no'    => '5',
                'pac_unit'    => '5'
            ]);
        }


    }
}
