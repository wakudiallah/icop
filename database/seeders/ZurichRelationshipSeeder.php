<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichRelationship;

class ZurichRelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $relationships = [
            ['code' => '1', 'desc' => 'PARENT/PARENT-IN-LAW'],
            ['code' => '2', 'desc' => 'SPOUSE'],
            ['code' => '3', 'desc' => 'CHILD'],
            ['code' => '4', 'desc' => 'SIBLING/SIBLING-IN- LAW/COUSIN/RELATIVE (AUNT, UNCLE, NIECE, NEPHEW & ETC.)'],
            ['code' => '5', 'desc' => 'FRIEND/CO-WORKER'],
        ];

        foreach ($relationships as $relationship) {
            if (!ZurichRelationship::where('code', $relationship['code'])->exists()) {
                ZurichRelationship::create($relationship);
            }
        }
    }
}
