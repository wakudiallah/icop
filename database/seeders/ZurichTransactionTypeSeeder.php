<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichTransactionType;

class ZurichTransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $transactionTypes = [
            ['code' => 'B', 'desc' => 'New Business/Renewal'],
            ['code' => 'V', 'desc' => 'New Vehicle (New registered vehicle)'],
        ];

        foreach ($transactionTypes as $transactionType) {
            if (!ZurichTransactionType::where('code', $transactionType['code'])->exists()) {
                ZurichTransactionType::create($transactionType);
            }
        }
        
    }
}
