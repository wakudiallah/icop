<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichVehicleRegisterLocation;

class ZurichVehicleRegisterLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $registerLocations = [
            ['code' => 'L', 'desc' => 'LOCAL REGISTRATION'],
            ['code' => 'O', 'desc' => 'OTHER FOREIGN REGISTRATION'],
        ];

        foreach ($registerLocations as $location) {
            if (!ZurichVehicleRegisterLocation::where('code', $location['code'])->exists()) {
                ZurichVehicleRegisterLocation::create($location);
            }
        }
        
    }
}
