<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichVehicleClass;

class ZurichVehicleClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vehicleClasses = [
            ['code' => '01', 'desc' => 'Motorcycle'],
            ['code' => '02', 'desc' => 'Private Car'],
        ];

        foreach ($vehicleClasses as $vehicleClass) {
            ZurichVehicleClass::create([
                'code' => $vehicleClass['code'],
                'desc' => $vehicleClass['desc'],
            ]);
        }
        
    }
}
