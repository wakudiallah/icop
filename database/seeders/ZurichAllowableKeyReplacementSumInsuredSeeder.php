<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowableKeyReplacementSumInsured;



class ZurichAllowableKeyReplacementSumInsuredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (!ZurichAllowableKeyReplacementSumInsured::where('no', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableKeyReplacementSumInsured::create([
                'no'    => '1',
                'key_replacement_sum_insured'    => '1000'
            ]);
        }


        if (!ZurichAllowableKeyReplacementSumInsured::where('no', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableKeyReplacementSumInsured::create([
                'no'    => '2',
                'key_replacement_sum_insured'    => '2000'
            ]);
        }


        if (!ZurichAllowableKeyReplacementSumInsured::where('no', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableKeyReplacementSumInsured::create([
                'no'    => '3',
                'key_replacement_sum_insured'    => '3000'
            ]);
        }





    }
}
