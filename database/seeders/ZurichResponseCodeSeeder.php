<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichResponseCode;

class ZurichResponseCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $responseCodes = [
            ['respCode' => '000', 'respMessage' => 'Success'],
            ['respCode' => '002', 'respMessage' => 'Unauthorized'],
            ['respCode' => '004', 'respMessage' => 'Data Insertion Failed'],
            ['respCode' => '007', 'respMessage' => 'Invalid Quotation number'],
            ['respCode' => '006', 'respMessage' => 'Quotation Validity Expired'],
            ['respCode' => '077', 'respMessage' => 'No record found'],
            ['respCode' => '099', 'respMessage' => 'System error, please contact IT support'],
            ['respCode' => '003', 'respMessage' => 'Mandatory data missing'],
            ['respCode' => '009', 'respMessage' => 'Please check the input table'],
            ['respCode' => '010', 'respMessage' => 'Expiry date should not be greater than two years'],
            ['respCode' => '011', 'respMessage' => 'Expiry date should be greater than Effective date'],
            ['respCode' => '012', 'respMessage' => 'Effective date should be today or future date'],
            ['respCode' => '013', 'respMessage' => 'Error! Basic details not available to do Validation'],
            ['respCode' => '014', 'respMessage' => 'Please send new IC in right format'],
            ['respCode' => '001', 'respMessage' => 'Covernote issued email sent failed'],
            ['respCode' => '015', 'respMessage' => 'Quotation is Referred'],
            ['respCode' => '016', 'respMessage' => 'Cover Note is already generated'],
            ['respCode' => '017', 'respMessage' => 'Quotation is declined'],
            ['respCode' => '018', 'respMessage' => 'Please send the Payment Details table'],
            ['respCode' => '019', 'respMessage' => 'Please make sure the payment amount is correct'],
            ['respCode' => '020', 'respMessage' => 'Quotation number is empty'],
            ['respCode' => '021', 'respMessage' => 'Please send the right search type'],
        ];

        foreach ($responseCodes as $responseCode) {
            if (!ZurichResponseCode::where('respCode', $responseCode['respCode'])->exists()) {
                ZurichResponseCode::create($responseCode);
            }
        }
    }
}
