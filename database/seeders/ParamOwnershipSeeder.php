<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamOwnershiplkm;

class ParamOwnershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (!ParamOwnershiplkm::where('ownership', 'Saloon Cars / Induvidual Owned')->exists()) {
            // Create the user only if it doesn't exist
            ParamOwnershiplkm::create([
                'ownership'    => 'Saloon Cars / Induvidual Owned',
                'status' => '1',
            ]);
        }

        if (!ParamOwnershiplkm::where('ownership', 'Saloon Cars / Company Owned')->exists()) {
            // Create the user only if it doesn't exist
            ParamOwnershiplkm::create([
                'ownership'    => 'Saloon Cars / Company Owned',
                'status' => '1',
            ]);
        }

        if (!ParamOwnershiplkm::where('ownership', 'Non-saloon Cars / Individual or Company Owned')->exists()) {
            // Create the user only if it doesn't exist
            ParamOwnershiplkm::create([
                'ownership'    => 'Non-saloon Cars / Individual or Company Owned',
                'status' => '1',
            ]);
        }

        if (!ParamOwnershiplkm::where('ownership', 'Motorcycle')->exists()) {
            // Create the user only if it doesn't exist
            ParamOwnershiplkm::create([
                'ownership'    => 'Motorcycle',
                'status' => '1',
            ]);
        }


    }


}
