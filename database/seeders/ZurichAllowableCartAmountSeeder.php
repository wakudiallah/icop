<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichAllowableCartAmount;

class ZurichAllowableCartAmountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (!ZurichAllowableCartAmount::where('cart_ammount', '50')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartAmount::create([
                'cart_ammount'    => '50'
            ]);
        }

        if (!ZurichAllowableCartAmount::where('cart_ammount', '100')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartAmount::create([
                'cart_ammount'    => '100'
            ]);
        }


        if (!ZurichAllowableCartAmount::where('cart_ammount', '500')->exists()) {
            // Create the user only if it doesn't exist
            ZurichAllowableCartAmount::create([
                'cart_ammount'    => '200'
            ]);
        }
    }
}
