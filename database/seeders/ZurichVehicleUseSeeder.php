<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichVehicleUse;

class ZurichVehicleUseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //zurich_vehicle_uses
        if (!ZurichVehicleUse::where('vehicle_use_code', '11')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '11',
                'vehicle_use_description'    => 'Private Use',
                'vehicle_class_code'    => '01 - Motorcycle',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '12')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '12',
                'vehicle_use_description'    => 'Business Use',
                'vehicle_class_code'    => '01 - Motorcycle',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '62')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '62',
                'vehicle_use_description'    => 'Motorcycle Trade',
                'vehicle_class_code'    => '01 - Motorcycle',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '13')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '13',
                'vehicle_use_description'    => 'Tuition Use',
                'vehicle_class_code'    => '01 - Motorcycle',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '1')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '1',
                'vehicle_use_description'    => 'Private Use (Drive to Work / Daily use)',
                'vehicle_class_code'    => '02 - Private Car',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '4')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '4',
                'vehicle_use_description'    => 'Private Use (Use Public Transport to Work / Weekend Use)',
                'vehicle_class_code'    => '02 - Private Car',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '2')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '2',
                'vehicle_use_description'    => 'Business Use',
                'vehicle_class_code'    => '02 - Private Car',
            ]);
        }


        if (!ZurichVehicleUse::where('vehicle_use_code', '3')->exists()) {
            // Create the user only if it doesn't exist
            ZurichVehicleUse::create([
                'vehicle_use_code'    => '3',
                'vehicle_use_description'    => 'Tuition Use',
                'vehicle_class_code'    => '02 - Private Car',
            ]);
        }





    }
}
