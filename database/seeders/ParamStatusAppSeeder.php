<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ParamStatusApplication;

class ParamStatusAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
               
        if (!ParamStatusApplication::where('code_status_app', 'NW')->exists()) {
            // Create the user only if it doesn't exist
            ParamStatusApplication::create([
                'code_status_app'    => 'NW',
                'status_app'    => 'New',
                'label'    => 'success',
                'status' => 1,
            ]);
        }

               
        if (!ParamStatusApplication::where('code_status_app', 'AA')->exists()) {
            // Create the user only if it doesn't exist
            ParamStatusApplication::create([
                'code_status_app'    => 'AA',
                'status_app'    => 'Approved',
                'label'    => 'primary',
                'status' => 1,
            ]);
        }
        
        
        if (!ParamStatusApplication::where('code_status_app', 'RJ')->exists()) {
            // Create the user only if it doesn't exist
            ParamStatusApplication::create([
                'code_status_app'    => 'RJ',
                'status_app'    => 'Rejected',
                'label'    => 'danger',
                'status' => 1,
            ]);
        }

    }
}
