<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichOccupation;

class ZurichOccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $occupations = [
            ['code' => '01', 'desc' => 'OFFICE WORKERS'],
            ['code' => '03', 'desc' => 'SALES OR SERVICE PERSONNEL (NOT IN OFFICE)'],
            ['code' => '04', 'desc' => 'OTHER PROFESSIONAL (DOCTORS, ACCOUNTANT, ETC)'],
            ['code' => '05', 'desc' => 'UNEMPLOYED'],
            ['code' => '06', 'desc' => 'HOMEMAKERS'],
            ['code' => '07', 'desc' => 'PROFESSIONAL DRIVERS'],
            ['code' => '08', 'desc' => 'STUDENT'],
            ['code' => '09', 'desc' => 'RETIRED PERSONS'],
            ['code' => '10', 'desc' => 'UNIFORMED PERSONNEL'],
            ['code' => '99', 'desc' => 'OTHERS'],
        ];

        foreach ($occupations as $occupation) {
            if (!ZurichOccupation::where('code', $occupation['code'])->exists()) {
                ZurichOccupation::create($occupation);
            }
        }
        
    }
}
