<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichPerformanceAndAestheticModification;


class ZurichPerformanceAndAestheticModificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $modifications = [
            ['code' => '1', 'desc' => 'None'],
            ['code' => '2', 'desc' => 'Tinted Windows'],
            ['code' => '4', 'desc' => 'Spoilers, Skirts, Valances, Bonnet Bulges, Flared Wings & Wheel Arches'],
            ['code' => '8', 'desc' => 'Lights Addition or Changes'],
            ['code' => '16', 'desc' => 'Complete Body kit & Panels (that changes the vehicle\'s identity)'],
            ['code' => '32', 'desc' => 'Brakes & Suspension'],
            ['code' => '64', 'desc' => 'Turbo/ Supercharging or Nitrous Oxide'],
            ['code' => '128', 'desc' => 'Transmission or Gearing Change'],
            ['code' => '256', 'desc' => 'Exhaust System Changes, Air Filter'],
            ['code' => '512', 'desc' => 'Engine Change, Alteration or Tuning'],
            ['code' => '1024', 'desc' => 'Roll Bars, Roll Cages, Removal of Seats'],
            ['code' => '2048', 'desc' => 'Replacement Seats, Steering Wheels, Pedals, Upholstery, Dashboard Changes'],
            ['code' => '4096', 'desc' => 'Paint Work'],
            ['code' => '8192', 'desc' => 'Wider Wheels or Tyres'],
        ];

        foreach ($modifications as $modification) {
            if (!ZurichPerformanceAndAestheticModification::where('code', $modification['code'])->exists()) {
                ZurichPerformanceAndAestheticModification::create($modification);
            }
        }


    }
}
