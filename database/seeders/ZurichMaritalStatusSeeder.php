<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichMaritalStatus;

class ZurichMaritalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $maritalStatuses = [
            ['code' => 'D', 'desc' => 'Divorced'],
            ['code' => 'M', 'desc' => 'Married'],
            ['code' => 'S', 'desc' => 'Single'],
            ['code' => 'W', 'desc' => 'Widowed'],
        ];

        foreach ($maritalStatuses as $status) {
            if (!ZurichMaritalStatus::where('code', $status['code'])->exists()) {
                ZurichMaritalStatus::create($status);
            }
        }
    }
}
