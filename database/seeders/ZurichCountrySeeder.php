<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichCountry;

class ZurichCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $countries = [
            ['code' => 'MAS', 'desc' => 'MALAYSIA'],
            ['code' => 'SIN', 'desc' => 'SINGAPORE'],
            ['code' => 'THA', 'desc' => 'THAILAND'],
        ];

        foreach ($countries as $country) {
            if (!ZurichCountry::where('code', $country['code'])->exists()) {
                ZurichCountry::create($country);
            }
        }
        
    }
}
