<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichRegionOfUsed;

class ZurichRegionOfUsedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $regions = [
            ['code' => 'E', 'desc' => 'EAST MALAYSIA'],
            ['code' => 'W', 'desc' => 'WEST MALAYSIA'],
        ];

        foreach ($regions as $region) {
            if (!ZurichRegionOfUsed::where('code', $region['code'])->exists()) {
                ZurichRegionOfUsed::create($region);
            }
        }
        
    }
}
