<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichIsmResponseCode;

class ZurichISMResponseCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $ism = [
            ['response_code' => '000', 'desc' => 'OK for successful NCD confirmation/Enquiry'],
            ['response_code' => '001', 'desc' => 'NCD confirmation is unavailable as confirmation already been taken earlier. Make sure no duplicate confirmation request, if not the confirmation already taken by another insurance / Takaful company'],
            ['response_code' => '002', 'desc' => 'Cancellation is unavailable as record is still available for confirmation. Make sure not duplicate cancellation or no confirmation is taken earlier'],
            ['response_code' => '003', 'desc' => 'Error in policy effect date or policy effect date is not match. Make sure policy effect date reported is correct'],
            ['response_code' => '004', 'desc' => 'Error in policy expiry date or policy expiry date is not match. Make sure expiry date is correct'],
            ['response_code' => '005', 'desc' => 'Error in NCDCFY. NCD value is not match. Make sure NCD is correct'],
            ['response_code' => '006', 'desc' => 'Request ID is not match. Make sure Request ID is correct or insurance / Takaful response_code is correct'],
            ['response_code' => '007', 'desc' => 'Response ID is not match. Make sure Response ID or the responding insurance / Takaful response_code is correct.'],
            ['response_code' => '008', 'desc' => 'Error in ID No1 or ID No2 or ID not matches. Make sure Id No1 or Id No2 is correct or Insurance / Takaful response_code reported is correct.'],
            ['response_code' => '009', 'desc' => 'Error in Vehicle registration number not matches. Make sure vehicle reg no is correct or insurance / Takaful response_code reported is correct'],
            ['response_code' => '010', 'desc' => 'Error in policy number field or policy number not matches. Make sure policy number is correct or the insurance/Takaful code reported is correct'],
            ['response_code' => '011', 'desc' => 'Error in insurance/Takaful field or Insurance / Takaful company code is not available. Please check whether insurance/Takaful code reported is correctly'],
            ['response_code' => '012', 'desc' => 'Signature not match. Check whether the correct key is used to sign.'],
            ['response_code' => '013', 'desc' => 'Encryption error, make sure the correct key is used to encrypt'],
            ['response_code' => '014', 'desc' => 'No NCD granted. Claims affecting NCD'],
            ['response_code' => '015', 'desc' => 'Late Claims Submitted After NCD Confirmation'],
            ['response_code' => '016', 'desc' => 'OK for Cancellation of Previous Confirmed NCD by Requesting Co'],
            ['response_code' => '017', 'desc' => 'OK for NCD confirmation. Policy currently is in Cancelled status'],
            ['response_code' => '018', 'desc' => 'Mismatch com-code from cancellation of taken NCD request'],
            ['response_code' => '019', 'desc' => 'Error in Engine No field. Make sure the engine no is correct or the Insurance/Takaful code reported is correct'],
            ['response_code' => '020', 'desc' => 'Error in Chassis No field or Chassis No is not match. Make sure the Chassis No is correct or the Insurance/Takaful code reported is correct'],
            ['response_code' => '021', 'desc' => 'Error in NCD effect date.'],
            ['response_code' => '022', 'desc' => 'Error in Next NCD Level'],
            ['response_code' => '023', 'desc' => 'Error in Next NCD Effective Date'],
            ['response_code' => '024', 'desc' => 'Error in Cover Type'],
            ['response_code' => '025', 'desc' => 'Error in vehicle class'],
            ['response_code' => '026', 'desc' => 'Error in insured name'],
            ['response_code' => '027', 'desc' => 'Error in Gross Premium'],
            ['response_code' => '028', 'desc' => 'Error in doc type'],
            ['response_code' => '029', 'desc' => 'Error in reason type'],
            ['response_code' => '030', 'desc' => 'OK for successful inserting new NCD record'],
            ['response_code' => '031', 'desc' => 'Overlapping of Period of Insurance between old policy and new policy.'],
            ['response_code' => '032', 'desc' => 'Error in previous insurance/Takaful field or previous Insurance / Takaful company code is not available. Please check whether insurance/Takaful is correctly'],
            ['response_code' => '033', 'desc' => 'Error on Ref No'],
            ['response_code' => '034', 'desc' => 'Error in previous Vehicle registration number not matches. Make sure previous vehicle regno is correct or insurance / Takaful is correct'],
            ['response_code' => '035', 'desc' => 'Error in previous policy number field or previous policy number not matches. Make sure previous policy number is correct or whether the insurance/Takaful is correct'],
            ['response_code' => '036', 'desc' => 'NCD already withdrawn. Withdraw of NCD is impossible. Make sure the correct vehicle number is provided'],
            ['response_code' => '037', 'desc' => 'Inserting new NCD record failed. Because NCD already confirmed by other insurance/ Takaful company'],
            ['response_code' => '038', 'desc' => 'Inserting new NCD record failed. Because NCD record already renewed'],
            ['response_code' => '039', 'desc' => 'Error on Accident date.'],
            ['response_code' => '040', 'desc' => 'Error on claim type.'],
            ['response_code' => '041', 'desc' => 'Claw back'],
            ['response_code' => '042', 'desc' => 'Claim notified. Policy currently is in cancelled status'],
            ['response_code' => '043', 'desc' => 'Period Of Insurance not matches'],
            ['response_code' => '044', 'desc' => 'OK for successful updating of new NCD record'],
            ['response_code' => '045', 'desc' => 'Fail to reinstate as policy is not cancelled'],
            ['response_code' => '047', 'desc' => 'Cancel policy impossible. Policy already cancelled'],
            ['response_code' => '048', 'desc' => 'Cancel policy successful'],
            ['response_code' => '049', 'desc' => 'Updating NCD record failed. NCD already withdrawn'],
            ['response_code' => '050', 'desc' => 'Withdrawn record not found. Cancel withdraw impossible'],
            ['response_code' => '051', 'desc' => 'Cancel withdrawn NCD successful.'],
            ['response_code' => '052', 'desc' => 'Error on NCD Change Date'],
            ['response_code' => '053', 'desc' => 'Data not completed as some of the fields were missing. Make sure to allocate space for those blank values'],
            ['response_code' => '054', 'desc' => 'Duplicated data'],
            ['response_code' => '055', 'desc' => 'Error on Previous Approval Code.'],
            ['response_code' => '056', 'desc' => 'Error on Previous Reference Number'],
            ['response_code' => '057', 'desc' => 'Withdrawn NCD successful'],
            ['response_code' => '058', 'desc' => 'Error on vehicle use code'],
            ['response_code' => '059', 'desc' => 'Cancel claim impossible. Due to Claims not found / vehicle has been updated to JPJ as Actual Total'],
            ['response_code' => '060', 'desc' => 'Cancel claims successful.'],
            ['response_code' => '061', 'desc' => 'Cancel claims fail. Claims already been cancelled early.'],
            ['response_code' => '062', 'desc' => 'New claim reported.'],
            ['response_code' => '063', 'desc' => 'Revise claim reported.'],
            ['response_code' => '064', 'desc' => 'Cancellation for Previous Claw back.'],
            ['response_code' => '065', 'desc' => 'Missing setting in CFY map to NCD.'],
            ['response_code' => '066', 'desc' => 'Update Claw back action successfully.'],
            ['response_code' => '067', 'desc' => 'OK for NCD confirmation/enquiry, Claw back action not been taken yet. Please check with previous insurance/Takaful.'],
            ['response_code' => '068', 'desc' => 'Withdraw of NCD is unavailable due to policy expired.'],
            ['response_code' => '069', 'desc' => 'Error on CFY. Provided CFY not in sequence.'],
            ['response_code' => '070', 'desc' => 'Withdraw NCD unavailable due to policy renewed.'],
            ['response_code' => '071', 'desc' => 'Error on NXTCFY. Provided NXTCFY not in sequence.'],
            ['response_code' => '072', 'desc' => 'Reject due to no record found.'],
            ['response_code' => '073', 'desc' => 'Cancel withdrawn NCD fail. It already been cancelled early.'],
            ['response_code' => '074', 'desc' => 'Fail to update existing record as policy currently in cancelled status.'],
            ['response_code' => '075', 'desc' => 'Withdraw NCD is unavailable as confirmation already been taken earlier.'],
            ['response_code' => '076', 'desc' => 'Cancel withdrawn NCD fail as it been confirmed earlier.'],
            ['response_code' => '077', 'desc' => 'Data not found.'],
            ['response_code' => '078', 'desc' => 'OK for successful NCD confirmation/Enquiry. Confirming Company Data not Updated for more than 5 Days'],
            ['response_code' => '079', 'desc' => 'No NCD granted. Claims affecting NCD. Confirming Company Data not Updated for more than 5 Days'],
            ['response_code' => '080', 'desc' => 'OK for NCD confirmation. Policy currently is in Cancelled status. Confirming Company Data not Updated for more than 5 Days'],
            ['response_code' => '081', 'desc' => 'Policy accepted with chassis number different from previous policy'],
            ['response_code' => '082', 'desc' => 'No NCD Granted. NCD have been withdrawn.'],
            ['response_code' => '083', 'desc' => 'Error on year of manufactured. [Applicable to ABI Sum Insured Only]'],
            ['response_code' => '084', 'desc' => 'Error on vehicle make.'],
            ['response_code' => '085', 'desc' => 'Error on vehicle model.'],
            ['response_code' => '086', 'desc' => 'Error on vehicle capacity.'],
            ['response_code' => '087', 'desc' => 'Error on vehicle NVIC.'],
            ['response_code' => '088', 'desc' => 'Successful market valuation enquiry. [Applicable to ABI Sum Insured Only]'],
            ['response_code' => '089', 'desc' => 'OK for successful NCD confirmation/Enquiry. CFY Confirmed is for <dd/mm/yyyy>.'],
            ['response_code' => '090', 'desc' => 'Previous vehicle number reported as Total Loss.'],
            ['response_code' => '091', 'desc' => 'NCD not granted due to a Theft claim affecting the NCD'],
            ['response_code' => '092', 'desc' => 'Stolen vehicle recovered and NCD is granted.'],
            ['response_code' => '093', 'desc' => 'Stolen vehicle recovered and NCD is not granted.'],
            ['response_code' => '094', 'desc' => 'Confirmation failed as vehicle chassis number reported as Total Loss'],
            ['response_code' => '095', 'desc' => 'Confirmation failed as vehicle chassis number reported as Stolen'],
            ['response_code' => '096', 'desc' => 'NCD Granted. New car previously reported as Stolen but subsequently recovered'],
            ['response_code' => '097', 'desc' => 'Error in Loss State Code. Make sure Loss State Code reported is correct'],
            ['response_code' => '098', 'desc' => 'Policy Record Not Found, Claims pending to be upload. Make sure Policy record submitted'],
            ['response_code' => '099', 'desc' => 'Confirmation Failed – Vehicle Class cannot be transferred.']
        ];

        foreach ($ism as $ism) {
            // Check if record exists before inserting
            if (!ZurichIsmResponseCode::where('response_code', $ism['response_code'])->exists()) {
                ZurichIsmResponseCode::create($ism);
            }
        }

    }
}
