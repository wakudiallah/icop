<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();   RoleAndPermissionSeeder

        $this->call([
            ParamEmailingSeeder::class,
            ParamInsuranceCompanySeeder::class,
            ParamInsurancePolicySeeder::class,
            ParamInsurancePromoSeeder::class,
            ParamOwnershipSeeder::class,
            ParamProtectionTypeSeeder::class,
            ParamRegionSeeder::class,
            ParamRoadtaxAmountSeeder::class,
            ParamStatusAppSeeder::class,
            PermissionSeeder::class,
            PostcodeD2Seeder::class,
            PostcodeSeeder::class,
            RoleAndPermissionSeeder::class,
            RoleSeeder::class,
            SetupBpChargeSeeder::class,
            SetupZurichCartSeeder::class,
            StateSeeder::class,
            ZurichAllowableCartAmountSeeder::class,
            ZurichAllowableCartDaySeeder::class,
            ZurichAllowableCourtesyCarDaySeeder::class,
            ZurichAllowableKeyReplacementSumInsuredSeeder::class,
            ZurichAllowablePaBasicUnitSeeder::class,
            ZurichAllowablePaCUnitSeeder::class,
            ZurichCiCodeSeeder::class,
            ZurichCountrySeeder::class,
            ZurichCoverTypeSeeder::class,
            ZurichErrorSeeder::class,
            ZurichExcessSeeder::class,
            ZurichFunctionalModificationCodeSeeder::class,
            ZurichISMResponseCodeSeeder::class,
            ZurichMaritalStatusSeeder::class,
            ZurichNationalityAndCodeSeeder::class,
            ZurichNcdStatusSeeder::class,
            ZurichOccupationSeeder::class,
            ZurichPerformanceAndAestheticModificationCodeSeeder::class,
            ZurichProductCodeCoverTypeCiCodeSeeder::class,
            ZurichProductCodeSeeder::class,
            ZurichReferralSeeder::class,
            ZurichRegionOfUsedSeeder::class,
            ZurichRelationshipSeeder::class,
            ZurichResponseCodeSeeder::class,
            ZurichStateSeeder::class,
            ZurichTransactionTypeSeeder::class,
            ZurichTypeSeeder::class,
            ZurichUomSeeder::class,
            ZurichVehicleBodyTypeSeeder::class,
            ZurichVehicleClassSeeder::class,
            ZurichVehicleMakeSeeder::class,
            ZurichVehicleRegisterLocationSeeder::class,
            ZurichVehicleUseSeeder::class,
            ZurichVoluntaryExcessSeeder::class,
        ]);

    }
}
