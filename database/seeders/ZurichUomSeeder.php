<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZurichUom;

class ZurichUomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $uoms = [
            ['code' => 'CC', 'desc' => 'CUBIC CAPACITY'],
            ['code' => 'HP', 'desc' => 'HORSE POWER'],
            ['code' => 'PS', 'desc' => 'PASSENGER'],
            ['code' => 'TON', 'desc' => 'TONNAGE'],
            ['code' => 'TP', 'desc' => 'TRADE PLATE'],
            ['code' => 'WT', 'desc' => 'WATT'],
            ['code' => 'KG', 'desc' => 'KILOGRAMS'],
        ];

        foreach ($uoms as $uom) {
            if (!ZurichUom::where('code', $uom['code'])->exists()) {
                ZurichUom::create($uom);
            }
        }
        
    }
}
