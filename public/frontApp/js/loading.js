// Simulate content loading
document.addEventListener("DOMContentLoaded", function() {
    // Show loading screen
    const loadingScreen = document.getElementById('loading-screen');
    loadingScreen.style.display = 'flex';

    // Simulate content loading
    setTimeout(function() {
        // Hide loading screen
        loadingScreen.style.display = 'none';

        // Show content
        const content = document.getElementById('content');
        content.style.display = 'block';
    }, 3000); // Change 3000 to the time your content takes to load in milliseconds
});
