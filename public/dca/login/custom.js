
$(function() {
	$("li.dropdown-submenu a").click(function(event) {
		if ($("ul.dropdown-menu.open").length > 0){
			$("ul.dropdown-menu.open").removeClass("open");
			$("li.dropdown-submenu>a.expand").removeClass("expand");
		}
		
		$(this).next("ul.dropdown-menu").addClass( "open" );
		$(this).addClass( "expand" );
		var parent = $(this);
		var target = $(this).next("ul.dropdown-menu");
		
		$("body").unbind("click");
		$("body").bind("click", function(e) {
			target.removeClass("open");
			parent.removeClass("expand");
			$("body").unbind("click");
		});
		event.stopPropagation();
	});
	
	//set up global ajax timeout.
	//$.ajaxSetup({
  	//	timeout: 15000
	//});
  //  $(".accordion").multiaccordion();
	
	$(".numbers").keypress(function(e) {
		var c='';
		if($.browser.msie) c=e.keyCode;
		else c=e.charCode;
		if (c==0 || c==118) {return true;}
		return !isNaN(String.fromCharCode(c));

	} );
	
	// cheeyung modify all button alignment problem
	$(':button, :submit, :reset').parent('span').each(function(){
		if ($(this).children().length == $(this).children(':button, :submit, :reset').length){
			$(this).css({"text-align":"center","float":"none"});
		}
	});
	
	try{
		$( ".accordion" ).accordion({
			animate: 150,
			heightStyle: "content",
			icons: false
		});
		
	    $(".accordion").multiaccordion();
	    
	}catch(e){}

	
	$('input[type=text]').bind("keydown", function(event){
	      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
	      return !(keycode == 13)
	});
	$('input[type=radio]').bind("keydown", function(event){
	      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
	      return !(keycode == 13)
	});
	$('input[type=checkbox]').bind("keydown", function(event){
	      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
	      return !(keycode == 13) 
	});
	
	// check users' browser, to support css input field on IE
	//Fix IE8 bug that will throw JS error.
	try{
	if($.browser.msie) {
		//$('select').addClass('wide');
		$('select').addClass('iefix');
		//change by thaihau, ONLY focus type=text 
		$('input[type=text]').focus(
			function() {
				var id = $(this).attr("id");
				var edit = 0;
				if(id==null || id=="") {
					var somerandom = Math.random().toString();
					somerandom = somerandom.substring(2,somerandom.length);
					id = "tempobj"+somerandom;
					$(this).attr("id",id);
					edit = 1;
				}
				var thisobject = document.getElementById(id);
				try{ thisobject.select();}catch(e){}
			}
		);
	}
	}catch(e){}
	

	
	// check users' browser, to support css button on Opera
	//if($.browser.opera) {
		//$('.button-rounded').addClass('wrapped').wrap('<span class="button-opera"></span>');
		//$('span.button-opera').append('<i class="right"></i>');
	//}
 	
	// disable browser autocomplete 
	$('form').attr('autocomplete', 'off');

	// set id same as name so that label can focus on it.
	$(':text, :password, :file, textarea, select').each(function() {
		if (this.id.length == 0)
			this.id = this.name;
	});

	// toggle checkbox checked/unchecked on grid table. 
	$(':checkbox[name=toggle], :checkbox.toggle').click(function() {
		var checked = this.checked;
		$(this.form).find('input[name=ids]').each(function() {
			if( this.type == 'checkbox' ) this.checked = checked;
		});
	});
	
	// toggle checkbox checked/unchecked on grid table. 
	$('input[name=toggleIntId]').click(function() {
		var checked = this.checked;
		$(this.form).find('input[name=serviceIds]').each(function() {
			this.checked = checked;
		});
	});
	
	//Toggle check box for parent child start (new version 20090618, reduce frm 2 function to 1 )
	$('input[name=serviceIds]').click(function() {
		var value = this.value;
		var check = this.checked;
		var parentId = $(this).attr('parent');
		var parentParentId = $('input[name=serviceIds][value=' + parentId + ']').attr('parent');
			
		if( this.checked )
		{
			//Check parent if child is checked
			$(this.form).find('input[name=serviceIds]').each(function() {
				
				if(this.value == parentId ){
					
					this.checked = true;
					
					var grandParentId = $(this).attr('parent');
					
					if ( grandParentId != '' ) {
						$(this.form).find('input[name=serviceIds]').each(function() {
							if(this.value == grandParentId ){
								this.checked = true;
							}
						});
					}					
				}
			});
		}
			
		//Check / uncheck child if parent is toggled
		$(this.form).find('input[name=serviceIds]').each(function() {
			if( value == $(this).attr('parent') ){
				 this.checked = check;
			}
			
		}); 
		
		
		//uncheck parent if no child is not checked
		var parentList = new Array();
		var childList = new Array();
		var p_count = 0;
		var c_count = 0;

		$(this.form).find('input[name=serviceIds]').each(function() {
			if(this.checked == true){
				
				if($(this).attr('parent') == ""){
					parentList[p_count] = this.value;
					p_count = p_count + 1; 
				}else{
					childList[c_count] = $(this).attr('parent')+"-"+this.value;
					c_count = c_count + 1;
				}
			}
		});

//		if(childList.length == 0){
//			$('input[name=serviceIds]').attr('checked','');
//		}

		$(this.form).find('input[name=serviceIds]').each(function() {
			if(this.value == parentId ){
				this.checked = false;
			}
		});
		
		for(i = 0; i < childList.length; i++){
			var parentz = childList[i].split("-");
			
			if(parentId == parentz[0]){
							
				$(this.form).find('input[name=serviceIds]').each(function() {
					if(this.value == parentId ){
						this.checked = true;
					}
				});
				
			}			
		}
		
		// level 2
		var parentList = new Array();
		var childList = new Array();
		var p_count = 0;
		var c_count = 0;
		$(this.form).find('input[name=serviceIds]').each(function() {
			if(this.checked == true){
				if($(this).attr('parent') == ""){
					parentList[p_count] = this.value;
					p_count = p_count + 1; 
				}else{
					childList[c_count] = $(this).attr('parent')+"-"+this.value;
					c_count = c_count + 1;
				}
			}
		});

		if (parentParentId) {
			
			$(this.form).find('input[name=serviceIds]').each(function() {
				if(this.value == parentParentId ){
					this.checked = false;
				}
			});
			
			for(i = 0; i < childList.length; i++){
				var parentz = childList[i].split("-");
				if(parentParentId == parentz[0]){
					
					$(this.form).find('input[name=serviceIds]').each(function() {
						if(this.value == parentParentId ){
							this.checked = true;
						}
					});
					
				}			
			}
		}
		// end level 2
			
	});
	//Toggle check box for parent child end
		
	
	// disable onSubmit function when call cancel method.
	$(':input[name=cancel]').click(function() {
	    $(this.form).attr('onsubmit', '')
			.find(':input').not(this).attr('disabled', true);
	});

	// prompt message when click on delete button.
	$(':input[name=delete]').click(function() {
		if (confirm($.messages.confirmDelete)) {
		    $(this.form).attr('onsubmit', '');
			return true;
	  	}
	  	return false;
	});

	//Prevent multiple submit
	$(':input[name=search], :input[name=resubmit], :input.noDuplicate').click(function() {
		if ($(this.form).valid()){
			$(this.form).find(':input[type=image]').each(function() {
				$(this).attr('disabled', 'disabled');
			});
			var url = $('form').attr('action').replace('?','?'+$(this).attr('name')+'=&');
			$(this.form).attr('action', url);
			$(this.form).submit();
			//Is not work for IE9
//			var input = document.getElementsByName($(this).attr('name'))[0];
//			var newIn = document.createElement('input');
//			newIn.setAttribute("type", 'submit');
//			newIn.setAttribute("name", $(this).attr('name'));
//			newIn.setAttribute("value", $(this).attr('name'));
//			$(newIn).css('display', 'none');
//			document.forms[0].appendChild(newIn);
//			$(newIn).click();
		}
	});

	// disable toggle checkbox if no "Delete all" or functin end with "All" function
	/**
	$(':checkbox[name=toggle]').each(function() {
		var form = $(this.form);
		if (form.find(':input[name$=All]').length == 0) {
			form.find(':checkbox[name=ids]').attr('disabled', 'disabled');
			$(this).attr('disabled', 'disabled');
		}
	});
	**/

	// prompt message when click on deleteAll or functin end with "All"  button.
	$(':input[name$=All]').each(function() {
		var form = $(this.form);

		if (form.find(':checkbox[name=ids]:enabled').length == 0) {
			form.find(':checkbox[name=toggle]').attr('disabled', 'disabled');
			$(this).attr('disabled', 'disabled');
		} else {		
			var container = $('<div class="errors" />');
			form.prepend(container).validate({
				 errorLabelContainer: container,
				 onsubmit : false,
				 onclick : false,
				 rules : { ids : "required" },
				 messages : {ids : $.messages.minSelectedError }
			});
			container.hide();
		}
	}).click(function() {
		//hide all errors div
		$(this.form).find('.errors').hide();
		
		//hide all Messages div
		$(this.form).find('.messages').hide();
		
		var action=$(this).attr('name');

		//if ($(':checkbox[name=ids]').valid()) {
		if ($(':input[name=ids]:checked').length){
				switch (action) {
				case 'deleteAll':
					//if( confirm($.messages.confirmDeleteAll))
					return true;
					break;
				case 'approveAll':
					if( confirm($.messages.confirmApproveAll))
					return true;
					break;	
				case 'rejectAll':
					if( confirm($.messages.confirmRejectAll))
					return true;
					break;	
				case 'returnAll':
					if( confirm($.messages.confirmReturnAll))
					return true;
					break;	
				}
	  	}
	  	else{
	  		var container = $('<div class="errors" />');
	  		$(this.form).prepend(container);
	  		container.html($.messages.minSelectedError);
	  		
	  		//auto scroll to top of the page
	  		scroll(0,0);
	  	}
	  	return false;
	});

	// focus on 1st input field on form
	//if (!$('div.ui-dialog').length){
	//	$(':text:visible:enabled:first:not(.date)').focus(); 
	//} 

	//chee yung auto scroll to top of the page!!
	scroll(0,0);

	// perform URL redirection on button with name begin with 'redirect'.
	$(':button[name^="redirect"]').click(function() {
		location.href = $(this).attr('name').substring(9);
	});
	$(':button[href]').click(function() {
		location.href = $(this).attr('href');
	});
	$(':button[name=print]').click(function() {
		if(typeof parent['mainFrame'] != 'undefined')
		{parent['mainFrame'].focus();parent['mainFrame'].print();
		}else{window.print();}
	});
	$(':button[action]').click(function() {
		this.form.action = $(this).attr('action');
	});

	if ($.fn.datepicker) $(':input.date').datepicker({
		dateFormat: 'dd/mm/yy'
	});
	if ($.fn.clockpick) {
		$(':input.time').clockpick({
		 	starthour : 0,
			endhour : 23,
			event: 'focus',
			military: true,
			layout: 'horizontal',
			hoursopacity: 0.9,
			minutesopacity:0.8});	
	}
	
	//fix IE tab menu
	//if ($.browser.msie && typeof DD_roundies != 'undefined'){
	//	DD_roundies.addRule('.menuList a:link, .menuList a:visited', '8px 8px 0 0');
		//extended DD_roundies to add rules to rounded button classes
		/**
		$.uicornerfix = function(r){
		DD_roundies.addRule('.button-rounded', r);
		DD_roundies.addRule('.button-top', r+' '+r+' 0 0');
		DD_roundies.addRule('.button-bottom', '0 0 '+r+' '+r);
		DD_roundies.addRule('.button-right', '0 '+r+' '+r+' 0');
		DD_roundies.addRule('.button-left', r+' 0 0 '+r);
		DD_roundies.addRule('.button-tl', r+' 0 0 0');
		DD_roundies.addRule('.button-tr', '0 '+r+' 0 0');
		DD_roundies.addRule('.button-br', '0 0 '+r+' 0');
		DD_roundies.addRule('.button-bl', '0 0 0 '+r);
		};
		**/
	//	$.uicornerfix('5px');
	//}
	
	if ($.fn.corner) $('.rounded').corner();
	
	//Vincent - To make sure Amount is in xx.xx format
	$('.amount').change(function(){
		var amount = $(this).val();
		
		num = amount.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		var new_amount =  num+'.'+cents;
		
		$(this).val(new_amount);
		
/**		
		if(/^\d?[1-9][0-9]{0,2}(,[0-9]{3})*(\.[0-9]{0,2})?$/.test(amount) ){
			if (/\.[0-9]$/.test(amount) ) {
	            amount += "0";
	            $(this).val(amount);
	        }
	        else if (/\.$/.test(amount)) {
	            amount += "00";
	            $(this).val(amount);
	        }
	        else if (!/\.[0-9]{2}$/.test(amount) ) {
	            amount += ".00";
	            $(this).val(amount);
	        }		
		}else{
			$(this).val(null);
		}
**/
	});
	
	
	
	


});

(function($) {
	
	
	$.indicator = {
		indicators : {},
		identity : $('#indicator'),
		show : function(element) {
			if (typeof element == 'undefined') {
				this.identity.show();
			} else {
				this.indicatorFor(element).show();
			}
		},
		hide : function(element) {
			if (typeof element == 'undefined') {
				this.identity.hide();
			} else {
				this.indicatorFor(element).hide();
			}
		},
		indicatorFor : function(element) {
			var obj = this.indicators[element.id];
			if (typeof obj == 'undefined') {
				obj = this.identity.clone()
					.attr('id', '')
					.appendTo($(element).parent());
				this.indicators[element.id] = obj;
			}
			return obj;
		}
	}

	$.parseJSON = function(data, secure) {
		try{
			if (typeof secure == 'undefined') secure = false;

			if (secure)
				data = data.substring(data.indexOf("\/\*")+2, data.lastIndexOf("\*\/"));

			return eval("(" + data + ")");
		} catch (e) {
			return {};
		}
	}

	$.fn.fieldCheck = function(type, url, inputs) {
		if (typeof inputs == 'undefined') inputs = this;

		$(this).each(function() {
			var _this = this;
			$(_this).bind(type, function() {
				var i = 0, param = {};

				$(inputs).each(function() {
					var val = $(this).fieldValue();
					val = (typeof val != 'undefined')? ((val.length == 1)? val[0] : val) : '';
					if (val.length > 0) {
						param[this.name] = val;
						i++;
					}
				});

				if ($(inputs).size() == i) {
					$.indicator.show(_this);
					$.post(url, param, function(data) {
						$.indicator.hide(_this);

						var json = $.parseJSON(data, true);
						var errmsg = json[_this.name]; 

						$.errorHandler.clearError(_this);

						if (typeof errmsg != 'undefined' && errmsg.length > 0) {
							$.errorHandler.addError(_this, errmsg);
							_this.focus();
						}						
					});
				}
			})			
		})
		return this;
	};

	$.errorHandler = {
		clearErrorMessages : function(parent) {
			// clear out any rows with error message.
			$('div.errorMessage', parent).remove();
		},

		clearErrorLabels : function(parent) {
		    // set all labels back to the normal class
		    $('label.errorLabel', parent).removeClass('errorLabel').addClass('label');

		    // set group back to normal
		    if ($(parent).hasClass('wwerr'))
		    	$(parent).removeClass('wwerr').addClass('wwgrp');
		    else
		    	$('div.wwerr', parent).removeClass('wwerr').addClass('wwgrp');			
		},

		clearError : function(input) {
			var elem = (input.type ? input : input[0]);
			var wwgrp = $(elem).parents('div.wwerr');
			if (typeof wwgrp != 'undefined') {
			    this.clearErrorMessages(wwgrp);
			    this.clearErrorLabels(wwgrp)
			}
		},
		
		addError : function(input, errmsg) { 
			var elem = (input.type ? input : input[0]);
			var wwgrp = $(elem).parents('div.wwgrp');
			if (typeof wwgrp != 'undefined') {
				var label = $('label.label:first', wwgrp);

				$(document.createElement('div')).addClass('errorMessage')
					.attr('id', 'wwerr_' + elem.name)
					.html(errmsg + '')
					.insertBefore(label);

				label.removeClass('label').addClass('errorLabel');
				wwgrp.removeClass('wwgrp').addClass('wwerr');
			}
		}
	}

	// Minimum required form fields validation.
	// Usage: $("#myform").minRequired()
	$.fn.minRequired = function() {
		var container;
		$(this).submit(function() {
			if (container) container.hide();

			var form = $(this);
			var filled = false;
			form.find(":text, :password, :file, textarea").each(function() {
				if (this.value.length > 0) {
					filled = true;
				}	
			})
			if (filled) return true;

			if (form.find(":checkbox:checked, :radio:checked, select:selected").length > 0) {
				return true;
			}
			if (container) {
				container.show();
			} else {
				container = $('<div class="errors">' + $.messages.minRequired + '</div>');
				form.prepend(container);
			}
			return false;
		})
	}

	/**
	 * Stripes Validation 
	 */
	$.validator.setDefaults({
		errorPlacement : function(error, element) {
			if (element.attr('type') == 'hidden') {
				var block = $('<div class="errors">').append(error);
				element.parents('form').prepend(block);
			} else {
				//element.after(error);
				element.parent().append(error);
			}
        },
        onkeyup : false
	})

	// apply validator rules according to stripes metadata
	$.fn.applyValidation = function(metadata) {
   		var rules = {};
   		for (name in metadata) {
   	   		rules[name] = $.translateMetadata(metadata[name]);
   	   	}
   		//http://jqueryvalidation.org/validate
   		return $(this[0]).validate({
   			debug: false,
   			//default true
   			focusInvalid:true,
   			//default false
   			focusCleanup:false,
   			//Disables onclick validation of checkboxes and radio buttons.
   			onclick:false,
   			rules : rules
   		});
	}

	// convert stripes metadata to validator rules.
	$.translateMetadata = function(properties) {
		var methods = {};
		for (name in properties) {
			switch (name) {
				case'on':			
					var onValue =properties[name];
					for(var i=0;i<onValue.length;i++){
						//Skip translate metadata to jquery validation rules
						switch (onValue[i].toLowerCase()) {	
							case 'deleteall':
							case 'approveall':
							case 'returnall':
							case 'rejectall':
								return {};
								break;
						}	
					}
					break;
				case 'encrypted':
					return { required:true };
				case 'required':
				case 'minlength':
				case 'maxlength':
					// these properties are the same so just copy
					methods[name] = properties[name];
					break;
				case 'minvalue': methods['min'] = properties[name]; break;
				case 'maxvalue': methods['max'] = properties[name]; break;
				case 'mask': methods['regex'] = properties[name]; break;
				case 'type':
					switch (properties[name].toLowerCase()) {
						case 'short':
						case 'int':
						case 'integer':
						case 'long':
							methods['digits'] = true;
							break;
						case 'float':
						case 'double':
						case 'bigdecimal':
							methods['number'] = true;
							break;
						case 'date':
							methods['dateValidate'] = true;
							break;
					}
					break;
				case 'typeConverter':
					switch (properties[name]) {
						case 'CreditCardTypeConverter':methods['creditcard'] = true; break;
						case 'EmailTypeConverter':methods['email'] = true; break;
						case 'DateTypeConverter':methods['date2'] = true; break;
					}
					break;
			}
		}
		return methods
	}
})(jQuery);

function numberOnly(e) {
    var c='';
    if($.browser.msie) c=e.keyCode;
    else c=e.charCode;
    if (c==0) {return true;}
    return !isNaN(String.fromCharCode(c));
} ;

function doLater(func, timeout) {
	return window.setTimeout(func, timeout);	
}

function isLeftClick(e) {
	if( (!$.browser.msie && e.button == 0) || ($.browser.msie && e.button == 1) ) {
		return true;
    } else if(e.button == 2){
       	return false;
    }
}


function bookmarksite(name,url )
{
 if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) 
 {
   alert('This function is not available in Google Chrome. Click the star symbol at the end of the address-bar or hit Ctrl-D to create a bookmark.');
 }
  if (window.sidebar) { // Mozilla Firefox
    window.sidebar.addPanel(name, url, "");
}
else if (window.external) { // IE
    window.external.AddFavorite(url, name);
}
else if (window.opera && window.print) {
    window.external.AddFavorite(url, name);
}
else {
    alert('not supported');
}
}

function checkChineseChar(f) {

	var chineseChar = /[\u2E80-\u2FD5\u3400-\u4DBF\u4E00-\u9FCC]+/gi;
	if (f.value.match (chineseChar))
	{
		f.value = f.value.replace(chineseChar,'');
	}

}

function checkNumChar(f) {
	// Replace all value that does not match with alphanumeric values
	f.value = f.value.replace(/[^a-zA-Z0-9 ]*/ig,'');
}

function checkSpecialSpace(f) {

	f.value = f.value.replace(/[^a-z0-9\-\'.\/() +,:?]*/ig,'');
}