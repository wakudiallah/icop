@extends('layouts.master')


@section('container')    
	
    @include('layouts.menu')
    @yield('menu')

    <div class="content">
    	@yield('content')
    </div>

    @include('layouts.footer')
    @yield('footer')

    @include('templates.public.overlay')
    @yield('overlay')

@stop

  