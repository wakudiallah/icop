@section('overlay')
<div class="overlay overlay-hugeinc">
	<button id="motor-insurance-close" type="button" class="overlay-close">Close</button>
	<div class="container container-table">
		<div class="row vertical-center-row">
			<div class="col-lg-6 col-offset-lg-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
				<div class="widget-body">
					<form id="login-form" class="smart-form" novalidate="novalidate">
						<header>
							<h2><strong>Create new account or sign-in.</strong></h2>
							<small>If you dont have any account yet, fill in your email and password, then you are good to go</small>
						</header>
						<fieldset>
							<section>
								<label class="label">E-mail</label>
								<label class="input"> <i class="icon-append fa fa-user"></i>
									<input type="email" name="email">
									<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
							</section>
							<section>
								<label class="label">Password</label>
								<label class="input"> <i class="icon-append fa fa-lock"></i>
									<input type="password" name="password">
									<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
								<div class="note">
									<a href="forgotpassword.html">Forgot password?</a>
								</div>
							</section>
						</fieldset>
						<footer>
							<a href="applicant/registration" class="btn btn-primary">Sign in</a>
						</footer>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@stop