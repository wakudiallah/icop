@section('javascripts')

	<!-- Basic javascript -->
	@javascripts('core')
	<script src="https://www.icopangkasa.com.my/public/builds/core/js/plugins/jquery.cookie-7b4c002c3fdeda1aa891b2f035cdad0f.js"></script>
<script src="https://www.icopangkasa.com.my/public/builds/core/js/app.config-fc8168f08086d8afdbdff74a4064f10e.js"></script>
<script src="https://www.icopangkasa.com.my/public/builds/core/js/app.min-fc8168f08086d8afdbdff74a4064f10e.js"></script>
	<!--Page scripts -->
	@yield('page-script')
	
@stop