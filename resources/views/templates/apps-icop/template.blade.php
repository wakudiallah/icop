<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="icon" type="image/png" href="{{ asset('favicon/favicon.ico') }}">

<title>{{config('insurance.system_name')}}</title>

<style>
     #home .home-thumb {
          height: 0vh !important;
     }

</style>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">


 
<!-- Stimulus Template http://www.templatemo.com/tm-498-stimulus -->
<link rel="stylesheet" href="{{asset('frontApp/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('frontApp/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('frontApp/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('frontApp/css/templatemo-style.css')}}">

<!-- update by me-->
<link rel="stylesheet" href="{{asset('frontApp/css/style-update.css')}}">

<!-- Loading  -->
<link rel="stylesheet" href="{{asset('frontApp/css/loading.css')}}">


<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

@stack('css')


</head>


<!-- PRE LOADER -->

<div class="preloader">
     <div class="spinner">
          <span class="spinner-rotate"></span>
     </div>
</div>

<!-- Loading  -->

<div class="loading-screen" id="loading-screen">
     <div class="loading-spinner"></div>
     <p>Please wait...</p>
 </div>
 


<!-- Navigation Section -->

<div class="navbar navbar-fixed-top custom-navbar" role="navigation">
     <div class="container">

          <!-- navbar header -->
          <div class="navbar-header">
               <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
               </button>
               <!-- <a href="#" class="navbar-brand">ICOP</a> -->
          </div>

          <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('car-insurance')}}" class="smoothScroll">Home</a></li>

                    @if(!Auth::guest())
                    <li><a class="smoothScroll">Hi, {{Auth::user()->name}}</a></li>
                    <li><a href="{{url('logout/user')}}" class="smoothScroll">Logout</a></li>
                    @else
                    <li><a href="{{url('user-login')}}" class="smoothScroll">Login</a></li>
                    @endif
               </ul>
          </div>

     </div>
</div>


<!-- Home Section -->

@yield('content')


<!-- loading -->
<script src="{{asset('frontApp/js/loading.js')}}"></script>



<!-- SCRIPTS -->

<script src="{{asset('frontApp/js/jquery.js')}}"></script>
<script src="{{asset('frontApp/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontApp/js/jquery.parallax.js')}}"></script>
<script src="{{asset('frontApp/js/smoothscroll.js')}}"></script>
<script src="{{asset('frontApp/js/wow.min.js')}}"></script>
<script src="{{asset('frontApp/js/custom.js')}}"></script>


     <script>
          function toggleRightClick(enable) {
          if (enable) {
               $(document).off("contextmenu");
          } else {
               $(document).on("contextmenu", function(e) {
                    e.preventDefault();
               });
          }
          }
          
          $(document).ready(function() {
          // Initially disable right-click
          toggleRightClick({{config('insurance.disable')}});
          
          // Example of how to enable right-click
          // toggleRightClick(true);
          });
     </script>
     

    

    <script>
     $(document).ready(function(){
         $('.info-icon').tooltip({
             trigger: 'hover' // Show tooltip on hover
         });
     });
 </script>

@stack('addjs')


     




</body>
</html>