<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="google-site-verification" content="XHJAWe662Xw-_9c9cPSX-9az8rHfgjVY5H4ptLZej2o" />

	<title>Icop Angkasa | Homepage</title>

	<!-- Basic style -->

	<link type="text/css" href="{{ asset('frontend/css/main.css')}}" rel="stylesheet">
	<link type="text/css" href="{{ asset('frontend/color/default.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/font-awesome/css/font-awesome.min4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/animate-core.css') }}" />	
	<link type="text/css" href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link type="text/css" href="{{ asset('frontend/css/default.css') }}" rel="stylesheet">

	
	
	<!-- Page style -->
	@yield('jasny')

	<!-- ICOP CSS -->
    <link rel="stylesheet" type="text/css" href="https://sasnets.com/icopangkasa/builds/icop/css/styles-6c0d86da48ec578bb5f857aa1f3405a5.css" />
    <link rel="stylesheet" type="text/css" href="https://sasnets.com/icopangkasa/builds/icop/css/overlay-eed69cdc511436941b5a5eb19086b42c.css" />
        
    <!-- Page style -->
    <link rel="stylesheet" type="text/css" href="https://sasnets.com/icopangkasa/builds/revolution/css/extralayers-af1f1c53b838e594bdbf09f93530aa28.css" />
    <link rel="stylesheet" type="text/css" href="https://sasnets.com/icopangkasa/builds/revolution/css/settings-eed69cdc511436941b5a5eb19086b42c.css" />	<link rel="stylesheet" type="text/css" href="https://sasnets.com/icopangkasa/builds/nivo/css/nivo-lightbox-theme/default/default-56764885234e16b987aa3ff095684f82.css" />	
    

	<!-- #FAVICONS -->
	<!-- <link rel="shortcut icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon"> -->

    

	<!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
