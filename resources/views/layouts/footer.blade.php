@section('footer')
<footer>
	<div class="icop footer-content clearfix">
		<div class="container custom-container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Syarikat</h3>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/about-us')}}">Mengenai Kami</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/faq')}}">Soalan Lazim</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/contact-us')}}">Hubungi Kami</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/tnc')}}">Terma dan Syarat</a></p>
					</div>
					<div class="fb-share-button" data-href="https://www.icopangkasa.com.my/public/daftar" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.icopangkasa.com.my%2Fpublic%2Fdaftar&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
				</div>

				<div class="col-md-3 col-sm-6 col-xs-12">
					<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Produk</h3>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/product/motor')}}">Kereta</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Kebakaran dan Banjir</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Isi Rumah</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Kemalangan Diri</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Perjalanan</a></p>
					</div>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Perubatan</a></p>
					</div>
				</div>
				<!--@if (!(Auth::user()))
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Koperasi</h3>
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('cooperative/register')}}">Pendaftaran Koperasi</a></p>
					</div>
				</div>
				@endif-->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Peralatan</h3>
					
					<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/insurance')}}">Perbezaan Insuran</a></p>
					</div>
					<!--<div class="recent-post clearfix">
						<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Mengaktifkan Lindungan</a></p>
					</div>-->
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Icop Angkasa © {{ date('Y') }} - icopangkasa.com.my</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=48242215"></script>-->


<!-- Basic javascript -->
<script src="https://sasnets.com/icopangkasa/builds/core/js/libs/jquery-2.0.2.min-7224ace21969038f3b4665f157ae01e7.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/libs/jquery-ui-1.10.3.min-9bae20ce0cf39ee5ab2140098dc872a8.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/bootstrap/bootstrap.min-7224ace21969038f3b4665f157ae01e7.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/plugins/classie-dbe9289d56f6cdc1831874a9958beb26.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/plugins/jquery.scrollUp-dbe9289d56f6cdc1831874a9958beb26.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/plugins/smoothscroll-dbe9289d56f6cdc1831874a9958beb26.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/plugins/gnmenu-7224ace21969038f3b4665f157ae01e7.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/core/js/gn-menu-967ce26356af0b21f8c42ec9a7125202.js"></script>	<script src="https://www.icopangkasa.com.my/public/builds/core/js/plugins/jquery.cookie-7b4c002c3fdeda1aa891b2f035cdad0f.js"></script>
<script src="https://www.icopangkasa.com.my/public/builds/core/js/app.config-fc8168f08086d8afdbdff74a4064f10e.js"></script>
<script src="https://www.icopangkasa.com.my/public/builds/core/js/app.min-fc8168f08086d8afdbdff74a4064f10e.js"></script>
	<!--Page scripts -->
		<script src="https://sasnets.com/icopangkasa/builds/nivo/js/plugins/nivo-lightbox.min-dbe9289d56f6cdc1831874a9958beb26.js"></script>	<script src="https://sasnets.com/icopangkasa/builds/themepunch/js/plugins/jquery.themepunch.plugins.min-dbe9289d56f6cdc1831874a9958beb26.js"></script>
<script src="https://sasnets.com/icopangkasa/builds/themepunch/js/plugins/jquery.themepunch.revolution.min-dbe9289d56f6cdc1831874a9958beb26.js"></script>	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.overlay').removeClass('open');
			jQuery('#motor-insurance-apply, #motor-insurance-close, #motor-compre').click(function() {
				jQuery('.overlay').toggleClass('open');
			});

			jQuery('.tp-banner').show().revolution({
				dottedOverlay : "none",
				delay : 8000,
				startwidth : 1170,
				startheight : 700,
				hideThumbs : 200,

				thumbWidth : 100,
				thumbHeight : 50,
				thumbAmount : 5,

				navigationType : "bullet",
				navigationArrows : "solo",
				navigationStyle : "preview4",

				touchenabled : "on",
				onHoverStop : "off",

				swipe_velocity : 0.7,
				swipe_min_touches : 1,
				swipe_max_touches : 1,
				drag_block_vertical : false,

				parallax : "mouse",
				parallaxBgFreeze : "on",
				parallaxLevels : [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

				keyboardNavigation : "off",

				navigationHAlign : "center",
				navigationVAlign : "bottom",
				navigationHOffset : 0,
				navigationVOffset : 20,

				soloArrowLeftHalign : "left",
				soloArrowLeftValign : "center",
				soloArrowLeftHOffset : 20,
				soloArrowLeftVOffset : 0,

				soloArrowRightHalign : "right",
				soloArrowRightValign : "center",
				soloArrowRightHOffset : 20,
				soloArrowRightVOffset : 0,

				shadow : 0,
				fullWidth : "off",
				fullScreen : "on",

				spinner : "spinner4",

				stopLoop : "off",
				stopAfterLoops : -1,
				stopAtSlide : -1,

				shuffle : "off",

				autoHeight : "off",
				forceFullWidth : "off",

				hideThumbsOnMobile : "off",
				hideNavDelayOnMobile : 1500,
				hideBulletsOnMobile : "off",
				hideArrowsOnMobile : "off",
				hideThumbsUnderResolution : 0,

				hideSliderAtLimit : 0,
				hideCaptionAtLimit : 0,
				hideAllCaptionAtLilmit : 0,
				startWithSlide : 0,
				fullScreenOffsetContainer : ""
			});

			//nivo lightbox
			$('.gallery-item a').nivoLightbox({
				effect: 'fadeScale',                             // The effect to use when showing the lightbox
				theme: 'default',                           // The lightbox theme to use
				keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
				clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
				onInit: function(){},                       // Callback when lightbox has loaded
				beforeShowLightbox: function(){},           // Callback before the lightbox is shown
				afterShowLightbox: function(lightbox){},    // Callback after the lightbox is shown
				beforeHideLightbox: function(){},           // Callback before the lightbox is hidden
				afterHideLightbox: function(){},            // Callback after the lightbox is hidden
				onPrev: function(element){},                // Callback when the lightbox gallery goes to previous item
				onNext: function(element){},                // Callback when the lightbox gallery goes to next item
				errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
			});

		});
		$.scrollUp();
	</script>

	<script>
  // window.REMODAL_GLOBALS = {
  //   NAMESPACE: 'remodal',
  //   DEFAULTS: {
  //     hashTracking: true,
  //     closeOnConfirm: true,
  //     closeOnCancel: true,
  //     closeOnEscape: true,
  //     closeOnOutsideClick: true,
  //     modifier: ''
  //   }
  // };
</script>


@stop
