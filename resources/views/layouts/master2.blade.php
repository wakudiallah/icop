
<!DOCTYPE html>
<html lang="en">

@include('layouts.head')
@yield('head')

<body class="menu-on-top" data-spy="scroll">

    <div id="wrapper">
        @yield('container')    
    </div>
   
    @include('templates.javascripts')
    @yield('javascripts')


</body>
</html>
