@section('menu')
<div class="container">
	<ul id="gn-menu" class="gn-menu-main">
		<li class="gn-trigger visible-xs visible-sm">
			<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
			<nav class="gn-menu-wrapper">
	<div class="gn-scroller">
		<ul class="gn-menu">
			<li><a href="{{ url('/')}}" class="gn-icon gn-icon-earth">Utama</a></li>
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='4'))
				<li><a href="{{ url('/coop/home') }}" class="gn-icon gn-icon-pictures">Coop</a></li>
			@endif
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='2'))
				<li ><a href="{{ url('/admin') }}" class="gn-icon gn-icon-pictures">Admin</a></li>
			@endif
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='6'))
				<li ><a href="{{ url('/angkasa/home') }}" class="gn-icon gn-icon-pictures">Angkasa</a></li>
			@endif
				<li><a href="{{ url('/product/product')}}" class="gn-icon gn-icon-photoshop">Produk</a></li>
				<li><a href="{{URL::to('page/promotion')}}" class="gn-icon gn-icon-videos">Promosi</a></li>
				<li><a href="{{ url('/page/contact-us') }}" class="gn-icon gn-icon-help">Hubungi</a></li>
			@if (!(Auth::user()))
				<li><a href="{{  url('/daftar') }}" class="gn-icon gn-icon-article">Klik Untuk Sebutharga</a></li>@endif
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='3'))
				<li ><a href="{{url('/applicant/mystatus')}}" class="gn-icon gn-icon-archive">Status</a></li>
				<li><a href="{{ url('/applicant/profil')}}" class="gn-icon gn-icon-pictures">Profil</a></li>
			@endif
			@if (!(Auth::user()))
				<li><a href="{{ url('/auth/login') }}" class="gn-icon gn-icon-cog">Daftar Masuk</a></li>
			@else
				<li><a href="{{ url('/logout')}}" class="gn-icon gn-icon-cog">Daftar Keluar</a></li>
			@endif
			
		</ul>
	</div><!-- /gn-scroller -->
	</nav>
	</li>
	
	<li class="hidden-xs hidden-sm">
		<a href="{{ url('/')}}"><img src="{{url('/img/icopangkasa.png')}}" width="95px" height="58px"></a>
	</li>

	@if (!(Auth::user())) 
		@elseif(!empty(Auth::user()->role_id=='4'))
			<li class="hidden-xs hidden-sm"><a href="{{ url('/coop/home') }}">Coop</a></li>
	@endif
	@if (!(Auth::user())) 
		@elseif(!empty(Auth::user()->role_id=='2'))
			<li class="hidden-xs hidden-sm"><a href="{{ url('/admin') }}">Admin</a></li>
	@endif
	@if (!(Auth::user())) 
		@elseif(!empty(Auth::user()->role_id=='6'))
		<li class="hidden-xs hidden-sm"><a href="{{ url('/angkasa/home') }}">Angkasa</a></li>
	@endif
		<li class="hidden-xs hidden-sm"><a href="{{ url('/product/product')}}">Produk</a></li>
		<li class="hidden-xs hidden-sm"><a href="{{URL::to('page/promotion')}}">Promosi</a></li>
		<li class="hidden-xs hidden-sm"><a href="{{ url('/page/contact-us') }}">Hubungi</a></li>
	@if (!(Auth::user()))
		<li class="hidden-xs hidden-sm"><a href="{{ url('/daftar') }}">Klik Untuk Sebutharga</a></li>
	@endif
	@if (!(Auth::user())) 
	@elseif(!empty(Auth::user()->role_id=='3'))
		<li class="hidden-xs hidden-sm"><a href="{{ url('/applicant/mystatus')}}">Status</a></li>
		<li class="hidden-xs hidden-sm"><a href="{{ url('/applicant/profil')}}">Profil</a></li>@endif
		<li>			
			@if (!(Auth::user()))
				<a href="{{ url('/auth/login') }}">Daftar Masuk</a>
			@else
				<li class="hidden-xs hidden-sm"><a href="{{ url('/logout') }}">Daftar Keluar</a></li>
			@endif
		</li>
	</div><!-- /.container -->
@stop