@section('head')
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="google-site-verification" content="XHJAWe662Xw-_9c9cPSX-9az8rHfgjVY5H4ptLZej2o" />

	<title>Icop Angkasa | Homepage</title>

	<!-- Basic style -->
	
	<link type="text/css" href="{{ url('/css/main.css')}}" rel="stylesheet">
		<link type="text/css" href="{{ url('/color/default.css"')}}' rel="stylesheet">

	<!--Page scripts -->


	<!-- Page style -->
	@yield('jasny')

	<!-- ICOP CSS -->
	@stylesheets('icop')

	<!-- Page style -->
	@yield('page-style')
	

	<!-- #FAVICONS -->
	<link rel="shortcut icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon">

	<!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
@endsection