@extends('layouts.app')

@section('title', 'Calculate Roadtax (LKM)')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">

      @include('sweetalert::alert')
      
        <section class="section" style="min-height: 100vh;">
            <div class="section-header">
                <h1>Calculate Roadtax (LKM)</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-roadtax-lkm') }}</div>
                </div>
            </div>
        
            <div class="section-body d-flex justify-content-center align-items-center" style="min-height: calc(100vh - 100px);">
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-form"></div>
                            </div>
        
                            <div class="card-body p-10">
                                <p>{{config('insurance.currency')}} <span id="roadtax_amount">0</span></p>
        
                                <form id="roadtaxForm" method="post" action="{{route('calculate-roadtax.store')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
        
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-form-label"> Engine CC </label>
                                        <input type="text" name="engine" placeholder="Engine" id="inputEngine" value="" class="form-control" min="1" max="1000000" step="1" required>
                                        @error('engine')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
        
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-form-label"> Region </label>
                                        <select name="region" class="form-control" required>
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            @foreach($region as $region)
                                                <option value="{{$region->id}}">{{ $region->region }}</option>
                                            @endforeach
                                        </select>
                                        @error('region')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
        
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-form-label"> Ownership </label>
                                        <select name="owner" class="form-control" required>
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            @foreach($owner as $owner)
                                                <option value="{{$owner->id}}">{{ $owner->ownership }}</option>
                                            @endforeach
                                        </select>
                                        @error('owner')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
        
                                    <div class="form-group mb-3">
                                        <button class="btn btn-success" id="submitForm" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
    </div>
@endsection

@push('scripts')

    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>


    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>

    <script>
      document.getElementById('inputEngine').addEventListener('input', function (e) {
          var value = this.value;
          if (isNaN(value) || value.includes('e')) {
              this.value = value.replace(/[^\d]/g, ''); // Only allow numbers
          }
      });
    </script>
    
    <script>
      $(document).ready(function() {
          $('#submitForm').click(function(e) {
              e.preventDefault(); // Prevent normal form submission

              var engineValue = $('#inputEngine').val();

              if (engineValue === "" || isNaN(engineValue) || engineValue <= 0) {
                  alert('Please enter a valid engine value');
                  return; // Stop the form from submitting
              }
              
              var formData = new FormData($('#roadtaxForm')[0]); // Capture form data
  
              $.ajax({
                  url: "{{ route('calculate-roadtax.store') }}",  // The route
                  method: "POST",                                 // POST request
                  data: formData,
                  processData: false,                             // For file upload
                  contentType: false,                             // For file upload
                  success: function(response) {

                    var formattedAmount = parseFloat(response.roadtax_amount).toLocaleString('en-US', { 
                        minimumFractionDigits: 2, 
                        maximumFractionDigits: 2 
                    });

                    $('#roadtax_amount').text(formattedAmount);


                  },
                  error: function(xhr, status, error) {
                      // Handle error response (e.g., show error message)
                      console.log(xhr.responseText);
                      alert('Error occurred. Please try again.');
                  }
              });
          });
      });
  </script>

@endpush
