@extends('layouts.app')

@section('title', 'Param Insurance Promo')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Param Param Insurance Promo </h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-insurance-promo-create') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('param-insurance-promo.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Insurance Product</label>
                                            <select name="insurance_product_id" class="form-control">
                                                @foreach($insCompany as $insCompany)
                                                <option value="" selected disabled hidden>- Please Select -</option>
                                                <option value="1">{{ $insCompany->insurance_product }}</option>
                                                @endforeach
                                            </select>
                                            @error('insurance_product_id')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Promo </label>
                                            <input type="text" name="promo" placeholder="Promo" id="inputTitle" value="" class="form-control">
                                            @error('promo')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                   
                                
                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @error('status')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
