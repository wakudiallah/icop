@extends('layouts.app')

@section('title', 'Param Roadtax Lkm')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">

      @include('sweetalert::alert')
      
        <section class="section">
            <div class="section-header">
                <h1>Param Roadtax Lkm (Lesen Kenderaan Motor)</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-roadtax-lkm') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                
                                @can('param-ownership-lkm-create')
                                <h4><a href="{{ route('param-roadtax-lkm.create') }}" class="btn btn-primary mr-1">+ Add</a></h4>
                                @endcan

                                <div class="card-header-form"></div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                                        <thead>
                                          <tr>
                                            <th>No</th>
                                            <th>Region</th>
                                            <th>Ownership</th>
                                            <th>Start (CC)</th>
                                            <th>End (CC)</th>
                                            <th>Kadar Flat</th>
                                            <th>Kadar Asas</th>
                                            <th>Kadar Progresif</th>
                                            <th>Status</th>
                                            <th width="15%">Action</th>
                                          </tr>
                                        </thead>
                                        <?php $i=1; ?>
                                          @foreach($data as $data)
                                            <tr>
                                              <td>{{$i++}}</td>
                                              <td> 
                                                @if(!empty($data->reg->region))
                                                {{ $data->reg->region }} 
                                                @endif
                                              </td>
                                              <td> 
                                                @if(!empty($data->owner->ownership))
                                                {{ $data->owner->ownership }} 
                                                @endif
                                              </td>
                                              <td> {{ $data->start }} </td>
                                              <td> {{ $data->end }} </td>
                                              <td> {{ $data->kadar_flat }} </td>
                                              <td> {{ $data->kadar_asas }} </td>
                                              <td> {{ $data->kadar_progressif }} </td>
                                              <td>
                                                @if($data->status == 1)
                                                <div class="badge badge-pill badge-success mb-1">Active</div>
                                                @else 
                                                <div class="badge badge-pill badge-danger mb-1">Inactive</div>
                                                @endif
                                              </td>

                                              <td>
                                                
                                                @can('param-roadtax-lkm-edit')
                                                  <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" href="{{ route('param-ownership-lkm.edit', $data->id) }}" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                                @endcan 

                                                @can('param-roadtax-lkm-delete')

                                                <input id="toggle-trigger" type="checkbox" data-toggle="toggle" class="btn togglefunction" data-on="Active" data-off="Inactive" data-size="xl"  data-id="{{$data->id}}" data-onstyle="success" data-offstyle="danger" {{ $data->status ? 'checked' : '' }}>                                                
                                                  @csrf 
                                                @endcan
                                              </td>
                                            </tr>
                                          @endforeach
                                        <tbody>
                                         
                                        </tbody>
                                      </table>
                                      

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')

    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

  <!-- Toogle -->
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
  
  <script>
      $(document).ready(function() {
        // Initialize the toggle buttons when the page loads
        $('input[data-toggle="toggle"]').bootstrapToggle();

        // Re-initialize the toggles when content is loaded via AJAX or pagination
        $('#myTable').on('draw.dt', function() {
            $('input[data-toggle="toggle"]').bootstrapToggle();
        });

        // Example if you are using AJAX for pagination
        $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            $.get(url, function(data) {
                $('.table-responsive').html(data);
                // Reinitialize the toggle buttons after content is loaded
                $('input[data-toggle="toggle"]').bootstrapToggle();
            });
        });
    });


  </script>

    <!-- Bootstrap Toggle CSS -->
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <!-- Bootstrap Toggle JS -->
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <!-- Initialize toggle once DOM is ready -->
    <script>
        $(document).ready(function() {
            $('input[data-toggle="toggle"]').bootstrapToggle();
        });
    </script>


  <script>
    $(function() {
        $('.togglefunction').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
            
            $.ajax({
                type: "POST",
                url: "{{url('param-roadtax-lkm/status')}}",
                data: {
                  '_token': '{{ csrf_token() }}',
                  'status': status, 
                  'id': id
                },
                success: function(data){
                    setInterval('location.reload()', 1000); 
                },
                error : function(){
                      alert("Status Change Unsuccessful !");
                }
            });
        })
    })
  </script>

@endpush
