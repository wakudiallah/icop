@extends('layouts.app')

@section('title', 'Param Postcode')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Param State </h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-postcode-edit') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('param-postcode.update', $postcode->id)}}" enctype='multipart/form-data'>
                                        @csrf 
                                        @method('PATCH')

                                        <div class="form-group">
                                            <label for="inputPostcode" class="col-form-label">Postcode </label>
                                            <input id="inputPostcode" type="text" name="postcode" placeholder="Enter Postcode" maxlength="5" value="{{$postcode->postcode}}" class="form-control">
                                            @error('postcode')
                                              <span class="text-danger">{{$message}}</span>
                                            @enderror
                                          </div>        
                                  
                                          <div class="form-group">
                                            <label for="inputArea" class="col-form-label">Area </label>
                                            <input id="inputArea" type="text" name="area" placeholder="Enter Area"  value="{{$postcode->area}}" class="form-control">
                                            @error('area')
                                              <span class="text-danger">{{$message}}</span>
                                            @enderror
                                          </div>
                                  
                                          <div class="form-group">
                                            <label for="inputPostOffice" class="col-form-label">Post Office </label>
                                            <input id="inputPostOffice" type="text" name="post_office" placeholder="Enter Post Office"  value="{{$postcode->post_office}}" class="form-control">
                                            @error('post_office')
                                              <span class="text-danger">{{$message}}</span>
                                            @enderror
                                          </div>
                                  
                                          <div class="form-group">
                                            <label for="state">State </label>
                                            <select name="state_code" class="form-control">
                                                <option value="">--Select State--</option>
                                                @foreach($state as $state)
                                                  <option value="{{$state->state_code}}" {{(($postcode->state_code == $state->state_code)? 'selected':'')}}>{{$state->state  }}</option>
                                                @endforeach
                                            </select>
                                             @error('state_code')
                                              <span class="text-danger">{{$message}}</span>
                                            @enderror
                                          </div>

                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status <!-- <span class="text-danger">*</span> --> </label>
                                            <select name="status" class="form-control">
                                              <option value="1" {{(($postcode->status=='1') ? 'selected' : '')}}>Active</option>
                                              <option value="0" {{(($postcode->status=='0') ? 'selected' : '')}}>Inactive</option>
                                            </select>
                                            @error('status')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                       

                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
