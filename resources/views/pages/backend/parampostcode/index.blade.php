@extends('layouts.app')

@section('title', 'Param Postcode')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">

      @include('sweetalert::alert')
      
        <section class="section">
            <div class="section-header">
                <h1>Param Postcode</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-postcode') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                
                                @can('param-sst-create')
                                <h4><a href="{{ route('param-postcode.create') }}" class="btn btn-primary mr-1">+ Add</a></h4>
                                @endcan

                                <div class="card-header-form"></div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered dataTables-example"  width="100%" cellspacing="0">
                                        <thead>
                                          <tr>
                                            <th>Id</th>
                                            <th>Postcode</th>
                                            <th>Area</th>
                                            <th>Post Office</th>
                                            <th>State</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        
                                      </table>
                                      

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
   
  <!-- Toogle -->
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


  <!--<script>
    $(function() {
        $('.togglefunction').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
            
            $.ajax({
                type: "POST",
                url: "{{url('param-state/status')}}",
                data: {
                  '_token': '{{ csrf_token() }}',
                  'status': status, 
                  'id': id
                },
                success: function(data){
                    setInterval('location.reload()', 1000); 
                },
                error : function(){
                      alert("Status Change Unsuccessful !");
                }
            });
        })
    })
  </script>-->

<script type="text/javascript">

    $(function () {
        var table = $('.dataTables-example').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('param-postcode.index') }}",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'postcode', name: 'postcode' },
                { data: 'area', name: 'area' },
                { data: 'post_office', name: 'post_office' },
                { data: 'state_code', name: 'state_code' },
                { 
                    data: 'status',
                    name: 'status',
                    render: function(data, type, row) {
                        if (data == 1) {
                            return '<span class="badge badge-success">Active</span>';
                        } else {
                            return '<span class="badge badge-warning">Inactive</span>';
                        }
                    }
                },
                {
                    data: null,
                    name: 'action',
                    sortable: false,
                    className: 'text-center',

                    render: function (data, type, row) {

                        data.status = parseInt(data.status); // Convert status to a number
                        var id = data.id; // Assuming data.id contains the id value
                        var editUrl = "{{ url('param-postcode') }}/" + id + "/edit";

                        return '<a href="' + editUrl + '" class="btn btn-primary btn-xl float-left mr-1" data-toggle="tooltip" title="Edit" data-placement="bottom"><i class="fas fa-pencil-alt"></i></a>' +
                            '<input style="width: 90px;" type="checkbox" data-on="Active" data-off="Inactive" data-width="90" data-height="35" data-size="xl" class="togglefunction" data-id="' + data.id + '" data-onstyle="success" data-offstyle="danger" ' + (data.status == 1 ? 'checked' : '') + '>';
                        
                    }
                }
            ],
            initComplete: function () {
                // Initialize Bootstrap Toggle
                $('.togglefunction').change(function() {
                    var status = $(this).prop('checked') == true ? 1 : 0; 
                    var id = $(this).data('id'); 
                    
                    $.ajax({
                        type: "POST",
                        url: "{{url('param-postcode/status')}}",
                        data: {
                        '_token': '{{ csrf_token() }}',
                        'status': status, 
                        'id': id
                        },
                        success: function(data){
                            setInterval('location.reload()', 1000); 
                        },
                        error : function(){
                            alert("Status Change Unsuccessful !");
                        }
                    });
                })
            },
            

            rowCallback: function(row, data) {
                    $('.togglefunction', row).bootstrapToggle({ size: 'mini' });
                }
        });
    });

</script>



@endpush
