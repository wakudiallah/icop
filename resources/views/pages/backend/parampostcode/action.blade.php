


@can('param-postcode-edit')
<a href="{{route('param-postcode.edit',$id)}}" class="btn btn-primary btn-xl float-left mr-1"  data-toggle="tooltip" title="Edit" data-placement="bottom"><i class="fas fa-pencil-alt"></i></a>
@endcan

@can('param-postcode-delete')
<input style="width: 90px;" id="toggle-trigger" type="checkbox" data-toggle="toggle" data-on="Active" data-off="Inactive"  data-size="xl" class="togglefunction" data-id="{{$id}}" data-onstyle="success" data-offstyle="danger" {{ $status ? 'checked' : '' }}>

@endcan



<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>



<script>
    $(function() {
        $('.togglefunction').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
          
        $.ajax({
            type: "POST",
            url: "{{url('param-postcode/status')}}",
            data: {
                    '_token': '{{ csrf_token() }}',
                    'status': status, 
                    'id': id
                  },
                  success: function(data){
                      setInterval('location.reload()', 1000); 
                  },
                  error : function(){
                        alert("Status Change Unsuccessful !");
                  }
                
                  
                });
            })
          })
  </script>

