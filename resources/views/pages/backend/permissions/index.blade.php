@extends('layouts.app')

@section('title', 'Permission')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Permission</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item"></div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4><h4><a href="{{ route('permissions.create') }}" class="btn btn-primary mr-1">+ Add</a></h4></h4>
                                
                                <div class="card-header-form">
                                    <!-- <form>
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control"
                                                placeholder="Search">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form> -->
                                </div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    <table class="table-striped table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th>Name</th>
                                                <th>Guard</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php $i=1; ?>
                                            @foreach($data as $data)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $data->name }}</td>
                                                <td> {{ $data->guard_name }} </td>
                                                <td> 
                                                    @can('permission-edit')
                                                    <a class="btn btn-primary btn-action mr-1"
                                                    data-toggle="tooltip" href="{{ route('permissions.edit', $data->id) }}"
                                                    title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                                    @endcan
                                                    
                                                    @can('permission-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $data->id],'style'=>'display:inline']) !!}
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-action', 'type' => 'submit']) !!}
                                                    {!! Form::close() !!}
                                                    @endcan
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>                                        
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
