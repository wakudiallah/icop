@extends('layouts.app')

@section('title', 'Permission')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Permission</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Components</a></div>
                    <div class="breadcrumb-item">Table</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4></h4>
                                
                                <div class="card-header-form">
                                    <!-- <form>
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control"
                                                placeholder="Search">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form> -->
                                </div>
                            </div> 

                            <div class="card-body p-10">
                                <form method="POST" action="{{ route('permissions.update', $permission->id) }}">
                                    @method('patch')
                                    @csrf
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input value="{{ $permission->name }}" 
                                            type="text" 
                                            class="form-control" 
                                            name="name" 
                                            placeholder="Name" required>
                          
                                        @if ($errors->has('name'))
                                            <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                          
                                    <button type="submit" class="btn btn-primary">Save permission</button>
                                    <a href="{{ route('permissions.index') }}" class="btn btn-default">Back</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
