@extends('layouts.app')

@section('title', 'Param Insurance Company')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Param Param Insurance Company </h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-insurance-company-create') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('param-insurance-company.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Insurance Product</label>
                                            <input id="inputTitle" type="text" name="insurance_product" placeholder="Insurance Product"  value="{{old('insurance_product')}}" class="form-control">
                                            @error('insurance_product')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>



                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Insurance Company</label>
                                            <input id="inputTitle" type="text" name="company" placeholder="Insurance Company"  value="{{old('company')}}" class="form-control">
                                            @error('company')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Register Company</label>
                                            <input id="inputTitle" type="text" name="register_company" placeholder="Register Company"  value="{{old('register_company')}}" class="form-control">
                                            @error('register_company')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Logo</label>
                                            <input id="inputTitle" type="file" name="logo" placeholder="Logo"  value="{{old('logo')}}" class="form-control">
                                            @error('logo')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                
                                
                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @error('status')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
