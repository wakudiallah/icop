@extends('layouts.app')

@section('title', 'Param Insurance Policy')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

     <link rel="stylesheet"
        href="{{ asset('library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Param Insurance Policy </h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-insurance-policy-edit') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('param-insurance-policy.update', $data->id)}}" enctype='multipart/form-data'>
                                        @csrf 
                                        @method('PATCH')

                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Insurance </label>
                                            
                                            <select name="insurance_company_id" class="form-control" id="one">
                                                <option value="" selected disabled hidden>- Select -</option>
                                                @foreach($insCompany as $insCompany)
                                                    <option value="{{$insCompany->id}}" {{ ( $insCompany->id == $data->insurance_company_id) ? 'selected' : '' }}>{{ $insCompany->insurance_product }}</option>
                                                @endforeach
                                            </select>

                                            @error('insurance_company_id')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>



                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Product Disclosure</label>
                                            <input id="inputTitle" type="text" name="product_disclosure" placeholder="Insurance Company"  value="{{old('product_disclosure',  $data->product_disclosure)}}" class="form-control">
                                            @error('product_disclosure')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Disclosure Published</label>
                                            <input id="inputTitle" type="text" name="disclosure_published" placeholder="Disclosure Published"  value="{{old('disclosure_published', $data->disclosure_published)}}" class="form-control datepicker">
                                            @error('disclosure_published')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>



                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Policy Wording</label>
                                            <input id="inputTitle" type="text" name="policy_wording" placeholder="Policy Wording"  value="{{old('policy_wording',  $data->policy_wording)}}" class="form-control">
                                            @error('policy_wording')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Policy Published</label>
                                            <input id="inputTitle" type="text" name="policy_published" placeholder="Policy Published"  value="{{old('policy_published',  $data->policy_published)}}" class="form-control datepicker">
                                            @error('policy_published')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                
                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status <!-- <span class="text-danger">*</span> --> </label>
                                            <select name="status" class="form-control">
                                              <option value="1" {{(($data->status=='1') ? 'selected' : '')}}>Active</option>
                                              <option value="0" {{(($data->status=='0') ? 'selected' : '')}}>Inactive</option>
                                            </select>
                                            @error('status')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                       

                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>


    <!-- JS Libraies -->
    <script src="{{ asset('library/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('library/cleave.js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/forms-advanced-forms.js') }}"></script>


    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
