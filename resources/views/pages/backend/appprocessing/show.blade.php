@extends('layouts.app')

@section('title', 'Pra application')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Application Processing - Financing</h1>
                <div class="section-header-breadcrumb">
                    <!-- <div class="breadcrumb-item active">{{ Breadcrumbs::render('home') }} </div> -->
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('pra-application') }}</div>
                    
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2> -->
                <!--<p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4> {{ $data->reg_no }} ( {{$data->cif->fullname}} ) </h4>
                                
                                <div class="card-header-form">
                                    <!-- <form>
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control"
                                                placeholder="Search">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form> -->
                                </div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <div class="card">
                                            <div class="card-body p-0">
                                                <div class="table-responsive">
                                                    <table class="table-striped table">
                                                        <tr>
                                                            <th>
                                                                <!-- <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                                                    <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                                                </div> -->
                                                            </th>
                                                            <th>Document</th>
                                                            <!-- <th>Progress</th> -->
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>

                                                        <tr>
                                                            <td class="p-0 text-center">
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1" {{ isset($checkDoc) && $checkDoc->check_nirc == 1 ? 'checked' : '' }}>
                                                                    <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                            <td> NRIC</td>
                                                            <!-- <td class="align-middle">
                                                                <div class="progress" data-height="4" data-toggle="tooltip"  title="100%">
                                                                    <div class="progress-bar bg-success" data-width="100"></div>
                                                                </div>
                                                            </td> -->
                                                            <td>
                                                                @if(!empty($doc->nric))
                                                                <div class="badge badge-success">Uploaded</div>
                                                                @else
                                                                <div class="badge badge-danger">Not Uploaded</div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button class="detail-btn btn btn-secondary" data-image="{{ $doc->nric }}">Show</button>
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td class="p-0 text-center">
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-2" {{ isset($checkDoc) && $checkDoc->check_payslip == 1 ? 'checked' : '' }}>
                                                                    <label for="checkbox-2" class="custom-control-label">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                            <td>PaySlip</td>
                                                            <!-- <td class="align-middle">
                                                                <div class="progress" data-height="4" data-toggle="tooltip" title="0%">
                                                                    <div class="progress-bar" data-width="0"></div>
                                                                </div>
                                                            </td> -->
                                                            
                                                            <td>
                                                                @if(!empty($doc->payslip))
                                                                <div class="badge badge-success">Uploaded</div>
                                                                @else
                                                                <div class="badge badge-danger">Not Uploaded</div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button class="detail-btn btn btn-secondary" data-image="{{ $doc->payslip }}">Show</button>
                                                                
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td class="p-0 text-center">
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-3" {{ isset($checkDoc) && $checkDoc->check_bpa179 == 1 ? 'checked' : '' }}>
                                                                    <label for="checkbox-3"
                                                                        class="custom-control-label">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                            <td>BPA 1/79</td>
                                                            <!-- <td class="align-middle">
                                                                <div class="progress" data-height="4" data-toggle="tooltip" title="70%">
                                                                    <div class="progress-bar bg-warning" data-width="70"></div>
                                                                </div>
                                                            </td> -->
                                                            
                                                            <td>
                                                                @if(!empty($doc->bpa179))
                                                                <div class="badge badge-success">Uploaded</div>
                                                                @else
                                                                <div class="badge badge-danger">Not Uploaded</div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button class="detail-btn btn btn-secondary" data-image="{{ $doc->bpa179 }}">Show</button>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="p-0 text-center">
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-4" {{ isset($checkDoc) && $checkDoc->check_eligibility_calculation == 1 ? 'checked' : '' }}>
                                                                    <label for="checkbox-4" class="custom-control-label">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                            <td>Eligibility Calculation</td>
                                                            <!-- <td class="align-middle">
                                                                <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                                                    <div class="progress-bar bg-success" data-width="100"></div>
                                                                </div>
                                                            </td>-->
                                                            
                                                            <td>
                                                                
                                                                <div class="badge badge-success">Completed</div>
                                                                
                                                            </td>

                                                            <td>
                                                                <button class="detail-btn btn btn-secondary" data-image="{{ $doc->bpa179XX }}">Show</button>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>

                                                <form method="post" action="{{route('app-processing.update', $data->uuid)}}" enctype='multipart/form-data'>
                                                    @csrf 
                                                    @method('PATCH')

                                                    <button class="btn btn-success" type="submit">Submit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-6">
                                        
                                        <div class="image-display">
                                            <div id="display-area"></div>
                                        </div>

                                        <!-- Hidden table to be shown if not an image or PDF -->
                                        <div class="additional-data" id="additional-data" style="display: none;">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <th>Pendapatan</th>
                                                        <th></th>
                                                        <th>RM</th>
                                                        <th></th>
                                                        <th>Potongan</th>
                                                        <th></th>
                                                        <th>RM</th>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="7"></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="7">Tetap</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Gaji Pokok</th>
                                                        <th></th>
                                                        <th>{{$gaji->gaji_pokok}}</th>
                                                        <th></th>
                                                        <th>PCB</th>
                                                        <th></th>
                                                        <th>{{$pot->pcb}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Imbuhan Tetap Perumahan</th>
                                                        <th></th>
                                                        <th>{{$gaji->imbuhan_tetap_perumahan}}</th>
                                                        <th></th>
                                                        <th>PERKESO</th>
                                                        <th></th>
                                                        <th>{{$pot->perkeso}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Imbuhan Tetap Khidmat Awam</th>
                                                        <th></th>
                                                        <th>{{$gaji->imbuhan_tetap_khidmat_awam}}</th>
                                                        <th></th>
                                                        <th>Cukai Pendapatan</th>
                                                        <th></th>
                                                        <th>{{$pot->cukai_pendapatan}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Bantuan Sara Hidup</th>
                                                        <th></th>
                                                        <th>{{$gaji->bantuan_sara_hidup}}</th>
                                                        <th></th>
                                                        <th>Pinjaman Perumahan</th>
                                                        <th></th>
                                                        <th>{{$pot->pinjaman_perumahan}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Bayaran Insentif Tugas Khas</th>
                                                        <th></th>
                                                        <th>{{$gaji->bayaran_insentif_tugas_khas}}</th>
                                                        <th></th>
                                                        <th>Zakat</th>
                                                        <th></th>
                                                        <th>{{$pot->zakat}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Perkhidmatan Kritikal</th>
                                                        <th></th>
                                                        <th>{{$gaji->perkhidmatan_kritikal}}</th>
                                                        <th></th>
                                                        <th>Lembaga Tabung Haji</th>
                                                        <th></th>
                                                        <th>{{$pot->lemabaga_tabung_haji}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Elaun Tetap (lain-lain)</th>
                                                        <th></th>
                                                        <th>{{$gaji->elaun_tetap_lain_lain}}</th>
                                                        <th></th>
                                                        <th>Pembiayaan Perumahan/Lain-lain</th>
                                                        <th></th>
                                                        <th>{{$pot->pembiayaan_perumahan_lain_lain}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Insentif </th>
                                                        <th></th>
                                                        <th>{{$gaji->insentif}}</th>
                                                        <th></th>
                                                        <th>Angkasa</th>
                                                        <th></th>
                                                        <th>{{$pot->angkasa}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Penanggungan Kerja </th>
                                                        <th></th>
                                                        <th>{{$gaji->penanggungan_kerja}}</th>
                                                        <th></th>
                                                        <th>Koperasi </th>
                                                        <th></th>
                                                        <th>{{$pot->koperasi}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Piutang gaji & elaun </th>
                                                        <th></th>
                                                        <th>{{$pot->piutang_gaji_elaun}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Angkasa bukan pinjaman </th>
                                                        <th></th>
                                                        <th>{{$pot->angkasa_bukan_pinjaman}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>YIR </th>
                                                        <th></th>
                                                        <th>{{$pot->yir}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>YIR 2 </th>
                                                        <th></th>
                                                        <th>{{$pot->yir2}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Jumlah Pendapatan (A) </th>
                                                        <th></th>
                                                        <th>{{$gaji->jumlah_pendapatan}}</th>
                                                        <th></th>
                                                        <th>Jumlah Potongan  </th>
                                                        <th></th>
                                                        <th>{{$pot->jumlah_potongan}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Jumlah Bersih  </th>
                                                        <th></th>
                                                        <th>{{$gaji->jml_bersih}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>  </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>  </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>

                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Percentage  </th>
                                                        <th></th>
                                                        <th>{{$gaji->percentage}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Max eligibility  </th>
                                                        <th></th>
                                                        <th>{{$gaji->max_eligibility}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Available deduction  </th>
                                                        <th></th>
                                                        <th>{{$gaji->available_deduction}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>(B)Jumlah Potongan</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th>{{$pot->jumlah_potongan}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>DSR</th>
                                                        <th>=</th>
                                                        <th>{{$pot->jumlah_potongan}}</th>
                                                        <th>x 100</th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th>{{$gaji->jumlah_pendapatan}}</th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th>=</th>
                                                        <th>{{$dsr->dsr_baru}}</th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Pembiayaan Baru (C) :</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>If Takaful coverage</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>If Roadtax</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Total</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th> </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Tempoh</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>bulan </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Kadar pulangan</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>% (tetap) </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Ansuran bulanan RM</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>DSR BARU PEMOHON</th>
                                                        <th>=</th>
                                                        <th></th>
                                                        <th>x</th>
                                                        <th>100</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th>=</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    
                                                    {{-- @foreach($additionalData as $data)
                                                    <tr>
                                                        <td>{{ $data->id }}</td>
                                                        <td>{{ $data->member_1 }}</td>
                                                        <td>{{ $data->created_at }}</td>
                                                    </tr>
                                                    @endforeach --}}
                                                </table>
                                            </div>
                                        </div>
                                        

                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('style')
    
    <style>
        #display-image {
            max-width: 100%;
            max-height: 100%;
        }
    </style>

@endpush

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

    <script>

        document.addEventListener('DOMContentLoaded', function() {
            document.querySelectorAll('.detail-btn').forEach(button => {
                button.addEventListener('click', function() {
                    const base64String = this.getAttribute('data-image');
                    const displayArea = document.getElementById('display-area');
                    const additionalData = document.getElementById('additional-data');

                    if (base64String) {
                        const mimeType = base64String.match(/^data:(.*?);base64,/)[1];

                        if (mimeType === 'application/pdf') {
                            displayArea.innerHTML = `<embed src="${base64String}" type="application/pdf" width="100%" height="600px">`;
                            additionalData.style.display = 'none';
                        } else if (mimeType.startsWith('image/')) {
                            displayArea.innerHTML = `<img src="${base64String}" alt="Detail Image" style="max-width: 100%; max-height: 100%;">`;
                            additionalData.style.display = 'none';
                        } else {
                            displayArea.innerHTML = 'Unsupported file type';
                            additionalData.style.display = 'table';
                        }
                    } else {
                        displayArea.innerHTML = '';
                        additionalData.style.display = 'table'; // Show the additional data table
                    }
                });
            });
        });


    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            document.querySelectorAll('.custom-control-input').forEach(checkbox => {
                checkbox.addEventListener('change', function() {
                    const documentId = this.id.split('-')[1];
                    const documentTypeElement = this.closest('tr').querySelector('td:nth-child(2)');

                    if (documentTypeElement) {
                        const documentType = documentTypeElement.innerText;
                        const isChecked = this.checked;
                        const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

                        if (csrfToken) {
                            fetch("{{route('app-processing.store')}}", {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': csrfToken
                                },
                                body: JSON.stringify({
                                    id: documentId,
                                    type: documentType,
                                    uuid: '{{$data->uuid}}',
                                    status: isChecked ? '1' : '0'
                                })
                            })
                            .then(response => response.json())
                            .then(data => {
                                if (data.success) {
                                    alert('Document status updated successfully');
                                } else {
                                    alert('Failed to update document status');
                                }
                            })
                            .catch(error => {
                                console.error('Error:', error);
                                alert('Document status updated successfully');
                            });
                        } else {
                            console.error('CSRF token not found');
                        }
                    } else {
                        console.error('Document type element not found');
                    }
                });
            });
        });



    </script>


@endpush
