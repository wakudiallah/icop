@extends('layouts.app')

@section('title', 'Roles')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Roles</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('roles-edit') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="POST" action="{{ route('roles.update', $role->id) }}">
                                        @method('patch')
                                        @csrf
                                        <div class="mb-3">
                                            <label for="name" class="form-label">Name</label>
                                            <input value="{{ $role->name }}" 
                                                type="text" 
                                                class="form-control" 
                                                name="name" 
                                                placeholder="Name" required>
                                        </div>
                                        
                                        <label for="permissions" class="form-label">Assign Permissions</label>
                              
                                        <table class="table table-striped">
                                            <thead>
                                                <th scope="col" width="1%"><input type="checkbox" name="all_permission"></th>
                                                <th scope="col" width="20%">Name</th>
                                                <th scope="col" width="1%">Guard</th> 
                                            </thead>
                              
                                            @foreach($permissions as $permission)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" 
                                                        name="permission[{{ $permission->name }}]"
                                                        value="{{ $permission->name }}"
                                                        class='permission'
                                                        {{ in_array($permission->name, $rolePermissions) 
                                                            ? 'checked'
                                                            : '' }}>
                                                    </td>
                                                    <td>{{ $permission->name }}</td>
                                                    <td>{{ $permission->guard_name }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                              
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                        <a href="{{ route('roles.index') }}" class="btn btn-default">Back</a>
                                    </form>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>


  
  <script type="text/javascript">
    $(document).ready(function() {
        $('[name="all_permission"]').on('click', function() {

            if($(this).is(':checked')) {
                $.each($('.permission'), function() {
                    $(this).prop('checked',true);
                });
            } else {
                $.each($('.permission'), function() {
                    $(this).prop('checked',false);
                });
            }
            
        });
    });
</script>

@endpush
