@extends('layouts.app')

@section('title', 'Role')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Role</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('roles') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4><a href="{{ route('roles.create') }}" class="btn btn-primary mr-1">+ Add</a></h4>

                                <div class="card-header-form">
                                    
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    <table class="table-striped table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Id Role</th>
                                                <th>Name</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php $i=1; ?>
                                            @foreach($data as $data)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $data->id }}</td>
                                                <td>{{ $data->name }}</td>
                                                
                                                <td>
                                                    <a href="{{ route('roles.show', $data->id) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                                    <a class="btn btn-primary btn-sm" href="{{ route('roles.edit', $data->id) }}"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody> 
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
