@extends('layouts.app')

@section('title', 'User')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>User</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('users-create') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('users.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}
                                        <div class="form-group">
                                          <label for="inputTitle" class="col-form-label">Name</label>
                                        <input id="inputTitle" type="text" name="name" placeholder="Enter name"  value="{{old('name')}}" class="form-control">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-form-label">Email</label>
                                          <input id="inputEmail" type="email" name="email" placeholder="Enter email"  value="{{old('email')}}" class="form-control">
                                          @error('email')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-form-label">Password</label>
                                          <input id="inputPassword" type="password" name="password" placeholder="Enter password"  value="{{old('password')}}" class="form-control">
                                          @error('password')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                        </div>
                                
                                        
                                        <div class="form-group">
                                            <label for="role" class="col-form-label">Role</label>
                                            <select name="role" class="form-control" id="one">
                                                <option value="" selected disabled hidden>- Select -</option>
                                                @foreach($roles as $roles)
                                                    <option value="{{$roles->id}}">{{$roles->name}}</option>
                                                @endforeach
                                            </select>
                                          @error('role')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                          </div>
                                
                                
                                          <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                          @error('status')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                          </div>
                                        <div class="form-group mb-3">
                                          <button type="reset" class="btn btn-warning">Reset</button>
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>
                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
