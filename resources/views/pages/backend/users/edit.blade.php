@extends('layouts.app')

@section('title', 'User')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>User</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('users-edit') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    
                                    <form method="post" action="{{route('users.update',$data->id)}}" enctype='multipart/form-data'>
                                        @csrf 
                                        @method('PATCH')

                                        <div class="form-group">
                                          <label for="inputTitle" class="col-form-label">Name</label>
                                        <input id="inputTitle" type="text" name="name" value="{{$data->name}}" placeholder="Enter name" class="form-control">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-form-label">Email</label>
                                          <input id="inputEmail" type="email" name="email" value="{{$data->email}}" placeholder="Enter email"  class="form-control">
                                          @error('email')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-form-label">Password</label>
                                          <input id="inputPassword" type="text" name="password"  placeholder="Enter password"  class="form-control">
                                          @error('password')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                        </div>

                                        <!-- $2y$10$qPGQ64zLSg6tE7ujSu9Ei.P117Z7uHjqKNKgn7mWRRiCf9RDm9R1m -->
                                
                                        
                                        <div class="form-group">
                                            <label for="role" class="col-form-label">Role</label>
                                            <select name="role" class="form-control" id="one">
                                                <option value="" selected disabled hidden>- Select -</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" {{(($role->id == $data->role) ? 'selected' : '')}}>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                          @error('role')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                          </div>
                                
                                
                                          <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1" {{(($data->status=='active') ? 'selected' : '')}}>Active</option>
                                                <option value="0" {{(($data->status=='inactive') ? 'selected' : '')}}>Inactive</option>
                                            </select>
                                          @error('status')
                                          <span class="text-danger">{{$message}}</span>
                                          @enderror
                                          </div>
                                        <div class="form-group mb-3">
                                          <button type="reset" class="btn btn-warning">Reset</button>
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>
                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
