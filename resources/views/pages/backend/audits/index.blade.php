@extends('layouts.app')

@section('title', 'Permission')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">

      @include('sweetalert::alert')
      
        <section class="section">
            <div class="section-header">
                <h1>Audit Trail</h1>
                <div class="section-header-breadcrumb">
                    
                    
                  <div class="breadcrumb-item">{{ Breadcrumbs::render('audit-trail') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                
                                <div class="card-header-form">
                                   
                                </div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                                        <thead>
                                          <tr>
                                            <th>No</th>
                                            <th>User</th>
                                            <th>Event</th>
                                            <th>Auditable Type</th>
                                            <th>Old Values</th>
                                            <th>New Values</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <?php $i=1; ?>
                                          @foreach($audit as $data)
                                            <tr>
                                              <td>{{$i++}}</td>
                                              <td>@if(!empty($data->audit_user->name))
                                                {{$data->audit_user->name}}
                                                @endif
                                              </td>
                                              <td>{{$data->event}}</td>
                                              <td>{{$data->auditable_type}}</td>
                                              <td>{{$data->old_values}}</td>
                                              <td>{{$data->new_values}}</td>
                                              <td>
                                               
                              
                                                @can('audit-trail-delete')
                                                  <form method="POST" action="{{url('audit-trail/destroy/'.$data->id)}}">
                                                    @csrf 
                                                        <button class="btn btn-danger btn-sm dltBtn" data-id={{$data->id}} style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                                  </form>
                                                @endcan
                                              </td>
                                            </tr>
                                          @endforeach
                                        <tbody>
                                         
                                        </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
