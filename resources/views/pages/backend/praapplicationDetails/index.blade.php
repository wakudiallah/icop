@extends('layouts.app')

@section('title', 'Pra application')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Pra Application - Details</h1>
                <div class="section-header-breadcrumb">
                    <!-- <div class="breadcrumb-item active">{{ Breadcrumbs::render('home') }} </div> -->
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('pra-application') }}</div>
                    
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</h3> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4></h4>
                                
                                <div class="card-header-form">
                                    
                                </div>
                            </div> 

                            <div class="card-body">
                                
                                        

                                        <h3>ZurichApiCalculatePremium_AllowableCartAmount</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Cart Amount</th>
                                                            <th>Cart Amount Ind</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($a as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->cart_amount }}</td>
                                                            <td>{{ $item->cart_amount_ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>

                                        <h3>ZurichApiCalculatePremium_AllowableCartDay</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Cart Days</th>
                                                            <th>Cart Days Ind</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($b as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->cart_days }}</td>
                                                            <td>{{ $item->cart_days_ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>
                                        <h3>ZurichApiCalculatePremium_AllowableKeyReplacement</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Key Replacement SI</th>
                                                            <th>Key Replacement SI Ind</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($c as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->key_Replacement_SI }}</td>
                                                            <td>{{ $item->key_Replacement_SI_Ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>
                                        <h3>ZurichApiCalculatePremium_AllowablePaBasicUnit</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>PA Basic Unit</th>
                                                            <th>PA Basic Unit Ind</th>
                                                            <th>PA Basic SI</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($d as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->pA_Basic_Unit }}</td>
                                                            <td>{{ $item->pA_Basic_Unit_Ind }}</td>
                                                            <td>{{ $item->pA_Basic_SI }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>

                                        <h3>ZurichApiCalculatePremium_AllowablePaExtendedCoverage</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>PA C Ext Cvr Code</th>
                                                            <th>PA C Ext Cvr Desc</th>
                                                            <th>Indicator</th>
                                                            <th>Min Units</th>
                                                            <th>Max Units</th>
                                                            <th>Additional Insured Person Indicator</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($e as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->paC_Ext_Cvr_Code }}</td>
                                                            <td>{{ $item->paC_Ext_Cvr_Desc }}</td>
                                                            <td>{{ $item->ind }}</td>
                                                            <td>{{ $item->min_Units }}</td>
                                                            <td>{{ $item->max_Units }}</td>
                                                            <td>{{ $item->additional_Insured_Person_Ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>
                                        
                                        
                                        <hr>

                                        <h3>ZurichApiCalculatePremium_AllowableVoluntaryExcess</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Voluntary Excess</th>
                                                            <th>Voluntary Excess Ind</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($f as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->voluntary_Excess }}</td>
                                                            <td>{{ $item->voluntary_Excess_Ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>
                                        <h3>ZurichApiCalculatePremium_AllowableWaterDamageCoverage</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Water Damage SI</th>
                                                            <th>Water Damage SI Ind</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($g as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->water_Damage_SI }}</td>
                                                            <td>{{ $item->water_Damage_SI_Ind }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <hr>
                                        

                                        <h3>ZurichApiCalculatePremium_CarReplacementDay</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Car Replacement Indicator</th>
                                                            <th>Car Replacement Days</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($h as $item)
                                                        <tr>
                                                            <td>{{ $item->uuid }}</td>
                                                            <td>{{ $item->car_Replacement_Ind }}</td>
                                                            <td>{{ $item->car_Replacement_Days }}</td>
                                                            <td>{{ $item->created_at }}</td>
                                                            <td>{{ $item->updated_at }}</td>
                                                            <td>{{ $item->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <h3>ZurichApiCalculatePremium_ErrorDisplay</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Error Code</th>
                                                            <th>Error Description</th>
                                                            <th>Severity</th>
                                                            <th>Remarks</th>
                                                            <th>Warning Indicator</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($i as $error)
                                                        <tr>
                                                            <td>{{ $error->uuid }}</td>
                                                            <td>{{ $error->error_Code }}</td>
                                                            <td>{{ $error->error_Desc }}</td>
                                                            <td>{{ $error->severity }}</td>
                                                            <td>{{ $error->remarks }}</td>
                                                            <td>{{ $error->warning_Ind }}</td>
                                                            <td>{{ $error->created_at }}</td>
                                                            <td>{{ $error->updated_at }}</td>
                                                            <td>{{ $error->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>



                                        <h3>ZurichApiCalculatePremium_paCVehicleDetail</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Vehicle Number</th>
                                                            <th>Number of Seats</th>
                                                            <th>Is Policyholder</th>
                                                            <th>PA Coverage SI</th>
                                                            <th>Annual Basic Premium</th>
                                                            <th>Annual Additional Seats Premium</th>
                                                            <th>Benefit Schedule Code</th>
                                                            <th>Medical Plan Entry Code</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($j as $vehicle)
                                                        <tr>
                                                            <td>{{ $vehicle->uuid }}</td>
                                                            <td>{{ $vehicle->vehicle_Number }}</td>
                                                            <td>{{ $vehicle->no_of_Seats }}</td>
                                                            <td>{{ $vehicle->is_Policyholder ? 'Yes' : 'No' }}</td>
                                                            <td>{{ $vehicle->paC_SI }}</td>
                                                            <td>{{ $vehicle->paC_Annual_Basic_Premium }}</td>
                                                            <td>{{ $vehicle->paC_Annual_Additional_Seats_Premium }}</td>
                                                            <td>{{ $vehicle->paC_Benefit_Schedule_Code }}</td>
                                                            <td>{{ $vehicle->paC_Medical_Plan_Entry_Code }}</td>
                                                            <td>{{ $vehicle->created_at }}</td>
                                                            <td>{{ $vehicle->updated_at }}</td>
                                                            <td>{{ $vehicle->deleted_at }}</td>
                                                            
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>


                                        <h3>ZurichApiCalculatePremium_premiumDetail</h3>

                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Excess Type</th>
                                                            <th>Vol Excess Type Applied</th>
                                                            <th>Excess Amount</th>
                                                            <th>Vol Excess Amount</th>
                                                            <th>Total Excess Amount</th>
                                                            <th>Package Applicable</th>
                                                            <th>Package Premium</th>
                                                            <th>Package Selected</th>
                                                            <th>Vehicle Model Code Out</th>
                                                            <th>Annual Base Premium Adjustment</th>
                                                            <th>Annual Schedule Premium</th>
                                                            <th>Annual Loading Amount</th>
                                                            <th>Annual Tuition Purposes Amount</th>
                                                            <th>Annual Tariff Premium</th>
                                                            <th>Annual All Riders</th>
                                                            <th>Annual NCD Amount</th>
                                                            <th>Annual Voluntary Excess Amount</th>
                                                            <th>Annual Total Extra Cover Premium</th>
                                                            <th>Annual Renewal Bonus Amount</th>
                                                            <th>Basic Annual Premium</th>
                                                            <th>Annual Act Base Premium Adjustment</th>
                                                            <th>Annual Act Schedule Premium</th>
                                                            <th>Annual Act Loading Amount</th>
                                                            <th>Annual Act Tuition Purposes Amount</th>
                                                            <th>Annual Act Tariff Premium</th>
                                                            <th>Annual Act All Riders</th>
                                                            <th>Annual Act NCD Amount</th>
                                                            <th>Annual Act Voluntary Excess Amount</th>
                                                            <th>Annual Act Total Extra Cover Premium</th>
                                                            <th>Annual Act Renewal Bonus Amount</th>
                                                            <th>Annual Act Premium</th>
                                                            <th>Basic Net Act</th>
                                                            <th>Act Premium</th>
                                                            <th>Basic Net Non-Act</th>
                                                            <th>Non-Act Premium</th>
                                                            <th>Commission Amount</th>
                                                            <th>Commission Percentage</th>
                                                            <th>GST on Commission Amount</th>
                                                            <th>Net Premium</th>
                                                            <th>Basic Premium Adjustment</th>
                                                            <th>Basic Premium Adjustment Percentage</th>
                                                            <th>Schedule Premium 0</th>
                                                            <th>Basic Premium</th>
                                                            <th>Trailer Premium</th>
                                                            <th>Load Amount</th>
                                                            <th>Trailer Loading Amount</th>
                                                            <th>Load Percentage</th>
                                                            <th>Trailer Loading Per</th>
                                                            <th>Tuition Load Amount</th>
                                                            <th>Trailer Tuition Load Amount</th>
                                                            <th>Tuition Load Percentage</th>
                                                            <th>Trailer Tuition Load Percentage</th>
                                                            <th>All Rider Amount</th>
                                                            <th>NCD Amount</th>
                                                            <th>Vol Excess Discount Amount</th>
                                                            <th>Vol Excess Discount Percentage</th>
                                                            <th>Total Basic Net Amount</th>
                                                            <th>Total Extra Cover Premium</th>
                                                            <th>Renewal Bonus Amount</th>
                                                            <th>Renewal Bonus Percentage</th>
                                                            <th>Basic Premium</th>
                                                            <th>Rebate Amount</th>
                                                            <th>Rebate Percentage</th>
                                                            <th>Gross Premium</th>
                                                            <th>GST Amount</th>
                                                            <th>GST Percentage</th>
                                                            <th>SST Amount</th>
                                                            <th>SST Percentage</th>
                                                            <th>Stamp Duty Amount</th>

                                                            <th>Total Motor Premium</th>
                                                            <th>PTV Amount</th>
                                                            <th>Total Payable Premium</th>



                                                            <th>PAC Max Additional Vehicle</th>
                                                            <th>PAC Multi Product Amount</th>
                                                            <th>PAC Multi Product Percentage</th>
                                                            <th>PAC Rebate Amount</th>
                                                            <th>PAC Rebate Percentage</th>
                                                            <th>PAC GST Amount</th>
                                                            <th>PAC GST Percentage</th>
                                                            <th>PAC Stamp Duty</th>
                                                            <th>PAC Total Premium</th>
                                                            <th>PAC Commission Amount</th>
                                                            <th>PAC Commission Percentage</th>
                                                            <th>PAC GST on Commission Amount</th>
                                                            <th>PAC Sum Insured</th>
                                                            <th>PAC Gross Premium</th>
                                                            <th>PAC Net Premium</th>
                                                            <th>PAC Minimum Premium</th>

                                                            <th>PAC Premium</th>
                                                            <th>PAC Additional Premium</th>
                                                            <th>PAC Additional Vehicle Premium</th>
                                                            <th>PAC Extra Cover</th>
                                                            <th>Renewal Cap Indicator</th>
                                                            <th>Basic Without Renewal Cap</th>
                                                            <th>Tariff Premium Pure</th>
                                                            <th>Tariff Premium</th>
                                                            <th>Detariff Premium Adjustment</th>
                                                            <th>Uncapped Detariff Premium</th>
                                                            <th>Loading Group</th>
                                                            <th>Detariff Lower Bound</th>
                                                            <th>Detariff Upper Bound</th>
                                                            <th>Chosen Vehicle SI Min Deviation</th>
                                                            <th>Chosen Vehicle SI Max Deviation</th>
                                                            <th>Chosen Vehicle SI Lower Bound</th>
                                                            <th>Chosen Vehicle SI Upper Bound</th>
                                                            <th>Loading Overwrite Indicator</th>
                                                            <th>PIA Blacklist Vehicle No Indicator</th>
                                                            <th>Comp Blacklist Vehicle No Indicator</th>
                                                            <th>Comp Blacklist Client Indicator</th>
                                                            <th>ABI Check Indicator</th>
                                                            <th>Allowable Motor Rebate Indicator</th>
                                                            <th>Allowable PAC Rebate Indicator</th>
                                                            <th>Calculate Method</th>
                                                            <th>Motor Min Premium Indicator</th>
                                                            <th>ABI Adopt Indicator</th>
                                                            <th>Motor Levy Percentage</th>
                                                            <th>Motor Levy Amount</th>
                                                            <th>Recommended Vehicle SI Type</th>
                                                            <th>Manual Benchmark SI Display</th>
                                                            <th>Manual Benchmark SI Text</th>
                                                            <th>Manual Benchmark SI Editable</th>
                                                            <th>Vehicle Use Code</th>
                                                            <th>AV SI Value</th>
                                                            <th>REC SI Value</th>
                                                            <th>REC SI Text</th>
                                                            <th>Vehicle SI Editable</th>
                                                            <th>Quote Expiry Date</th>
                                                            <th>Vehicle Class</th>
                                                            <th>AV SI Lower Bound</th>
                                                            <th>AV SI Upper Bound</th>
                                                            <th>MV SI Lower Bound</th>
                                                            <th>MV SI Upper Bound</th>
                                                            <th>ISM Cover Type</th>
                                                            <th>NCD Class</th>
                                                            <th>NCD Applicable Indicator</th>
                                                            <th>DDL Agree Value</th>
                                                            <th>REC Vehicle SI</th>
                                                            <th>PAC SI Per Seat</th>
                                                            <th>Total ZDA Premium</th>
                                                            <th>ZDA Add On</th>
                                                            <th>PAC Type Offered</th>
                                                            <th>PAC Description</th>
                                                            <th>PAC Max Units</th>
                                                            <th>PAC Min Units</th>
                                                            <th>REC PAC Min Units</th>
                                                            <th>PAC Compulsory Indicator</th>
                                                            <th>PAC Type Applicable</th>
                                                            <th>Wakalah Commission MTR Percentage</th>
                                                            <th>Wakalah Commission MTR Amount</th>
                                                            <th>Wakalah Expense MTR Percentage</th>
                                                            <th>Wakalah Expense MTR Amount</th>
                                                            <th>Wakalah Fee MTR Percentage</th>
                                                            <th>Wakalah Fee MTR Amount</th>
                                                            <th>Wakalah Commission PA Percentage</th>
                                                            <th>Wakalah Commission PA Amount</th>
                                                            <th>Wakalah Expense PA Percentage</th>
                                                            <th>Wakalah Expense PA Amount</th>
                                                            <th>Wakalah Fee PA Percentage</th>
                                                            <th>Wakalah Fee PA Amount</th>
                                                            <th>Referral Data</th>
                                                            <th>Error Details</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($k as $data)
                                                        <tr>
                                                            <td>{{ $data->uuid }}</td>
                                                            <td>{{ $data->excessType }}</td>
                                                            <td>{{ $data->volExcessTypeApplied }}</td>
                                                            <td>{{ $data->excessAmt }}</td>
                                                            <td>{{ $data->volExcessAmt }}</td>
                                                            <td>{{ $data->totExcessAmt }}</td>
                                                            <td>{{ $data->package_Applicable }}</td>
                                                            <td>{{ $data->package_Prem }}</td>
                                                            <td>{{ $data->package_Selected }}</td>
                                                            <td>{{ $data->vehicle_Model_Code_Out }}</td>
                                                            <td>{{ $data->annual_BasePremAdjustment }}</td>
                                                            <td>{{ $data->annual_Schedule_Premium }}</td>
                                                            <td>{{ $data->annual_Loading_Amount }}</td>
                                                            <td>{{ $data->annual_Tuition_Purposes_Amount }}</td>
                                                            <td>{{ $data->annual_Tariff_Premium }}</td>
                                                            <td>{{ $data->annual_All_Riders }}</td>
                                                            <td>{{ $data->annual_NCD_Amount }}</td>
                                                            <td>{{ $data->annual_Voluntary_Excess_Amount }}</td>
                                                            <td>{{ $data->annual_Total_Extra_Cover_Premium }}</td>
                                                            <td>{{ $data->annual_Renewal_Bonus_Amount }}</td>
                                                            <td>{{ $data->basicAnnualPrem }}</td>
                                                            <td>{{ $data->annual_Act_BasePremAdjustment }}</td>
                                                            <td>{{ $data->annual_Act_Schedule_Premium }}</td>
                                                            <td>{{ $data->annual_Act_Loading_Amount }}</td>
                                                            <td>{{ $data->annual_Act_Tuition_Purposes_Amount }}</td>
                                                            <td>{{ $data->annual_Act_Tariff_Premium }}</td>
                                                            <td>{{ $data->annual_Act_All_Riders }}</td>
                                                            <td>{{ $data->annual_Act_NCD_Amount }}</td>
                                                            <td>{{ $data->annual_Act_Voluntary_Excess_Amount }}</td>
                                                            <td>{{ $data->annual_Act_Total_Extra_Cover_Premium }}</td>
                                                            <td>{{ $data->annual_Act_Renewal_Bonus_Amount }}</td>
                                                            <td>{{ $data->annual_Act_Premium }}</td>
                                                            <td>{{ $data->basicNetAct }}</td>
                                                            <td>{{ $data->actPrem }}</td>
                                                            <td>{{ $data->basicNetNonAct }}</td>
                                                            <td>{{ $data->nonActPrem }}</td>
                                                            <td>{{ $data->commAmt }}</td>
                                                            <td>{{ $data->commPct }}</td>
                                                            <td>{{ $data->gstOnCommAmt }}</td>
                                                            <td>{{ $data->nettPrem }}</td>
                                                            <td>{{ $data->basic_Premium_Adjustment }}</td>
                                                            <td>{{ $data->basic_Premium_Adjustment_Pct }}</td>
                                                            <td>{{ $data->schedule_Premium_0 }}</td>
                                                            <td>{{ $data->basicPrem }}</td>
                                                            <td>{{ $data->trailerPremium }}</td>
                                                            <td>{{ $data->loadAmt }}</td>
                                                            <td>{{ $data->trailer_Loading_Amount }}</td>
                                                            <td>{{ $data->loadPct }}</td>
                                                            <td>{{ $data->trlLoadingPer }}</td>
                                                            <td>{{ $data->tuitionLoadAmt }}</td>
                                                            <td>{{ $data->trailerTuitionLoadAmt }}</td>
                                                            <td>{{ $data->tuitionLoadPct }}</td>
                                                            <td>{{ $data->trailerTuitionLoadPer }}</td>
                                                            <td>{{ $data->allRiderAmt }}</td>
                                                            <td>{{ $data->ncdAmt }}</td>
                                                            <td>{{ $data->volExcessDiscAmt }}</td>
                                                            <td>{{ $data->volExcessDiscPct }}</td>
                                                            <td>{{ $data->totBasicNetAmt }}</td>
                                                            <td>{{ $data->totExtCoverPrem }}</td>
                                                            <td>{{ $data->rnwBonusAmt }}</td>
                                                            <td>{{ $data->rnwBonusPct }}</td>
                                                            <td>{{ $data->basic_Premium }}</td>
                                                            <td>{{ $data->rebateAmt }}</td>
                                                            <td>{{ $data->rebatePct }}</td>
                                                            <td>{{ $data->grossPrem }}</td>
                                                            <td>{{ $data->gsT_Amt }}</td>
                                                            <td>{{ $data->gsT_Pct }}</td>
                                                            <td>{{ $data->ssT_Amt }}</td>
                                                            <td>{{ $data->ssT_Pct }}</td>
                                                            <td>{{ $data->stampDutyAmt }}</td>
                                                            <td>{{ $data->totMtrPrem }}</td>
                                                            <td>{{ $data->ptV_Amt }}</td>
                                                            <td>{{ $data->ttlPayablePremium }}</td>
                                                            <td>{{ $data->paC_Max_Additional_Vehicle }}</td>
                                                            <td>{{ $data->paC_MultiProductAmt }}</td>
                                                            <td>{{ $data->paC_MultiProductPct }}</td>
                                                            <td>{{ $data->paC_RebateAmt }}</td>
                                                            <td>{{ $data->paC_RebatePct }}</td>
                                                            <td>{{ $data->paC_GSTAmt }}</td>
                                                            <td>{{ $data->paC_GSTPct }}</td>
                                                            <td>{{ $data->paC_StampDuty }}</td>
                                                            <td>{{ $data->paC_TotPrem }}</td>
                                                            <td>{{ $data->paC_CommAmt }}</td>
                                                            <td>{{ $data->paC_CommPct }}</td>
                                                            <td>{{ $data->paC_GstOnCommAmt }}</td>
                                                            <td>{{ $data->paC_SumInsured }}</td>
                                                            <td>{{ $data->paC_GrossPrem }}</td>
                                                            <td>{{ $data->paC_NettPrem }}</td>
                                                            <td>{{ $data->paC_MinPrem }}</td>
                                                            <td>{{ $data->paC_Prem }}</td>
                                                            <td>{{ $data->paC_AddPrem }}</td>
                                                            <td>{{ $data->paC_AddVehPrem }}</td>
                                                            <td>{{ $data->pacExtraCover }}</td>
                                                            <td>{{ $data->renewal_Cap_Ind }}</td>
                                                            <td>{{ $data->basic_Without_Ren_Cap }}</td>
                                                            <td>{{ $data->tariff_Premium_Pure }}</td>
                                                            <td>{{ $data->tariff_Premium }}</td>
                                                            <td>{{ $data->detariff_Premium_Adjustment }}</td>
                                                            <td>{{ $data->uncapped_Detariff_Premium }}</td>
                                                            <td>{{ $data->loading_Group }}</td>
                                                            <td>{{ $data->detariff_Lower_Bound }}</td>
                                                            <td>{{ $data->detariff_Upper_Bound }}</td>
                                                            <td>{{ $data->chosen_Vehicle_SI_Min_Deviation }}</td>
                                                            <td>{{ $data->chosen_Vehicle_SI_Max_Deviation }}</td>
                                                            <td>{{ $data->chosen_Vehicle_SI_Lower_Bound }}</td>
                                                            <td>{{ $data->chosen_Vehicle_SI_Upper_Bound }}</td>
                                                            <td>{{ $data->loading_Overwrite_Ind }}</td>
                                                            <td>{{ $data->piaM_Blacklist_Veh_No_Ind }}</td>
                                                            <td>{{ $data->comp_BL_Veh_No_Ind }}</td>
                                                            <td>{{ $data->comp_BL_Client_Ind }}</td>
                                                            <td>{{ $data->abI_Check_Ind }}</td>
                                                            <td>{{ $data->allowable_Motor_Rebate_Ind }}</td>
                                                            <td>{{ $data->allowable_PAC_Rebate_Ind }}</td>
                                                            <td>{{ $data->calculate_Method }}</td>
                                                            <td>{{ $data->motor_Min_Prem_Ind }}</td>
                                                            <td>{{ $data->abI_Adopt_Ind }}</td>
                                                            <td>{{ $data->motor_Levy_Per }}</td>
                                                            <td>{{ $data->motor_Levy_Amount }}</td>
                                                            <td>{{ $data->recommended_Vehicle_SI_Type }}</td>
                                                            <td>{{ $data->manual_Benchmark_SI_Display }}</td>
                                                            <td>{{ $data->manual_Benchmark_SI_Text }}</td>
                                                            <td>{{ $data->manual_Benchmark_SI_Editable }}</td>
                                                            <td>{{ $data->vehicle_Use_Code }}</td>
                                                            <td>{{ $data->aV_SI_Value }}</td>
                                                            <td>{{ $data->rec_SI_Value }}</td>
                                                            <td>{{ $data->rec_SI_Text }}</td>
                                                            <td>{{ $data->veh_SI_Editable }}</td>
                                                            <td>{{ $data->quote_Exp_Date }}</td>
                                                            <td>{{ $data->vehicle_Class }}</td>
                                                            <td>{{ $data->aV_SI_Lower_Bound }}</td>
                                                            <td>{{ $data->aV_SI_Upper_Bound }}</td>
                                                            <td>{{ $data->mV_SI_Lower_Bound }}</td>
                                                            <td>{{ $data->mV_SI_Upper_Bound }}</td>
                                                            <td>{{ $data->isM_Cover_Type }}</td>
                                                            <td>{{ $data->ncD_Class }}</td>
                                                            <td>{{ $data->ncD_Applicable_Ind }}</td>
                                                            <td>{{ $data->ddlAgreeValue }}</td>
                                                            <td>{{ $data->rec_Vehicle_SI }}</td>
                                                            <td>{{ $data->paC_SI_Per_Seat }}</td>
                                                            <td>{{ $data->total_ZDA_Premium }}</td>
                                                            <td>{{ $data->zdA_Add_On }}</td>
                                                            <td>{{ $data->paC_Type_Offered }}</td>
                                                            <td>{{ $data->paC_Desc }}</td>
                                                            <td>{{ $data->paC_Max_Units }}</td>
                                                            <td>{{ $data->paC_Min_Units }}</td>
                                                            <td>{{ $data->rec_PAC_Min_Units }}</td>
                                                            <td>{{ $data->paC_Compulsory_Ind }}</td>
                                                            <td>{{ $data->paC_Type_Applicable }}</td>
                                                            <td>{{ $data->wakalah_Commission_Mtr_Pct }}</td>
                                                            <td>{{ $data->wakalah_Commission_Mtr_Amt }}</td>
                                                            <td>{{ $data->wakalah_Expense_Mtr_Pct }}</td>
                                                            <td>{{ $data->wakalah_Expense_Mtr_Amt }}</td>
                                                            <td>{{ $data->wakalah_Fee_Mtr_Pct }}</td>
                                                            <td>{{ $data->wakalah_Fee_Mtr_Amt }}</td>
                                                            <td>{{ $data->wakalah_Commission_PA_Pct }}</td>
                                                            <td>{{ $data->wakalah_Commission_PA_Amt }}</td>
                                                            <td>{{ $data->wakalah_Expense_PA_Pct }}</td>
                                                            <td>{{ $data->wakalah_Expense_PA_Amt }}</td>
                                                            <td>{{ $data->wakalah_Fee_PA_Pct }}</td>
                                                            <td>{{ $data->wakalah_Fee_PA_Amt }}</td>
                                                            <td>{{ $data->referralData }}</td>
                                                            <td>{{ $data->errorDetails }}</td>
                                                            <td>{{ $data->created_at }}</td>
                                                            <td>{{ $data->updated_at }}</td>
                                                            <td>{{ $data->deleted_at }}</td>
                                                            
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <hr>

                                        <h3>ZurichApiCalculatePremium_quotationInfo</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Quotation Number</th>
                                                            <th>NCD Message</th>
                                                            <th>NCD Percentage</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($l as $data)
                                                        <tr>
                                                            <td>{{ $data->uuid }}</td>
                                                            <td>{{ $data->quotationNo }}</td>
                                                            <td>{{ $data->ncdMsg }}</td>
                                                            <td>{{ $data->ncdPct }}</td>
                                                            <td>{{ $data->created_at }}</td>
                                                            <td>{{ $data->updated_at }}</td>
                                                            <td>{{ $data->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <h3>ZurichApiCalculatePremium_extraCoverData</h3>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>UUID</th>
                                                            <th>Extra Cover Code</th>
                                                            <th>Extra Cover Sum Insured</th>
                                                            <th>Extra Cover Premium</th>
                                                            <th>Compulsory Indicator</th>
                                                            <th>Created At</th>
                                                            <th>Updated At</th>
                                                            <th>Deleted At</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($m as $data)
                                                        <tr>
                                                            <td>{{ $data->uuid }}</td>
                                                            <td>{{ $data->extCoverCode }}</td>
                                                            <td>{{ $data->extCoverSumInsured }}</td>
                                                            <td>{{ $data->extCoverPrem }}</td>
                                                            <td>{{ $data->compulsory_Ind }}</td>
                                                            <td>{{ $data->created_at }}</td>
                                                            <td>{{ $data->updated_at }}</td>
                                                            <td>{{ $data->deleted_at }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>

                                            </div>

                                            
                                        
                                            <h3>ZurichApiCalculatePremium_allowableCourtesyCar</h3>
                                            <div class="row">
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>UUID</th>
                                                                
                                                                <th>Courtesy Car Days</th>
                                                                <th>Courtesy Car Days Indicator</th>
                                                                <th>Created At</th>
                                                                <th>Updated At</th>
                                                                <th>Deleted At</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($n as $data)
                                                            <tr>
                                                                <td>{{ $data->uuid }}</td>
                                                                
                                                                <td>{{ $data->courtesy_Car_Days }}</td>
                                                                <td>{{ $data->courtesy_Car_Days_Ind }}</td>
                                                                <td>{{ $data->created_at }}</td>
                                                                <td>{{ $data->updated_at }}</td>
                                                                <td>{{ $data->deleted_at }}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>


                                        </div>


                                        <hr>
                                            <h3>ZurichApiCalculatePremium_AllowableExtCvr</h3>

                                            <div class="row">
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>UUID</th>
                                                                <th>Ext Cvr Code</th>
                                                                <th>Ext Cvr Desc</th>
                                                                <th>Applicable Ind</th>
                                                                <th>Compulsory Ind</th>
                                                                <th>Recommended Ind</th>
                                                                <th>Endorsement Ind</th>
                                                                <th>EC Input Type</th>
                                                                <th>Created At</th>
                                                                <th>Updated At</th>
                                                                <th>Deleted At</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($o as $data)
                                                                <tr>
                                                                    <td>{{ $data->uuid }}</td>
                                                                    <td>{{ $data->ext_Cvr_Code }}</td>
                                                                    <td>{{ $data->ext_Cvr_Desc }}</td>
                                                                    <td>{{ $data->applicable_Ind }}</td>
                                                                    <td>{{ $data->compulsory_Ind }}</td>
                                                                    <td>{{ $data->recommended_Ind }}</td>
                                                                    <td>{{ $data->endorsement_Ind }}</td>
                                                                    <td>{{ $data->eC_Input_Type }}</td>
                                                                    <td>{{ $data->created_at }}</td>
                                                                    <td>{{ $data->updated_at }}</td>
                                                                    <td>{{ $data->deleted_at }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                            
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection


@push('style')
  
  <!-- Step Step -->
  <link rel="stylesheet" href="{{ asset('css/stepstepXX.css') }}">  

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  

@endpush


@push('scripts')
   
   
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush



