@extends('layouts.app')

@section('title', 'Pra application')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">

        @include('sweetalert::alert')
        
        <section class="section">
            <div class="section-header">
                <h1>Application SMS Akad</h1>
                <div class="section-header-breadcrumb">
                    <!-- <div class="breadcrumb-item active">{{ Breadcrumbs::render('home') }} </div> -->
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('pra-smsakad') }}</div>
                    
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4></h4>
                                
                                <div class="card-header-form">
                                    <!-- <form>
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control"
                                                placeholder="Search">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form> -->
                                </div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    <table class="table-striped table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th>Vehicle</th>
                                                <th>IC</th>
                                                <th>Email</th>
                                                <th>Updated At</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php $i=1; ?>
                                            @foreach($data as $data)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $data->vehicle }}</td>
                                                <td> {{ $data->ic }} </td>
                                                <td> {{ $data->email }} </td>
                                                <td> {{ $data->updated_at }} </td>
                                                <td> <span class="badge badge-{{$data->status->label}}">{{ $data->status->status_app }}</span></td>
                                                <td>
                                                    <a class="btn btn-success btn-action mr-1"
                                                    data-toggle="tooltip" href="{{ url('sms/send/'. $data->uuid) }}"
                                                    title="Edit"><i class="fas fa-book"></i></a>
                                                    
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>                                        
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
