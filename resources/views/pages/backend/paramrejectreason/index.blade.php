@extends('layouts.app')

@section('title', 'Param Reject Reason')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">

      @include('sweetalert::alert')
      
        <section class="section">
            <div class="section-header">
                <h1>Param Reject Reason</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-reject-reason') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                
                                @can('param-reject-reason-create')
                                <h4><a href="{{ route('param-reject-reason.create') }}" class="btn btn-primary mr-1">+ Add</a></h4>
                                @endcan

                                <div class="card-header-form"></div>
                            </div> 

                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                                        <thead>
                                          <tr>
                                            <th>No</th>
                                            <th>Reject Reason</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <?php $i=1; ?>
                                          @foreach($data as $data)
                                            <tr>
                                              <td>{{$i++}}</td>
                                              <td>
                                                {{ $data->reject_reason }}
                                              </td>
                                              
                                              <td>
                                                @if($data->status == 1)
                                                <div class="badge badge-pill badge-success mb-1">Active</div>
                                                @else 
                                                <div class="badge badge-pill badge-danger mb-1">Inactive</div>
                                                @endif
                                              </td>

                                              <td>
                                                
                                                @can('param-reject-reason-edit')
                                                  <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" href="{{ route('param-reject-reason.edit', $data->id) }}" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                                @endcan 

                                                @can('param-reject-reason-delete')

                                                <input id="toggle-trigger" type="checkbox" data-toggle="toggle" class="btn togglefunction" data-on="Active" data-off="Inactive" data-size="xl"  data-id="{{$data->id}}" data-onstyle="success" data-offstyle="danger" {{ $data->status ? 'checked' : '' }}>

                                                
                                                  <!-- 
                                                  <form method="POST" action="{{url('param-type-insurance/destroy/'.$data->id)}}">
                                                    @csrf 
                                                        <button class="btn btn-danger dltBtn" data-id={{$data->id}} data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                                  </form>
                                                  -->
                                                @endcan
                                              </td>
                                            </tr>
                                          @endforeach
                                        <tbody>
                                         
                                        </tbody>
                                      </table>
                                      

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

  <!-- Toogle -->
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


  <script>
    $(function() {
        $('.togglefunction').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
            
            $.ajax({
                type: "POST",
                url: "{{url('param-reject-reason/status')}}",
                data: {
                  '_token': '{{ csrf_token() }}',
                  'status': status, 
                  'id': id
                },
                success: function(data){
                    setInterval('location.reload()', 1000); 
                },
                error : function(){
                      alert("Status Change Unsuccessful !");
                }
            });
        })
    })
  </script>

@endpush
