@extends('layouts.app')

@section('title', 'Setup Zurich Cart')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

     <!-- summernote -->
     <link rel="stylesheet"  href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setup Zurich Cart</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('setup-zurichcart-create') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('setup-zurich-cart.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}


                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Cart Day</label>
                                            <select name="cart_day_id" class="form-control">
                                                <option value="" selected disabled hidden>- Please Select -</option>
                                                @foreach($day as $day)
                                                <option value="{{$day->id}}">{{$day->cart_day}}</option>
                                                @endforeach
                                            </select>
                                            @error('cart_day_id')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Cart Amount</label>
                                            <select name="cart_amount_id" class="form-control">
                                                <option value="" selected disabled hidden>- Please Select -</option>
                                                @foreach($amount as $amount)
                                                <option value="{{$amount->id}}">{{$amount->cart_ammount}}</option>
                                                @endforeach
                                            </select>
                                            @error('cart_amount_id')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Desc</label>
                                            <input id="inputTitle" type="text" name="desc" placeholder="Desc"  value="{{old('desc')}}" class="form-control">
                                            @error('desc')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                
                                
                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @error('status')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>

    <!-- summernote -->
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
 
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
