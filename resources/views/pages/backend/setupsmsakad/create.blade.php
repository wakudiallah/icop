@extends('layouts.app')

@section('title', 'Setup Zurich Cart')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

     <!-- summernote -->
     <link rel="stylesheet"  href="{{ asset('library/summernote/dist/summernote-bs4.min.css') }}">

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setup SMS Akad</h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('setup-smsakad-create') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{route('setup-smsakad.store')}}" enctype='multipart/form-data'>
                                        {{csrf_field()}}


                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Opening </label>
                                            <input id="inputTitle" type="text" name="opening" placeholder="Opening"  value="" class="form-control">
                                            @error('opening')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                        

                                      

                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Text Basic Contribution </label>
                                            <input id="inputTitle" type="text" name="text_basic_contribution" placeholder="Text Basic Contribution"  value="" class="form-control">
                                            @error('text_basic_contribution')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Text Caj Perkhidmatan </label>
                                            <input id="inputTitle" type="text" name="text_caj_perkhidmatan" placeholder="Text Caj Perkhidmatan"  value="" class="form-control">
                                            @error('text_caj_perkhidmatan')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>



                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Sms Akad</label>
                                            <textarea class="summernote-simple" name="sms_akad" placeholder="Sms Akad" class="form-control">
                                               
                                            </textarea>
                                            @error('sms_akad')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label for="inputTitle" class="col-form-label"> Closing</label>
                                            <input id="inputTitle" type="text" name="closing" placeholder="Closing"  value="" class="form-control">
                                            @error('closing')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                
                                
                                        <div class="form-group">
                                            <label for="status" class="col-form-label">Status</label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            @error('status')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Submit</button>
                                        </div>

                                      </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>

    <!-- summernote -->
    <script src="{{ asset('library/summernote/dist/summernote-bs4.min.js') }}"></script>
 
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
