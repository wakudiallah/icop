@extends('layouts.app')

@section('title', 'Pra application')

@push('style')
   
    <!-- datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Pra Application - Financing</h1>
                <div class="section-header-breadcrumb">
                    <!-- <div class="breadcrumb-item active">{{ Breadcrumbs::render('home') }} </div> -->
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('pra-application') }}</div>
                    
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->

              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4></h4>
                                
                                <div class="card-header-form">
                                    
                                </div>
                            </div> 

                            <div class="card-body">
                                
                              
                                <div class="row">
                                    <div class="col-12">
                                            
                                        <div class="row">
                                            <div class="col-12 col-lg-8 offset-lg-2">
                                                <div class="wizard-steps">
                                                    <div class="wizard-step wizard-step-active" id="step-1">
                                                        <div class="wizard-step-icon">
                                                            <i class="fas fa-car"></i>
                                                        </div>
                                                        <div class="wizard-step-label">
                                                            Data Vehicle
                                                        </div>
                                                    </div>
                                
                                                    <div class="wizard-step" id="step-2">
                                                        <div class="wizard-step-icon">
                                                            <i class="fas fa-user"></i>
                                                        </div>
                                                        <div class="wizard-step-label">
                                                            CIF 
                                                        </div>
                                                    </div>

                                                    <div class="wizard-step" id="step-3">
                                                        <div class="wizard-step-icon">
                                                            <i class="fas fa-newspaper"></i>
                                                        </div>
                                                        <div class="wizard-step-label">
                                                            Insurance
                                                        </div>
                                                    </div>
                                
                                                    <div class="wizard-step" id="step-4">
                                                        <div class="wizard-step-icon">
                                                            <i class="fas fa-server"></i>
                                                        </div>
                                                        <div class="wizard-step-label">
                                                            Financing
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                
                                        <form id="wizardForm" method="POST" action="{{route('pra-finance.store')}}" class="wizard-content mt-2" enctype='multipart/form-data'>
                                            {{csrf_field()}}

                                            <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                            
                                            <!-- Step 1 -->
                                            <div class="wizard-pane wizard-step-active" id="pane-1">
                                                
                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Vehicle Number </label>
                                                        <input id="inputTitle" type="text" name="vehicle_number" placeholder=""  value="{{$pra->vehicle}}" class="form-control" readonly>
                                                        @error('vehicle_number')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
        
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Chassis Number</label>
                                                        <input id="inputTitle" type="text" name="chassis_number" placeholder=""  value="{{$pra->chassisNo}}" class="form-control" readonly>
                                                        @error('chassis_number')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Register ID</label>
                                                        <input id="inputTitle" type="text" name="register_id" placeholder=""  value="{{$pra->reg_no}}" class="form-control" readonly>
                                                        @error('register_id')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
        
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Year Of Manufacture</label>
                                                        <input id="inputTitle" type="text" name="year_of_manufacture" placeholder="{{$pra->yearOfMake}}"  value="" class="form-control" readonly>
                                                        @error('year_of_manufacture')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Engine Number</label>
                                                        <input id="inputTitle" type="text" name="engine_number" placeholder=""  value="{{$pra->engineNo}}" class="form-control" readonly>
                                                        @error('engine_number')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
        
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Model</label>
                                                        <input id="inputTitle" type="text" name="variant" placeholder="{{$pra->model}}"  value="" class="form-control" readonly>
                                                        @error('variant')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">NCD</label>
                                                        <input id="inputTitle" type="text" name="ncd" placeholder=""  value="{{$pra->ncdAmt}}" class="form-control" readonly>
                                                        @error('ncd')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
        
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Vehicle Capacity</label>
                                                        <input id="inputTitle" type="text" name="vehicle_capacity" placeholder=""  value="{{$pra->capacity}} {{$pra->uom}}" class="form-control" readonly>
                                                        @error('vehicle_capacity')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                </div>
                                                
                                                
                                                <!-- <div class="form-group row">
                                                    <div class="col-lg-4 col-md-6">
                                                    <a href="{{url('check-details/'.$uuid )}}" class="btn btn-warning">Check Details </a>
                                                    </div>
                                                </div> 


                                                <div class="form-group row">
                                                    <label class="col-md-4 text-md-right mt-2 text-left">Role</label>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="selectgroup w-100">
                                                            <label class="selectgroup-item">
                                                                <input type="radio" name="role" value="developer" class="selectgroup-input">
                                                                <span class="selectgroup-button">Developer</span>
                                                            </label>
                                                            <label class="selectgroup-item">
                                                                <input type="radio" name="role" value="ceo" class="selectgroup-input">
                                                                <span class="selectgroup-button">CEO</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                
                                                
                                                <div class="form-group row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-lg-4 col-md-7 text-right">
                                                        <button type="button" class="btn btn-icon icon-right btn-primary" onclick="nextStep()">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                


                                            <!--============================= Step 2 =============================-->
                                            <div class="wizard-pane" id="pane-2">


                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Full Name</label>
                                                        <input id="inputTitle" type="text" name="full_name" placeholder=""  value="{{ $pra->fullname }}" class="form-control" readonly>
                                                        @error('full_name')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Phone</label>
                                                        <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{$pra->phone}}" class="form-control" readonly>
                                                        @error('phone')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Email</label>
                                                        <input id="inputTitle" type="text" name="email" placeholder=""  value="{{$pra->email}}" class="form-control" readonly>
                                                        @error('email')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Address (Line 1)</label>
                                                        <input id="inputTitle" type="text" name="address1" placeholder=""  value="{{$pra->address1}}" class="form-control" readonly>
                                                        @error('address1')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">Address (Line 2)</label>
                                                        <input id="inputTitle" type="text" name="address2" placeholder=""  value="{{$pra->address2}}" class="form-control" readonly>
                                                        @error('address2')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="col-form-label">NRIC</label>
                                                        <input id="inputTitle" type="text" name="nric" placeholder=""  value="{{$pra->ic}}" class="form-control" readonly>
                                                        @error('nric')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Date of Birth</label>
                                                            <input id="inputTitle" type="text" name="dob" placeholder=""  value="{{$pra->dob}}" class="form-control" readonly>
                                                            @error('dob')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Age</label>
                                                            <input id="inputTitle" type="text" name="age" placeholder=""  value="{{$pra->age}}" class="form-control" readonly>
                                                            @error('age')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Marital Status</label>
                                                            <input id="inputTitle" type="text" name="dob" placeholder=""  value="{{$pra->marital_status}}" class="form-control" readonly>
                                                            @error('dob')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Gender</label>
                                                            <input id="inputTitle" type="text" name="gender" placeholder=""  value="{{$pra->gender}}" class="form-control" readonly>
                                                            @error('gender')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Postcode</label>
                                                            <input id="inputTitle" type="text" name="postcode" placeholder=""  value="{{$pra->postcode}}" class="form-control" readonly>
                                                            @error('postcode')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">State</label>
                                                            <input id="inputTitle" type="text" name="state" placeholder=""  value="{{$pra->state}}" class="form-control" readonly>
                                                            @error('state')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="form-group row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-lg-4 col-md-7 text-right">
                                                        <button type="button" class="btn btn-icon icon-left btn-secondary" onclick="prevStep()"><i class="fas fa-arrow-left"></i> Previous</button>
                                                        <button type="button" class="btn btn-icon icon-right btn-primary" onclick="nextStep()">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--============================= Step 3 Insurance =============================-->
                                            <div class="wizard-pane" id="pane-3">


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Insurance Selected</label>
                                                            <input id="inputTitle" type="text" name="insurance" placeholder=""  value="" class="form-control" readonly>
                                                            @error('insurance')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Basic Contribution</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($pra->totBasicNetAmt,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Rebate (NCD)</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($pra->ncdAmt,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                                                        <thead>
                                                          <tr>
                                                            <th>No</th>
                                                            <th>Extra Cover</th>
                                                            <th>Ext Cover Premium</th>
                                                            <th>Status</th>
                                                          </tr>
                                                        </thead>
                                                        <?php $i=1; ?>
                                                        <tbody>
                                                            
                                                            @foreach($extraCover as $data)
                                                            <tr>
                                                                <td>{{$i++}}</td>
                                                                <td>
                                                                    @if($data->extCoverCode == "XX")
                                                                    RoadTax
                                                                    @elseif($data->extCoverCode == 202)
                                                                    TOWING AND CLEANING DUE TO WATER DAMAGE
                                                                    @elseif($data->extCoverCode == 200)
                                                                    PA Basic
                                                                    @elseif($data->extCoverCode == 57)
                                                                    INCLUSION OF SPECIAL PERILS
                                                                    @elseif($data->extCoverCode == 72)
                                                                    LEGAL LIABILITY OF PASSENGERS FOR NEGLIGENT ACTS
                                                                    @elseif($data->extCoverCode == 112)
                                                                    CART
                                                                    @elseif($data->extCoverCode == 01)
                                                                    All Driver
                                                                    @elseif($data->extCoverCode == 100)
                                                                    Legal Liability to Passengers
                                                                    @elseif($data->extCoverCode == 25)
                                                                    Strike Riot & Civil Commotion
                                                                    @elseif($data->extCoverCode == "89A")
                                                                    Cover For Windscreens, Windows and Sunroof
                                                                    @endif
                                                                </td>
                                                                <td>{{number_format($data->extCoverPrem,2)}}</td>
                                                                <td>
                                                                    @if($data->status == 1)
                                                                    Yes
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Total Extra Cover</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($totExtraCover,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Sst</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($totalSst,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Stamp Duty</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($pra->stampDutyAmt,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Total Payable</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($payable,2)}}" class="form-control" readonly>
                                                            @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                               


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Deduction Scheme (Month)</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{$pra->deduc_schema}}" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Processing Fee</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($processFee,2)}}" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Bpa Charges({{$bpaFee->bpa_charges}}%)</label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($bpaCharge,2)}}" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Total </label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($totalCalculateMonthly,2)}}" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="inputTitle" class="col-form-label">Monthly instalment </label>
                                                            <input id="inputTitle" type="text" name="phone" placeholder=""  value="{{number_format($monthlyInstallment,2)}}" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>


                                         

                                                <div class="form-group row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-lg-4 col-md-7 text-right">
                                                        <button type="button" class="btn btn-icon icon-left btn-secondary" onclick="prevStep()"><i class="fas fa-arrow-left"></i> Previous</button>
                                                        <button type="button" class="btn btn-icon icon-right btn-primary" onclick="nextStep()">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- Step 3 -->
                                            <div class="wizard-pane" id="pane-4">

                                                <!------========================   DSR Calculation ==================------>
                                                <div class="form-group row">
                                                    <div class="col-md-7">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="left-align" width="10%">Pendapatan</th>
                                                                        <th width="40%"> RM</th>
                                                                        <th class="left-align" width="10%">Potongan</th>
                                                                        <th width="40%"> RM</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <tr>
                                                                        <td class="left-align">Tetap:</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Gaji Pokok</td>
                                                                        <td><input type="text" class="form-control number-input" name="gaji_pokok" id="gaji_pokok" placeholder="0.00"  value=""></td>

                                                                        <td class="left-align">PCB</td>
                                                                        <td><input type="text" class="form-control number-input" name="pcb" id="pcb" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Imbuhan Tetap Perumahan</td>
                                                                        <td><input type="text" class="form-control number-input" name="imbuhan_tetap_perumahan" id="imbuhan_tetap_perumahan" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">PERKESO</td>
                                                                        <td><input type="text" class="form-control number-input" name="perkeso" id="perkeso" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Imbuhan Tetap Khidmat Awam</td>
                                                                        <td><input type="text" class="form-control number-input" name="imbuhan_tetap_khidmat_awam" id="imbuhan_tetap_khidmat_awam" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Cukai Pendapatan</td>
                                                                        <td><input type="text" class="form-control number-input" name="cukai_pendapatan" id="cukai_pendapatan" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Bantuan Sara Hidup</td>
                                                                        <td><input type="text" class="form-control number-input" name="bantuan_sara_hidup" id="bantuan_sara_hidup" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Pinjaman Perumahan</td>
                                                                        <td><input type="text" class="form-control number-input" name="pinjaman_perumahan" id="pinjaman_perumahan" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Bayaran Insentif Tugas Khas</td>
                                                                        <td><input type="text" class="form-control number-input" name="bayaran_insentif_tugas_khas" id="bayaran_insentif_tugas_khas" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Zakat</td>
                                                                        <td><input type="text" class="form-control number-input" name="zakat" id="zakat" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Perkhidmatan Kritikal</td>
                                                                        <td><input type="text" class="form-control number-input" name="perkhidmatan_kritikal" id="perkhidmatan_kritikal" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Lembaga Tabung Haji</td>
                                                                        <td><input type="text" class="form-control number-input" name="lemabaga_tabung_haji" id="lemabaga_tabung_haji" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Elaun Tetap (lain-lain)</td>
                                                                        <td><input type="text" class="form-control number-input" name="elaun_tetap_lain_lain" id="elaun_tetap_lain_lain" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Pembiayaan Perumahan/Lain-lain</td>
                                                                        <td><input type="text" class="form-control number-input" name="pembiayaan_perumahan_lain_lain" id="pembiayaan_perumahan_lain_lain" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Insentif</td>
                                                                        <td><input type="text" class="form-control number-input" name="insentif" id="insentif" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Angkasa</td>
                                                                        <td><input type="text" class="form-control number-input" name="angkasa" id="angkasa" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="left-align">Penanggungan Kerja</td>
                                                                        <td><input type="text" class="form-control number-input" name="penanggungan_kerja" id="penanggungan_kerja" placeholder="0.00"  value=""></td>
                                                                        <td class="left-align">Koperasi</td>
                                                                        <td><input type="text" class="form-control number-input" name="koperasi" id="koperasi" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td class="left-align">Piutang gaji & elaun</td>
                                                                        <td><input type="text" class="form-control number-input" name="piutang_gaji_elaun" id="piutang_gaji_elaun" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td class="left-align">Angkasa bukan pinjaman</td>
                                                                        <td><input type="text" class="form-control number-input" name="angkasa_bukan_pinjaman" id="angkasa_bukan_pinjaman" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td class="left-align">YIR</td>
                                                                        <td><input type="text" class="form-control number-input" name="yir" id="yir" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td class="left-align">YIR 2</td>
                                                                        <td><input type="text" class="form-control number-input" name="yir2" id="yir2" placeholder="0.00"  value=""></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <hr>
                                                                    
                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">      
                                                                    <tr>
                                                                        <td class="red left-align"><strong data-toggle="tooltip" title="Sum Jumlah Pendapatan">Jumlah Pendapatan (A)</strong></td>
                                                                        <td class="red"><strong><input type="text" data-toggle="tooltip" title="Sum Jumlah Pendapatan" class="form-control number-input" name="jumlah_pendapatan" id="jumlah_pendapatan" placeholder="0.00"  value="" readonly></strong></td>

                                                                        <td class="left-align"><strong data-toggle="tooltip" title="Sum Jumlah Potongan">Jumlah Potongan</strong></td>
                                                                        <td><strong><input type="text" data-toggle="tooltip" title="Sum Jumlah Potongan" class="form-control number-input" name="jumlah_potongan" id="new_jumlah_potongan" placeholder="0.00"  value="" readonly></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td class="left-align"><strong data-toggle="tooltip" title="Jumlah Pendapatan - Jumlah Potongan">Jumlah Bersih</strong> </td>
                                                                        <td><strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="Jumlah Pendapatan - Jumlah Potongan" name="jml_bersih" id="jml_bersih" placeholder="0.00"  value="" readonly></strong></td>
                                                                    </tr>
                                                                    
                                                            </table>
                                                        </div>

                                                        <hr>

                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">   
                                                                <tr>
                                                                    <td class="left-align">
                                                                        <strong data-toggle="tooltip" title=" Jumlah Pendapatan * {{config('insurance.max_eligibility')}}">Percentage</strong>
                                                                    </td>
                                                                    <td>
                                                                        <strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="Jumlah Pendapatan * {{config('insurance.max_eligibility')}}" name="X" id="nilai_X" placeholder="0.00"  value="" readonly></strong>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </table> 
                                                        </div>

                                                        

                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">   
                                                                <tr>
                                                                    <td class="left-align">
                                                                        <strong data-toggle="tooltip" title="Percentage - Jumlah Potongan (Payslip)">Max Deduction</strong>
                                                                    </td>
                                                                    <td>
                                                                        <strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="Max Deduction" name="" id="MaxDeduction" placeholder="0.00"  value="" readonly></strong>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </table> 
                                                        </div>

                                                        <!--<div class="table-responsive">
                                                            <table class="table table-hover small-font"> 
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="left-align"><strong data-toggle="tooltip" title=" Jumlah Pendapatan * {{config('insurance.max_eligibility')}}">Percentage</strong></td>
                                                                    <td><strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="Jumlah Pendapatan * {{config('insurance.max_eligibility')}}" name="percentage" id="percentage" placeholder="0.00"  value="" readonly></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="left-align"><strong data-toggle="tooltip" title="(Jumlah Potongan / Jumlah Pendapatan) * 100">Max eligibility</strong></td>
                                                                    <td><strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="(Jumlah Potongan / Jumlah Pendapatan) * 100" name="max_eligibility" id="max_eligibility" placeholder="0.00"  value="" readonly></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="left-align"><strong data-toggle="tooltip" title="Max Eligibility - Jumlah Potongan">Available deduction</strong></td>
                                                                    <td><strong><input type="text" class="form-control number-input" data-toggle="tooltip" title="Max Eligibility - Jumlah Potongan" name="available_deduction" id="available_deduction" placeholder="0.00"  value="" readonly></strong></td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                        <hr> -->

                                                   

                                                        <!-- <div class="card">
                                                            <div class="card-header">
                                                                <h4>PENGIRAAN DSR</h4>
                                                                <div class="card-header-action">
                                                                    <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="collapse show" id="mycard-collapse">
                                                                <div class="card-body">
                                                                    <b style="font-size: 13pt !important"> $$\frac{\text{Jumlah Potongan}}{\text{Jumlah Pendapatan}} \times 100 \%$$ </b>
                                                                </div>
                                                            </div>
                                                        </div> -->



                                                        <!-- <hr>
                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">
                                                                <tr>
                                                                    <td class="red left-align">(B)Jumlah Potongan</td>
                                                                    <td></td>
                                                                    <td class="left-align"></td>
                                                                    <td><input type="text" class="form-control number-input" name="b_jumlah_potongan" id="b_jumlah_potongan" placeholder="0.00"  value="" readonly></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <hr>

                                                        <div class="table-responsive">
                                                            <table class="table table-hover small-font">
                                                                <tr>
                                                                    <td class="left-align">DSR</td>
                                                                    <td>=</td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input" name="dsr_jumlah_potongan" id="dsr_jumlah_potongan" placeholder="0.00"  value="" readonly> </td>
                                                                    <td rowspan="3">x 100</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td> <hr> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align"></td>
                                                                    <td></td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input" name="dsr_jumlah_pendapatan" id="dsr_jumlah_pendapatan" placeholder="0.00"  value="" readonly></td>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align"></td>
                                                                    <td>=</td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input" name="dsr_percent" id="dsr_percent" placeholder="0.00"  value="" readonly></td>
                                                                    <td>%</td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                        <hr> -->
                                                        
                                                        <div class="table-responsive">
                                                             <!-- <table class="table table-hover green small-font">
                                                                <tr>
                                                                    <td class="left-align">Pembiayaan Baru (C):</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">If Takaful coverage</td>
                                                                    <td><input type="text" class="form-control number-input" name="total_payable" id="total_payable" placeholder="0.00"  value="{{number_format($payable,2)}}" readonly></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">If Roadtax</td>
                                                                    <td><input type="text" class="form-control number-input" name="roadtax" id="roadtax" placeholder="0.00"  value="{{ $pra->extCoverPrem }}" readonly></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"> <hr> </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <td class="left-align">Total</td>
                                                                    <td><input type="text" class="form-control number-input" data-toggle="tooltip" title="Coverage + Roadtax" name="jml_financing_roadtax" id="jml_financing_roadtax" placeholder="0.00"  value="{{number_format($pra->extCoverPrem,2)}}" readonly></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4"> <hr> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">Tempoh</td>
                                                                    <td><input type="text" class="form-control number-input" name="tempoh" id="tempoh" value="{{$pra->deduc_schema}}" readonly></td>
                                                                    <td class="left-align">bulan</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">Kadar pulangan</td>
                                                                    <td><input type="text" class="form-control number-input" name="tempoh" id="tempoh" placeholder="0.00" value="" readonly></td>
                                                                    <td class="left-align">% (tetap)</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">Ansuran bulanan RM</td>
                                                                    <td><input type="text" class="form-control number-input" name="monthly_installment" id="monthly_installment" value="{{number_format($monthlyInstallment,2)}}" readonly></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4"><hr></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align">DSR BARU PEMOHON</td>
                                                                    <td>=</td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input"  name="dsr_baru_jumlah_potongan" data-toggle="tooltip" title="Jumlah Potongan + Ansuran Bulanan" id="dsr_baru_jumlah_potongan" placeholder="0.00" value="" readonly> </td>
                                                                    <td rowspan="3">x 100</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><hr></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align"></td>
                                                                    <td></td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input" data-toggle="tooltip" title="Jumlah Pendapatan" name="dsr_baru_jumlah_pendapatan" id="dsr_baru_jumlah_pendapatan" placeholder="0.00" value="" readonly></td>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td class="left-align"></td>
                                                                    <td>=</td>
                                                                    <td class="left-align"><input type="text" class="form-control number-input" name="dsr" id="dsr" placeholder="0.00" value="" readonly> </td>
                                                                    <td>%</td>
                                                                </tr>

                                                            </table> -->

                                                            <div class="form-group">
                                                                <label>Status</label>
                                                                <select class="form-control" name="status" required>
                                                                    <option value="" selected disabled hidden>- Please Select -</option>
                                                                    <option value="">Approve</option>
                                                                    <option value="">Reject</option>
                                                                    
                                                                </select>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Remark</label>
                                                                <textarea class="form-control" name="remark" data-height="150" required  maxlength="200"></textarea>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        
                                                        <!-- Document -->
                                                        <div class="chocolat-parent">
                                                            <a href="{{$doc->payslip}}" class="chocolat-image" title="Just an example">
                                                                <div data-crop-image="285">
                                                                    <img alt="image" src="{{$doc->payslip}}" class="img-fluid">
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <!-- End of Document -->

                                                        <!-- Modal Image -->
                                                        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="imageModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="imageModalLabel">Payslip Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{$doc->payslip}}" alt="Payslip Image" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Modal Image -->

                                                        <!-- Document -->
                                                        <div class="chocolat-parent">
                                                            <a href="{{$doc->nric}}" class="chocolat-image" title="Just an example">
                                                                <div data-crop-image="285">
                                                                    <img alt="image" src="{{$doc->nric}}" class="img-fluid">
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <!-- End of Document -->

                                                        <!-- Modal Image -->
                                                        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="imageModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="imageModalLabel">Nric Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{$doc->nric}}" alt="Payslip Image" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Modal Image -->


                                                        <!-- <img src="{{$doc->nric}}" alt="" class="img img-fluid"> -->

                                                        <embed src="{{$doc->bpa179}}" type="application/pdf" width="100%" height="600px" />
                                                    </div>
                                                </div>
                                                <!--========================  End DSR Calculation ========================-->

                                                <div class="form-group row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-lg-4 col-md-7 text-right">
                                                        <button type="button" class="btn btn-icon icon-left btn-secondary" onclick="prevStep()"><i class="fas fa-arrow-left"></i> Previous</button>
                                                        <button type="submit" class="btn btn-icon icon-right btn-primary">Submit <i class="fas fa-check"></i></button>
                                                    </div>
                                                </div>


                                            </div>

                                            <!-- End of Step 3 -->



                                        </form>
                                    
                                        </div>
                                    </div>
                                </div>


                                <!------  Modal Submit ------>
                                <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modal title #exampleModal</h5>
                                                <button type="button"
                                                    class="close"
                                                    data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Modal body text goes here.</p>
                                            </div>
                                            <div class="modal-footer bg-whitesmoke br">
                                                <button type="button"
                                                    class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button"
                                                    class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------  End Modal Submit ------>


                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection


@push('style')
  
  <!-- Step Step -->
  <link rel="stylesheet" href="{{ asset('css/stepstepXX.css') }}">  

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('library/chocolat/dist/css/chocolat.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('library/prismjs/themes/prism.min.css') }}">


  <style>
    .wizard-step {
        /*display: none;*/
    }

    .wizard-pane {
        display: none;
    }
    
    .wizard-step-active {
        display: block;
    }
    .wizard-step-icon {
        font-size: 1.5rem;
    }
  </style>


    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        
        th {
            background-color: #f2f2f2;
        }
        .left-align {
            text-align: left;
        }
        .red {
            color: red;
        }
        .green {
            background-color: #d9ead3;
        }
        .form-control {
            width: 100%;
            padding: 6px;
            box-sizing: border-box;
        }
        
        .small-font {
            font-size: 8pt;
        }

        .red-text {
            color: red;
        }
        
    </style>

@endpush


@push('scripts')
   
   <!-- Step Step -->
   <script src="{{ asset('js/stepstepXX.js') }}"></script>

  <script src="{{ asset('library/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('library/prismjs/prism.js') }}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('js/page/bootstrap-modal.js') }}"></script>

   <script>
        let currentStep = 1;

        function nextStep() {
            const totalSteps = 4;
            if (currentStep < totalSteps) {
                document.getElementById(`pane-${currentStep}`).classList.remove('wizard-step-active');
                document.getElementById(`step-${currentStep}`).classList.remove('wizard-step-active');
                currentStep++;
                document.getElementById(`pane-${currentStep}`).classList.add('wizard-step-active');
                document.getElementById(`step-${currentStep}`).classList.add('wizard-step-active');
            }
        }

        function prevStep() {
            if (currentStep > 1) {
                document.getElementById(`pane-${currentStep}`).classList.remove('wizard-step-active');
                document.getElementById(`step-${currentStep}`).classList.remove('wizard-step-active');
                currentStep--;
                document.getElementById(`pane-${currentStep}`).classList.add('wizard-step-active');
                document.getElementById(`step-${currentStep}`).classList.add('wizard-step-active');
            }
        }
    </script>


    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <!-- formula Match -->
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    <script type="text/javascript">
        MathJax = {
            tex: {
                inlineMath: [['$', '$'], ['\\(', '\\)']]
            }
        };
    </script>
    <!-- End formula match -->
    

    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

    
    <!-- Input Type For DSR Calculation -->
    <script>
        $(document).ready(function() {
            // Initialize tooltips
            $('[data-toggle="tooltip"]').tooltip();
            var percentageRejected = {{ config('insurance.percentage_rejected') }};
            var percentageMaxEligibility = {{ config('insurance.max_eligibility') }};


            function formatNumber(value) {
                var cleanValue = value.replace(/[^0-9.]/g, '');
                var numericValue = parseFloat(cleanValue);
                if (!isNaN(numericValue)) {
                    return numericValue.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                } else {
                    return '';
                }
            }

            function updateJumlahPendapatan() {
                var gajiPokok = parseFloat(unformatNumber($('#gaji_pokok').val())) || 0;
                var imbuhanTetapPerumahan = parseFloat(unformatNumber($('#imbuhan_tetap_perumahan').val())) || 0;
                var imbuhan_tetap_khidmat_awam = parseFloat(unformatNumber($('#imbuhan_tetap_khidmat_awam').val())) || 0;
                var bantuan_sara_hidup = parseFloat(unformatNumber($('#bantuan_sara_hidup').val())) || 0;
                var bayaran_insentif_tugas_khas = parseFloat(unformatNumber($('#bayaran_insentif_tugas_khas').val())) || 0;
                var perkhidmatan_kritikal = parseFloat(unformatNumber($('#perkhidmatan_kritikal').val())) || 0;
                var elaun_tetap_lain_lain = parseFloat(unformatNumber($('#elaun_tetap_lain_lain').val())) || 0;
                var insentif = parseFloat(unformatNumber($('#insentif').val())) || 0;
                var penanggungan_kerja = parseFloat(unformatNumber($('#penanggungan_kerja').val())) || 0;
                var jumlahPendapatan = gajiPokok + imbuhanTetapPerumahan + imbuhan_tetap_khidmat_awam + bantuan_sara_hidup + bayaran_insentif_tugas_khas + perkhidmatan_kritikal + elaun_tetap_lain_lain + insentif + penanggungan_kerja;
                
                var X = jumlahPendapatan * percentageMaxEligibility;
               
                $('#nilai_X').val(formatNumber(X.toString()));

                $('#jumlah_pendapatan').val(formatNumber(jumlahPendapatan.toString()));
                $('#dsr_jumlah_pendapatan').val(formatNumber(jumlahPendapatan.toString()));
                $('#dsr_baru_jumlah_pendapatan').val(formatNumber(jumlahPendapatan.toString()));

                var pcb = parseFloat(unformatNumber($('#pcb').val())) || 0;
                var perkeso = parseFloat(unformatNumber($('#perkeso').val())) || 0;
                var cukai_pendapatan = parseFloat(unformatNumber($('#cukai_pendapatan').val())) || 0;
                var pinjaman_perumahan = parseFloat(unformatNumber($('#pinjaman_perumahan').val())) || 0;
                var zakat = parseFloat(unformatNumber($('#zakat').val())) || 0;
                var lemabaga_tabung_haji = parseFloat(unformatNumber($('#lemabaga_tabung_haji').val())) || 0;
                var pembiayaan_perumahan_lain_lain = parseFloat(unformatNumber($('#pembiayaan_perumahan_lain_lain').val())) || 0;
                var angkasa = parseFloat(unformatNumber($('#angkasa').val())) || 0;
                var koperasi = parseFloat(unformatNumber($('#koperasi').val())) || 0;
                var piutang_gaji_elaun = parseFloat(unformatNumber($('#piutang_gaji_elaun').val())) || 0;
                var angkasa_bukan_pinjaman = parseFloat(unformatNumber($('#angkasa_bukan_pinjaman').val())) || 0;
                var yir = parseFloat(unformatNumber($('#yir').val())) || 0;
                var yir2 = parseFloat(unformatNumber($('#yir2').val())) || 0;

                var jumlahPotongan = pcb + perkeso + cukai_pendapatan + pinjaman_perumahan + zakat + lemabaga_tabung_haji + pembiayaan_perumahan_lain_lain + angkasa + koperasi + piutang_gaji_elaun + angkasa_bukan_pinjaman + yir + yir2;

                var newJumlahBersih = jumlahPendapatan - jumlahPotongan;
                var Max_Deduc = X - jumlahPotongan;
                
                $('#new_jumlah_potongan').val(formatNumber(jumlahPotongan.toString()));
                $('#jml_bersih').val(formatNumber(newJumlahBersih.toString()));
                $('#MaxDeduction').val(formatNumber(Max_Deduc.toString()));


            }

            function updateJumlahPotonganX(){
                var pcb = parseFloat(unformatNumber($('#pcb').val())) || 0;
                var perkeso = parseFloat(unformatNumber($('#perkeso').val())) || 0;
                var cukai_pendapatan = parseFloat(unformatNumber($('#cukai_pendapatan').val())) || 0;
                var pinjaman_perumahan = parseFloat(unformatNumber($('#pinjaman_perumahan').val())) || 0;
                var zakat = parseFloat(unformatNumber($('#zakat').val())) || 0;
                var lemabaga_tabung_haji = parseFloat(unformatNumber($('#lemabaga_tabung_haji').val())) || 0;
                var pembiayaan_perumahan_lain_lain = parseFloat(unformatNumber($('#pembiayaan_perumahan_lain_lain').val())) || 0;
                var angkasa = parseFloat(unformatNumber($('#angkasa').val())) || 0;
                var koperasi = parseFloat(unformatNumber($('#koperasi').val())) || 0;
                var piutang_gaji_elaun = parseFloat(unformatNumber($('#piutang_gaji_elaun').val())) || 0;
                var angkasa_bukan_pinjaman = parseFloat(unformatNumber($('#angkasa_bukan_pinjaman').val())) || 0;
                var yir = parseFloat(unformatNumber($('#yir').val())) || 0;
                var yir2 = parseFloat(unformatNumber($('#yir2').val())) || 0;
                var jumlahPotongan = pcb + perkeso + cukai_pendapatan + pinjaman_perumahan + zakat + lemabaga_tabung_haji + pembiayaan_perumahan_lain_lain + angkasa + koperasi + piutang_gaji_elaun + angkasa_bukan_pinjaman + yir + yir2;
                var monthly_installment = parseFloat(unformatNumber($('#monthly_installment').val())) || 0;
                var dsr_baru_jumlah_potongan = monthly_installment + jumlahPotongan;
                
                $('#new_jumlah_potongan').val(formatNumber(pcb.toString()));

                $('#dsr_jumlah_potongan').val(formatNumber(jumlahPotongan.toString()));
                $('#dsr_jumlah_potongan').val(formatNumber(jumlahPotongan.toString()));
                $('#b_jumlah_potongan').val(formatNumber(jumlahPotongan.toString()));
                $('#dsr_baru_jumlah_potongan').val(formatNumber(dsr_baru_jumlah_potongan.toString()));
            }

            function updateJumlahBersih(){
                var jumlah_pendapatan = parseFloat(unformatNumber($('#jumlah_pendapatan').val())) || 0;
                var jumlah_potongan = parseFloat(unformatNumber($('#jumlah_potongan').val())) || 0;
                var jumlahBersih = jumlah_pendapatan - jumlah_potongan;
                $('#jml_bersih').val(formatNumber(jumlahBersih.toString()));
            }

            function updatePercentage(){
                var jumlah_pendapatan = parseFloat(unformatNumber($('#jumlah_pendapatan').val())) || 0;
                var jumlah_potongan = parseFloat(unformatNumber($('#jumlah_potongan').val())) || 0;
                var dsr_baru_jumlah_potongan = parseFloat(unformatNumber($('#dsr_baru_jumlah_potongan').val())) || 0;
                
                var dsr = (dsr_baru_jumlah_potongan / jumlah_pendapatan) * 100;

                var percentage = (jumlah_potongan / jumlah_pendapatan) * 100;
                $('#percentage').val(formatNumber(percentage.toString()));
                $('#dsr').val(formatNumber(dsr.toString()));
                
            }

            function updateMaxElig(){
                var jumlah_pendapatan = parseFloat(unformatNumber($('#jumlah_pendapatan').val())) || 0;
                var max_eligibility = jumlah_pendapatan * percentageMaxEligibility;
                $('#max_eligibility').val(formatNumber(max_eligibility.toString()));
            }

            function updateX(){
                var jumlah_pendapatan = parseFloat(unformatNumber($('#jumlah_pendapatan').val())) || 0;
                var X = jumlah_pendapatan * percentageMaxEligibility;
                $('#X').val(formatNumber(X.toString()));
            }

            function updateAvailableDed(){
                var max_eligibility = parseFloat(unformatNumber($('#max_eligibility').val())) || 0;
                var jumlah_potongan = parseFloat(unformatNumber($('#jumlah_potongan').val())) || 0;
                var availableDeduction = max_eligibility - jumlah_potongan;
                $('#available_deduction').val(formatNumber(availableDeduction.toString()));
            }

            function updateDsr(){
                var jumlah_pendapatan = parseFloat(unformatNumber($('#jumlah_pendapatan').val())) || 0;
                var jumlah_potongan = parseFloat(unformatNumber($('#jumlah_potongan').val())) || 0;
                var dsrPercentage = (jumlah_potongan / jumlah_pendapatan) * 100;
                $('#dsr_percent').val(formatNumber(dsrPercentage.toString()));
            }

            function updateTotal(){
                var total_payable = parseFloat(unformatNumber($('#total_payable').val())) || 0;
                var roadtax = parseFloat(unformatNumber($('#roadtax').val())) || 0;
                var jmlFinancingRoadtax = roadtax + total_payable;
                $('#jml_financing_roadtax').val(formatNumber(jmlFinancingRoadtax.toString()));
            }

            function checkValueAndUpdateColor() {
                var value = parseFloat($('#dsr').val().replace(/,/g, '')) || 0;
                if (value >= percentageRejected ) {
                    $('#dsr').addClass('red-text');
                } else {
                    $('#dsr').removeClass('red-text');
                }
            }

            function unformatNumber(value) {
                return value.replace(/[^0-9.]/g, '');
            }

            $('.number-input').on('focus', function() {
                var value = $(this).val();
                $(this).val(unformatNumber(value));
            });

            $('.number-input').on('blur', function() {
                var value = $(this).val();
                $(this).val(formatNumber(value));
            });

            $('.number-input').on('blur', function() {
                var value = $(this).val();
                $(this).val(formatNumber(value));
                updateJumlahPendapatan();
                updateJumlahPotongan();
                updateJumlahBersih();
                updatePercentage();
                updateMaxElig();
                updateAvailableDed();
                updateDsr();
                updateTotal();
                updateX();
                checkValueAndUpdateColor();
            });

            

            $('.number-input').on('input', function() {
                var value = $(this).val();
                $(this).val(unformatNumber(value));
            });

            /*$('.number-input').on('keypress', function(e) {
                if (e.which === 13) { // Enter key code
                    e.preventDefault(); // Prevent default action
                    var inputs = $('.number-input');
                    var index = inputs.index(this);
                    if (index < inputs.length - 1) {
                        inputs.eq(index + 1).focus(); // Focus the next input field
                    }
                }
            });*/

            $('.number-input').on('keypress', function(e) {
                if (e.which === 13) { // Enter key code
                    e.preventDefault(); // Prevent default action
                    var nextInput;
                    if (this.id === 'gaji_pokok') {
                        nextInput = $('#imbuhan_tetap_perumahan');
                    } else if (this.id === 'imbuhan_tetap_perumahan') {
                        nextInput = $('#imbuhan_tetap_khidmat_awam');
                    } else if (this.id === 'imbuhan_tetap_khidmat_awam') {
                        nextInput = $('#bantuan_sara_hidup');
                    } else if (this.id === 'bantuan_sara_hidup') {
                        nextInput = $('#bayaran_insentif_tugas_khas');
                    } else if (this.id === 'bayaran_insentif_tugas_khas') {
                        nextInput = $('#perkhidmatan_kritikal');
                    } else if (this.id === 'perkhidmatan_kritikal') {
                        nextInput = $('#elaun_tetap_lain_lain');
                    } else if (this.id === 'elaun_tetap_lain_lain') {
                        nextInput = $('#insentif');
                    } else if (this.id === 'insentif') {
                        nextInput = $('#penanggungan_kerja');
                    } else if (this.id === 'penanggungan_kerja') {
                        nextInput = $('#pcb');
                    } else if (this.id === 'pcb') {
                        nextInput = $('#perkeso');
                    } else if (this.id === 'perkeso') {
                        nextInput = $('#cukai_pendapatan');
                    } else if (this.id === 'cukai_pendapatan') {
                        nextInput = $('#pinjaman_perumahan');
                    } else if (this.id === 'pinjaman_perumahan') {
                        nextInput = $('#zakat');
                    } else if (this.id === 'zakat') {
                        nextInput = $('#lemabaga_tabung_haji');
                    } else if (this.id === 'lemabaga_tabung_haji') {
                        nextInput = $('#pembiayaan_perumahan_lain_lain');
                    } else if (this.id === 'pembiayaan_perumahan_lain_lain') {
                        nextInput = $('#angkasa');
                    } else if (this.id === 'angkasa') {
                        nextInput = $('#koperasi');
                    } else if (this.id === 'koperasi') {
                        nextInput = $('#piutang_gaji_elaun');
                    } else if (this.id === 'piutang_gaji_elaun') {
                        nextInput = $('#angkasa_bukan_pinjaman');
                    } else if (this.id === 'angkasa_bukan_pinjaman') {
                        nextInput = $('#yir');
                    } else if (this.id === 'yir') {
                        nextInput = $('#yir2');
                    } else {
                        nextInput = $(this).closest('.form-group').next('.form-group').find('.number-input');
                    }
                    nextInput.focus();
                    $(this).trigger('blur'); // Trigger blur event to format the number and update jumlah_pendapatan
                }
            });

        });

        function onlyAllowNumbers(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
                event.preventDefault();
            }
        }
    </script>
    <!-- End Input Type For DSR Calculation -->


    <!-- Tooltip -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            // Initialize all tooltips on the page
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- End of Tooltip -->


   

    


@endpush



