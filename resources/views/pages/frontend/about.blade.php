@extends('frontend.template')

@section('main-content')

    <!-- about section -->
    <section class="about_section layout_padding">
        <div class="container">
            <div class="row d-flex align-items-start">
                <div class="col-md-6 d-flex flex-column">
                    <div class="detail-box" style="text-align: justify; flex-grow: 1;">
                        <h2>
                            Mengenai icop.my 
                        </h2>
    
                        <p>ICOP adalah singkatan kepada icop.my yang merupakan satu platfom pembelian takaful atas talian yang terbaru. Dimiliki oleh Masamart Marketing Sdn Bhd (MMSB).</p>
    
                        <p>Kami disokong oleh rakan teknologi NetXpert Sdn Bhd. Ianya merupakan sebuah syarikat berteraskan IT mempunyai kepakaran dan berpengalaman yang luas disektor kewangan dan perkhidmatan.</p>
    
                        <p>Kami juga telah menjalinkan satu ikatan dengan Angkatan Koperasi Kebangsaan Malaysia Berhad (Angkasa) bertujuan berkerjasama memasarkan produk takaful kepada anggota dan gerakan koperasi di Malaysia.</p>
    
                        <p>Pembiayaan mudah juga ada disediakan untuk penjawat awam dan kakitangan syarikat swasta terpilih yang mempunyai kemudahan potongan gaji BPA.</p>
    
                        <p>Pembiayaan yang ditawarkan ini merupakan satu kemudahan pembayaran secara ansuran melalui potongan gaji BPA.</p>
    
                        <p>Kami berkomited untuk membantu anda dengan menyediakan nilai perlindungan yang terbaik.</p>
    
                        <p>Kami boleh dihubungi melalui ………….</p>
                    </div>
    
                    <br>
    
                    <div class="detail-box" style="text-align: justify;">
                        <h2>
                            Mendapatkan sebut harga
                        </h2>
    
                        <p>Cara mendapatkan sebut harga amatlah mudah, anda cuma perlu melangkapkan borang yang disediakan dengan menjawab soalan mengenai anda dan kenderaan sahaja. Kami akan membuat yang membuat yang lain.</p>
    
                        <p>Kami juga ingin mensyorkan agar anda mendapatkan perlindungan tambahan “additional perils” saperti bencana alam dan kemalangan diri bagi menjaga kepentingan diri, kewangan dan aset anda.</p>
                    </div>
                </div>
    
                <div class="col-md-6 d-flex flex-column">
                    <div class="detail-box" style="text-align: justify; flex-grow: 1;">
                        <h2>
                            Mengenai pembiayaan
                        </h2>
    
                        <p>Pembiayaan untuk penjawat awam dan kakitangan syarikat swasta terpilih disediakan untuk pembelian perlindungan takaful dan kos sampingan yang lain-lain.</p>
    
                        <p>Kriteria kelulusan yang mudah dan cepat. Pemohon hanya perlu muat naik dokumen yang diperlukan tertakluk kepada syarat dan terma pembiayaan.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
  <!-- end about section -->



  <!-- info section -->
  <!--
  <section class="info_section ">
    <div class="container">
      <h4>
        Get In Touch
      </h4>
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <div class="info_items">
            <div class="row">
              <div class="col-md-4">
                <a href="">
                  <div class="item ">
                    <div class="img-box ">
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                    </div>
                    <p>
                      Lorem Ipsum is simply dummy text
                    </p>
                  </div>
                </a>
              </div>
              <div class="col-md-4">
                <a href="">
                  <div class="item ">
                    <div class="img-box ">
                      <i class="fa fa-phone" aria-hidden="true"></i>
                    </div>
                    <p>
                      +02 1234567890
                    </p>
                  </div>
                </a>
              </div>
              <div class="col-md-4">
                <a href="">
                  <div class="item ">
                    <div class="img-box">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                    <p>
                      demo@gmail.com
                    </p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="social-box">
      <h4>
        Follow Us
      </h4>
      <div class="box">
        <a href="">
          <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
        <a href="">
          <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <a href="">
          <i class="fa fa-youtube" aria-hidden="true"></i>
        </a>
        <a href="">
          <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
      </div>
    </div>
  </section>
  -->
  <!-- end info_section -->



@endsection


@push('style')
  <style>

    .detail-box {
        min-height: 300px; /* Adjust this value as needed */
    }

    .about_section .row {
          display: flex;
      }

      .about_section .detail-box {
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          height: 100%; /* Ensure boxes take equal height */
      }
  </style>
@endpush