@extends('templates.apps-icop.template')

@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3>{{ __('messages.moto')}} </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb" style="background: white !important;">


                        <div class="section-title">

                             <!-- <h1 class="wow fadeInUp" data-wow-delay="0.6s">Hello, I am <strong>Stimulus</strong> currently York city.</h1> -->

                             
                        <div id="contact-form">

                             <form action="{{ route('login') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                              {{csrf_field()}}

                                   <div class="wow fadeInUp" data-wow-delay="1s">
                                       <label for="vehicle" class="col-form-label">Email </label>
                                       
                                       <div class="input-with-icon">
                                           <input name="email" type="email" class="form-control" id="email" placeholder="Email" value="">
                                       </div>
                                       <div class="error-container" id="vehicle-error"></div>
                                   </div>
                                   
                   

                                  <div class="wow fadeInUp" data-wow-delay="1.2s">
                                       <label for="ic" class="col-form-label">Password </label>
                                       <div class="input-with-icon">
                                           <input name="password" type="password" class="form-control" id="password" placeholder="Password" value="">
                                           <i class="fa fa-eye" id="togglePassword1"></i>
                                       </div>
                                       <div class="error-container" id="ic-error"></div>
                                   </div>


                                   <div class="wow fadeInUp" data-wow-delay="1.4s">
                                     <p class="color-white"><a href="{{route('password.request')}}">Forgot Password</a></p>
                                   </div>



                                
                                  <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="1.8s" style="text-align: center !important;">
                                       
                                       <button class="button-circle">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text">   Login</div>
                                  </div>

                             </form>
                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

<style>

     .red-required {
     color: red;
     font-size: 12pt;
     }

     .invalid-feedback {
          display: none;
          color: red;
     }

     .wow .error-container {
          margin-top: -15px;
          margin-bottom: 10px;
          color: red !important;
          font-size: 10pt;
          font-style: italic;  /* Added to make the font italic */
          
     }

     .is-invalid {
          border-color: red !important;
     }
     .is-invalid ~ .invalid-feedback {
          display: block;
     }

 </style>


@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


      <script>
        document.getElementById('togglePassword1').addEventListener('click', function (e) {
            const password = document.getElementById('password');
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle('fa-eye-slash');
        });
      </script>


     <script>
          $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip(); // Initialize tooltips
  
              // Add custom validation method for non-numeric characters
              $.validator.addMethod("nonNumeric", function(value, element) {
                  return this.optional(element) || /^[^\d]+$/.test(value);
              }, "Please enter non-numeric characters only.");
  
              $("#contact").validate({
                  rules: {
                      vehicle: {
                          required: true,
                          maxlength: 88
                      },
                      ic: {
                          required: true,
                          minlength: 12,
                          maxlength: 12,
                          nonNumeric: false
                      },
                      postcode: {
                          required: true,
                          maxlength: 5,
                          minlength: 5,
                          nonNumeric: false
                      }
                  },
                  messages: {
                      vehicle: {
                          required: 'Please enter your vehicle.'
                      },
                      ic: {
                          required: 'Please enter your New IC Number',
                          minlength: 'Please enter exactly 12 numeric characters.',
                          maxlength: 'Please enter exactly 12 numeric characters.',
                          nonNumeric: 'Please enter a valid IC number numeric characters.'
                      },
                      postcode: {
                          required: 'Please enter your postcode.',
                          minlength: 'Please enter exactly 5 numeric characters.',
                          maxlength: 'Please enter exactly 5 numeric characters.',
                          nonNumeric: 'Please enter a valid Postcode numeric characters.'
                      }
                  },

                  errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },

                highlight: function(element) {
                    $(element).addClass('is-invalid');
                },

                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                },

                /*submitHandler: function(form) {
                    // form.submit(); // Uncomment this to submit the form
                    console.log('Form submitted successfully');
                }*/
              });
  
  
              // Enable or disable the submit button based on form validity
              /* $('input').on('keyup blur change', function() {
                  if ($("#contact").valid()) {
                      $('#calculate').prop('disabled', false);
                  } else {
                      $('#calculate').prop('disabled', true);
                  }
              }); */


          });
      </script>

@endpush