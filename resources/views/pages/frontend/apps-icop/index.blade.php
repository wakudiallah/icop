@extends('templates.apps-icop.template')

@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-6 left">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3>{{ __('messages.moto')}} </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb" style="background: white !important;">

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                        <div class="section-title">

                             <!-- <h1 class="wow fadeInUp" data-wow-delay="0.6s">Hello, I am <strong>Stimulus</strong> currently York city.</h1> -->

                             
                        <div id="contact-form">

                             <form action="{{ url('/result') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                              {{csrf_field()}}

                                   <div class="wow fadeInUp" data-wow-delay="1s">
                                       <label for="vehicle" class="col-form-label">{{__('messages.vehicle')}} <span class="red-required">*</span> </label>
                                       
                                       <div class="input-with-icon">
                                           <input name="vehicle" type="text" class="form-control" id="vehicle_number" placeholder="Vehicle Number" maxlength="200" value="{{ old('vehicle') }}">
                                           <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                       </div>
                                       <div class="error-container" id="vehicle-error"></div>
                                   </div>
                                   

                                  <div class="wow fadeInUp" data-wow-delay="1.2s">
                                       <label for="ic" class="col-form-label">{{__('messages.mykad')}} <span class="red-required">*</span></label>
                                       <div class="input-with-icon">
                                           <input name="ic" type="text" class="form-control" id="ic" placeholder="IC Number" maxlength="12" value="{{ old('ic') }}" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                           <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.ic')}}"></i>
                                       </div>
                                       <div class="error-container" id="ic-error"></div>
                                   </div>

                                

                                  <!-- <div class="wow fadeInUp" data-wow-delay="1.4s">
                                       <label for="postcode" class="col-form-label">{{__('messages.postcode')}} <span class="red-required">*</span></label>
                                       <div class="input-with-icon">
                                            <input type="text" name="postcode" class="form-control" id="postcode" placeholder="Postcode" maxlength="5" value="{{ old('postcode') }}" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                            <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.postcode')}}"></i>
                                        </div>
                                        <div class="error-container" id="postcode-error"></div>
                                        
                                  </div> -->
                                  
         
                                  <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="1.8s" style="text-align: center !important;">
                                       
                                       <button class="button-circle">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text">{{__('messages.continue')}}</div>
                                  </div>

                             </form>
                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

    <style>

        .red-required {
        color: red;
        font-size: 12pt;
        }

        .invalid-feedback {
            display: none;
            color: red;
        }

        .wow .error-container {
            margin-top: -15px;
            margin-bottom: 10px;
            color: red !important;
            font-size: 10pt;
            font-style: italic;  /* Added to make the font italic */
            
        }
    

        .is-invalid {
            border-color: red !important;
        }
        .is-invalid ~ .invalid-feedback {
            display: block;
        }

    </style>


@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


    
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip(); // Initialize tooltips

            // Token embedded from server-side
            var token = '{{ $token }}';

            // Add custom validation method for non-numeric characters
            $.validator.addMethod("nonNumeric", function(value, element) {
                return this.optional(element) || /^[^\d]+$/.test(value);
            }, "Please enter non-numeric characters only.");

            // Add custom validation method for month in IC
            $.validator.addMethod("validMonth", function(value, element) {
                if (value.length < 4) {
                    return false; // If the input length is less than 4, we can't check the month
                }
                var month = parseInt(value.substring(2, 4), 10);
                return month >= 1 && month <= 12;
            }, "Please enter 12 digits of your New IC Number");

            // Add custom validation method for date in IC
            $.validator.addMethod("validDate", function(value, element) {
                if (value.length < 6) {
                    return false; // If the input length is less than 6, we can't check the date
                }
                var date = parseInt(value.substring(4, 6), 10);
                return date >= 1 && date <= 31;
            }, "Please enter 12 digits of your New IC Number");

            // Add custom validation method for postcode
            /*$.validator.addMethod("validPostcode", function(value, element) {
                if (value.length === 5) {
                    var deferred = $.Deferred();

                    $.ajax({
                        url: '{{ url('get-postcode-details') }}/' + value,
                        type: 'GET',
                        success: function(response) {
                            console.log(response); // Log the response for debugging

                            // Check the response structure and respCode
                            if (response === "000") {
                                console.log('Postcode is valid');
                                deferred.resolve(true); // Resolve with true indicating valid postcode
                            } else {
                                console.log('Postcode is not valid');
                                deferred.resolve(false); // Resolve with false indicating invalid postcode
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log("Error:", status, error); 
                            deferred.resolve(false); // Resolve with false indicating invalid postcode
                        }
                    });

                    return deferred.promise();
                }

                return false; // Return false immediately if the postcode length is not 5
            }, "Please enter a valid postcode.");*/


            $("#contact").validate({
                rules: {
                    vehicle: {
                        required: true,
                        maxlength: 88
                    },
                    ic: {
                        required: true,
                        minlength: 12,
                        maxlength: 12,
                        validMonth: true, // Apply the custom month validation
                        validDate: true  // Apply the custom date validation
                    },
                    /*postcode: {
                        required: true,
                        maxlength: 5,
                        minlength: 5,
                        validPostcode: true // Apply the custom postcode validation
                    }*/
                },
                messages: {
                    vehicle: {
                        required: 'Please enter your vehicle.'
                    },
                    ic: {
                        required: 'Please enter your New IC Number',
                        minlength: 'Please enter exactly 12 numeric characters.',
                        maxlength: 'Please enter exactly 12 numeric characters.',
                        validMonth: 'Please enter 12 digits of your New IC Number',
                        validDate: 'Please enter 12 digits of your New IC Number'
                    },
                    /*postcode: {
                        required: 'Please enter your postcode.',
                        minlength: 'Please enter exactly 5 numeric characters.',
                        maxlength: 'Please enter exactly 5 numeric characters.',
                        validPostcode: 'Please enter a valid postcode.'
                    }*/
                },
                errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },
                highlight: function(element) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                }
            });

            // Convert input values to uppercase
            $('input').on('keyup blur change', function() {
                $(this).val($(this).val().toUpperCase());
            });

        });
    </script>





@endpush