@extends('templates.apps-icop.template')



@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  

    <div class="container full-container">
         <div class="row">

              <div class="col-md-6 col-sm-6 left">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline" id="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}}  </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb">

                    Summary

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                        <div id="contact-form">

                            <!-- <label for="vehicle" class="col-form-label">Maklumat Kenderaan </label> --> 
                            <!--<img src="{{asset('gif/success.gif')}}" alt="" align="center" width="30%">  --> 
               
                            <section id="experience" class="parallax-section" style="margin-bottom: 20px !important">
                                <div class="color-white experience-thumb" style="border: 1px !important">
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.register_no')}} : </p>
                                                <h3 class="media-heading"> <b> {{ $data->reg_no }} </b> </h3>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.nombor_pendaftaran_kenderaan')}} : </p>
                                                <h3 class="media-heading"> {{ $data->vehicle }} </h3>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">NRIC :</p>
                                                <h3 class="media-heading"> {{ $data->ic }} </h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Full Name :</p>
                                                <h3 class="media-heading"> {{ $cif->fullname }} </h3>
                                            </div>
                                        </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Phone :</p>
                                                <h3 class="media-heading"> {{ $cif->phone }} </h3>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Email :</p>
                                                <h3 class="media-heading"> {{ $data->email }}</h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>
                                                <div class="media-body">
                                                    <p class="color-white">Marital Status :</p>
                                                    <h3 class="media-heading"> {{ $cif->marital->desc }} </h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>
                                                <div class="media-body">
                                                    <p class="color-white">Gender :</p>
                                                    @if($cif->gender == "F")
                                                    <h3 class="media-heading"> Female </h3>
                                                    @elseif($cif->gender == "M")
                                                    <h3 class="media-heading"> Male </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Address (Line 1):</p>
                                                <h3 class="media-heading"> {{ $cif->address1 }} </h3>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Address (Line 2):</p>
                                                <h3 class="media-heading"> {{ $cif->address2 }}</h3>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>
                                                <div class="media-body">
                                                    <p class="color-white">Postcode:</p>
                                                    <h3 class="media-heading">{{ $cif->postcode }} </h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>
                                                <div class="media-body">
                                                    <p class="color-white">State:</p>
                                                    <h3 class="media-heading">{{ $cif->state }} </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">Region:</p>
                                                @if(!empty($cif->region->region))
                                                <h3 class="media-heading">{{ $cif->region->region }} </h3>
                                                
                                                @endif
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.inclusion_of_special_perils')}} :</p>
                                                @if (isset($covers['57']) && $covers['57']['status'] == 1)
                                                <h3 class="media-heading">Yes </h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.windscreens')}} :</p>
                                                @if (isset($covers['89A']) && $covers['89A']['status'] == 1)
                                                <h3 class="media-heading"> Yes</h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.riot')}} :</p>
                                                @if (isset($covers['25']) && $covers['25']['status'] == 1)
                                                <h3 class="media-heading">Yes </h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.legal_of_passenger')}} :</p>
                                                @if (isset($covers['72']) && $covers['72']['status'] == 1)
                                                <h3 class="media-heading">Yes </h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.legal_to_passenger')}} :</p>
                                                @if (isset($covers['100']) && $covers['100']['status'] == 1)
                                                <h3 class="media-heading">Yes </h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.pa')}} :</p>
                                                @if (isset($covers['200']) && $covers['200']['status'] == 1)
                                                <h3 class="media-heading">Yes</h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.all_driver')}} :</p>
                                                @if (isset($covers['01']) && $covers['01']['status'] == 1)
                                                <h3 class="media-heading">Yes </h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.towing')}} :</p>
                                                @if (isset($covers['202']) && $covers['202']['status'] == 1)
                                                <h3 class="media-heading">Yes</h3>
                                                @else
                                                <h3 class="media-heading">- </h3>
                                                @endif
                                            </div>
                                        </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>

                                                <div class="media-body">
                                                    <p class="color-white">{{__('messages.cart')}} :</p>
                                                    @if (isset($covers['112']) && $covers['112']['status'] == 1)
                                                    <h3 class="media-heading"> Yes</h3>
                                                    @else
                                                    <h3 class="media-heading">- </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                                <div class="media-object media-left">
                                                    <i class="fa fa-check"></i>
                                            </div>
                                            
                                                <div class="media-body">
                                                    <p class="color-white">{{__('messages.roadtax')}} :</p>
                                                    @if (isset($covers['XX']) && $covers['XX']['status'] == 1)
                                                    <h3 class="media-heading">Yes</h3>
                                                    @else
                                                    <h3 class="media-heading">- </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </section>


                            <div class="wow fadeInUp" data-wow-delay="1s">
                                <label for="ic" class="col-form-label">{{ __('messages.basic_premium') }} : </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->totBasicNetAmt,2)}} </label>
                            </div>


                            <div class="wow fadeInUp" data-wow-delay="1.5s">
                                <label for="ic" class="col-form-label">{{ __('messages.rebate') }} (NCD) : </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->rebateAmt,2)}} </label>
                            </div>



                            <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                <label for="ic" class="col-form-label">{{ __('messages.add_on') }}: </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} <span id="totalExtraCover"> {{number_format($intSumExtra,2)}}</span> </label>
                            </div>

                            
                            <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                
                                <div id="add_cover"></div>
                                
                                
                                <div id="add_cover_winscreen">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['89A']) && $covers['89A']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover"> Cover For Windscreens, Windows and Sunroof:</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} {{ number_format($covers['89A']['extCoverPrem'], 2) }} 
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_riot">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['25']) && $covers['25']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover"> Strike Riot & Civil Commotion:</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_riot"> {{ number_format($covers['25']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_legal_liability">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['72']) && $covers['72']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">  Legal Liability Of Passengers For Negligence Coverage :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_legal_liability"> {{ number_format($covers['72']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                
                                <div id="add_cover_legal_liability_passenger">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['100']) && $covers['100']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">   Legal Liability to Passengers :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_legal_liability_passenger"> {{ number_format($covers['100']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_pa_plus">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['200']) && $covers['200']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">  PA Plus :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_pa_plus"> {{ number_format($covers['200']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_perils">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['57']) && $covers['57']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">  Inclusion of Special Perils :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_perils"> {{ number_format($covers['57']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_all_driver">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['01']) && $covers['01']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">   All Drivers :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_all_driver"> {{ number_format($covers['01']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_towing">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['202']) && $covers['202']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">   Towing and Cleaning Due To Water Demage :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_towing"> {{ number_format($covers['202']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_cover_cart">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['112']) && $covers['112']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">  Cart :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_cart"> {{ number_format($covers['112']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>

                                <div id="add_roadtax">
                                    <div data-wow-delay="1.7s">
                                        @if (isset($covers['XX']) && $covers['XX']['status'] == 1)
                                            <label for="ic" class="color-white extra-cover">  Roadtax :</label>
                                            <label for="ic" class="color-white extra-cover pull-right">
                                                {{ config('insurance.currency') }} <span id="add_cover_cart"> {{ number_format($covers['XX']['extCoverPrem'], 2) }}</span>
                                            </label>
                                        @else
                                        @endif
                                    </div>
                                </div>
                                
                                
                            </div>


                            <div class="wow fadeInUp" data-wow-delay="1.4s">
                                <label for="ic" class="col-form-label">{{ __('messages.duti_setem') }} : </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->stampDutyAmt,2)}} </label>
                            </div>


                            <div class="wow fadeInUp" data-wow-delay="1.2s">
                                <label for="ic" class="col-form-label">{{ __('messages.sst') }} % : </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($totalSst,2)}} </label>
                            </div>


                            <hr class="wow fadeInUp" data-wow-delay="1.6s">

                            <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                <label for="ic" class="col-form-label">{{ __('messages.premium_perlu_dibayar') }} : </label>
                                <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} <span id="totalPayable"> {{number_format($basicPremium->ttlPayablePremium,2)}} </span></label>
                            </div>


                            <hr class="wow fadeInUp" data-wow-delay="1.7s">



                            <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                <label for="ic" class="col-form-label">{{__('messages.mode_pembayaran')}} : </label>
                                <br>
                                <label class="radio-inline color-white radio-modebayaran">
                                    <input type="radio" name="payment_mode" id="inlineRadio1" value="1"> {{__('messages.online_payment')}}
                                </label>
                                <br>
                                <label class="radio-inline color-white radio-modebayaran">
                                    <input type="radio" name="payment_mode" id="inlineRadio2" value="0" checked> {{__('messages.pembiayaan')}}
                                </label>
                            </div>

                            <hr class="wow fadeInUp" data-wow-delay="1.7s">

                            <div class="wow fadeInUp" data-wow-delay="1.8s">
                                <label class="color-white label-term">
                                    <input type="checkbox" id="iagree" onchange="updateCheckbox(this)"> {{__('messages.saya_bersetuju')}}<b><i> <u>   <a href="" data-toggle="modal" data-target="#TermCondition" data-whatever="@mdo" class="TermCondition"> {{__('messages.terma_dan_syarat')}} </a></u></i></b>
                                </label>
                            </div>




                            <!-- Modal Confrimation -->
                                <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirm Payment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        Are you sure you want to proceed?
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="button" id="confirmPaymentBtn" class="btn btn-primary">OK</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            <!-- End Modal Confrimation -->


                            <!-- Modal Login Financing -->
                            <div class="modal fade" id="ModalLoginFinancing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="color-white">{{__('messages.create_new_account')}}</h3>
                                    </div>

                                    <div class="modal-body">
                                    <form method="POST" id="contact" action="{{ url('/financing') }}"  class="contactFinancing createAccount">
                                        {{csrf_field()}}


                                        <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                        <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                        <input type="text" name="total_payable" value="{{ $basicPremium->ttlPayablePremium }}">
                                        

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                            <div class="input-with-icon">
                                                <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                            <div class="input-with-icon">
                                                <input type="password" id="password" name="password" class="form-control">
                                                <i class="fa fa-eye" id="togglePassword1"></i>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.confirm_password')}} <span class="red-required">*</span> </label>
                                            <div class="input-with-icon">
                                                <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                                                <i class="fa fa-eye" id="togglePassword2"></i>
                                            </div>
                                        </div>

                                        <!-- Error Message -->
                                        <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                    
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                    </form>

                                </div>
                                </div>
                            </div>
                            <!-- End Modal Login Financing -->


                            <!-- =========== Modal Login Online Payment & create Account =========== -->
                            <div class="modal fade" id="ModalLoginOnlinePayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="color-white">{{__('messages.create_new_account')}}</h3>
                                    </div>

                                    <div class="modal-body">
                                    <form method="POST" id="contact" action="{{ route('pay-option.store') }}"  class="contact createAccount">
                                        {{csrf_field()}}

                                        <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                        <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                            <div class="input-with-icon">
                                                <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                            <div class="input-with-icon">
                                                <input type="password" id="password" name="password" class="form-control">
                                                <i class="fa fa-eye" id="togglePassword1"></i>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.confirm_password')}} <span class="red-required">*</span> </label>
                                            <div class="input-with-icon">
                                                <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                                                <i class="fa fa-eye" id="togglePassword2"></i>
                                            </div>
                                        </div>

                                        <!-- Error Message -->
                                        <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                    
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                    </form>

                                </div>
                                </div>
                            </div>
                            <!-- ============== End Modal Online Payment ============== -->



                            <!-- Modal Login Financing -->
                            <div class="modal fade" id="ModalJustLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="color-white">Login</h3>
                                    </div>

                                    <div class="modal-body">
                                    <form method="POST" id="contact" action="{{ url('/financing') }}"  class="contact createAccount">
                                        {{csrf_field()}}


                                        <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                        <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>
                                        <input type="email" id="email" name="email" value="{{$data->email}}" hidden>

                                        <input type="text" name="total_payable" value="{{ $basicPremium->ttlPayablePremium }}" hidden>


                                    

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                            <div class="input-with-icon">
                                                <input type="password" id="" name="password" class="form-control">
                                                <i class="fa fa-eye" id="togglePassword1"></i>
                                            </div>
                                        </div>

                                        

                                        <!-- Error Message -->
                                        <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                    
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                    </form>

                                </div>
                                </div>
                            </div>
                            <!-- End Modal Login Financing -->



                            <!-- =============== Modal Login Online Payment =============== -->
                            <div class="modal fade" id="ModalJustLoginOnlinePayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="color-white">Login</h3>
                                    </div>

                                    <div class="modal-body">
                                    <form method="POST" id="contact" action="{{ route('pay-option.store') }}"  class="contact createAccount">
                                        {{csrf_field()}}


                                        <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                        <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                            <div class="input-with-icon">
                                                <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="col-form-label">{{__('messages.password')}} </label>
                                            <div class="input-with-icon">
                                                <input type="password" id="passwordView" name="password" class="form-control">
                                                <i class="fa fa-eye" id="togglePasswordView"></i>
                                            </div>
                                        </div>

                                        <!-- Error Message -->
                                        <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                    
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                    </form>

                                </div>
                                </div>
                            </div>
                            <!-- =============== End Modal Login Online Payment =============== -->




                            <!-- Modal Term & Condition -->
                            <div class="modal fade" id="TermCondition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h3 class="color-white">{{__('messages.terma_dan_syarat')}} </h3>
                                    </div>

                                    <div class="modal-body">

                                        <p class="color-white" align="justify"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                        
                                        <p class="color-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                        <p class="color-white"><input type="checkbox" id="iagree" onchange="updateCheckbox(this)"> {{__('messages.saya_bersetuju')}} {{__('messages.terma_dan_syarat')}} </p>
                                    </div>

                                </div>
                                </div>
                            </div>
                            <!-- End Modal Term & Condition -->

                                    
                            <form action="{{ url('/result') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                                {{csrf_field()}}


                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="uuid" value="{{ $uuid }}">
                                <input type="hidden" name="total_payable" value="{{ $basicPremium->ttlPayablePremium }}">

                                
         
                                <div class="wow fadeInUp col-md-6 col-sm-8 button-arrow" data-wow-delay="1.8s" style="text-align: center !important; margin-bottom: 100px !important">
                                            
                                    <button class="button-circle" onclick="submitForm(event)">
                                        <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                    </button>
                                    <div class="button-circle-text" onclick="submitForm(event)">{{__('messages.continue')}}</div>
                                </div>

                            </form>

                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection


@push('css')

    <link rel="stylesheet" href="{{asset('frontApp/css/update-maklumatkenderaan.css')}}">

    <link rel="stylesheet" href="{{asset('frontApp/css/update-additional.css')}}"> 
    
    <style>


        .red-required {
        color: red;
        font-size: 12pt;
        }

        .invalid-feedback {
            display: none;
            color: red;
        }

        .wow .error-container {
            margin-top: -15px;
            margin-bottom: 10px;
            color: red !important;
            font-size: 10pt;
            font-style: italic;  /* Added to make the font italic */
        }

        .is-invalid {
            border-color: red !important;
        }

        .is-invalid ~ .invalid-feedback {
            display: block;
        }

        
        /* color panel result*/
        #experience .experience-thumb{
            background-color: #f6f6f6 !important;
        }

        .color-white{
            color: #3b5999 !important;
            
        }

        .extra-cover{
            font-size: 9pt;
            font-weight: normal !important;
        }

        /* end color panel result*/

        .error-up{
            margin-top: 10px;
        }

        .label-term{
            font-size: 11pt;
        }


    </style>

@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


    
    <!-- submitForm -->
    <script>

        function updateCheckbox(checkbox) {
                if (checkbox.checked) {
                    $('#myModal').modal('show');
                }
                
            }
            
            function submitForm(event) {

                event.preventDefault(); // Prevent the form from submitting immediately

                var paymentForm = document.getElementById('contact');
                var selectedPaymentMode = document.querySelector('input[name="payment_mode"]:checked').value;
                var iAgreeCheckbox = document.getElementById('iagree');
                var paymentUrl;

                if (!iAgreeCheckbox.checked) {
                    // If the checkbox is not checked, do not proceed
                    alert("Please agree to the terms and conditions.");
                    iAgreeCheckbox.checked = true;
                    return;
                }

                // Show the confirmation modal first
                $('#confirmationModal').modal('show');

                // Once the user confirms by clicking "OK" in the confirmation modal
                document.getElementById('confirmPaymentBtn').addEventListener('click', function () {

                    

                    // Function to add CSRF token to the form
                    function addCsrfToken() {
                        var csrfToken = document.createElement('input');
                        csrfToken.setAttribute('type', 'hidden');
                        csrfToken.setAttribute('name', '_token');
                        csrfToken.setAttribute('value', '{{ csrf_token() }}');
                        paymentForm.appendChild(csrfToken);
                    }

                    // Add guest and account status logic
                    var isGuest = {{ Auth::guest() ? 'true' : 'false' }};
                    var checkAccount = {{ $checkAccount !== true ? 'false' : 'true' }};

                    /*====================== Online pay =====================*/
                    if (selectedPaymentMode == "1") { // Online pay

                        if (isGuest && !checkAccount) {
                            console.log("Guest without account detected");
                            // Handle guest without account for online payment

                        } else if (isGuest && checkAccount) {
                            $('#ModalJustLoginOnlinePayment').modal('show'); // Account exists, show login modal
                        } else {
                            paymentUrl = "{{ route('pay-option.store') }}";
                            paymentForm.setAttribute('action', paymentUrl); // Set the form action attribute
                            paymentForm.setAttribute('method', 'POST'); // Set the form method to POST
                            addCsrfToken(); // Add CSRF token to the form

                            paymentForm.submit(); // Submit the form after confirmation
                        }
                    } 
                    
                    /*============================ Financing ==========================*/
                    else {


                        if (isGuest && !checkAccount) {
                            $('#ModalLoginFinancing').modal('show'); // Show the login modal for financing
                        } else if (isGuest && checkAccount) {
                            $('#ModalJustLogin').modal('show'); // Account exists, show login modal
                        } else {
                            paymentUrl = "{{ url('financing') }}";
                            paymentForm.setAttribute('action', paymentUrl); // Set the form action attribute
                            paymentForm.setAttribute('method', 'POST'); // Set the form method to POST
                            addCsrfToken(); // Add CSRF token to the form

                            paymentForm.submit(); // Submit the form after confirmation
                        }
                    }

                });
                }


        function addOtherValue() {
            const payStatus = document.getElementById('realPayStatus').value;
            const realPayable = document.getElementById('realPayable').value;

            // Create hidden input for payStatus
            const payStatusInput = document.createElement('input');
            payStatusInput.setAttribute('type', 'hidden');
            payStatusInput.setAttribute('name', 'payStatus');
            payStatusInput.setAttribute('value', payStatus);
            paymentForm.appendChild(payStatusInput);

            console.log('payStatus input added: ', payStatusInput);

            // Create hidden input for realPayable
            const realPayableInput = document.createElement('input');
            realPayableInput.setAttribute('type', 'hidden');
            realPayableInput.setAttribute('name', 'realPayable');
            realPayableInput.setAttribute('value', realPayable);
            paymentForm.appendChild(realPayableInput);

            console.log('realPayable input added: ', realPayableInput);
        }

        
        // Function to update the input field based on the selected radio button
        function updatePaymentMode() {
            const selectedPaymentMode = document.querySelector('input[name="payment_mode"]:checked').value;
            document.getElementById('checkOptionMethod').value = selectedPaymentMode;
        }

        // Add event listeners to the radio buttons
        document.querySelectorAll('input[name="payment_mode"]').forEach((radio) => {
            radio.addEventListener('change', updatePaymentMode);
        });

        // Initialize the input field with the default selected value
        updatePaymentMode();
    </script>
    <!-- End submitform -->


   

    


    

@endpush