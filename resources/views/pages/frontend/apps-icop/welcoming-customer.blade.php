@extends('templates.apps-icop.template')

@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container">
         <div class="row">
              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}} </h3>
                   </div>
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumbx">

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    
                        <div id="contact-form">

                            <label for="vehicle" class="col-form-label" style="font-size: 17pt !important"> {{__('messages.greeting_selamat_datang')}} </label>
                            
                            <div class="table-responsive">
                                
                                <table id="myTable" class="display table table-responsive" style="width:100%; font-size: 12px;">
                                    
                                    <thead>
                                        <tr>
                                            <td><h4 class="media-heading color-white">{{__('messages.reg_no')}} </h4> </td>
                                            <td><h4 class="media-heading color-white">{{__('messages.aksi')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.nombor_chasis')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.jumlah_pembiayaan')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.angsur_bulanan')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.status')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.submitted_at')}}</h4></td>
                                        </tr>
                                    </thead>

                                    

                                    @foreach($Pra as $pra)
                                    <tr>
                                        <td><p class="color-white">{{ $pra->reg_no }} </p></td>
                                        
                                        <td>
                                            
                                            <a href="{{url('detail-financing/'.$pra->uuid)}}" class="btn btn-info">Detail</a>

                                            @if(empty($pra->doc->bpa179))
                                                <p class="color-white"> <button type="button" class="btn btn-danger staticBackdrop{{$pra->id}}" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Upload Bpa 1/79</button>
                                            @endif
                                           
                                            <!-------------  Modal ----------->
                                            <div class="modal fade" id="staticBackdrop{{$pra->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Upload Bpa 1/79  </h1>
                                                        
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="wow fadeInUp form-file" data-wow-delay="0.6s">
                                                                <label for="bpa" class="col-form-label">{{__('messages.BPA1_79')}} <!-- <span class="red-required">*</span> --></label>
                                                                
                                                                <div id="previewContainerBpa">
                                                                    @if(!empty($data->doc->bpa179))
                                                                        @if($data->ext_bpa == "pdf")
                                                                            <img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">
                                                                        @else
                                                                            <img src="{{ $data->doc->bpa179 }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                                
                                                                <div class="input-with-icon">
                                                                    <form id="contact" enctype="multipart/form-data" class="form-with-tooltip">
                                                                        <input name="bpa" type="file" class="form-control" id="fileInputBpa" accept="image/*, application/pdf" style="flex: 1;">
                                                                        <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.bpa_179')}}" style="margin-left: 10px;"></i>
                                                                    </form>
                                                                </div>
                                                                <div class="error-container" id="bpa179-error">
                                                                    Please upload your Bpa 1/79.
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <p class="color-white">{{ $pra->vehicle }} </p>
                                        </td>

                                        <td><p class="color-white">{{config('insurance.currency')}} {{$pra->finance->total_payable}} </p></td>
                                        <td><p class="color-white">{{config('insurance.currency')}} {{$pra->finance->monthly_installment}} ( {{$pra->finance->deduc_schema}} Month )</p></td>
                                        <td>
                                            <p class="color-white"><span class="label label-{{$pra->status->label}}">{{$pra->status->status_app}}</span></p>
                                            @if(empty($pra->doc->bpa179))
                                            <p class="color-white"><span class="label label-danger">Bpa 1/79 Not Upload </span></p>
                                            @endif
                                        </td>
                                        
                                        <td><p class="color-white"> {{date('Y-m-d', strtotime($pra->created_at) )}}</p></td>
                                    </tr>
                                    @endforeach

                                    
                                </table>

                            </div>

                            <div class="get-quote">
                                <h3 class="color-white blink">{{__('messages.hash')}} </h3>
                                <h3 class="color-white"> <a href="{{url('car-insurance')}}" class="btn btn-primary">New Quote</a> </h3>
                            </div>
                            
                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" rel="stylesheet">

    <style>

        
        #home .home-thumbx{

            padding: 40px 80px 30px 80px !important;
            width: 100% !important;
            margin-top: 160px !important;

        }


        .get-quote{
            text-align: center;
            margin-top: 70px;
            
        }

        .blink{
            animation: blinker 2s linear infinite;
        }

        @keyframes blinker {
            50% {
            opacity: 0;
            }
        }

        .red-required {
        color: red;
        font-size: 12pt;
        }

        .invalid-feedback {
            display: none;
            color: red;
        }

        .wow .error-container {
            margin-top: -15px;
            margin-bottom: 10px;
            color: red !important;
            font-size: 10pt;
            font-style: italic;  /* Added to make the font italic */
            
        }
    

        .is-invalid {
            border-color: red !important;
        }
        .is-invalid ~ .invalid-feedback {
            display: block;
        }

        
        /* color panel result*/
        #experience .experience-thumb{
            background-color: #f6f6f6 !important;
        }

        .color-white{
            color: #3b5999 !important;
            
        }
        /* end color panel result*/


        


    </style>

@endpush



@push('addjs')

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
    
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                "paging": true,    // Enable paging
                "searching": true, // Enable searching
                "ordering": true,  // Enable column ordering
                "language": {
                    "search": '<span class="custom-search-label">Search:</span>',  // Customize search label
                    "paginate": {
                        "first": '<span class="custom-paginate">First</span>',
                        "last": '<span class="custom-paginate">Last</span>',
                        "next": '<span class="custom-paginate">Next</span>',
                        "previous": '<span class="custom-paginate">Previous</span>'
                    }
                }
            });
        });
    </script>
    


     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>
        
        @foreach($Pra as $pra)
        <script>
            $(document).on('click', '.staticBackdrop{{$pra->id}}', function() {
                $('#staticBackdrop{{$pra->id}}').modal('show');
            });
        </script>
        @endforeach




        <script>
            document.getElementById('fileInputBpa').addEventListener('change', function(e) {
                const file = e.target.files[0];
                
                //Check if Selected a file
                if(!file){
                    alert("Please select a file.");
                    e.target.value = ''; //Clear the selected file input
                    return;
                }
    
                //Check if the selected file is too large(greater than 6MB)
                const maxSize = 6 * 1024 * 1024; //6Mb in bytes
                if(file.size > maxSize){
                    alert("Please select a file smaller than 6MB");
                    e.target.value = ''; //Clear the selected file input
                    return;
                }
    
                // Check if the selected file is a valid image or PDF file
                if (!file.type.match(/^image\/(jpeg|jpg|png)$/) && !file.type.match(/^application\/pdf$/)) {
                    alert("Please select a valid JPG, JPEG, PNG image file, or a PDF file.");
                    e.target.value = ''; // Clear the selected file input
                    return;
                }
    
                const formData = new FormData();
                formData.append('file', file);
                formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token
    
                const uploadUrl = "{{ url('upload-bpa') }}/{{ $uuid }}"; // Append the ID to the URL
                
            
                fetch(uploadUrl, {
                    method: 'POST',
                    body: formData,
                    headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                })
                .then(response => response.json())
                .then(data => {
                    const previewContainer = document.getElementById('previewContainerBpa');
                    if (data.file_type === 'image') {
                        previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                        
                    } else if (data.file_type === 'pdf') {
                        previewContainer.innerHTML = `<img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">`;
                        
                    }
    
    
                     // Hide the error container upon successful upload
                     const errorContainer = document.getElementById('bpa179-error');
                    if (errorContainer) {
                        errorContainer.classList.add('hidden');
                    }
    
                    // Hide the previewUploaded element
                    const previewUploadedElement = document.querySelector('.previewUploaded');
                    if (previewUploadedElement) {
                        previewUploadedElement.classList.add('hidden');
                    }
    
                    
    
                })
                .catch(error => console.error('Error:', error));
            });
    
        </script>


        

@endpush