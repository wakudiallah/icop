@extends('templates.apps-icop.template')

@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline" id="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}} </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb">

                        <div id="contact-form">


                            <label for="vehicle" class="col-form-label">{{__('messages.insuran_kenderaan')}} </label>  
                            
                            <p style="color: red"> {{__('messages.require_image')}}  </p>
                           
                            @foreach($insCompany as $insCompany)
                            <section id="experience" class="parallax-section" style="margin-bottom: 20px !important">
                                <div class="color-white experience-thumb" style="border: 1px !important">
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="parallax-section">

                                                @php
                                                    $imageData = base64_decode($insCompany->logo);
                                                    $imageSrc = 'data:image/png;base64,' . base64_encode($imageData);
                                                @endphp

                                                <div class="media-body"  align="center">
                                                    <img src="{{ $imageSrc }}" alt="Base64 Image" class="img img-responsive" style="max-width: 200pt !important">
                                                    
                                                </div>
                                                    @foreach($insCompany->promo as $promos)
                                                    <p class="media-heading">{{ $promos->promo }}</p>
                                                    @endforeach
                                                   
                                                <div class="">
                                                    <div class="col-md-6">
                                                        <!-- <p class="media-heading"> </p> -->
                                                        @if(!empty($insCompany->insurancePolicy->product_disclosure))
                                                        <p> <a href="{{ $insCompany->insurancePolicy->product_disclosure }}" target="_blank">Product Disclosure</a> </p>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6">
                                                        @if(!empty($insCompany->insurancePolicy->policy_wording))
                                                        <p> <a href="{{ $insCompany->insurancePolicy->policy_wording }}" target="_blank">Policy Wording</a> </p>
                                                        @endif
                                                        <!-- <p class="media-heading">Policy Wording </p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </section>
                            @endforeach

                            <!-- <section id="experience" class="parallax-section" style="margin-bottom: 20px !important">
                                <div class="color-white experience-thumb" style="border: 1px !important">
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="parallax-section">

                                                <div class="media-body"  align="center">
                                                    <img src="{{asset('frontApp/images/images.png')}}" alt="Base64 Image" class="img img-responsive" style="max-width: 200pt !important">
                                                    
                                                </div>
                                                
                                                    <p class="media-heading">PERCUMA Khidmat Tunda Kecemasan 24-jam sehingga jarak 150km</p>
                                                    <p class="media-heading">PERCUMA 1 Pemandu Tambahan</p>
                                                    <p class="media-heading">PERCUMA 24-jam Bantuan Tuntutan</p>
                                                    <p class="media-heading">Insurans Kereta Terbaik 2018</p>
                                                    <p class="media-heading">Kelulusan Segera bagi Nota Perlindungan</p>
                                                    <p class="media-heading">Sesuai bagi Kegunaan E-hailing</p>
                                                
                                              
                                                <div class="">
                                                    <div class="col-md-6">
                                                        <p class="media-heading">Product Disclosure </p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="media-heading">Policy Wording </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </section>
                        -->



                             <form action="{{ url('/chose-add-protection') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                              {{csrf_field()}}

                                        <!--  Just nak check -->
                                        <!-- <input type="text" value="{{$uuid}}">
                                        <input type="text" value="{{$basicPrem->totBasicNetAmt}}">
                                        <input type="text" value="{{$basicPrem->stampDutyAmt}}">
                                        <input type="text" value="{{$basicPrem->gst_Pct}}">
                                        <input type="text" value="{{$basicPrem->gst_Amt}}">
                                        <input type="text" value="{{$basicPrem->rebateAmt}}">
                                        <input type="text" value="{{$basicPrem->rebatePct}}">
                                        <input type="text" value="{{$basicPrem->ncdAmt}}"> -->
                                        <!--  End Just nak check -->

                                       

                                        <input type="text" value="{{$uuid}}" name="uuid" hidden>
                                        <input type="text" value="{{$basicPrem->totBasicNetAmt}}" name="basic_premium" hidden>
                                        <input type="text" value="{{$basicPrem->gst_Amt}}" name="sst" hidden>
                                        <input type="text" value="{{$basicPrem->stampDutyAmt}}" name="duti_setem" hidden>
                                        <input type="text" value="1" name="insurance_id" hidden>   <!-- Zurich Insurance -->
                                        <input type="text" value="{{$basicPrem->ttlPayablePremium}}" name="payable" hidden>
                                

                                        <div class="wow fadeInUp" data-wow-delay="1s">
                                                
                                            <!-- <h4 for="ic" class="col-form-label">Premium Asas </h4> -->
                                            <label for="ic" class="col-form-label">{{__('messages.basic_premium')}}  : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPrem->totBasicNetAmt,2)}} </label>
                                                
                                        </div>

                                        <!-- <div class="wow fadeInUp" data-wow-delay="1.2s">
                                            <label for="ic" class="col-form-label">{{__('messages.gross_premium')}} <br> (Selepas NCD {{$basicPrem->ncdAmt}} %): </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} </label>
                                        </div> -->


                                        <div class="wow fadeInUp" data-wow-delay="1.3s">
                                            <label for="ic" class="col-form-label">{{__('messages.rebate')}} (NCD): </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPrem->rebateAmt,2)}}</label>
                                        </div>


                                        <div class="wow fadeInUp" data-wow-delay="1.2s">
                                            <label for="ic" class="col-form-label">{{__('messages.sst')}} ( {{$basicPrem->gst_Pct}} % )   : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPrem->gst_Amt,2)}} </label>
                                        </div>


                                        <div class="wow fadeInUp" data-wow-delay="1.4s">
                                            <label for="ic" class="col-form-label">{{__('messages.duti_setem')}}  : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPrem->stampDutyAmt,2)}} </label>
                                        </div>



                                        

                                        <hr class="wow fadeInUp" data-wow-delay="1.6s">
                                        
                                        <div class="wow fadeInUp" data-wow-delay="1.4s">
                                            <label for="ic" class="col-form-label">{{__('messages.premium_perlu_dibayar')}}  : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}}  {{number_format($basicPrem->ttlPayablePremium, 2)}}  </label>
                                    </div>
                                  

         
                                  <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="1.8s" style="text-align: center !important; margin-bottom: 50px !important">
                                       <button class="button-circle">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text">{{__('messages.continue')}} </div>
                                  </div>

                             </form>
                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

    <link rel="stylesheet" href="{{asset('frontApp/css/update-select-insurance.css')}}">


  


@endpush


@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


     <script>
          $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip(); // Initialize tooltips
  
              // Add custom validation method for non-numeric characters
              $.validator.addMethod("nonNumeric", function(value, element) {
                  return this.optional(element) || /^[^\d]+$/.test(value);
              }, "Please enter non-numeric characters only.");
  
              $("#contactXXX").validate({
                  rules: {
                      vehicle: {
                          required: true,
                          maxlength: 88
                      },
                      ic: {
                          required: true,
                          minlength: 12,
                          maxlength: 12,
                          nonNumeric: false
                      },
                      postcode: {
                          required: true,
                          maxlength: 5,
                          minlength: 5,
                          nonNumeric: false
                      }
                  },
                  messages: {
                      vehicle: {
                          required: 'Please enter your vehicle.'
                      },
                      ic: {
                          required: 'Please enter your New IC Number',
                          minlength: 'Please enter exactly 12 numeric characters.',
                          maxlength: 'Please enter exactly 12 numeric characters.',
                          nonNumeric: 'Please enter a valid IC number numeric characters.'
                      },
                      postcode: {
                          required: 'Please enter your postcode.',
                          minlength: 'Please enter exactly 5 numeric characters.',
                          maxlength: 'Please enter exactly 5 numeric characters.',
                          nonNumeric: 'Please enter a valid Postcode numeric characters.'
                      }
                  },

                  errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },

                highlight: function(element) {
                    $(element).addClass('is-invalid');
                },

                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                },

                /*submitHandler: function(form) {
                    // form.submit(); // Uncomment this to submit the form
                    console.log('Form submitted successfully');
                }*/
              });
  
              // Convert input values to uppercase
              $('input').on('keyup blur change', function() {
                  $(this).val($(this).val().toUpperCase());
              });
  
              // Enable or disable the submit button based on form validity
              /* $('input').on('keyup blur change', function() {
                  if ($("#contact").valid()) {
                      $('#calculate').prop('disabled', false);
                  } else {
                      $('#calculate').prop('disabled', true);
                  }
              }); */


          });
      </script>



@endpush