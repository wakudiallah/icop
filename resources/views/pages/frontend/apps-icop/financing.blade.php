@extends('templates.apps-icop.template')


@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  

    <div class="container">

        <meta name="csrf-token" content="{{ csrf_token() }}">

         <div class="row">

              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline" id="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}} </h3>
                   </div>    
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb">

                        <div id="contact-form">

                            <form id="myForm" action="{{url('dashboard-customer')}}" method="post">
                                {{csrf_field()}}
                                
                                <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                <input type="text" name="monthly" id="monthlyInput" value="" hidden>
                                <input type="text" name="schema" id="schemaInput" value="" hidden>

                                <label for="vehicle" class="col-form-label"> {{ __('messages.pembayaran_pembiayaan')}}<span class="red-required">*</span></label>
                                
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <td><h4 class="media-heading color-white">{{__('messages.skim_potongan')}} </h4> </td>
                                            <td><h4 class="media-heading color-white">{{__('messages.jumlah_pembiayaan')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.angsur_bulanan')}}</h4></td>
                                            <td><h4 class="media-heading color-white">{{__('messages.pilih')}}</h4></td>
                                        </tr>
                                    </thead>

                                    @foreach($monthly as $month)
                                    <tr>
                                        <td><p class="color-white">{{$month->installment}}  {{__('messages.bulan')}} </p></td>
                                        <td><p class="color-white">{{config('insurance.currency')}} {{number_format($finance->total_payable,2)}}    </p></td>
                                        <td>
                                            <p class="color-white">
                                            {{config('insurance.currency')}} 
                                            @php
                                                $installmentAmount = $finance->total_payable / $month->installment 
                                            @endphp
                                            {{ number_format(ceil($installmentAmount), 2) }}

                                            </p>
                                        </td>


                                        <td>
                                            <p class="color-white">
                                                <input type="radio" name="monthly_installment" id="inlineRadio{{ $month->id }}" 
                                                       value="{{ $month->id }}"
                                                       @if($month->id == $selectedMonthId->deduc_schema) checked @endif 
                                                       onchange="updateValues({{ $month->id }}, {{ $month->installment }}, {{ number_format(ceil($installmentAmount), 2) }})" required> 
                                                {{ $month->name }} <!-- Optional: Display month name -->
                                            </p>
                                        </td>
                                    </tr>
                                    @endforeach

                                </table>
                                <h5 class="color-white" style="margin-bottom: 60px">*{{__('messages.note_schema')}}.</h5>

                                <div class="list-group">
                                    <i class="list-group-item">
                                        <div class="media-body">
                                            <h3 class="color-white">{{__('messages.note_download_bpa')}}</h3>
                                            
                                            
                                            <a class="btn btn-success" href="{{ route('documents.show', $doc->document) }}" target="_blank"><i class="fa fa-file"></i> {{__('messages.BPA1_79')}}</a> 
                                           


                                        </div>
                                    </i>
                                </div>
                            </form>

                            

                                  <input type="text" name="uuid" value="{{$uuid}}" hidden>

                                  <div class="wow fadeInUp" data-wow-delay="0.2s">

                                    <label for="ic" class="col-form-label">{{__('messages.mykad')}} <span class="red-required">*</span></label>
                                    <div id="previewContainer">
                                        @if(!empty($data->nric))
                                        <img src="{{ $data->nric }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                        @endif
                                    </div>

                                       
                                       <div class="input-with-icon">
                                            <form id="contact" enctype="multipart/form-data"  class="form-with-tooltip">
                                                
                                                <input name="ic" type="file" class="form-control" id="fileInput" accept="image/*, application/pdf">
                                                <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.nric')}}"></i>
                                            </form>
                                       </div>
                                       <div class="error-container" id="ic-error">
                                            Please upload your NRIC.
                                       </div>
                                   </div>


                                   <div class="wow fadeInUp form-file" data-wow-delay="0.4s">
                                    
                                    <label for="payslip" class="col-form-label">{{__('messages.pay_slip')}} <span class="red-required">*</span></label>

                                    <div id="previewContainerPayslip">
                                        @if(!empty($data->payslip))
                                            <img src="{{ $data->payslip }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                        @endif
                                    </div>

                                        <div class="input-with-icon">
                                            <form id="contact" enctype="multipart/form-data" class="form-with-tooltip">
                                                <input name="payslip" type="file" class="form-control" id="fileInputPaySlip" accept="image/*, application/pdf">
                                                <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.pay_slip')}}"></i>
                                            </form>
                                        </div>
                                        <div class="error-container" id="payslip-error">
                                            Please upload your Payslip.
                                        </div>
                                    </div>


                                    <div class="wow fadeInUp form-file" data-wow-delay="0.6s">
                                        <label for="bpa" class="col-form-label">{{__('messages.BPA1_79')}} <!-- <span class="red-required">*</span> --></label>
                                        
                                        <div id="previewContainerBpa">
                                            @if(!empty($data->bpa179))
                                                @if($data->ext_bpa == "pdf")
                                                    <img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">
                                                @else
                                                    <img src="{{ $data->bpa179 }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                                @endif
                                            @endif
                                        </div>
                                        
                                        <div class="input-with-icon">
                                            <form id="contact" enctype="multipart/form-data" class="form-with-tooltip">
                                                <input name="bpa" type="file" class="form-control" id="fileInputBpa" accept="image/*, application/pdf" style="flex: 1;">
                                                <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.bpa_179')}}" style="margin-left: 10px;"></i>
                                            </form>
                                        </div>
                                        <div class="error-container" id="bpa179-error">
                                            Please upload your Bpa 1/79.
                                        </div>
                                    </div>


                                  <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="0s" style="text-align: center !important; margin-bottom: 50px !important">
                                       <button class="button-circle" onclick="submitForm()">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text" onclick="submitForm()">{{__('messages.continue')}}</div>
                                  </div>

                             <!-- </form> -->

                             


                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

    <link rel="stylesheet" href="{{asset('frontApp/css/update-maklumatkenderaan.css')}}">


<style>

        .error {
            color: red;
            font-size: 10pt !important;
        }

        .error-radio {
            outline: 2px solid red;
        }

        .error-container {
            display: none;
            color: red;
            border-color: red !important;
        }

        .error-border {
            border-bottom: 2px solid red;
        }

        .hidden {
            display: none;
        }

     .red-required {
     color: red;
     font-size: 12pt;
     }

     .invalid-feedback {
          display: none;
          color: red;
     }

     .wow .error-container {
          margin-top: -15px;
          margin-bottom: 10px;
          color: red !important;
          font-size: 10pt;
          font-style: italic;  /* Added to make the font italic */
          
     }
 

     .is-invalid {
          border-color: red !important;
     }
     .is-invalid ~ .invalid-feedback {
          display: block;
     }

     
     /* color panel result*/
     #experience .experience-thumb{
        background-color: #f6f6f6 !important;
     }

     .color-white{
        color: #3b5999 !important;
        
     }
     /* end color panel result*/

    .list-group{
        margin-bottom: 10%;
    }

    /* Left Side full */

    /* End left Side full */
    .form-with-tooltip{
        background: transparent !important; 
        color: black !important; 
        display: flex; 
        align-items: center;
    }

    .form-file{
        margin-top: 35px;
    }

 </style>

 
@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>

      
        <script>
            $(document).ready(function() {
                $("#myForm").validate({
                    rules: {
                        monthly_installment: {
                            required: true
                        }
                    },
                    messages: {
                        monthly_installment: {
                            required: "Please select an option"
                        }
                    },
                        invalidHandler: function(event, validator) {
                        // Focus on the first invalid element
                        if (validator.numberOfInvalids()) {
                            $(validator.errorList[0].element).focus();
                        }
                    }
                });

                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });

            function checkUploads(callback){
                $.ajax({
                    url: "{{ url('check-upload') }}/{{ $uuid }}",
                    method : 'POST',
                    dataType : 'json',
                    success : function(response){
                        
                        //let uploadsValid = response.nric && response.payslip && response.checkBpa179;
                        let uploadsValid = response.nric && response.payslip;

                        //Hide all error messages initially
                        $('#ic-error').hide();
                        $('#payslip-error').hide();
                        $('#bpa179-error').hide();

                        if (!uploadsValid) {
                            if (!response.nric) {
                                $('#ic-error').show();
                                $('#ic-error')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
                            } else if (!response.payslip) {
                                $('#payslip-error').show();
                                $('#payslip-error')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
                            } /*else if (!response.checkBpa179) {
                                $('#bpa179-error').show();
                                $('#bpa179-error')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
                            }*/
                        }
                        callback(uploadsValid);


                    },
                    error: function(){
                        alert("An error occurred while checking the uploads. Please try again.");
                        callback(false);
                    }
                });
            }

            function submitForm() {
                if ($("#myForm").valid()) {
                    checkUploads(function(uploadsValid) {
                        if (uploadsValid) {
                            $("#myForm").submit();
                        }
                    });
                }
            }
        </script>



         
    <script>
        document.getElementById('fileInput').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            // Check if a file is selected
            if (!file) {
                alert("Please select a file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            // Check if the selected file is too large (greater than 6MB)
            const maxSize = 6 * 1024 * 1024; // 6MB in bytes
            if (file.size > maxSize) {
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image file
            if (!file.type.match(/^image\/(jpeg|jpg|png)$/)) {
                alert("Please select a valid JPG, JPEG, or PNG image file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-ic') }}/{{ $uuid }}"; // Append the ID to the URL
            
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainer');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<embed src="${data.base64Data}" type="application/pdf" width="100%" height="600px" />`;
                }

                // Hide the error container upon successful upload
                const errorContainer = document.getElementById('ic-error');
                if (errorContainer) {
                    errorContainer.classList.add('hidden');
                }

                // Hide the previewUploaded element
                const previewUploadedElement = document.querySelector('.previewUploaded');
                if (previewUploadedElement) {
                    previewUploadedElement.classList.add('hidden');
                }
            })
            .catch(error => console.error('Error:', error));
        });
    </script>

    <script>
        document.getElementById('fileInputPaySlip').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            //Check if Selected a file
            if(!file){
                alert("Please select a file.");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            //Check if the selected file is too large(greater than 6MB)
            const maxSize = 6 * 1024 * 1024; //6Mb in bytes
            if(file.size > maxSize){
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image file
            if (!file || !file.type.match(/^image\/(jpeg|jpg|png)$/)) {
                alert("Please select a valid JPG, JPEG, or PNG image file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-payslip') }}/{{ $uuid }}"; // Append the ID to the URL
            
        
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
                headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainerPayslip');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                    
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<embed src="${data.base64Data}" type="application/pdf" width="100%" height="600px" />`;
                    
                }

                 // Hide the error container upon successful upload
                 const errorContainer = document.getElementById('payslip-error');
                if (errorContainer) {
                    errorContainer.classList.add('hidden');
                }

                // Hide the previewUploaded element
                const previewUploadedElement = document.querySelector('.previewUploaded');
                if (previewUploadedElement) {
                    previewUploadedElement.classList.add('hidden');
                }


            })
            .catch(error => console.error('Error:', error));
        });

    </script>


    <script>
        document.getElementById('fileInputBpa').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            //Check if Selected a file
            if(!file){
                alert("Please select a file.");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            //Check if the selected file is too large(greater than 6MB)
            const maxSize = 6 * 1024 * 1024; //6Mb in bytes
            if(file.size > maxSize){
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image or PDF file
            if (!file.type.match(/^image\/(jpeg|jpg|png)$/) && !file.type.match(/^application\/pdf$/)) {
                alert("Please select a valid JPG, JPEG, PNG image file, or a PDF file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-bpa') }}/{{ $uuid }}"; // Append the ID to the URL
            
        
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
                headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainerBpa');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                    
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">`;
                    
                }


                 // Hide the error container upon successful upload
                 const errorContainer = document.getElementById('bpa179-error');
                if (errorContainer) {
                    errorContainer.classList.add('hidden');
                }

                // Hide the previewUploaded element
                const previewUploadedElement = document.querySelector('.previewUploaded');
                if (previewUploadedElement) {
                    previewUploadedElement.classList.add('hidden');
                }

                

            })
            .catch(error => console.error('Error:', error));
        });

    </script>




     <script>
      

          $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip(); // Initialize tooltips
  
              // Add custom validation method for non-numeric characters
              $.validator.addMethod("nonNumeric", function(value, element) {
                  return this.optional(element) || /^[^\d]+$/.test(value);
              }, "Please enter non-numeric characters only.");
  
              $("#contactXXX").validate({
                  rules: {
                      vehicle: {
                          required: true,
                          maxlength: 88
                      },
                      ic: {
                          required: true,
                          minlength: 12,
                          maxlength: 12,
                          nonNumeric: false
                      },
                      postcode: {
                          required: true,
                          maxlength: 5,
                          minlength: 5,
                          nonNumeric: false
                      }
                  },
                  messages: {
                      vehicle: {
                          required: 'Please enter your vehicle.'
                      },
                      ic: {
                          required: 'Please enter your New IC Number',
                          minlength: 'Please enter exactly 12 numeric characters.',
                          maxlength: 'Please enter exactly 12 numeric characters.',
                          nonNumeric: 'Please enter a valid IC number numeric characters.'
                      },
                      postcode: {
                          required: 'Please enter your postcode.',
                          minlength: 'Please enter exactly 5 numeric characters.',
                          maxlength: 'Please enter exactly 5 numeric characters.',
                          nonNumeric: 'Please enter a valid Postcode numeric characters.'
                      }
                  },

                  errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },

                highlight: function(element) {
                    $(element).addClass('is-invalid');
                },

                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                },

                /*submitHandler: function(form) {
                    // form.submit(); // Uncomment this to submit the form
                    console.log('Form submitted successfully');
                }*/
              });
  
              // Convert input values to uppercase
              $('input').on('keyup blur change', function() {
                  $(this).val($(this).val().toUpperCase());
              });
  
              // Enable or disable the submit button based on form validity
              /* $('input').on('keyup blur change', function() {
                  if ($("#contact").valid()) {
                      $('#calculate').prop('disabled', false);
                  } else {
                      $('#calculate').prop('disabled', true);
                  }
              }); */


          });
      </script>


    <!--  update value input type -->
    <script>
        function updateValues(id, schemaValue, monthlyValue) {
            document.getElementById('schemaInput').value = schemaValue;
            document.getElementById('monthlyInput').value = monthlyValue;
        }
    </script>
     <!--  End update value input type -->



@endpush