@extends('templates.apps-icop.template')



@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  

    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-6">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline" id="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}}</h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb">

                        <div id="contact-form">

                            <label for="vehicle" class="col-form-label">{{__('messages.perlindungan_tambahan')}} </label>  
                            
                            <!-- <div class="media-body"  align="center">
                                <img src="{{asset('frontApp/images/images.png')}}" alt="Base64 Image" class="img img-responsive" style="max-width: 200pt !important">
                            </div> -->

                            <section id="experience" class="parallax-section" style="margin-bottom: 20px !important">
                                <div class="color-white experience-thumb">

                                    @php
                                        $imageData = base64_decode($selectedInsurance->insuranceCompany->logo);
                                        $imageSrc = 'data:image/png;base64,' . base64_encode($imageData);
                                    @endphp
                                    
                                    <div class="media-body"  align="center">
                                        <img src="{{$imageSrc}}" alt="Base64 Image" class="img img-responsive" style="max-width: 200pt !important">
                                    </div>
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="parallax-section">

                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </section>


                            


                                <!-- <div class="list-group">
                                    <a class="list-group-item">
                                        <label for="ic" class="col-form-label">{{ __('messages.perlindungan_bencana_khas')}} <span class="red-required">*</span></label>
                                        <div class="media-body">
                                            

                                            <label for="customRange1" class="color-white">Example range</label>
                                            <input type="range" class="form-range" id="customRange1">
                                        </div>
                                    </a>
                                </div> -->

                                <!-- 
                                <div class="container">
                                    <div class="row value-container">
                                        
                                        <div class="col-md-6 col-sm-12">
                                            <div class="value-box" style="border-color: gray;">
                                                <h3 class="media-heading">Market Value<span class="info-icon">&#9432;</span></h3>
                                                
                                                <p class="color-white">RM 56,000.00 - RM 65,000.00</p>
                                            </div>
                                        </div>
                                        
                                       
                                        <div class="col-md-6 col-sm-12">
                                            <div class="value-box" style="border-color: gray;">
                                                <h3 class="media-heading">Agreed Value <span class="info-icon">&#9432;</span></h3>
                                                
                                                <p class="color-white">RM 10,000.00 - RM 65,000.00</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row" id="contact">  
                                    <div class="wow fadeInUp" data-wow-delay="1s">
                                        <label for="ic" class="col-form-label">Agreed Value </label>
                                        <div class="input-with-icon">
                                            <input name="value" type="text" class="form-control" id="value" placeholder="Value" value="">
                                        </div>
                                        <div class="error-container" id="value-error"></div>
                                    </div>
                                </div>
    
                                
                                <div class="row">
                                    <div class="list-group range-slider">
                                        <a class="list-group-item">
                                            <label for="ic" class="col-form-label">Agreed Value <span class="red-required">*</span></label>
                                            <div class="media-body">
                                                <input type="range" class="form-range" id="customRange1">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                 -->
                            <div class="list-group">

                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Cover For Windscreens, Windows and Sunroof <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="windscreen" id="inlineRadio1_winscreen" value="1" onclick="toggleProtection(true)"> {{ __('messages.ya')}}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="windscreen" id="inlineRadio2_winscreen" value="0" onclick="toggleProtection(false)" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                    
                                    <div class="form-detail" id="protection-details" style="display: none;">
                                        <form action="" id="dupcontact">
                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">Jumlah Perlindungan <span class="red-required">*</span> </label>
                                                
                                                <div class="input-with-icon" id="protection-details">
                                                    <input name="windscreenValue" type="text" class="form-control" placeholder="Amount Of Protection" value="">
                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="vehicle-error"></div>
                                                <p>Jumlah perlindungan yang disyorkan:  </p>
                                                <p>Jumlah minima:</p>
                                                
                                            </div>

                                            <button class="btn btn-primary" id="SubmitWindscreen">Submit</button>

                                        </form>
                                    </div>

                                </a>
                            </div>

                            <!-- Strike Riot & Civil Commotion // CODE 25 -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Strike Riot & Civil Commotion <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="strike" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="strike" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End Strike Riot & Civil Commotion -->


                            <!-- Legal Liability Of Passengers For Negligence Coverage  // CODE 72 -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Legal Liability Of Passengers For Negligence Coverage <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="legal_liability" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="legal_liability" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End Legal Liability Of Passengers For Negligence Coverage -->

                            
                            <!-- Legal Liability to Passengers //  CODE 100 -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Legal Liability to Passengers <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="legal_liability_to_passenger" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="legal_liability_to_passenger" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End Legal Liability to Passengers -->


                            <!-- PA Plus //  CODE 200 -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> PA Plus <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="pa_plus" id="inlineRadio1_pa_plus" value="1" onclick="togglePA(true)"> {{ __('messages.ya')}}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="pa_plus" id="inlineRadio2_pa_plus" value="0" onclick="togglePA(false)" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                    

                                    <div class="form-detail" id="pa-plus-details" style="display: none;">
                                        <form action="" id="dupcontact">
                                            <div class="wow">
                                                <!-- <label for="vehicle" class="col-form-label">Jumlah Perlindungan <span class="red-required">*</span> </label> -->
                                                
                                                <div class="plan-options" id="plan-options" style="display:none;">
                                                    <!-- <button type="button" data-plan="plan1">Plan 1</button> -->
                                                    <button type="button" data-plan="1"><label class="btn btn-secondary">Plan 1</label></button>
                                                    <button type="button" data-plan="2"><label class="btn btn-secondary">Plan 2</label></button>
                                                    <button type="button" data-plan="3"><label class="btn btn-secondary">Plan 3</label></button>
                                                    <button type="button" data-plan="4"><label class="btn btn-secondary">Plan 4</label></button>
                                                    <button type="button" data-plan="5"><label class="btn btn-secondary">Plan 5</label></button>

                                                </div>
                                                
                                            </div>

                                        </form>
                                    </div>

                                </a>
                            </div>
                            <!-- End PA Plus -->


                            <!-- Inclusion of Special Perils //  CODE  57-->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Inclusion of Special Perils <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="inclusion_of_special_perils" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="inclusion_of_special_perils" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End Inclusion of Special Perils -->


                            <!-- Private Hire Car Endorsement -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">X Private Hire Car Endorsement <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="private_hire_car" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="private_hire_car" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End Private Hire Car Endorsement -->


                            <!-- All Driver // Code 01 with all_Driver_Ind = y-->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">All Driver <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="all_driver" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="all_driver" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>
                            <!-- End All Driver -->

                         

                            <!-- PA Plus //  CODE 202 -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label"> Towing and Cleaning Due To Water Demage  <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="towing_clean" id="inlineRadio1_towing_clean" value="1" onclick="togglePA(true)"> {{ __('messages.ya')}}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="towing_clean" id="inlineRadio2_towing_clean" value="0" onclick="togglePA(false)" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                    

                                    <div class="form-detail" id="towing-details" style="display: none;">
                                        <form action="" id="dupcontact">
                                            <div class="wow">
                                                <!-- <label for="vehicle" class="col-form-label">Jumlah Perlindungan <span class="red-required">*</span> </label> -->
                                                
                                                <div class="plan-options" id="towing-options" >
                                                    <!-- <button type="button" data-plan="plan1">Plan 1</button> -->
                                                    <button type="button" data-plan="1"><label class="btn btn-secondary">Plan 1</label></button>
                                                    <button type="button" data-plan="2"><label class="btn btn-secondary">Plan 2</label></button>
                                                    <button type="button" data-plan="3"><label class="btn btn-secondary">Plan 3</label></button>

                                                </div>
                                                
                                            </div>

                                        </form>
                                    </div>
                                </a>
                            </div>
                            <!-- End PA Plus -->


                            <!-- CART -->
                            <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">CART <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="cart" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="cart" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                            
                                    <!-- style="display: none;" -->
                                    <div class="form-detail" id="cart_details" style="display: none;">
                                        <form action="" id="dupcontact">
                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">Your Vehicle is in workshop (up to 14 days) <span class="red-required">*</span> </label>
                            
                                                <div class="input-with-icon">
                                                    <select name="cart_option" class="form-control" id="cart_option"> 
                                                        <option value="" selected disabled hidden>- Please Select -</option>
                                                        @foreach($cart as $cartItem)
                                                        <option value="{{$cartItem->id}}"> {{$cartItem->desc}}</option>
                                                        @endforeach
                                                    </select>
                            
                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="vehicle-error"></div>
                                            </div>
                                        </form>
                                    </div>
                                </a>
                            </div>
                            
                            <!-- End CART -->



                            <!-- Pemandu Tambahan -->
                            {{-- <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">{{ __('messages.pemandu_tambahan')}} <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="drivers" id="inlineRadio1" value="1" onclick="toggleAddDriver(true)"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="drivers" id="inlineRadio2" value="0" onclick="toggleAddDriver(false)" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>

                                    <div class="form-detail" id="AddDriver-details" style="display: none;">
                                        
                                        <div id="driverList"></div>

                                        <form action="" id="dupcontact" class="dupcontact">

                                            <input type="text" name="uuid" id="uuid" value="{{ $uuid }}" hidden>

                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">Nama Penuh Pemandu <span class="red-required">*</span> </label>
                                                
                                                <div class="input-with-icon">
                                                    <input name="driver_name" type="text" class="form-control" id="driver_name" placeholder="Driver Name" maxlength="200" value="{{ old('driver_name') }}" maxlength="200">
                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="driver_name-error"></div>
                                            </div>

                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">NRIC Pemandu <span class="red-required">*</span> </label>
                                                
                                                <div class="input-with-icon">
                                                    <input name="driver_nric" type="text" class="form-control" id="driver_nric" placeholder="Driver NRIC" value="{{ old('driver_nric') }}" maxlength="12" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="driver_nric-error"></div>
                                            </div>


                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">Hubungan <span class="red-required">*</span> </label>
                                                
                                                <div class="input-with-icon">
                                                    {{-- <select name="code_relation" class="form-control" id="code_relation" required> 
                                                        <option value="" disabled hidden {{ old('relation') === null ? 'selected' : '' }}>- Please Select -</option>
                                                        @foreach($relation as $relation)
                                                        <option value="{{$relation->code}}" {{ old('relation') == $relation->code ? 'selected' : ''}} >{{$relation->desc}}</option>
                                                        @endforeach
                                                    </select>  --}}
                                                    {{-- <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="code_relation-error"></div>
                                            </div>

                                            <button class="btn btn-primary" id="addDriverButton">Tambah Pemandu</button>

                                        </form>
                                    </div>

                                  </a>
                              </div>--}}
                              <!-- End Pemandu Tambahan -->

                              <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">Test <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="test_api" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="test_api" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>
                                  </a>
                            </div>


                              

                              <div class="list-group">
                                <a class="list-group-item">
                                    <label for="ic" class="col-form-label">{{ __('messages.roadtax')}} <span class="red-required">*</span></label>
                                    <div class="media-body">
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="roadtax" id="inlineRadio1" value="1"> {{ __('messages.ya') }}
                                        </label>
                                        <label class="radio-inline color-white">
                                            <input type="radio" name="roadtax" id="inlineRadio2" value="0" checked> {{ __('messages.tidak') }}
                                        </label>
                                    </div>

                                    <div class="form-detail" id="">
                                        <form action="" id="dupcontact">
                                            <div class="wow">
                                                <label for="vehicle" class="col-form-label">Tempah Roadtax <span class="red-required">*</span> </label>
                                                
                                                <div class="input-with-icon">
                                                    <select name="tempah_roadtax" class="form-control" id="tempah_roadtax" required> 
                                                        <option value="" disabled hidden {{ old('tempah_roadtax') === null ? 'selected' : '' }}>- Please Select -</option>
                                                        
                                                        <option value="" >12 Bulan</option>
                                                        
                                                    </select>

                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                                <div class="error-container" id="vehicle-error"></div>
                                                <p>Saya mengesankan saya tidak diseranai hitam oleh JPJ/PDRM</p>
                                            </div>
                                            
                                        </form>
                                    </div>

                                  </a>
                              </div>


                             <form action="{{ url('/result') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                              {{csrf_field()}}
                                
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="text" value="{{$uuid}}" name="uuid" hidden>
                                    <input type="text" value="A" name="a" hidden>
                            
                                    {{-- <div class="wow fadeInUp mb5" data-wow-delay="1s">            
                                        <!-- <h4 for="ic" class="col-form-label">Premium Asas </h4> -->
                                        <label for="ic" class="col-form-label">{{ __('messages.jenis_perlindungan') }} <span class="red-required">*</span></label>
                                        <!-- <label for="ic" class="col-form-label pull-right">KOMPREHENSIF  <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="hover-icon" style="color: red"> <i class="fa fa-pen"></i> </a> </label> -->
                                        <select name="type-protection" class="form-control pull-right" id="type-protection" required> 
                                            <option value="" disabled hidden {{ old('relation') === null ? 'selected' : '' }}>- Please Select -</option>
                                            @foreach($relation as $relation)
                                            <option value="{{$relation->code}}" {{ old('relation') == $relation->code ? 'selected' : ''}} >{{$relation->desc}}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}

                                    
                              
                                    <div class="wow fadeInUp" data-wow-delay="1s">
                                        <label for="ic" class="col-form-label">{{ __('messages.basic_premium') }} : </label>
                                        <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->totBasicNetAmt,2)}} </label>
                                    </div>


                                    <div class="wow fadeInUp" data-wow-delay="1.2s">
                                            <label for="ic" class="col-form-label">{{ __('messages.sst') }} ( {{number_format($basicPremium->gst_Pct,0)}} %) : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->gst_Amt,2)}} </label>
                                    </div>


                                    <div class="wow fadeInUp" data-wow-delay="1.4s">
                                            <label for="ic" class="col-form-label">{{ __('messages.duti_setem') }} : </label>
                                            <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->stampDutyAmt,2)}} </label>
                                    </div>
                                    <div class="wow fadeInUp" data-wow-delay="1.5s">
                                        <label for="ic" class="col-form-label">Rebate : </label>
                                        <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} {{number_format($basicPremium->rebateAmt,2)}} </label>
                                    </div>

                                    <hr class="wow fadeInUp" data-wow-delay="1.6s">
                                    
                                    <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                        <label for="ic" class="col-form-label">{{ __('messages.premium_perlu_dibayar') }} : </label>
                                        <input type="text" name="total_payable" value="{{$selectedInsurance->ttlPayablePremium}}" hidden>
                                        <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} <span id="totalPayable"> {{number_format($basicPremium->ttlPayablePremium,2)}} </span></label>
                                    </div>

                                    <hr class="wow fadeInUp" data-wow-delay="1.7s">
                                    <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                        <label for="ic" class="col-form-label">Add Cover: </label>
                                        <label for="ic" class="col-form-label pull-right">{{config('insurance.currency')}} <span id="totalExtraCover"></span> </label>
                                    </div>

                                    
                                    <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                        
                                        <div id="add_cover"></div>
                                        <div id="add_cover_riot"></div>
                                        <div id="add_cover_legal_liability"></div>
                                        <div id="add_cover_legal_liability_passenger"></div>
                                        <div id="add_cover_pa_plus"></div>
                                        <div id="add_cover_perils"></div>
                                        <div id="add_cover_all_driver"></div>
                                        <div id="add_cover_towing"></div>
                                        <div id="add_cover_cart"></div>
                                        
                                    </div>



                                    <div class="wow fadeInUp mb7" data-wow-delay="1.7s">
                                        <label for="ic" class="col-form-label">{{__('messages.mode_pembayaran')}} : </label>
                                        <br>
                                        <label class="radio-inline color-white radio-modebayaran">
                                            <input type="radio" name="payment_mode" id="inlineRadio1" value="1"> {{__('messages.online_payment')}}
                                        </label>
                                        <br>
                                        <label class="radio-inline color-white radio-modebayaran">
                                            <input type="radio" name="payment_mode" id="inlineRadio2" value="0" checked> {{__('messages.pembiayaan')}}
                                        </label>
                                    </div>

                                    <input type="text" value="{{$data->email}}" hidden>
                                
                                    
                                    <div class="wow fadeInUp" data-wow-delay="1.8s">
                                        <label class="color-white label-term">
                                            <input type="checkbox" id="iagree" onchange="updateCheckbox(this)"> {{__('messages.saya_bersetuju')}} <b><i> <u>   <a href="" data-toggle="modal" data-target="#TermCondition" data-whatever="@mdo" class="TermCondition"> {{__('messages.terma_dan_syarat')}} </a></u></i></b>
                                        </label>
                                    </div>
                                        
                                  
                                  <div class="wow fadeInUp col-md-6 col-sm-8 button-arrow" data-wow-delay="1.8s" style="text-align: center !important; margin-bottom: 100px !important">
                                       
                                        <button class="button-circle" onclick="submitForm()">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text" onclick="submitForm()">{{__('messages.continue')}}</div>
                                  </div>

                             </form>
                        </div>
                        
                        <!-- ========================  Button Fix ==================== -->
                        <div class="bottom-panel" id="bottomPanel">
                            
                            <h3> <b> {{config('insurance.currency')}} <span id="total-amount"> {{number_format($basicPremium->ttlPayablePremium,2)}} </span> </b> </h3>
                        </div>
                        <!-- ========================  End Button Fix ==================== -->


                        

                        <!-- Modal Jenis Perlindungan -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-groupx">
                                                <label for="fullname" class="col-form-label">Jenis Perlindungan <span class="red-required">*</span> </label>
                                                <div class="input-with-icon">
                                                    <select class="form-control" id="cars" name="protection_type">
                                                        @foreach($coverType as $coverType)
                                                        <option value="{{$coverType->code}}">{{$coverType->desc}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.vehicle')}}"></i>
                                                </div>
                                            </div>

                                            <input type="text" name="email_hidden" value="{{$data->email}}" hidden>
                                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modal Jenis Perlindungan -->


                        <!-- Modal Login Financing -->
                        <div class="modal fade" id="ModalLoginFinancing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="color-white">{{__('messages.create_new_account')}}</h3>
                                </div>

                                <div class="modal-body">
                                  <form method="POST" id="contact" action="{{ url('/financing') }}"  class="contactFinancing createAccount">
                                    {{csrf_field()}}


                                    <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                    <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                        <div class="input-with-icon">
                                            <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                        <div class="input-with-icon">
                                            <input type="password" id="password" name="password" class="form-control">
                                            <i class="fa fa-eye" id="togglePassword1"></i>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.confirm_password')}} <span class="red-required">*</span> </label>
                                        <div class="input-with-icon">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                                            <i class="fa fa-eye" id="togglePassword2"></i>
                                        </div>
                                    </div>

                                    <!-- Error Message -->
                                    <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                </form>

                              </div>
                            </div>
                        </div>
                        <!-- End Modal Login Financing -->


                        <!-- =========== Modal Login Online Payment & create Account =========== -->
                        <div class="modal fade" id="ModalLoginOnlinePayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="color-white">{{__('messages.create_new_account')}}</h3>
                                </div>

                                <div class="modal-body">
                                  <form method="POST" id="contact" action="{{ route('pay-option.store') }}"  class="contact createAccount">
                                    {{csrf_field()}}

                                    <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                    <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                        <div class="input-with-icon">
                                            <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                        <div class="input-with-icon">
                                            <input type="password" id="password" name="password" class="form-control">
                                            <i class="fa fa-eye" id="togglePassword1"></i>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.confirm_password')}} <span class="red-required">*</span> </label>
                                        <div class="input-with-icon">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                                            <i class="fa fa-eye" id="togglePassword2"></i>
                                        </div>
                                    </div>

                                    <!-- Error Message -->
                                    <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                </form>

                              </div>
                            </div>
                        </div>
                        <!-- ============== End Modal Online Payment ============== -->



                        <!-- Modal Login Financing -->
                        <div class="modal fade" id="ModalJustLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="color-white">Login</h3>
                                </div>

                                <div class="modal-body">
                                  <form method="POST" id="contact" action="{{ url('/financing') }}"  class="contact createAccount">
                                    {{csrf_field()}}


                                    <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                    <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>
                                    <input type="email" id="email" name="email" value="{{$data->email}}">

                                  

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.password')}} <span class="red-required">*</span> </label>
                                        <div class="input-with-icon">
                                            <input type="password" id="" name="password" class="form-control">
                                            <i class="fa fa-eye" id="togglePassword1"></i>
                                        </div>
                                    </div>

                                    

                                    <!-- Error Message -->
                                    <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                </form>

                              </div>
                            </div>
                        </div>
                        <!-- End Modal Login Financing -->



                        <!-- =============== Modal Login Online Payment =============== -->
                        <div class="modal fade" id="ModalJustLoginOnlinePayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="color-white">Login</h3>
                                </div>

                                <div class="modal-body">
                                  <form method="POST" id="contact" action="{{ route('pay-option.store') }}"  class="contact createAccount">
                                    {{csrf_field()}}


                                    <input type="text" name="uuid" value="{{$uuid}}" hidden>
                                    <input type="text" name="fullname" value="{{$cif->fullname}}" hidden>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.email')}}  </label>
                                        <div class="input-with-icon">
                                            <input type="email" id="email" name="email" value="{{$data->email}}" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fullname" class="col-form-label">{{__('messages.password')}} </label>
                                        <div class="input-with-icon">
                                            <input type="password" id="passwordView" name="password" class="form-control">
                                            <i class="fa fa-eye" id="togglePasswordView"></i>
                                        </div>
                                    </div>

                                    <!-- Error Message -->
                                    <div id="passwordError" class="error-message" style="display: none; color: red;">Passwords do not match!</div>
                                  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                </form>

                              </div>
                            </div>
                        </div>
                        <!-- =============== End Modal Login Online Payment =============== -->




                        <!-- Modal Term & Condition -->
                        <div class="modal fade" id="TermCondition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="color-white">{{__('messages.terma_dan_syarat')}}</h3>
                                </div>

                                <div class="modal-body">

                                    <p class="color-white" align="justify"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    
                                    <p class="color-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p class="color-white"><input type="checkbox" id="iagree" onchange="updateCheckbox(this)"> {{__('messages.saya_bersetuju')}} {{__('messages.terma_dan_syarat')}} </p>
                                </div>

                              </div>
                            </div>
                        </div>
                        <!-- End Modal Term & Condition -->


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>




@endsection



@push('css')

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('frontApp/css/update-additional.css')}}"> 
    <link rel="stylesheet" href="{{asset('frontApp/css/duplicate-contact.css')}}"> 
    
    

    <style>
        .total-amount {
            font-size: 24px;
            font-weight: bold;
        }

        /* CSS for shaking animation */
        @keyframes shake {
            0%, 100% { transform: translateX(0); }
            10%, 30%, 50%, 70%, 90% { transform: translateX(-10px); }
            20%, 40%, 60%, 80% { transform: translateX(10px); }
        }

        .shake {
            animation: shake 10.0s;
        }

        .form-detail{
            margin-top: 20px;
        }

        /* check box in line */
        .label-term {
            margin-top: 70px;
            display: flex;
            align-items: center;
        }

        .extra-cover{
            font-size: 9pt;
            font-weight: normal !important;
        }

        .label-term input[type="checkbox"] {
            margin-right: 10px; /* Adjust this value to fine-tune the spacing between the checkbox and the label */
        }
        /* end check box in line */

        /* radio in line */
        .radio-modebayaran {
            display: flex;
            align-items: center;
            margin-bottom: -30px; /* Adjust this value if you need more or less space between the radio button groups */
        }

        .radio-modebayaran input[type="radio"] {
            margin-right: 10px; /* Adjust this value to control the space between the radio button and the text */
        }
        /* end radio in line */

    </style>


<style>

    /**/
    .range-slider{
        margin-bottom: 85px;
    }
    /**/


     /* Panel Option */
    .value-container {
        display: flex;
        justify-content: space-between;
        margin-bottom: 15px;
    }

    .value-box {
        padding: 20px;
        border: 2px solid;
        border-radius: 5px;
        text-align: center;
        background-color: #f8f9fa; /* Light background for better contrast */
    }

    .color-white {
        color: #000; /* Changed to black for better readability */
    }

    .info-icon {
        display: inline-block;
        margin-top: 10px;
        color: #1E90FF; /* Change color as needed */
        cursor: pointer;
    }

    @media (max-width: 767px) {
        .value-container {
            flex-direction: column;
        }
    }

    /* End Panel Option */
</style>


 
@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script src="{{asset('frontApp/js/add_protection.js')}}"></script>
    

    <!-- Password -->
    <script>
        document.getElementById('togglePassword1').addEventListener('click', function (e) {
            const password = document.getElementById('password');
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle('fa-eye-slash');
        });
    
        document.getElementById('togglePassword2').addEventListener('click', function (e) {
            const confirmPassword = document.getElementById('confirm_password');
            const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
            confirmPassword.setAttribute('type', type);
            this.classList.toggle('fa-eye-slash');
        });
    
        const password = document.getElementById('password');
        const confirmPassword = document.getElementById('confirm_password');
        const passwordError = document.getElementById('passwordError');
    
        function validatePasswordMatch() {
            if (password.value === confirmPassword.value) {
                passwordError.style.display = 'none';
            } else {
                passwordError.style.display = 'block';
            }
        }
    
        password.addEventListener('input', validatePasswordMatch);
        confirmPassword.addEventListener('input', validatePasswordMatch);

    
        $('input[type="password"], input[type="email"]').on('input', function() {
            removeUppercase(this);
        });

        // Password Match Validation
        document.querySelector('form.contact').addEventListener('submit', function(event) {
            const password = document.getElementById('password').value;
            const confirmPassword = document.getElementById('confirm_password').value;
            if (password !== confirmPassword) {
                event.preventDefault();
                document.getElementById('passwordError').style.display = 'block';
            } else {
                document.getElementById('passwordError').style.display = 'none';
            }
        });


    </script>
    <!-- End Password -->


    <!-- Just Login To View Password -->
    <script>
        document.getElementById('togglePasswordView').addEventListener('click', function () {
            var passwordInput = document.getElementById('passwordView');
            var icon = document.getElementById('togglePasswordView');
            
            if (passwordInput.type === 'password') {
                passwordInput.type = 'text';
                icon.classList.remove('fa-eye');
                icon.classList.add('fa-eye-slash');
            } else {
                passwordInput.type = 'password';
                icon.classList.remove('fa-eye-slash');
                icon.classList.add('fa-eye');
            }
        });
    </script>
    <!-- End Just Login To View Password -->


    

    

    <script>
        $(document).on('click', '.TermCondition', function() {
            $('#TermCondition').modal('show');
        });
    </script>



     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


    <script>

        function updateCheckbox(checkbox) {
                if (checkbox.checked) {
                    $('#myModal').modal('show');
                }
                
            }
            
        function submitForm() {
            var paymentForm = document.getElementById('contact');
            var selectedPaymentMode = document.querySelector('input[name="payment_mode"]:checked').value;
            //document.getElementById('checkOptionMethod').value = selectedPaymentMode;
            var iAgreeCheckbox = document.getElementById('iagree');
            var paymentUrl;

            if (!iAgreeCheckbox.checked) {
                // If the checkbox is not checked, do not proceed
                alert("Please agree to the terms and conditions.");
                iAgreeCheckbox.checked = true;
                return false;
            }

            // Function to add CSRF token to the form
            function addCsrfToken() {
                var csrfToken = document.createElement('input');
                csrfToken.setAttribute('type', 'hidden');
                csrfToken.setAttribute('name', '_token');
                csrfToken.setAttribute('value', '{{ csrf_token() }}');
                paymentForm.appendChild(csrfToken);
            }


            var isGuest = {{ Auth::guest() ? 'true' : 'false' }};
            var checkAccount = {{ $checkAccount !== true ? 'false' : 'true' }};


            /*====================== Online pay =====================*/
            if (selectedPaymentMode == "1") { //online pay

                if( isGuest && !checkAccount ){
                    $('#ModalLoginOnlinePayment').modal('show'); // Show the login modal for financing & create account
                }else if(isGuest && checkAccount){
                    $('#ModalJustLoginOnlinePayment').modal('show'); //account already ada Financing
                }else{
                    paymentUrl = "{{ route('pay-option.store') }}";
                    paymentForm.setAttribute('action', paymentUrl); // Set the form action attribute
                    paymentForm.setAttribute('method', 'POST'); // Set the form method to POST
                    addCsrfToken(); // Add CSRF token to the form
                    paymentForm.submit(); // Submit the form
                }                
            } 
            
            /*============================ Financing ========================== */
            else {
                
                if ( isGuest && !checkAccount ) {
                    $('#ModalLoginFinancing').modal('show'); // Show the login modal for financing & create account
                }else if(isGuest && checkAccount){
                    $('#ModalJustLogin').modal('show'); //account already ada Financing
                } 
                else {
                    
                    paymentUrl = "{{ url('financing') }}";
                    paymentForm.setAttribute('action', paymentUrl); // Set the form action attribute
                    paymentForm.setAttribute('method', 'POST'); // Set the form method to POST
                    addCsrfToken(); // Add CSRF token to the form
                    paymentForm.submit(); // Submit the form
                }                
            }
            
            return false;
            
        }

        // Function to update the input field based on the selected radio button
        function updatePaymentMode() {
            const selectedPaymentMode = document.querySelector('input[name="payment_mode"]:checked').value;
            document.getElementById('checkOptionMethod').value = selectedPaymentMode;
        }

        // Add event listeners to the radio buttons
        document.querySelectorAll('input[name="payment_mode"]').forEach((radio) => {
            radio.addEventListener('change', updatePaymentMode);
        });

        // Initialize the input field with the default selected value
        updatePaymentMode();
    </script>



    <script>
        let lastScrollTop = 0;

        document.addEventListener('scroll', function() {
            const bottomPanel = document.getElementById('bottomPanel');
            const backgroundPosition = window.getComputedStyle(bottomPanel).backgroundPosition;
            const currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

            // Extracting the vertical position value
            const verticalPosition = parseInt(backgroundPosition.split(' ')[1]);

            if (verticalPosition <= 314 && verticalPosition >= 106) {
                if (currentScrollTop > lastScrollTop) {
                    // Scroll down
                    bottomPanel.style.display = 'none';
                } else {
                    // Scroll up
                    bottomPanel.style.display = 'block';
                }
            } else {
                bottomPanel.style.display = 'block';
            }

            lastScrollTop = currentScrollTop <= 0 ? 0 : currentScrollTop;
        });


    </script>



    <!-- Selected Fucntion calculate -->
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            const baseAmount = {{ $selectedInsurance->payable }};
            const windscreenProtectionCost = 200;
            const additionalDriversCost = 300;
            const additionalDisasterCost = 200;

            const totalAmountElement = document.getElementById('total-amount');
            
            const updateTotalAmount = () => {
                let totalAmount = baseAmount;
                if (document.querySelector('input[name="windscreen"]:checked').value === '1') {
                    totalAmount += windscreenProtectionCost;
                }
                if (document.querySelector('input[name="drivers"]:checked').value === '1') {
                    totalAmount += additionalDriversCost;
                }
                if (document.querySelector('input[name="disaster"]:checked').value === '1') {
                    totalAmount += additionalDisasterCost;
                }
                totalAmountElement.textContent = totalAmount.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });

                // Add the shake animation
                totalAmountElement.classList.add('shake');
                // Remove the shake animation after it ends
                totalAmountElement.addEventListener('animationend', () => {
                    totalAmountElement.classList.remove('shake');
                }, { once: true });
            }
            
            document.querySelectorAll('input[type="radio"]').forEach(radio => {
                radio.addEventListener('change', updateTotalAmount);
            });
        });
    </script>
    <!-- End Selected Fucntion calculate -->


    <!-- Bootstrap Bundle with Popper -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    
    <!-- Select Yes Cermin -->
    <script>
        function toggleProtection(show) {
            var protectionDetails = document.getElementById('protection-details');
            if (show) {
                protectionDetails.style.display = 'block';
            } else {
                protectionDetails.style.display = 'none';
            }
        }
    
        // If you need to check the initial state on page load
        document.addEventListener('DOMContentLoaded', function() {
            var initialState = document.querySelector('input[name="windscreen"]:checked').value;
            toggleProtection(initialState == '1');
        });
    </script>


    <!-- Select Yes PA -->
    <script>
        function togglePA(show) {
            var protectionDetails = document.getElementById('pa-plus-details');
            if (show) {
                protectionDetails.style.display = 'block';
            } else {
                protectionDetails.style.display = 'none';
            }
        }

        // If you need to check the initial state on page load
        document.addEventListener('DOMContentLoaded', function() {
            var initialState = document.querySelector('input[name="pa_plus"]:checked').value;
            togglePA(initialState == '1');
        });
    </script>
    <!-- End Select Yes PA -->


    



    <!-- Select Yes Add Driver -->
    <script>
        function toggleAddDriver(show){
            var protectionDetails = document.getElementById('AddDriver-details');
            if (show) {
                protectionDetails.style.display = 'block';
            } else {
                protectionDetails.style.display = 'none';
            }
        }

        // If you need to check the initial state on page load
        document.addEventListener('DOMContentLoaded', function() {
            var initialState = document.querySelector('input[name="drivers"]:checked').value;
            toggleAddDriver(initialState == '1');
        });

    </script>

    <script>
        var uuid = '{{ $uuid }}';
    </script>


    <!-- insert Additional Driver   -->
    <script>
        
        $(document).ready(function() {
            // Convert input text to uppercase
            $('#driver_name, #driver_nric').on('input', function() {
                this.value = this.value.toUpperCase();
            });


            //Fetch and display existing drivers
            $.ajax({
                type: 'GET',
                url: '{{ route("get.drivers", ":uuid") }}'.replace(':uuid', uuid),
                success: function(response) {
                    response.drivers.forEach(function(driver) {
                        var existingDriver = '<div class="driver-entry" data-id="' + driver.id + '">' +
                                            '<p>' + driver.driver_name + ' (' + driver.driver_nric + ') - ' + 
                                            '<button class="delete-driver" data-id="' + driver.id + '">X</button></p></div>';
                        $('#driverList').append(existingDriver);
                    });
                },
                error: function(response) {
                    console.log('Error:', response);
                }
            });


            // Add custom validation method for month in IC
            $.validator.addMethod("validMonth", function(value, element) {
                if (value.length < 4) {
                    return false; // If the input length is less than 4, we can't check the month
                }
                var month = parseInt(value.substring(2, 4), 10);
                return month >= 1 && month <= 12;
            }, "Please enter 12 digits of your New IC Number");

            // Add custom validation method for date in IC
            $.validator.addMethod("validDate", function(value, element) {
                if (value.length < 6) {
                    return false; // If the input length is less than 6, we can't check the date
                }
                var date = parseInt(value.substring(4, 6), 10);
                return date >= 1 && date <= 31;
            }, "Please enter 12 digits of your New IC Number");


    
            // Validate and submit the form via AJAX
            $('.dupcontact').validate({
                rules: {
                    driver_name: {
                        required: true,
                        maxlength: 100
                    },
                    driver_nric: {
                        required: true,
                        maxlength: 12,
                        minlength: 12,
                        digits: true,
                        validMonth: true, // Apply the custom month validation
                        validDate: true  // Apply the custom date validation
                    },
                    code_relation: {
                        required: true
                    }
                },
                messages: {
                    driver_name: {
                        required: "Nama Penuh Pemandu is required",
                        maxlength: "Nama Penuh Pemandu cannot exceed 100 characters"
                    },
                    driver_nric: {
                        required: "NRIC Pemandu is required",
                        maxlength: 'Please enter exactly 12 numeric characters.',
                        minlength: 'Please enter exactly 12 numeric characters.',
                        digits: "NRIC Pemandu must be numeric",
                        validMonth: 'Please enter 12 digits of your New IC Number',
                        validDate: 'Please enter 12 digits of your New IC Number'
                    },
                    code_relation: {
                        required: "Hubungan is required"
                    }
                },

                errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },

                submitHandler: function(form) {
                    var formData = {
                        driver_name: $('#driver_name').val(),
                        driver_nric: $('#driver_nric').val(),
                        code_relation: $('#code_relation').val(),
                        uuid: $('#uuid').val(),
                        _token: '{{ csrf_token() }}'
                    };
    
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('add.driver') }}',
                        data: formData,
                        dataType: 'json',
                        success: function(response) {
                            var newDriver = '<div class="driver-entry" data-id="' + response.driver.id + '">' +
                                        '<p>' + response.driver.driver_name + ' (' + response.driver.driver_nric + ') - ' + 
                                        '<button class="delete-driver" data-id="' + response.driver.id + '">X</button></p></div>';
                            
                            $('#driverList').append(newDriver);
                            // Clear the form fields
                            $('#driver_name').val('');
                            $('#driver_nric').val('');
                            $('#code_relation').val('');
                        },
                        error: function(response) {
                            // Handle validation errors
                            if (response.status === 400 && response.responseJSON.message === 'Driver already exists') {
                                alert('Driver already exists, Please check your Name and Nric');
                            } else {
                                // Handle validation errors
                                var errors = response.responseJSON.errors;
                                $('#driver_name-error').text(errors.driver_name ? errors.driver_name[0] : '');
                                $('#driver_nric-error').text(errors.driver_nric ? errors.driver_nric[0] : '');
                                $('#code_relation-error').text(errors.relation ? errors.code_relation[0] : '');
                            }
                        }
                    });
                }
            });

            // Delete driver via AJAX
            $('#driverList').on('click', '.delete-driver', function() {
                var driverId = $(this).data('id');
                $.ajax({
                    type: 'DELETE',
                    url: '{{ route("delete.driver", ":id") }}'.replace(':id', driverId),  // Construct the URL with the driver ID
                    data: {
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(response) {
                        if(response.success) {
                            $('.driver-entry[data-id="' + driverId + '"]').remove();
                        }
                    },
                    error: function(response) {
                        console.log('Error:', response);
                    }
                });
            });
        });
    </script>
    <!-- End Additional Driver -->


    <!-- Select Radio Button -->
    <script>
        document.querySelectorAll('input[type="radio"]').forEach((elem) => {
            elem.addEventListener("change", function(event) {
                if (event.target.value == 1) {
                    // Yes is selected, trigger the corresponding API call
                    let apiEndpoint = getApiEndpoint(event.target.name);
                    
                    if (event.target.name === "pa_plus") {
                        // Show the PA Plus plan options
                        document.getElementById('plan-options').style.display = 'block';

                        // Add event listeners for the PA Plus plan options
                        document.querySelectorAll('.plan-options button').forEach(planButton => {
                            planButton.addEventListener("click", function() {
                                let selectedPlan = this.getAttribute("data-plan");
                                let planApiEndpoint = getApiEndpoint("pa_plus", selectedPlan);
                                triggerApiCall(planApiEndpoint);
                            });
                        });
                    }

                    else if (event.target.name === "towing_clean") {
                        // Show the Towing and Cleaning plan options
                        document.getElementById('towing-details').style.display = 'block';

                        // Add event listeners for the Towing and Cleaning plan options
                        document.querySelectorAll('#towing-options button').forEach(planButton => {
                            planButton.addEventListener("click", function() {
                                let selectedPlan = this.getAttribute("data-plan");
                                let planApiEndpoint = getApiEndpoint("towing_clean", selectedPlan);
                                triggerApiCall(planApiEndpoint);
                            });
                        });
                    }

                    else if (event.target.name === "cart"){
                        //Show the Cart 
                        document.getElementById('cart_details').style.display = 'block';
                        document.getElementById('cart_option').style.display = 'block';

                        //add event listeners for the cart
                        document.getElementById('cart_option').addEventListener("change", function() {
                            let selectedValue = this.value;
                            console.log('Selected Cart Option:', selectedValue);
                            let cartApiEndpoint = getApiEndpoint("cart");

                            // Trigger the API call with the selected cart value
                            triggerApiCall(cartApiEndpoint, { selected_cart_option: selectedValue });
                        });

                    }

                    else {

                        triggerApiCall(apiEndpoint);
                    }

                } else {
                    //No is selected, trigger a call to the controller
                    let apiEndpoint = getApiEndpoint(event.target.name, 'no');
                    if (apiEndpoint){
                        triggerApiCall(apiEndpoint);
                    }
                    
                    // No is selected, hide the plan options for PA Plus
                    if (event.target.name === "pa_plus") {
                        document.getElementById('plan-options').style.display = 'none';
                    }

                    // No is selected, hide the plan options for Towing and Cleaning
                    if (event.target.name === "towing_clean") {
                        document.getElementById('towing-details').style.display = 'none';
                    }

                    if (event.target.name === "cart") {
                        document.getElementById('cart_details').style.display = 'none';
                    }
                }
            });
        });

        function getApiEndpoint(optionName, planName = null) {
            switch(optionName) {
                
                case "strike":
                    return planName === 'no' ? "{{ url('/strike-no') }}/" + uuid : "{{ url('/strike-api/') }}/" + uuid;  // Handle "No" for strike
                case "cart": 
                    return planName === 'no' ? "{{ url('/cart-no') }}/" + uuid : "{{ url('/cart/') }}/" + uuid;
                case "inclusion_of_special_perils": 
                    return "{{ url('/inclusion-special-perils/') }}/" + uuid;
                case "legal_liability":
                    return "{{ url('/legal-liability/') }}/" + uuid;
                case "all_driver":
                    return "{{ url('/all-driver/') }}/" + uuid;
                case "legal_liability_to_passenger":
                    return "{{ url('/legal-liability-passenger/') }}/" + uuid;
                case "test_api":
                    return "{{ url('/test-api/') }}/" + uuid;
                case "pa_plus":
                    return planName === 'no' ? "{{ url('/pa-plus-no') }}/" + uuid : "{{ url('/pa-plus') }}/" + planName + "/" + uuid;  // Route for PA Plus with planName
                case "towing_clean":
                    return planName == 'no' ? "{{ url('/towing-cleaning-no') }}/" + uuid : "{{ url('/towing-cleaning') }}/" + planName + "/" + uuid;  // Route for Towing and Cleaning with planName
                default:
                    return null;
            }
        }

        function triggerApiCall(url, requestData = {}) {
            if (url) {
                //console.log('Request Payload:', requestData); // Log the payload before sending

                fetch(url, {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}', // Include CSRF token for security
                    },
                    body: JSON.stringify(requestData)     //data sent
                })
                .then(response => response.json())
                .then(data => {
                    console.log('API Response:', data);
                    // Handle the response data here
                    if(data.success){
                        console.log('API Response:', data.extCode);
                        // Handle specific case where extCode === 25
                        if (data.extCode === "25" && data.status === true) {
                            insertSuccessForRiot(data.premium, data.totalExtCover, data.payable);  // Call the function to insert success message
                        } else if(data.extCode === "25" && data.status === false){
                            console.log("The 'No' option is selected, disabling insertSuccessForRiot.");
                            disableInsertSuccessForRiot(data.totalExtCover, data.payable);
                        } else if(data.extCode === "72"){
                            insertSuccessForLegalLiability(data.premium);
                        } else if(data.extCode === "100"){
                            insertSuccessForLegalLiabilityPassenger(data.premium);
                        } else if(data.extCode === "200"){
                            insertSuccessForPA(data.premium);
                        } else if(data.extCode === "112" && data.status === true){
                            insertSuccessForCart(data.premium);
                        }else if (data.extCode === "112" && data.status === false){
                            disableInsertSuccessForCart();
                        }else {
                            // Handle other cases where the API was successful
                            console.log('Other extCode:', data.extCode);
                        }

                    }else{

                    // Handle the case when success is false
                    console.error('API call failed:', data.message);

                    }
                })
                .catch(error => {
                    console.error('Error:');
                    // Handle the error here
                });
            }
        }


        function insertSuccessForRiot(premium, totalExtCover, payable) {
            
            const addCoverDiv = document.getElementById('add_cover_riot');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');
            

            // Update the content inside #add_cover
            addCoverDiv.innerHTML = `
                <label for="ic" class="color-white extra-cover">
                    Strike Riot & Civil Commotion:
                    
                </label>
                <label for="ic" class="color-white extra-cover pull-right">
                    {{ config('insurance.currency') }} ${parseFloat(premium).toFixed(2)}
                </label>
            `;
            
            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;

        } 

        function disableInsertSuccessForRiot(totalExtCover, payable, payable) {
            const addCoverDiv = document.getElementById('add_cover_riot');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            if (addCoverDiv) {
                // You can either clear the content or hide the div
                addCoverDiv.innerHTML = '';  // Clear the content
                // Or hide the element
                // addCoverDiv.style.display = 'none';
            }

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }


        function insertSuccessForCart(premium, totalExtCover, payable) {
            
            const addCoverDiv = document.getElementById('add_cover_cart');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            // Update the content inside #add_cover
            addCoverDiv.innerHTML = `
                <label for="ic" class="color-white extra-cover">
                    Cart:
                    
                </label>
                <label for="ic" class="color-white extra-cover pull-right">
                    {{ config('insurance.currency') }} ${parseFloat(premium).toFixed(2)}
                </label>
            `;
        } 

        function disableInsertSuccessForCart() {
            const addCoverDiv = document.getElementById('add_cover_cart');
            if (addCoverDiv) {
                // You can either clear the content or hide the div
                addCoverDiv.innerHTML = '';  // Clear the content
                // Or hide the element
                // addCoverDiv.style.display = 'none';
            }
        }

        

        function insertSuccessForLegalLiability(premium, totalExtCover, payable) {
            
            const addCoverDiv = document.getElementById('add_cover_legal_liability');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            

            // Update the content inside #add_cover
            addCoverDiv.innerHTML = `
                <label for="ic" class="color-white extra-cover">
                    Legal Liability Of Passengers For Negligence Coverage :
                    
                </label>
                <label for="ic" class="color-white extra-cover pull-right">
                    {{ config('insurance.currency') }} ${parseFloat(premium).toFixed(2)}
                </label>
            `;

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}`;

            
        }


        function disableInsertSuccessForRiot(totalExtCover, payable) {
            const addCoverDiv = document.getElementById('add_cover_riot');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            if (addCoverDiv) {
                // You can either clear the content or hide the div
                addCoverDiv.innerHTML = '';  // Clear the content
                // Or hide the element
                // addCoverDiv.style.display = 'none';
            }

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }



        function insertSuccessForLegalLiabilityPassenger(premium, totalExtCover, payable) {
            
            const addCoverDiv = document.getElementById('add_cover_legal_liability_passenger');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            // Update the content inside #add_cover
            addCoverDiv.innerHTML = `
                <label for="ic" class="color-white extra-cover">
                    Legal Liability to Passengers :
                    
                </label>
                <label for="ic" class="color-white extra-cover pull-right">
                    {{ config('insurance.currency') }} ${parseFloat(premium).toFixed(2)}
                </label>
            `;

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }

        function disableInsertSuccessForLegalLiabilityPassenger(totalExtCover, payable) {
            const addCoverDiv = document.getElementById('add_cover_legal_liability_passenger');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            if (addCoverDiv) {
                // You can either clear the content or hide the div
                addCoverDiv.innerHTML = '';  // Clear the content
                // Or hide the element
                // addCoverDiv.style.display = 'none';
            }

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }



        function insertSuccessForPA(premium, totalExtCover, payable) {
            
            const addCoverDiv = document.getElementById('add_cover_pa_plus');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            // Update the content inside #add_cover
            addCoverDiv.innerHTML = `
                <label for="ic" class="color-white extra-cover">
                    PA Plus :
                    
                </label>
                <label for="ic" class="color-white extra-cover pull-right">
                    {{ config('insurance.currency') }} ${parseFloat(premium).toFixed(2)}
                </label>
            `;

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }


        function disableInsertSuccessForCart(totalExtCover, payable) {
            const addCoverDiv = document.getElementById('add_cover_pa_plus');

            const addTotalExtraCover = document.getElementById('totalExtraCover');

            const addTotalPayable = document.getElementById('totalPayable');

            if (addCoverDiv) {
                // You can either clear the content or hide the div
                addCoverDiv.innerHTML = '';  // Clear the content
                // Or hide the element
                // addCoverDiv.style.display = 'none';
            }

            addTotalExtraCover.innerHTML = `${parseFloat(totalExtCover).toFixed(2)}
            `;

            addTotalPayable.innerHTML = `${parseFloat(payable).toFixed(2)}
            `;
        }
        
        

    </script>    
    <!-- End Select Radio Button -->


  

   


@endpush