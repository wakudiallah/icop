@extends('templates.apps-icop.template')



@section('content')


<section id="home" class="parallax-section">

    @include('sweetalert::alert')  


    <div class="container full-container">
         <div class="row">

              <div class="col-md-6 col-sm-6 left">

                   <div class="home-img-logo"> 
                   </div>
                   
                   <div class="home-img"> 
                   </div>

                   <div class="tagline" id="tagline">
                        <h1> <b> {{ __('messages.welcome')}} </b></h1>
                        <h3> {{ __('messages.moto')}}  </h3>
                   </div>
                   
                   
              </div>

              <div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
                   <div class="home-thumb">

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                        <div id="contact-form">

                            <!-- <label for="vehicle" class="col-form-label">Maklumat Kenderaan </label> --> 
                            <!--<img src="{{asset('gif/success.gif')}}" alt="" align="center" width="30%">  --> 
               
                            <section id="experience" class="parallax-section" style="margin-bottom: 20px !important">
                                <div class="color-white experience-thumb" style="border: 1px !important">
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.2s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.nombor_pendaftaran_kenderaan')}} : </p>
                                                <h3 class="media-heading">{{$vehRegNo}} </h3>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.nombor_chasis')}} :</p>
                                                <h3 class="media-heading"> {{ $vehChassisNo }}</h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.nombor_enjin')}} :</p>
                                                <h3 class="media-heading"> {{$vehEngineNo}}</h3>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.tahun_pembuatan')}} :</p>
                                                <h3 class="media-heading"> {{$vehMakeYear}}</h3>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.varian')}} :</p>
                                                <h3 class="media-heading">{{$variant}}</h3>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                        
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.ncd')}} :</p>
                                                <h3 class="media-heading"> {{$ncd}} %</h3>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>
                                            <div class="media-body">
                                                <p class="color-white">{{__('messages.kapasiti_kenderaan')}} :</p>
                                                <h3 class="media-heading">{{$engineSize}} {{$cc}} </h3>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="wow fadeInUp color-white media" data-wow-delay="1.6s">
                                            <div class="media-object media-left">
                                                <i class="fa fa-check"></i>
                                        </div>

                                            <div class="media-body">
                                                <p class="color-white">Variant :</p>
                                                <h3 class="media-heading"> 
                                                    <select name="variant" class="form-control" id="variant" required> 
                                                        <option value="" selected disabled hidden>- Please Select -</option>
                                                        @if(!empty($Vehiclevarint) && is_array($Vehiclevarint))
                                                            @foreach($Vehiclevarint as $variant)
                                                            <option value="{{ $variant['id'] }}">{{ $variant['vehBody'] }} - {{ $variant['vehVariantDesc'] }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">No variants available</option>
                                                        @endif
                                                    </select>
                                                    <br>
                                                    <div class="error-container error-up" id="variant-error"></div>
                                                </h3>

                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>
                            </section>

                            
                            <form action="{{ url('/chose-insurance-result') }}" method="post" id="contact" style="background: transparent !important; color: black !important;">
                                  {{csrf_field()}}
                                
                                  <input type="text" value="{{$uuid}}" name="uuid" hidden>
                                  <input type="text" value="{{$vehRegNo}}" name="vehicle" hidden>
                                  <input type="text" value="{{$postcode}}" name="postcode" hidden>
                                  <input type="text" value="{{$eId}}" name="eId" hidden>


                                  <div class="row">  
                                    <div class="wow fadeInUp" data-wow-delay="1s">
                                        <label for="ic" class="col-form-label">{{__('messages.mykad')}} </label>
                                        <div class="input-with-icon">
                                            <input name="ic" type="text" class="form-control" id="ic" placeholder="IC Number" maxlength="12" value="{{$ic}}" readonly>
                                        </div>
                                        <div class="error-container" id="ic-error"></div>
                                    </div>
                                  </div>

                                  <div class="row"> 
                                   <div class="wow fadeInUp" data-wow-delay="1.2s">
                                    <label for="ic" class="col-form-label">{{__('messages.nama_penuh')}} <span class="red-required">*</span> </label>
                                    <div class="input-with-icon">
                                        <input name="fullname" type="text" class="form-control" id="fullname" placeholder="Full Name"  value="{{ old('fullname') }}" required>
                                        <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.full_name')}}"></i>
                                    </div>
                                    <div class="error-container" id="fullname-error"></div>
                                  </div>
                                </div>


                                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="wow fadeInUp" data-wow-delay="1.2s">
                                            <label for="fullname" class="col-form-label">{{__('messages.phone')}} <span class="red-required">*</span> </label>
                                            
                                            <div class="input-with-icon">
                                                <input name="phone" type="tel" class="form-control" id="phone" placeholder="Phone"  maxlength="10" value="{{ old('phone') }}" required pattern="[0-9]{10}" >
                                                <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.phone')}}"></i>
                                            </div>
                                            <div class="error-container" id="phone-error"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="wow fadeInUp" data-wow-delay="1.2s">
                                            <label for="email" class="col-form-label">{{__('messages.email')}} <span class="red-required">*</span> </label>
                                            
                                            <div class="input-with-icon">
                                                @if(Auth::guest())
                                                <input name="email" type="email" class="form-control" id="email" placeholder="E-mail" value="{{ old('email') }}" required>
                                                @else
                                                <input name="email" type="email" class="form-control" id="email" placeholder="E-mail" value="{{ Auth::user()->email }}" required readonly>
                                                @endif
                                                <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.email')}}"></i>
                                            </div>
                                            <div class="error-container" id="email-error"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="wow fadeInUp" data-wow-delay="1.3s">
                                        <label for="email" class="col-form-label">{{__('messages.marital_status')}} <span class="red-required">*</span> </label>
                                        
                                        <div class="input-with-icon">
                                            <select name="marital_status" class="form-control" id="marital_status" required> 
                                                <option value="" disabled hidden {{ old('marital_status') === null ? 'selected' : '' }}>- Please Select -</option>
                                                @foreach($zMaritalSts as $zMaritalSts)
                                                <option value="{{$zMaritalSts->code}}" {{ old('marital_status') == $zMaritalSts->code ? 'selected' : ''}} >{{$zMaritalSts->desc}}</option>
                                                @endforeach
                                            </select>
                                            <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.marital_status')}}"></i>
                                        </div>
                                        <div class="error-container" id="marital_status-error"></div>
                                    </div>
                                </div>
                                   
                                <div class="row"> 
                                  <div class="wow fadeInUp" data-wow-delay="1.4s">
                                       <label for="address1" class="col-form-label">{{__('messages.alamat1')}} <span class="red-required">*</span></label>
                                       <div class="input-with-icon">
                                            <input name="address1" type="text" class="form-control" id="address1" placeholder="Address (Line 1)" value="{{ old('address1') }}">
                                            <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.address_1')}}"></i>
                                        </div>
                                        <div class="error-container" id="address1-error"></div>
                                  </div>
                                </div>

                                <div class="row"> 
                                  <div class="wow fadeInUp" data-wow-delay="1.4s">
                                    <label for="address2" class="col-form-label">{{__('messages.alamat2')}} <span class="red-required">*</span></label>
                                    <div class="input-with-icon">
                                         <input name="address2" type="text" class="form-control" id="address2" placeholder="Address (Line 2)" value="{{ old('address2') }}">
                                         <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.address_2')}}"></i>
                                     </div>
                                     <div class="error-container" id="address2-error"></div>
                                  </div>
                                </div>

                                <div class="row"> 
                                  <div class="col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay="1.2s">
                                        <label for="postcode" class="col-form-label">{{__('messages.poskod')}}  </label>
                                        
                                        <div class="input-with-icon">
                                            <input name="postcode" type="text" class="form-control postcode" id="postcode" value="{{ old('postcode') }}" placeholder="Postcode" maxlength="5" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                            <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.postcode')}}"></i>
                                        </div>
                                        <div class="error-container" id="postcode-error"></div>
                                    </div>
                                  </div>

                                  <div class="col-md-6">
                                    <div class="wow fadeInUp" data-wow-delay="1.2s">
                                        <label for="state" class="col-form-label">{{__('messages.negri')}} </label>
                                        
                                        <div class="input-with-icon">
                                            <input name="state" type="text" class="form-control state" id="state" placeholder="State" value="" readonly>
                                        </div>
                                        <div class="error-container" id="state-error"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row"> 
                                    <div class="wow fadeInUp" data-wow-delay="1.4s">
                                      <label for="region" class="col-form-label">{{__('messages.region')}} <span class="red-required">*</span></label>
                                      <div class="input-with-icon">
                                            <select name="region" class="form-control" id="region" required> 
                                                <option value="" disabled hidden {{ old('region') === null ? 'selected' : '' }}>- Please Select -</option>
                                                @foreach($region as $region)
                                                <option value="{{$region->id}}" {{ old('region') == $region->id ? 'selected' : ''}} >{{$region->region}}</option>
                                                @endforeach
                                            </select>
                                            <i class="fas fa-info-circle info-icon" data-toggle="tooltip" data-placement="top" title="{{config('insuranceTooltip.region')}}"></i>
                                        </div>
                                       <div class="error-container" id="region-error"></div>
                                    </div>
                                  </div>
                                  
         
                                  <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="1.8s" style="text-align: center !important; margin-bottom: 50px !important">
                                       
                                       <button class="button-circle">
                                            <span>&#10140;</span> <!-- Unicode character for right arrow -->
                                        </button>
                                        <div class="button-circle-text">{{__('messages.continue')}}</div>
                                  </div>

                             </form>
                        </div>


                        </div>
                   </div>
              </div>


         </div>
    </div>
</section>

@endsection

@push('css')

<link rel="stylesheet" href="{{asset('frontApp/css/update-maklumatkenderaan.css')}}">

<style>


    .red-required {
     color: red;
     font-size: 12pt;
     }

    .invalid-feedback {
          display: none;
          color: red;
     }

    .wow .error-container {
          margin-top: -15px;
          margin-bottom: 10px;
          color: red !important;
          font-size: 10pt;
          font-style: italic;  /* Added to make the font italic */
     }

    .is-invalid {
          border-color: red !important;
    }

    .is-invalid ~ .invalid-feedback {
          display: block;
     }

     
     /* color panel result*/
     #experience .experience-thumb{
        background-color: #f6f6f6 !important;
     }

     .color-white{
        color: #3b5999 !important;
        
     }
     /* end color panel result*/

     .error-up{
        margin-top: 10px;
     }


 </style>

@endpush



@push('addjs')

     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

     <script>
          // Initialize tooltips
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
      </script>


    <script>
      
          $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip(); // Initialize tooltips
  
              // Add custom validation method for non-numeric characters
              $.validator.addMethod("nonNumeric", function(value, element) {
                  return this.optional(element) || /^[^\d]+$/.test(value);
              }, "Please enter non-numeric characters only.");

              

              // Add custom validation method for phone number starting with 0
            $.validator.addMethod("startsWithZero", function(value, element) {
                return this.optional(element) || /^0\d*$/.test(value);
            }, "The phone number must start with 0 and contain only numeric characters.");

            // Add custom validation method for valid postcode
            $.validator.addMethod("validPostcode", function(value, element) {
                const postcode = value;
                let isValid = false;
                if (postcode) {
                    $.ajax({
                        url: '{{ url('get-postcode-zurich') }}',
                        type: 'GET',
                        data: { postcode: postcode, uuid: '{{ $uuid }}' },
                        async: false, // Make the request synchronous
                        success: function(response) {
                            $('#state').val(response.state);
                            isValid = true;
                        },
                        error: function() {
                            $('#state').val('');
                            isValid = false;
                        }
                    });
                }
                return isValid;
            }, "Invalid postcode or unable to fetch state.");
  
              $("#contact").validate({
                  rules: {
                      fullname: {
                          required: true,
                          maxlength: 88
                      },
                      email: {
                          required: true,
                          //minlength: 12,
                          //maxlength: 12,
                          //nonNumeric: false
                      },
                      phone:{
                        required: true,
                        nonNumeric: false,
                        maxlength: 10,
                        minlength: 9,
                        startsWithZero: true // Apply custom validation method
                      },
                      marital_status:{
                        required: true,
                      },
                      address1:{
                        required: true,
                      },
                      address2:{
                        required: true,
                      },
                      postcode: {
                          required: true,
                          maxlength: 5,
                          minlength: 5,
                          nonNumeric: false,
                          validPostcode: true
                      }
                  },
                  messages: {
                    fullname: {
                          required: 'Please enter your full name.'
                      },
                      phone:{
                        required: 'Please enter your phone number',
                        nonNumeric: 'Please enter a valid Phone Number numeric characters.',
                        minlength: 'Please enter exactly 9 numeric characters.',
                        maxlength: 'Please enter exactly 10 numeric characters.',
                        startsWithZero: 'The phone number must start with 0 and contain only numeric characters.'
                      },
                      email: {
                          required: 'Please enter your email eddress',
                          //minlength: 'Please enter exactly 12 numeric characters.',
                          //maxlength: 'Please enter exactly 12 numeric characters.',
                          //nonNumeric: 'Please enter a valid IC number numeric characters.'
                      },
                      marital_status:{
                        required: 'Please select your marital status',
                      },
                      address1:{
                        required: 'Please enter your address first line',
                      },
                      address2:{
                        required: 'Please enter your address second line',
                      },
                      postcode: {
                          required: 'Please enter your postcode.',
                          minlength: 'Please enter exactly 5 numeric characters.',
                          maxlength: 'Please enter exactly 5 numeric characters.',
                          nonNumeric: 'Please enter a valid Postcode numeric characters.',
                          validPostcode: 'Invalid postcode or unable to fetch state.'
                      }
                  },

                  errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "-error").html(error);
                },

                highlight: function(element) {
                    $(element).addClass('is-invalid');
                },

                unhighlight: function(element) {
                    $(element).removeClass('is-invalid');
                },

                /*submitHandler: function(form) {
                    // form.submit(); // Uncomment this to submit the form
                    console.log('Form submitted successfully');
                }*/

              });
  
              // Convert input values to uppercase
              $('input').on('keyup blur change', function() {
                if ($(this).attr('type') !== 'email') {
                  $(this).val($(this).val().toUpperCase());
                }
              });

              $('input').on('input', function() {
                if ($(this).attr('type') !== 'email') {
                    $(this).val($(this).val().toUpperCase());
                }
              });

  
              // Enable or disable the submit button based on form validity
              /* $('input').on('keyup blur change', function() {
                  if ($("#contact").valid()) {
                      $('#calculate').prop('disabled', false);
                  } else {
                      $('#calculate').prop('disabled', true);
                  }
              }); */

          });
      </script>
    
      <!-- Script for Phone -->
      <script>
        document.getElementById('phone').addEventListener('input', function (e) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    </script>
      <!-- End Script for Phone -->


      <!-- Get Postcode -->
      <!-- <script>
        $(document).ready(function() {
            $('#postcode').on('blur', function() {
                const postcode = $(this).val();
                if (postcode) {
                    $('#postcode-error').hide();
                    $.ajax({
                        url: '{{ url('get-postcode-zurich') }}',
                        type: 'GET',
                        data: { postcode: postcode },
                        success: function(response) {
                            $('#state').val(response.state);
                        },
                        error: function() {
                            $('#state').val('');
                            $('#postcode-error').text('Invalid postcode or unable to fetch state.').show();
                        }
                    });
                } else {
                    $('#postcode-error').text('Please enter your postcode.').show();
                }
            });
        });
    </script> -->
      <!-- End Get Postcode -->


      <script>

        $(document).ready(function() {
            // Attach submit event listener to the form
            $("#contact").on("submit", function(event) {
                // Get the selected value of the variant dropdown
                var variant = $("#variant").val();

                // Check if the variant is selected
                if (variant === "" || variant === null) {
                    // Prevent form submission
                    event.preventDefault();

                    // Display the error message in the specified div with red color
                    $("#variant-error").html("Please select your vehicle variant.").css("color", "red");

                    // Focus on the variant dropdown
                    $("#variant").focus();
                } else {

                    // If a valid option is selected, clear any previous error message
                    $("#variant-error").html("");

                }
            });

            // Clear the error message when the user selects a valid variant
            $("#variant").on("change", function() {
                if ($(this).val() !== "" && $(this).val() !== null) {
                    // Clear the error message
                    $("#variant-error").html("");
                }
            });
        });


      </script>



@endpush