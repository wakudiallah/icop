@extends('frontend.template')

@push('style')
  
 <style>
  .form-control{
    color: black !important;
  }


  .checkmark-container {
    position: relative;
    width: 60px;
    height: 60px;
    }

    .checkmark {
    width: 40px;
    height: 20px;
    border-bottom: 3px solid #2ecc71;
    border-right: 3px solid #2ecc71;
    transform-origin: bottom right;
    animation: drawCheck 0.5s ease forwards;
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(45deg);
    }

    @keyframes drawCheck {
    0% {
        opacity: 0;
        transform: translate(-50%, -50%) rotate(45deg) scale(0.1);
    }
    100% {
        opacity: 1;
        transform: translate(-50%, -50%) rotate(45deg) scale(1);
    }
    }

  </style>


@endpush

@section('main-content')

 <!-- professional section -->
 <section class="professional_section layout_padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="img-box">
            <img src="{{ asset('front/images/professional-img.png') }}" alt="">
          </div>
        </div>
        <div class="col-md-6 ">
          <div class="detail-box">
            <h2>
                Your Quote Details
            </h2>
                             
            <h5>NRIC Pemilik : <b>{{ $data->ic}}</b></h5>
            <h5>Kendaraan :  <b> Proton X50 2024</b></h5>
            <h5>No Plat Kendaraan : <b>{{ $data->vehicle}} </b></h5>
            <h5>Tahun : <b>2024 </b></h5>
            <h5>Jenis Perlindungan : <b>{{ $protect->protection_type}} </b></h5>
            <h5>E-hailing Quote :  <b>@if(!empty($data->pra_ehailing->ehailing)){{ $data->pra_ehailing->ehailing}} @endif</b></h5>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end professional section -->


<!-- contact section -->

@include('sweetalert::alert')



  <section class="service_section layout_padding">
    <div class="container ">
      <div class="row">
        <div class="col-sm-12 col-md-12 mx-auto">
          <div class="box ">
            <div class="img-box">
              <img src="{{ asset('front/images/s1.png') }}" alt="" />
            </div>
            <div class="detail-box">
              <h3>
                Jumlah Perlindungan :  RM 10.000
              </h3>
            </div>
            <div class="row">
                <div class="col-6">
                    <p>Product Disclosure<p>
                </div>
                <div class="col-6">
                    <p>Policy Wording <p>
                </div>
            </div>

            <hr>

            <div class="row">
                <ul>
                    <li><h5>PERCUMA Khidmat Tunda Kecemasan 24-jam sehingga jarak 150km </h5></li>
                    <li><h5>PERCUMA 1 Pemandu Tambahan</h5></li>
                    <li>PERCUMA 24-jam Bantuan Tuntutan</li>
                    <li>Insurans Kereta Terbaik 2018</li>
                    <li>Kelulusan Segera bagi Nota Perlindungan</li>
                    <li>Sesuai bagi Kegunaan E-hailing</li>
                </ul>
                
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>


  <!-- end contact section -->


  @endsection


  @push('scripts')
    
   <script>
        // Wait for the page to load
        window.addEventListener('load', function() {
        // Get the checkmark element
        const checkmark = document.querySelector('.checkmark');
        
        // Add a class to trigger animation
        checkmark.classList.add('animate');
        });


   </script>

  @endpush