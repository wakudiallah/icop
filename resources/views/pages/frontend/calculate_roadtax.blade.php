@extends('frontend.template')

@section('main-content')

    <!-- about section -->
  <section class="about_section layout_padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-md-6">
          <div class="detail-box">
            <h2>
              Calculate Roadtax
            </h2>
            

            <div class="card">
                <div class="card-header">
                    <div class="card-header-form"></div>
                </div> 

                <div class="card-body p-10">

                  <p> {{config('insurance.currency')}} <span id="roadtax_amount">0</span>  </p>

                  <form id="roadtaxForm" method="post" action="{{route('calculate-roadtax.store')}}" enctype='multipart/form-data'>
                    {{csrf_field()}}

                      <div class="form-group">
                          <label for="inputTitle" class="col-form-label"> Engine CC </label>
                          <input type="text" name="engine" placeholder="Engine" id="inputEngine" value="" class="form-control" min="1" max="1000000" step="1" required>
                          @error('engine')
                              <span class="text-danger">{{$message}}</span>
                          @enderror
                      </div>  
                      
                      <div class="form-group">
                          <label for="inputTitle" class="col-form-label"> Region </label>
                          <select name="region" class="form-control" required>
                              <option value="" selected disabled hidden>- Please Select -</option>  
                              @foreach($region as $region)
                              <option value="{{$region->id}}">{{ $region->region }}</option>
                              @endforeach
                          </select>
                          @error('region')
                              <span class="text-danger">{{$message}}</span>
                          @enderror
                      </div>


                      <div class="form-group">
                        <label for="inputTitle" class="col-form-label"> Ownership </label>
                        <select name="owner" class="form-control" required>
                            <option value="" selected disabled hidden>- Please Select -</option>  
                            @foreach($owner as $owner)
                            <option value="{{$owner->id}}">{{ $owner->ownership }}</option>
                            @endforeach
                        </select>
                        @error('owner')
                            <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="form-group mb-3">
                      <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                      <button class="btn btn-success" id="submitForm" type="submit">Submit</button>
                    </div>

                  </form>

                </div>
            </div>
            
          </div>
        </div>
        <div class="col-lg-7 col-md-6">
          <div class="img-box">
            <img src="images/about-img.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->


@endsection


@push('scripts')

    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>


    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>

    <script>
      document.getElementById('inputEngine').addEventListener('input', function (e) {
          var value = this.value;
          if (isNaN(value) || value.includes('e')) {
              this.value = value.replace(/[^\d]/g, ''); // Only allow numbers
          }
      });
    </script>
    
    <script>
      $(document).ready(function() {
          $('#submitForm').click(function(e) {
              e.preventDefault(); // Prevent normal form submission

              var engineValue = $('#inputEngine').val();

              if (engineValue === "" || isNaN(engineValue) || engineValue <= 0) {
                  alert('Please enter a valid engine value');
                  return; // Stop the form from submitting
              }
              
              var formData = new FormData($('#roadtaxForm')[0]); // Capture form data
  
              $.ajax({
                  url: "{{ route('calculate-roadtax.store') }}",  // The route
                  method: "POST",                                 // POST request
                  data: formData,
                  processData: false,                             // For file upload
                  contentType: false,                             // For file upload
                  success: function(response) {

                    var formattedAmount = parseFloat(response.roadtax_amount).toLocaleString('en-US', { 
                        minimumFractionDigits: 2, 
                        maximumFractionDigits: 2 
                    });

                    $('#roadtax_amount').text(formattedAmount);


                  },
                  error: function(xhr, status, error) {
                      // Handle error response (e.g., show error message)
                      console.log(xhr.responseText);
                      alert('Error occurred. Please try again.');
                  }
              });
          });
      });
  </script>

@endpush
