

<section class="clients-section">
    <h2>Rakan kerjasama</h2>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="" alt="">
            </div>
            <div class="swiper-slide">
                <img src="{{ asset('front/images/angkasalogo.png') }}">
            </div>
            <div class="swiper-slide">
              <img src="{{ asset('front/images/image2.png') }}" alt="" />
            </div>
            <div class="swiper-slide">
                <img src="{{ asset('front/images/zurich.png') }}" alt="">
            </div>
            <div class="swiper-slide">
                <img src="" alt="">
            </div>
           
        </div>
        <!-- Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</section>


@push('style')

<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
  
<style>
    .clients-section {
        padding: 40px 0;
        background-color: #fff;
        text-align: center;
    }

    .clients-section h2 {
        font-size: 2em;
        margin-bottom: 20px;
    }

    .swiper-container {
        width: 80%;
        margin: auto;
    }

    /* Center images using Flexbox */
    .swiper-slide {
        display: flex;
        justify-content: center;  /* Centers the image horizontally */
        align-items: center;      /* Centers the image vertically */
        height: 200px;            /* Define the height of each slide */
    }

    .swiper-slide img {
        width: 100px;
        height: auto;
    }

    .swiper-pagination-bullet {
        background: rgb(39, 20, 138);
    }
</style>

@endpush


@push('scripts')

    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 5,
            spaceBetween: 30,
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>

@endpush