@extends('frontend.template')

@section('main-content')

    <div class="hero_area">
    <!-- slider section -->
    <section class="slider_section ">
      <div class="container ">
        <div class="row">
          <div class="col-md-6 ">
            <div class="detail-box">
              
              <h1>
                {{-- __('messages.moto')--}} Ingin tahu berapa harga premium kenderaan anda?
              </h1>
              <p> {{--__('messages.hash')--}}</p>
              <!-- <a href="">
                Contact Us
              </a> -->
            </div>
          </div>
          <div class="col-md-6">
            <div class="img-box">
               <img src="{{ asset('front/images/animate.png') }}" alt="" class="img img-responsive" height="40%" width="40%">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>

  <!-- feature section -->
  <section class="feature_section">
    <div class="container">
      <div class="feature_container">
        
        <div class="col-md-6">
        <a href="{{ url('car-insurance') }}" target="_blank">
            <div class="box">
              <div class="img-box">
                <svg width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2 14.803v6.447c0 .414.336.75.75.75h1.614a.75.75 0 0 0 .74-.627L5.5 19h13l.395 2.373a.75.75 0 0 0 .74.627h1.615a.75.75 0 0 0 .75-.75v-6.447a5.954 5.954 0 0 0-1-3.303l-.78-1.17a1.994 1.994 0 0 1-.178-.33h.994a.75.75 0 0 0 .671-.415l.25-.5A.75.75 0 0 0 21.287 8H19.6l-.31-1.546a2.5 2.5 0 0 0-1.885-1.944C15.943 4.17 14.141 4 12 4c-2.142 0-3.943.17-5.405.51a2.5 2.5 0 0 0-1.886 1.944L4.399 8H2.714a.75.75 0 0 0-.67 1.085l.25.5a.75.75 0 0 0 .67.415h.995a1.999 1.999 0 0 1-.178.33L3 11.5c-.652.978-1 2.127-1 3.303zm15.961-4.799a4 4 0 0 0 .34.997H5.699c.157-.315.271-.65.34-.997l.632-3.157a.5.5 0 0 1 .377-.39C8.346 6.157 10 6 12 6c2 0 3.654.156 4.952.458a.5.5 0 0 1 .378.389l.631 3.157zM5.5 16a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zM20 14.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" fill="#000000"/></svg>
            </div>
            
            <h5 class="name">
              
                {{__('messages.car_insurance')}}
            </h5>
            </div>
        </a>
        </div>

        <div class="col-md-6">
        <a href="#">
            <div class="box">
            
              <div class="img-box">
                <svg fill="#000000" width="800px" height="800px" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M15 5 A 1.0001 1.0001 0 1 0 15 7L17.359375 7L21.046875 14.990234C19.812861 15.90307 19 17.356539 19 19C19 21.749579 21.250421 24 24 24C26.749579 24 29 21.749579 29 19C29 16.250421 26.749579 14 24 14C23.606807 14 23.228985 14.057613 22.861328 14.144531L20.947266 10L22 10L22 7L20.5 7C20.195746 7 19.914259 7.0921754 19.677734 7.2480469L19.175781 6.1621094C18.850003 5.4545914 18.137409 5 17.359375 5L15 5 z M 5.5 10C4.8481216 10 4.2984868 10.41875 4.0917969 11L2 11 A 1.0001 1.0001 0 1 0 2 13L5 13L5.6738281 13L5.3828125 14.023438C5.2556006 14.01353 5.1298367 14 5 14C2.243 14 0 16.243 0 19C0 21.757 2.243 24 5 24C7.4143633 24 9.433596 22.279096 9.8984375 20L13.947266 20L14 20 A 1.0007574 1.0007574 0 0 0 14.046875 20L15 20L15.359375 20C16.312375 20 17.133313 19.327578 17.320312 18.392578L18 15L12.867188 15L11.724609 13L13 13L19 13L18.46875 11.792969C17.98975 10.703969 16.912656 10 15.722656 10L12 10C11.372438 10 10.819739 10.29422 10.453125 10.746094L5.7460938 10.019531L5.7402344 10.025391C5.6612344 10.012391 5.583 10 5.5 10 z M 7.7539062 13L9.4179688 13L12.275391 18L6.3261719 18L6.8964844 16L8.9746094 16C8.5295279 15.411595 7.9574472 14.923894 7.3027344 14.576172L7.7539062 13 z M 24 16C25.668699 16 27 17.331301 27 19C27 20.668699 25.668699 22 24 22C22.331301 22 21 20.668699 21 19C21 18.15103 21.34795 17.393032 21.90625 16.849609L23.091797 19.419922 A 1.0005834 1.0005834 0 0 0 24.908203 18.580078L23.730469 16.027344C23.820795 16.019319 23.907427 16 24 16 z M 4.8125 16.019531L4.0820312 18.576172 A 1.0001 1.0001 0 0 0 4.0097656 19.212891 A 1.0001 1.0001 0 0 0 5.2304688 20L7.8164062 20C7.4021391 21.161251 6.3016094 22 5 22C3.346 22 2 20.654 2 19C2 17.410258 3.2472651 16.118546 4.8125 16.019531 z"/></svg>
              </div>
            
          
            
            
            <h5 class="name">
              {{__('messages.motor_insurance')}}
            </h5>
            </div>
        </a>
    </div>


      </div>
    </div>
  </section>
  <!-- end feature section -->


  <!-- about section -->
  <section class="about_section layout_padding-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="detail-box">
            <h2>
              Mengenai icop.my
            </h2>
            <p>
              ICOP adalah singkatan kepada icop.my yang merupakan satu platfom pembelian takaful atas talian yang terbaru. Dimiliki oleh Masamart Marketing Sdn Bhd (MMSB).</p>
            
            <p>Kami disokong oleh rakan teknologi NetXpert Sdn Bhd. Ianya merupakan sebuah syarikat berteraskan IT mempunyai kepakaran dan berpengalaman yang luas disektor kewangan dan perkhidmatan . 
            </p>
            <p>
              Kami juga telah menjalinkan satu ikatan dengan Angkatan Koperasi Kebangsaan Malaysia Berhad (Angkasa) bertujuan berkerjasama memasarkan produk takaful kepada anggota dan gerakan koperasi di Malaysia.
            </p>
            <a href="{{url('/about')}}">
              Read More
            </a>
          </div>
        </div>
        
      </div>
    </div>
  </section>
  <!-- end about section -->


  <!-- professional section -->
  <section class="professional_section layout_padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="img-box">
            <img src="{{ asset('front/images/about-us.png') }}" alt=""> 
          </div>
        </div>
        <div class="col-md-6 ">
          <div class="detail-box">
            <h2>
              Mendapatkan sebut harga <br>
              harga
            </h2>
            <p>Cara mendapatkan sebut harga amatlah mudah, anda cuma perlu melangkapkan borang yang disediakan dengan menjawab soalan mengenai anda dan kenderaan sahaja. Kami akan membuat yang membuat yang lain.</p>
              <p>Kami juga ingin mensyorkan agar anda mendapatkan perlindungan tambahan “additional perils” saperti bencana alam dan kemalangan diri bagi menjaga kepentingan diri, kewangan dan aset anda </p>
            <!-- <a href="">
              Read More
            </a> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end professional section -->





@endsection


@push('style')

  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
  
  <style>
      .clients-section {
          padding: 40px 0;
          background-color: #fff;
          text-align: center;
      }

      .clients-section h2 {
          font-size: 2em;
          margin-bottom: 20px;
      }

      .swiper-container {
          width: 80%;
          margin: auto;
      }

      /* Center images using Flexbox */
      .swiper-slide {
          display: flex;
          justify-content: center;  /* Centers the image horizontally */
          align-items: center;      /* Centers the image vertically */
          height: 200px;            /* Define the height of each slide */
      }

      .swiper-slide img {
          width: 100px;
          height: auto;
      }

      .swiper-pagination-bullet {
          background: rgb(39, 20, 138);
      }
  </style>


  <style>
    @media (max-width: 767px) {

        .feature_section .feature_container .box {
          margin-bottom: 20px;
        }
    }

  </style>

@endpush


@push('scripts')

    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 5,
            spaceBetween: 30,
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>

@endpush