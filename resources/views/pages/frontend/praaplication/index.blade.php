@extends('layouts.dca.template')

@push('style') 
    <link href="{{asset('dca/asset/css/addcss.css')}}" rel="stylesheet')}}">
@endpush

@section('content')


@include('layouts.dca.menu')

@include('sweetalert::alert')  
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
                <div class="row">
                    <div class="col-md-1">
                        <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none" >
                        
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">

                        <div class="front-left">
                           
                            @include('layouts.dca.left_front')
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                       <?php if (!(Auth::user())) {?>
                        <div class="consultantion-form">
                            <div class="text-center">
                                
                            </div>
                            <h2 class="mb70" style="text-align: center !important;font-weight: 600; line-height: 220px !important;">Icop</h2>
                           

                        {!! Form::open(['url' => '/result','class' => 'smart-form client-form', 'id' =>'smart-form-register', 'method' => 'POST'  ]) !!}
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <input type="hidden" value="1" name="link">
                         
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_vehicle')}}">{{__('messages.vehicle')}}<sup>*</sup></label>
                                <input type="text" id="vehicle"  onkeyup="this.value = this.value.toUpperCase()" name="vehicle"   
                                 size="55" class="form-control"  @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_ic')}}">{{__('messages.mykad')}}  <sup>*</sup></label>
                                <input name="ic" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                type = "tel"
                                maxlength = "12"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber"  minlength="12" maxlength="12"   
                               class="form-control" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46) )">
                            </div>
                        </div>
                       
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="name">{{__('messages.postcode')}} <sup>*</sup></label>
                                <input name="postcode" class="form-control" type="text" id="postcode" onkeyup="this.value = this.value.toUpperCase()"  maxlength="88"  
                                @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}"  @endif >
                            </div>
                        </div>


                        <div class="row">
                          <div class="col-lg-12 col-md-12" align="right">
                            <button type="reset" name="singlebutton" class="btn btn-light  btn-sm ">
                            Reset</button>
                            
                            <button type="submit" name="singlebutton" class="btn btn-primary  btn-sm">Submit</button>
                           
                           
                            <!-- <button type="submit" name="button1" value=" ">
                            <img src="{{url('/img/arrow.png')}}" width="32" height="32" alt="submit" border="0" />
                            </button>
                        -->
                        </div>
                    </div>
                    </form>

                </div>
                  <?php } ?>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>
</div>


 @include('pages.frontend.praaplication.js')
 @endsection
