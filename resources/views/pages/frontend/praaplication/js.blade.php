@include('layouts.dca.js')

    <script>
        $(document).ready(function() {
            $("#Employment").select2();   
        });
        $(document).ready(function() {
            $("#title").select2();   
        });
    </script>

    <script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
    </script>

    <script type="text/javascript">
        function addEvent(el, e, f) {
            if (el.attachEvent) {
                return el.attachEvent('on'+e, f);
            }
            else {
                return el.addEventListener(e, f, false);
            }
        }

        var ajax = new XMLHttpRequest();
            ajax.onload = function(e) {
        }
    </script>

    <script type="text/javascript">
        ;(function () {
        var AdgTooltipSimple

        AdgTooltipSimple = class AdgTooltipSimple {
        constructor (el) {
            this.$el = $(el)
            this.value = this.$el.attr('data-adg-tooltip-simple')
            this.$el.attr('data-adg-tooltip-simple', null)
            this.initContainer()
            this.attachContentToEl()
            this.initElEvents()
            this.initContainerEvents()
        }

        initContainer () {
            this.$container = $(
                "<span class='adg-tooltip-simple' aria-hidden='true'></span>"
            )
            this.$el.after(this.$container)
            this.initIcon()
            return this.initBalloon()
        }

        initIcon () {
            // Set focusable="false" for IE, see https://stackoverflow.com/questions/18646111/disable-onfocus-event-for-svg-element
            this.$icon = $(
            "<span class='adg-tooltip-simple-icon' style='font-size:8px!important'><svg class='icon' focusable='false'><use xlink:href='#tooltip' /></svg></span>"
            )
            return this.$container.append(this.$icon)
        }

        initBalloon () {
          this.$balloon = $(
            `<span class='adg-tooltip-simple-balloon' hidden>${this.value}</span>`
          )
          this.$balloon.attr('id', `${this.$el.attr('id')}-balloon`)
          return this.$container.append(this.$balloon)
        }

        attachContentToEl () {
          var valueElement
          valueElement = $(
            `<span class='adg-visually-hidden'> (${this.value})</span>`
          )
          if (this.$el.is('input, textarea, select')) {
            return $(`label[for='${this.$el.attr('id')}'`).append(valueElement)
          } else {
            return this.$el.append(valueElement)
          }
        }

        initElEvents () {
          this.$el.focusin(() => {
            return this.show()
          })
          this.$el.focusout(() => {
            if (!this.$container.is(':hover')) {
              return this.hide()
            }
          })
          this.$el.mouseenter(() => {
            return this.show()
          })
          this.$el.mouseleave(() => {
            if (!this.$el.is(':focus')) {
              return this.hide()
            }
          })
          return this.$el.keyup(e => {
            if (e.keyCode === 27) {
              // Esc
              if (this.$balloon.is(':visible')) {
                return this.hide()
              } else {
                return this.show()
              }
            }
          })
        }

        initContainerEvents () {
          this.$container.mouseenter(() => {
            return this.show()
          })
          return this.$container.mouseleave(() => {
            if (!this.$el.is(':focus')) {
              return this.hide()
            }
          })
        }

        show () {
          return this.$balloon.attr('hidden', false)
        }

        hide () {
          return this.$balloon.attr('hidden', true)
        }
      }

      $(document).ready(function () {
        return $('[data-adg-tooltip-simple]').each(function () {
          return new AdgTooltipSimple(this)
        })
      })
        }.call(this))

    </script>

    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
        }

        /* Firefox */
        input[type=number] {
          -moz-appearance: textfield;
        }
    </style>
       

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="{{asset('/asset/js/gambar/lib/jquery.mousewheel-3.0.6.pack.js')}}"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="{{asset('/asset/js/gambar/source/jquery.fancybox.js?v=2.1.5')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/jquery.fancybox.css?v=2.1.5')}}" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}" />
    <script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')}}" />
    <script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')}}"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="{{asset('/asset/js/gambar/source/helpers/jquery.fancybox-media.js?v=1.0.6')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

            /*
             *  Different effects
             */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title : {
                        type : 'outside'
                    },
                    overlay : {
                        speedOut : 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                helpers : {
                    title : {
                        type : 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS    : 'fancybox-custom',
                closeClick : true,

                openEffect : 'none',

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        css : {
                            'background' : 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect : 'elastic',
                openSpeed  : 150,

                closeEffect : 'elastic',
                closeSpeed  : 150,

                closeClick : true,

                helpers : {
                    overlay : null
                }
            });

            /*
             *  Button helper. Disable animations, hide close button, change title type and content
             */

            $('.fancybox-buttons').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    buttons : {}
                },

                afterLoad : function() {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
             *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
             */

            $('.fancybox-thumbs').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,
                arrows    : false,
                nextClick : true,

                helpers : {
                    thumbs : {
                        width  : 50,
                        height : 50
                    }
                }
            });

            /*
             *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect : 'none',
                    closeEffect : 'none',
                    prevEffect : 'none',
                    nextEffect : 'none',

                    arrows : false,
                    helpers : {
                        media : {},
                        buttons : {}
                    }
                });

            /*
             *  Open manually
             */

            $("#fancybox-manual-a").click(function() {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function() {
                $.fancybox.open({
                    href : 'iframe.html',
                    type : 'iframe',
                    padding : 5
                });
            });

            $("#fancybox-manual-c").click(function() {
                $.fancybox.open([
                    {
                        href : '1_b.jpg',
                        title : 'My title'
                    }, {
                        href : '2_b.jpg',
                        title : '2nd title'
                    }, {
                        href : '3_b.jpg'
                    }
                ], {
                    helpers : {
                        thumbs : {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });


        });
    </script>

    <script type="text/javascript">
        $.validator.addMethod("dobRange", function (value, element) {
            var dob = $('#ICNumber').val();
            var day = dob.substr(4, 2);
            var month =  dob.substr(2, 2);
            var year = dob.substr(0, 2);
            if(year > 30) {
                tahun2 = "19"+year;
            }
            else {
                tahun2 = "20"+year;
            }
            var max =  ({{ config('dca.max_age') }});
            var min =  ({{ config('dca.min_age') }});
                     var mydate = new Date();
            mydate.setFullYear(tahun2, month-1, day);

            var currdate = new Date();
            currdate.setFullYear(currdate.getFullYear() - min);

            var currmax = new Date();
            currmax.setFullYear(currmax.getFullYear() - max);
            if ((currdate < mydate) || (currmax > mydate))
                    return false;
                return true;
            }, "The financing is only for applicant aged between "+ ({{ config('dca.min_age') }}) +" - " + ({{ config('dca.max_age') }}) +" years old");
    </script>

    <script type="text/javascript">
        $( document ).ready(function( $ ) {
            $('input').on('keyup blur change', function() {
                if ($("#smart-form-register").valid()) {
                    $('#calculate').prop('disabled', false);  
                } else {
                    $('#calculate').prop('disabled', 'disabled');
                }
            });
            $("#smart-form-register2").hide();
            $("#smart-form-register").validate({
            // Rules for form validation
            rules : {
                vehicle: {
                    required : true,
                    maxlength:88
                },
                ic : {
                    required : true,
                    minlength:12,
                    maxlength:12,
                    pattern:/^\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])\d{6}$/,
                    dobRange:true
                },
                postcode: {
                    required: true
                },
            },

            //Messages for form validation
            messages : {
                vehicle: {
                    required : 'Please enter your vehicle'
                },
                
                ic: {
                    required: 'Please enter your New IC Number',
                    minlength: "Please enter 12 digits of your New IC Number",
                    before:"before",
                    pattern:'Please enter a valid New IC Number'
                },
                
                postcode: {
                    required: 'Please enter postcode'
                }
                

              }
            });
        });
    </script>

    <script type="text/javascript">
        $("#Employment").change(function() {
            var package = $('#Employment').val();
            $.ajax({
                url: "<?php  print url('/'); ?>/package/"+package,
                dataType: 'json',
                data: {
                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        $("#package").val(data[k].package.name );
                        $("#package_id").val(data[k].package.code );
                        $("#Employment2").val(data[k].name );
                    });
                }
            });
        });
    </script>

    <script>
        function isNumberKey(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
                if (key.length == 0) return;
                    var regex = /^[0-9.,\b]+$/;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }

        function minmax(value, min, max) 
            {
                if(parseInt(value) < min || isNaN(value)) 
                    return 2; 
                else if(parseInt(value) > max) 
                    return max; 
                else return value;
            }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
        var found = [];
            $("#Employment option").each(function() {
                if($.inArray(this.value, found) != -1) $(this).remove();
                found.push(this.value);
            });
         });
    </script>

    <script type="text/javascript">
        $('input.Currency_BasicSalary').on('blur', function() {
             if (document.getElementById('BasicSalary').value == '') 
                { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0.00 }
        else{
          const value = this.value.replace(/,/g, '');
          this.value = parseFloat(value).toLocaleString('en-US', {
            style: 'decimal',
            maximumFractionDigits: 2,
            minimumFractionDigits: 2
          });
           }
        });
    </script>
    <script type="text/javascript">
        $('input.Currency_Deduction').on('blur', function() {
             if (document.getElementById('Deduction').value == '') 
                { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0.00 }
            else{
          const value = this.value.replace(/,/g, '');
          this.value = parseFloat(value).toLocaleString('en-US', {
            style: 'decimal',
            maximumFractionDigits: 2,
            minimumFractionDigits: 2
          });
           }
        });
    </script>

    <script type="text/javascript">
        $('input.Currency_LoanAmount').on('blur', function() {
            if (document.getElementById('LoanAmount').value == '') 
                { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0.00 }
            else{
                const value = this.value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('en-US', {
                    style: 'decimal',
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 2
                });
            }
        });
    </script>


    <script language="javascript">
        function getkey(e)
        {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function goodchars(e, goods, field)
        {
            var key, keychar;
            key = getkey(e);
            if (key == null) return true;
     
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            goods = goods.toLowerCase();
     
            // check goodkeys
            if (goods.indexOf(keychar) != -1)
                return true;
            // control keys
            if ( key==null || key==0 || key==8 || key==9 || key==27 )
                return true;
            if (key == 13) 
                {
                    var i;
                    for (i = 0; i < field.form.elements.length; i++)
                    if (field == field.form.elements[i])
                    break;
                    i = (i + 1) % field.form.elements.length;
                    field.form.elements[i].focus();
                    return false;
                };
            // else return false
                return false;
        }
    </script>
    <script type="text/javascript">
        /**
            * jQuery.browser.mobile (http://detectmobilebrowser.com/)
            *
            * jQuery.browser.mobile will be true if the browser is a mobile device
            *  
        **/
    (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
    </script>
    <script type="text/javascript">
        $('#PhoneNumber').live('keyup', function(event){preventDot(event)});
        function preventDot(event){
            if (event.charCode == 45){
                event.preventDefault();
                return false;
            }    
        }
    </script>

    <script type="text/javascript">
        if(jQuery.browser.mobile)
        {
            $( "#majikan" ).on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[a-zA-Z@&(),.'0123456789 ]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^a-zA-z\s@&(),.'123456789]/gi, ''))
                }
            });
            $( "#FullName" ).on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[a-zA-Z/\@,.' ]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^a-zA-z\/,.@']/gi, ''))
                }
            });
   
            $("#ICNumber").on("input change paste keyup",
            function filterNumericAndDecimal(event) {
                var formControl;
                formControl = $(event.target);
                var newtext = formControl.val().replace(/[^0-9]+/g, "");
                formControl.val(''); //without this the DOT will not go away on my phone!
                formControl.val(newtext);
            });
            
            $("#PhoneNumber").on("input change paste keyup",
            function filterNumericAndDecimal(event) {
                var formControl;
                formControl = $(event.target);
                var newtext = formControl.val().replace(/[^0-9]+/g, "");
                formControl.val(''); //without this the DOT will not go away on my phone!
                formControl.val(newtext);
            });
            $("#PhoneNumbers").on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[0123456789]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^0123456789]/gi, ''))
                }
            });
            $("#BasicSalary").on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[0123456789.,]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^0123456789.,]/gi, ''))
                }
            });
            $("#Deduction").on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[0123456789.,]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^0123456789.,]/gi, ''))
                }
            });
            $("#LoanAmount").on("keyup", function(e) {
                var inVal=" ";
                inVal=$(this).val();
                var regex = new RegExp(/^[0123456789.,]+$/);
                if(!(regex.test(inVal)||$(this).IsNumeric) ){
                    $(this).val($(this).val().replace(/[^0123456789.,]/gi, ''))
                }
            });
        }
        else{
            $(document).ready(function(){
            //called when key is pressed in textbox
         
            $("#majikan").keypress(function(event){
                var inputValue = event.charCode;
                if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 32) && (inputValue != 38)  && (inputValue != 64) && (inputValue != 40)
                  && (inputValue != 41) && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 48 && inputValue != 49 && inputValue != 50 && inputValue != 51 && inputValue != 52 && inputValue != 53 && inputValue != 54 && inputValue != 55 && inputValue != 56 && inputValue != 57 && inputValue != 45 && inputValue != 95 && inputValue != 46 && inputValue != 44 && inputValue != 47 && inputValue != 39)){
                    event.preventDefault();
                }
            });
            $("#FullName").keypress(function(event){
                var inputValue = event.charCode;
                if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
                    event.preventDefault();
                }
            });
        });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#majikantext").keypress(function(event){
                var inputValue = event.charCode;
                if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39)&& (inputValue != 55)){
                    event.preventDefault();
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#majikantexts").keypress(function (e) {
                var keyCode = e.keyCode || e.which;
     
                $("#lblError").html("");
     
                //Regex for Valid Characters i.e. Alphabets and Numbers.
                var regex = /^[a-zA-Z z0-9 ]+$/;
     
                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only Alphabets and Numbers allowed.");
                }
     
                return isValid;
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $("#submit_tenure").click(function(){
            var tenure = $('input[name=tenure]:checked').val();
            var terms = $('input[name=terms]:checked').val();
            if(tenure>0){
                if(terms>0){
                    $('#myModal').modal('show');
                }
                else{
                     bootbox.alert('Please check consent box if you want to proceed');
                }
            }else{
                 bootbox.alert('Please Select Tenure');
            }
           
            });
        });
    </script>



    <script type="text/javascript">
        $(document).ready(function(){
            $('#ICNumber').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#PhoneNumber').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#FullName').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#majikan').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#Deduction').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#LoanAmount').on("cut copy paste",function(e) {
                e.preventDefault();
            });
            $('#BasicSalary').on("cut copy paste",function(e) {
                e.preventDefault();
            });
        });
    </script>
    <script>
        $("#reset").on("click", function () {
            $('select option').prop('selected', function() {
                return this.defaultSelected;
            });
           
        });
    </script>

    <script type="text/javascript">
        $(document).on("wheel", "input[type=number]", function (e) {
            $(this).blur();
        })
    </script>

    <script type="text/javascript">
 
        var browser = {
                isIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                navigator: navigator.appVersion,
                getVersion: function() {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
                

            };

        if (browser.isIe () && browser.getVersion() <= 11) {

            alert("You are currently using Internet Explorer" + browser.getVersion() + " or are viewing the site in Compatibility View, please upgrade for a better user experience.");
            window.location.href = "https://www.microsoft.com/en-us/download/internet-explorer.aspx";
        }
    </script>

    <script type="text/javascript">
        var userAgent = navigator.userAgent.toLowerCase(),
            browser   = '',
            version   = 0;

        $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());

        // Is this a version of IE?
        if ($.browser.msie) {
          userAgent = $.browser.version;
          userAgent = userAgent.substring(0,userAgent.indexOf('.'));  
          version = userAgent;
          browser = "Internet Explorer";
        }

        // Is this a version of Chrome?


        // Is this a version of Safari?
        if ($.browser.safari) {
          userAgent = userAgent.substring(userAgent.indexOf('safari/') + 7);  
          userAgent = userAgent.substring(0,userAgent.indexOf('.'));
          version = userAgent;  
          browser = "Safari";
          if (version <= 12) {

              alert("You are currently using Safari " + version + " or are viewing the site in Compatibility View, please upgrade for a better user experience.");
              window.location.href = "https://support.mozilla.org/en-US/kb/update-firefox-latest-release";
          }
        }

        // Is this a version of Mozilla?
        if ($.browser.mozilla) {
          //Is it Firefox?
          if (navigator.userAgent.toLowerCase().indexOf('firefox') != -1) {
            userAgent = userAgent.substring(userAgent.indexOf('firefox/') + 8);
            userAgent = userAgent.substring(0,userAgent.indexOf('.'));
            version = userAgent;
            browser = "Firefox"
            if (version <= 75) {
              alert("You are currently using Firefox " + version + " or are viewing the site in Compatibility View, please upgrade for a better user experience.");

        window.location.href = "https://support.mozilla.org/en-US/kb/update-firefox-latest-release";
          }
          }
          // If not then it must be another Mozilla
          else {
            browser = "Mozilla (not Firefox)"
          }
        }

        // Is this a version of Opera?
        if ($.browser.opera) {
          userAgent = userAgent.substring(userAgent.indexOf('version/') + 8);
          userAgent = userAgent.substring(0,userAgent.indexOf('.'));
          version = userAgent;
          browser = "Opera";
           if (version <= 68) {

              alert("You are currently using Opera " + version + " or are viewing the site in Compatibility View, please upgrade for a better user experience.");
              window.location.href = "https://support.mozilla.org/en-US/kb/update-firefox-latest-release";
          }
        }
    </script>

    <script type="text/javascript">
        var currentBoxNumber = 0;
        $(".form-control").keyup(function (event) {
            if (event.keyCode == 13) {
                textboxes = $("input.form-control");
                currentBoxNumber = textboxes.index(this);
                console.log(textboxes.index(this));
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1];
                    nextBox.focus();
                    nextBox.select();
                    event.preventDefault();
                    return false;
                }
            }
        });
    </script>

    <script type="text/javascript">
        $("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#error-alert").slideUp(500);
    });
    </script>