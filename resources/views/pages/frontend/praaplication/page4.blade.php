@extends('layouts.dca.template')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <style type="text/css">
        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/materialicons/v50/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
        }

        .material-icons {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 24px;
            line-height: 1;
            letter-spacing: normal;
            text-transform: none;
            display: inline-block;
            white-space: nowrap;
            word-wrap: normal;
            direction: ltr;
            -webkit-font-feature-settings: 'liga';
            -webkit-font-smoothing: antialiased;
        }

        input[type='file'] {
          color: transparent;
        }

        img {
            pointer-events: none;
        }

        img[src='']{
            display: none;
        }
    </style>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

@include('layouts.dca.menu')
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
            @include('sweetalert::alert')  
            <div class="row">
               <div class="col-md-1">
                    <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">
                    <div class="front-left">
                        @include('layouts.dca.left_front')
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">
                        

                        <h3 class="mb30"><b>Application Details </b></h3>
                            <fieldset>
                                <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                    <tr>
                                        <td width="50%">
                                            <label class="try control-label" for="name" >{{trans('mbsb.name')}}</label>
                                        </td>
                                        <td width="5%"><b>: </b></td>
                                        <td> <b>c</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="try control-label" for="name">{{trans('mbsb.ic')}}</label>
                                        
                                        </td>
                                        <td><b>: </b></td>
                                        <td><b> c</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="try control-label" for="name">{{trans('mbsb.ref')}}</label>
                                        
                                        </td>
                                        <td><b>: </b></td>
                                        <td><b> c</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="try control-label" for="name">{{trans('mbsb.financing')}}</label>
                                        
                                        </td>
                                        <td><b>: </b></td>
                                        <td><b>c</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="name">{{trans('mbsb.tenure')}}</label>
                                            
                                        </td>
                                        <td><b>: </b></td>
                                        <td> <b>v </b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="name">{{trans('mbsb.package')}}</label>
                                        
                                        </td>
                                        <td><b>: </b></td>
                                        <td><b>
                                            r
                                            </b>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                            </fieldset>

                            <form class="smart-form client-form">
                                <div class=" col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if(!empty($document1->upload))
                                                @if(!empty($document1->extension!='pdf'))
    	                                           <img src="data:image/png;base64, {{ $document1->base64 }}" alt="Image Preview" style="width: 245px!important;height:117px!important"/>
                                                @else
                                                    <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document1->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><img width='110px' height='110px' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                @endif
                                            @else
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}">Front {{trans('mbsb.ic')}}  <span>*</span></label>
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">   
                                             
                                            <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra"/>

                                            <input id="fileupload1"  class="hidden-input test1 form-control " @if(empty($document1->name)) required @endif   class="btn btn-primary" type="file" name="file1" >

                                            <input type="hidden" name="document1"  id="documentx1"  value="Front NRIC">
                                            &nbsp; <span id="document1"> </span>

                                            <input type='hidden' value='@if(!empty($document1->name)){{$document1->upload}}@endif' id='a1' name='a1'/>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if(!empty($document2->upload))
                                                @if(!empty($document2->extension!='pdf'))
                                                    <img src="data:image/png;base64, {{ $document2->base64 }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>         
                                                @else
                                                    <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document2->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><img width='110px' height='110px' src="{{ url('/') }}/asset/img/pdf.png"></img></a>

                                                @endif
                                                @else
                                            @endif
                                        </div>
                                    </div><br>
                                 
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}">Back {{trans('mbsb.ic')}}  <span>*</span></label>
                                            
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">   
                                            <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra"/>    
                                            <input  class="hidden-input test1 form-control" id="fileupload2"  @if(empty($document2->name)) required @endif   class="btn btn-default" type="file" name="file2"/>
                                            <input type="hidden" name="document2"  id="documentx2"  value="Back NRIC">&nbsp; <span id="document2"> </span>
                                            <input type='hidden' value='@if(!empty($document2->name)){{$document2->upload}}@endif' id='a2' name='a2'/> 
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="">
                                        @if(!empty($document3->upload))
                                            @if(!empty($document3->extension!='pdf'))
                                               <img src="data:image/png;base64, {{ $document3->base64 }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>  @else
                                                <a href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id_pra)}}/{{$document3->upload}}" class="btn bg-light-blue btn-lg" target='_blank'><img width='110px' height='110px' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                            @endif
                                        @else
                                        @endif
                                        <div class="caption">
                                            <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}"> {{trans('mbsb.latest_payslip')}} </label>
                                            <p>
                                                {{ csrf_field() }}  
                                                <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra"/>    
                                                <input class="hidden-input test1 form-control" id="fileupload3"  @if(empty($document3->name)) required @endif   class="btn btn-default" type="file" name="file3"/>
                                                <input type="hidden" name="document3"  id="documentx3"  value="Payslip">&nbsp; <span id="document3"> </span>
                                                <input type='hidden' value='@if(!empty($document3->name)){{$document3->upload}}@endif' id='a3' name='a3'/>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form class="smart-form client-form" id='smart-form-register'>
                        
                            
                            

                                    <div class="col-lg-12 col-md-12" >
                                        <div class="form-group">
                                            <label class="control-label" for="name" data-adg-tooltip-simple="">
                                            </label>
                                            <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control">
                                        </div>
                                    </div>
                                    <div id="error_message" style="color: red!important;font-weight: 400;display: none"></div>
                                    <input type="hidden" readonly=""  value=""   maxlength="50"  id="message" name="message" class="form-control"  style=" background-color: #ccc;">
                                 
                       
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input type='checkbox'  id='terms'  name='terms' value='1' required="required" ><b style="color: #0155a2!important"> I agree with the Personal Financing-i <a href="#" align='right' id="term" ><u>Terms and conditions</u></a></b><br>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    @if(empty($document2->upload) || empty($document1->upload))
                                    <a href="#" class="btn btn-primary btn-lg" disabled onclick="myFunction()">Submit</a>
                                    @else
                                       <a align='right' id="submit_upload" class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- Modal -->
<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">TERMA DAN SYARAT</h4>
            </div>
            <div class="modal-body custom-scroll terms-body" style="padding: 36px!important">
                {{-- @include('home.terma') --}}
                </b></h3></div>   
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                   
                    <h4 class="modal-title" id="myModalLabel">Account Verification</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'form/uploaddoc', 'enctype' => 'multipart/form-data']) !!}
                        <fieldset>
                            <div class="form-body">   
                                <div class="form-group">
                                    <font color='black'>{{trans('mbsb.ic')}}</font>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                     <!--<input type="hidden" name="id_branch" id="id_branch" onClick="checkBranch()">-->
                                    <input type="icnumber" id="icnumber" maxlength='12' minlength='12' class="form-control" name="icnumber" value=""  required onKeyPress="if(this.value.length==12) return false;"  onkeypress="return isNumberKey(event)" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46) )" >
                                    <input type="hidden" id="fullname" name="fullname" value="$pra->fullname"  readonly >
                                    <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra" readonly >
                                    <input type='hidden' id='status_penyelesaian' name="debt_consolidation" value='0'/>
                                    <input type="hidden" name="referral_code" id="referral_code">
                                </div>
                            </div>
                        </fieldset>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-bs-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                Submit
                            </button>
                        </div>
                    {!! Form::close() !!}   
                </div>
            </div>
        </div>
    </div>
   
@include('pages.frontend.praaplication.upload_js')


   

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="state"]').on('change', function() {
              
                var stateID = $(this).val();
                 var wilayah    = $('#wilayah').val();
                if(stateID) {
                    $.ajax({
                        url:  "<?php  print url('/'); ?>/branch/"+stateID,

                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                             $('select[name="city"]').append("<option value=''>choose</option>");
                            $.each(data, function(key, value) {

                               

                                $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                             
                            });


                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });
        });
    </script>
    <?php for ($x = 1; $x <= 15; $x++) {  ?>

<script>
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
        $('#fileupload{{$x}}').fileupload({
            url: url,
            dataType: 'json',
            success: function ( data) {
                var text = $('#documentx{{$x}}').val();
                var fi =  data.file;
                if(fi =='error'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='ext'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='size'){
                    alert("Please upload file below {{config('dca.max_file_mb')}} MB");
                }
            else{
                $("#document{{$x}}a").hide();
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                alert('Successfully upload '+text+' document');
                window.location.href = window.location.href;
            }
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>

<?php } ?>

    <script>
        function myFunction() {
            alert("Please Upload Mandatory Documents");
        }
    </script>
    <script type="text/javascript">
        var select = document.getElementById('branch');
        var input = document.getElementById('id_branch');
        select.onchange = function() {
            input.value = select.value;
        }
    </script>

 @endsection

@push('addjs')
    <script>
        $(document).ready(function(){
            $("#term").click(function(){
                $('#termModal').modal('show');
            });
        });
    </script>

    <script type="text/javascript">
        $( "#branch" ).change(function() { 
        var _token      = $('#token').val();
        var id_pra      = $("#id_praapplication").val();
        var id_branch   = $("#branch").val();

        $.ajax({
            type: "POST",
            data: {  _token : _token, id_pra : id_pra, id_branch : id_branch},
            url: "{{ url('/select_branch') }}",
            });
         });
    </script>
     <script>
        $(document).ready(function() {
            $("#state").select2();   
        });
        $(document).ready(function() {
            $("#branch").select2();   
        });
    </script>
@endpush
