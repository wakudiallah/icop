@include('layouts.dca.js')

<script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
</script>
<script type="text/javascript">
  ;(function () {
  var AdgTooltipSimple

  AdgTooltipSimple = class AdgTooltipSimple {
    constructor (el) {
      this.$el = $(el)
      this.value = this.$el.attr('data-adg-tooltip-simple')
      this.$el.attr('data-adg-tooltip-simple', null)
      this.initContainer()
      this.attachContentToEl()
      this.initElEvents()
      this.initContainerEvents()
    }

    initContainer () {
      this.$container = $(
        "<span class='adg-tooltip-simple' aria-hidden='true'></span>"
      )
      this.$el.after(this.$container)
      this.initIcon()
      return this.initBalloon()
    }

    initIcon () {
      // Set focusable="false" for IE, see https://stackoverflow.com/questions/18646111/disable-onfocus-event-for-svg-element
      this.$icon = $(
        "<span class='adg-tooltip-simple-icon' style='font-size:8px!important'><svg class='icon' focusable='false'><use xlink:href='#tooltip' /></svg></span>"
      )
      return this.$container.append(this.$icon)
    }

    initBalloon () {
      this.$balloon = $(
        `<span class='adg-tooltip-simple-balloon' hidden>${this.value}</span>`
      )
      this.$balloon.attr('id', `${this.$el.attr('id')}-balloon`)
      return this.$container.append(this.$balloon)
    }

    attachContentToEl () {
      var valueElement
      valueElement = $(
        `<span class='adg-visually-hidden'> (${this.value})</span>`
      )
      if (this.$el.is('input, textarea, select')) {
        return $(`label[for='${this.$el.attr('id')}'`).append(valueElement)
      } else {
        return this.$el.append(valueElement)
      }
    }

    initElEvents () {
      this.$el.focusin(() => {
        return this.show()
      })
      this.$el.focusout(() => {
        if (!this.$container.is(':hover')) {
          return this.hide()
        }
      })
      this.$el.mouseenter(() => {
        return this.show()
      })
      this.$el.mouseleave(() => {
        if (!this.$el.is(':focus')) {
          return this.hide()
        }
      })
      return this.$el.keyup(e => {
        if (e.keyCode === 27) {
          // Esc
          if (this.$balloon.is(':visible')) {
            return this.hide()
          } else {
            return this.show()
          }
        }
      })
    }

    initContainerEvents () {
      this.$container.mouseenter(() => {
        return this.show()
      })
      return this.$container.mouseleave(() => {
        if (!this.$el.is(':focus')) {
          return this.hide()
        }
      })
    }

    show () {
      return this.$balloon.attr('hidden', false)
    }

    hide () {
      return this.$balloon.attr('hidden', true)
    }
  }

  $(document).ready(function () {
    return $('[data-adg-tooltip-simple]').each(function () {
      return new AdgTooltipSimple(this)
    })
  })
    }.call(this))
</script>

<style> 
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
</style>
        

<script type="text/javascript">
    $(document).ready(function() {
        /*
         *  Simple image gallery. Uses default settings
         */

        $('.fancybox').fancybox();

        /*
         *  Different effects
         */

        // Change title type, overlay closing speed
        $(".fancybox-effects-a").fancybox({
            helpers: {
                title : {
                    type : 'outside'
                },
                overlay : {
                    speedOut : 0
                }
            }
        });

        // Disable opening and closing animations, change title type
        $(".fancybox-effects-b").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            helpers : {
                title : {
                    type : 'over'
                }
            }
        });

        // Set custom style, close if clicked, change title type and overlay color
        $(".fancybox-effects-c").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });

        // Remove padding, set opening and closing animations, close if clicked and disable overlay
        $(".fancybox-effects-d").fancybox({
            padding: 0,

            openEffect : 'elastic',
            openSpeed  : 150,

            closeEffect : 'elastic',
            closeSpeed  : 150,

            closeClick : true,

            helpers : {
                overlay : null
            }
        });

        /*
         *  Button helper. Disable animations, hide close button, change title type and content
         */

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons : {}
            },

            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });


        /*
         *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
         */

        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });

        /*
         *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
        */
        $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect : 'none',
                closeEffect : 'none',
                prevEffect : 'none',
                nextEffect : 'none',

                arrows : false,
                helpers : {
                    media : {},
                    buttons : {}
                }
            });

        /*
         *  Open manually
         */

        $("#fancybox-manual-a").click(function() {
            $.fancybox.open('1_b.jpg');
        });

        $("#fancybox-manual-b").click(function() {
            $.fancybox.open({
                href : 'iframe.html',
                type : 'iframe',
                padding : 5
            });
        });

        $("#fancybox-manual-c").click(function() {
            $.fancybox.open([
                {
                    href : '1_b.jpg',
                    title : 'My title'
                }, {
                    href : '2_b.jpg',
                    title : '2nd title'
                }, {
                    href : '3_b.jpg'
                }
            ], {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    }
                }
            });
        });


    });
</script>




<style>
    .bs-example{
        margin: 20px;
    }
</style>
<script type="text/javascript">
   
    // Validation
    $(function() {
         //runAllForms();

         $("#i-agree").click(function(){
          $this=$("#terms");
          if($this.checked) {
            $('#termModal').modal('toggle');
          } else {
            $this.prop('checked', true);
            $('#termModal').modal('toggle');
          }
        });
        $("#smart-form-register1").validate({

            // Rules for form validation
           rules : {
                
                email : {
                    required : true,
                    validate_email : true
                },
                password:{
                  required:true
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    validate_email : 'Please enter a VALID email address'
                },
                password : {
                    required : 'Please enter your password'
                }
                    }
        });

    });
</script>


<?php $session =  Config::get('dca.session_timeout');
?>
<script type="text/javascript">
    var sessServerAliveTime = 10 * 60 * 2;
    var sessionTimeout =  <?php  print $session; ?> * 60000;
    var sessLastActivity;
    var idleTimer, remainingTimer;
    var isTimout = false;

    var sess_intervalID, idleIntervalID;
    var sess_lastActivity;
    var timer;
    var isIdleTimerOn = false;
    localStorage.setItem('sessionSlide', 'isStarted');

    function sessPingServer() {
    if (!isTimout) {
        return true;
    }
}

function sessServerAlive() {
    sess_intervalID = setInterval('sessPingServer()', sessServerAliveTime);
}

function initSessionMonitor() {
    $(document).bind('keypress.session', function (ed, e) {
        sessKeyPressed(ed, e);
    });
    $(document).bind('mousedown keydown', function (ed, e) {

        sessKeyPressed(ed, e);
    });
    sessServerAlive();
    startIdleTime();
}

$(window).scroll(function (e) {
    localStorage.setItem('sessionSlide', 'isStarted');
    startIdleTime();
});

function sessKeyPressed(ed, e) {
    var target = ed ? ed.target : window.event.srcElement;
    var sessionTarget = $(target).parents("#session-expire-warning-modal").length;

    if (sessionTarget != null && sessionTarget != undefined) {
        if (ed.target.id != "btnSessionExpiredCancelled" && ed.target.id != "btnSessionModal" && ed.currentTarget.activeElement.id != "session-expire-warning-modal" && ed.target.id != "btnExpiredOk"
             && ed.currentTarget.activeElement.className != "modal fade modal-overflow in" && ed.currentTarget.activeElement.className != 'modal-header'
    && sessionTarget != 1 && ed.target.id != "session-expire-warning-modal") {
            localStorage.setItem('sessionSlide', 'isStarted');
            startIdleTime();
        }
    }
}

function startIdleTime() {
    stopIdleTime();
    localStorage.setItem("sessIdleTimeCounter", $.now());
    idleIntervalID = setInterval('checkIdleTimeout()', 1000);
    isIdleTimerOn = false;
}

var sessionExpired = document.getElementById("session-expired-modal");
function sessionExpiredClicked(evt) {
     window.location.replace("{{ url('logout_user') }}");
}

sessionExpired.addEventListener("click", sessionExpiredClicked, false);
function stopIdleTime() {
    clearInterval(idleIntervalID);
    clearInterval(remainingTimer);
}

function checkIdleTimeout() {
     // $('#sessionValue').val() * 60000;
    var idleTime = (parseInt(localStorage.getItem('sessIdleTimeCounter')) + (sessionTimeout)); 
    if ($.now() > idleTime + 60000) {
        $("#session-expire-warning-modal").modal('hide');
        $("#session-expired-modal").modal('show');
        clearInterval(sess_intervalID);
        clearInterval(idleIntervalID);

        $('.modal-backdrop').css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 100);
        $("#session-expired-modal").css('z-index', 2000);
        $('#btnExpiredOk').css('background-color', '#428bca');
        $('#btnExpiredOk').css('color', '#fff');

        isTimout = true;

        sessLogOut();

    }
    else if ($.now() > idleTime) {
        ////var isDialogOpen = $("#session-expire-warning-modal").is(":visible");
        if (!isIdleTimerOn) {
            ////alert('Reached idle');
            localStorage.setItem('sessionSlide', false);
            countdownDisplay();

            $('.modal-backdrop').css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 500);
            $('#session-expire-warning-modal').css('z-index', 1500);
            $('#btnOk').css('background-color', '#428bca');
            $('#btnOk').css('color', '#fff');
            $('#btnSessionExpiredCancelled').css('background-color', '#428bca');
            $('#btnSessionExpiredCancelled').css('color', '#fff');
            $('#btnLogoutNow').css('background-color', '#428bca');
            $('#btnLogoutNow').css('color', '#fff');

            $("#seconds-timer").empty();
            $("#session-expire-warning-modal").modal('show');

            isIdleTimerOn = true;
        }
    }
}

$("#btnSessionExpiredCancelled").click(function () {
    $('.modal-backdrop').css("z-index", parseInt($('.modal-backdrop').css('z-index')) - 500);
});

$("#btnOk").click(function () {
    $("#session-expire-warning-modal").modal('hide');
    $('.modal-backdrop').css("z-index", parseInt($('.modal-backdrop').css('z-index')) - 500);
    startIdleTime();
    clearInterval(remainingTimer);
    localStorage.setItem('sessionSlide', 'isStarted');
});

$("#btnLogoutNow").click(function () {
    localStorage.setItem('sessionSlide', 'loggedOut');
     window.location.replace("{{ url('logout_user') }}");
    sessLogOut();
    $("#session-expired-modal").modal('hide');

});
$('#session-expired-modal').on('shown.bs.modal', function () {
    $("#session-expire-warning-modal").modal('hide');
    $(this).before($('.modal-backdrop'));
    $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
});

$("#session-expired-modal").on("hidden.bs.modal", function () {
    window.location = "Logout.html";
});
$('#session-expire-warning-modal').on('shown.bs.modal', function () {
    $("#session-expire-warning-modal").modal('show');
    $(this).before($('.modal-backdrop'));
    $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
});

function countdownDisplay() {

    var dialogDisplaySeconds = 60;

    remainingTimer = setInterval(function () {
        if (localStorage.getItem('sessionSlide') == "isStarted") {
            $("#session-expire-warning-modal").modal('hide');
            startIdleTime();
            clearInterval(remainingTimer);
        }
        else if (localStorage.getItem('sessionSlide') == "loggedOut") {         
            $("#session-expire-warning-modal").modal('hide');
            $("#session-expired-modal").modal('show');
        }
        else {

            $('#seconds-timer').html(dialogDisplaySeconds);
            dialogDisplaySeconds -= 1;
        }
    }
    , 1000);
};

function sessLogOut() {
    window.location.replace("{{ url('logout_user') }}");
}
</script>

<script type="text/javascript">    
    $( "#ref_codess" ).change(function() {//or keyup/BLUR
        var ref_code = $('#ref_code').val();
         var token      = $('#token').val();
        
       
            if( ( $(this).val().length == 6 ) || ( $(this).val().length ==10 ) || ( $(this).val().length ==7 )) {
                
                $.ajax({

                    url: "<?php  print url('/'); ?>/referral/check_code/"+ref_code,
                    dataType: 'json',
                    data: {  token : token},
                    success: function (data, status,response) {
                        $('#fa_name').empty();
                        $('#fa_name').val("");
                        
                        if(data == "invalid"){
                            bootbox.alert('Invalid Referral Code');

                           $('#error_message').html('<p>'+ 'Invalid referal code' + '</p>');
                           $("#message").val('1');
                             return false;

                        }
                        else{
                            jQuery.each(data,function (k) {
                                $("#fa_name").val(data[k].name);
                                $("#referral_code").val(data[k].referral_mo);
                                $("#message").val('0');
                                $('#error_message').html('');
                            });
                        }
                       
                    },
                    error: function (data) {
                        
                    }
                });
            }
            else{
               $('#error_message').html('');
            }
      //  }
        //else{
           //location.reload();     
       // }
    });
</script>

<script type="text/javascript">
    $("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#error-alert").slideUp(500);
});
</script>

<script type="text/javascript">
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
</script>