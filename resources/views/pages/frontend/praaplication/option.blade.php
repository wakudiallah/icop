@extends('layouts.dca.template')
@section('content')

@push('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
    

@push('css') 
    <link href="{{asset('dca/asset/css/pembiayaan.css')}}" rel="stylesheet')}}">
@endpush

<style>
    .table>:not(caption)>*>* {
        color: #0155a2 !important;
    }
</style>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

@include('layouts.dca.menu')

    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
            @include('sweetalert::alert')  
            <div class="row">
               <div class="col-md-1">
                    <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">
                    <div class="front-left">
                        @include('layouts.dca.left_front')
                    </div>
                </div>


                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">

                        <h2 class="mb30"><b>{{__('messages.pembayaran_pembiayaan')}} </b></h2>
                       
                        <!--  -->
                        <fieldset class="mb30">
                            <table class="table table-bordered table-hover" border='1' style="font-size: 13px!important;text-align: center!important;margin-left: -8px !important; color: #0155a2 !important">
                                <thead>
                                    <tr>
                                        <th class="blue-color" valign="middle"><b>{{__('messages.skim_potongan')}}</b></th>
                                        <th style="text-align: center!important"><b>{{__('messages.jumlah_pembiayaan')}}</b></th>
                                        <th style="text-align: center!important"><b>{{__('messages.angsur_bulanan')}}</b></th>
                                        <th style="text-align: center!important"> <b >{{__('messages.pilih')}}</b></th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>3 Bulan</td>
                                        <td>RM XXXX</td>
                                        <td>RM XXX</td>
                                        <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="" required></td>
                                    </tr>
                                    <tr>
                                        <td>6 Bulan</td>
                                        <td>RM XXXX</td>
                                        <td>RM XXX</td>
                                        <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="" required></td>
                                    </tr>
                                    <tr>
                                        <td>10 Bulan</td>
                                        <td>RM XXXX</td>
                                        <td>RM XXX</td>
                                        <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>

                           
                        <form class="smart-form client-form">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div id="previewContainer">
                                            @if(!empty($doc->nric))
                                            <img src="{{ $doc->nric }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}">{{__('messages.mykad')}}  <span>*</span></label>
                                        <form id="uploadForm" enctype="multipart/form-data">
                                            <input class="hidden-input test1 form-control" name="ic" type="file" id="fileInput" accept="image/*, application/pdf">
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <br>
    
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="previewContainerPayslip">
                                            @if(!empty($doc->payslip))
                                            <img src="{{ $doc->payslip }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}">{{__('messages.pay_slip')}} <span>*</span></label>
                                        <form id="uploadForm" enctype="multipart/form-data">
                                            <input class="hidden-input test1 form-control" name="payslip" type="file" id="fileInputPaySlip" accept="image/*, application/pdf">
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="previewContainerBpa">
                                            @if(!empty($doc->bpa179))
                                                @if($doc->ext_bpa == "pdf")
                                                    <!-- <embed src="{{$doc->base64Data}}" type="application/pdf" /> -->
                                                    <!-- <a href="{{ $doc->bpa179 }}" target="_blank" rel="noopener noreferrer"> <img src="{{asset('front/images/pdffile2.png')}}" alt="" class="img img-responsiver" style="width: 80px!important; height: 80px !important"> </a> -->
                                                    
                                                    
                                                        <img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">
                                                    
                                                
                                                @else
                                                    <img src="{{ $doc->bpa179 }}" alt="Image Preview" style="width: 245px!important;height: 117px!important"/>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="name" data-adg-tooltip-simple="{{trans('mbsb.tooltip_upload')}}">{{__('messages.BPA1_79')}} <span>*</span></label>
                                        <form id="uploadForm" enctype="multipart/form-data">
                                            <input class="hidden-input test1 form-control" name="bpa179" type="file" id="fileInputBpa" accept="image/*, application/pdf">
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>
                            <br>

                            
                        </form class="smart-form client-form" id='smart-form-register'>



                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                   
                                       <a href="{{url('home-user')}}" align='right' id="submit_upload" class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


   



 @endsection



@push('addjs')
    
    <script>
        document.getElementById('fileInput').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            //Check if Selected a file
            if(!file){
                alert("Please select a file.");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            //Check if the selected file is too large(greater than 6MB)
            const maxSize = 6 * 1024 * 1024; //6Mb in bytes
            if(file.size > maxSize){
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image file
            if (!file || !file.type.match(/^image\/(jpeg|jpg|png)$/)) {
                alert("Please select a valid JPG, JPEG, or PNG image file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-ic') }}/{{ $id }}"; // Append the ID to the URL
            
        
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
               
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainer');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                    document.querySelector('.previewUploaded').classList.add('hidden');
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<embed src="${data.base64Data}" type="application/pdf" width="100%" height="600px" />`;
                    document.querySelector('.previewUploaded').classList.add('hidden');
                }

            })
            .catch(error => console.error('Error:', error));
        });

    </script>

    <script>
        document.getElementById('fileInputPaySlip').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            //Check if Selected a file
            if(!file){
                alert("Please select a file.");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            //Check if the selected file is too large(greater than 6MB)
            const maxSize = 6 * 1024 * 1024; //6Mb in bytes
            if(file.size > maxSize){
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image file
            if (!file || !file.type.match(/^image\/(jpeg|jpg|png)$/)) {
                alert("Please select a valid JPG, JPEG, or PNG image file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-payslip') }}/{{ $id }}"; // Append the ID to the URL
            
        
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
                headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainerPayslip');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                    
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<embed src="${data.base64Data}" type="application/pdf" width="100%" height="600px" />`;
                    
                }

            })
            .catch(error => console.error('Error:', error));
        });

    </script>


    <script>
        document.getElementById('fileInputBpa').addEventListener('change', function(e) {
            const file = e.target.files[0];
            
            //Check if Selected a file
            if(!file){
                alert("Please select a file.");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            //Check if the selected file is too large(greater than 6MB)
            const maxSize = 6 * 1024 * 1024; //6Mb in bytes
            if(file.size > maxSize){
                alert("Please select a file smaller than 6MB");
                e.target.value = ''; //Clear the selected file input
                return;
            }

            // Check if the selected file is a valid image or PDF file
            if (!file.type.match(/^image\/(jpeg|jpg|png)$/) && !file.type.match(/^application\/pdf$/)) {
                alert("Please select a valid JPG, JPEG, PNG image file, or a PDF file.");
                e.target.value = ''; // Clear the selected file input
                return;
            }

            const formData = new FormData();
            formData.append('file', file);
            formData.append('_token', '{{ csrf_token() }}'); // Include CSRF token

            const uploadUrl = "{{ url('upload-bpa') }}/{{ $id }}"; // Append the ID to the URL
            
        
            fetch(uploadUrl, {
                method: 'POST',
                body: formData,
                headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            })
            .then(response => response.json())
            .then(data => {
                const previewContainer = document.getElementById('previewContainerBpa');
                if (data.file_type === 'image') {
                    previewContainer.innerHTML = `<img src="${data.base64Data}" alt="Uploaded Image" width="30%">`;
                    
                } else if (data.file_type === 'pdf') {
                    previewContainer.innerHTML = `<img src="{{ asset('front/images/pdffile2.png') }}" alt="" class="img img-responsive" style="width: 80px!important; height: 80px !important">`;
                    
                }

            })
            .catch(error => console.error('Error:', error));
        });

    </script>


    <!-- // Download pdf file -->

    <script>
        // Base64 encoded PDF data from your database
        const base64Data = "{{ $doc->bpa179 }}";

        // Check if base64Data is empty or null
        if (!base64Data) {
            console.error("Base64 data is empty or null.");
        } else {
            try {
                // Convert Base64 to a Blob
                const byteCharacters = atob(base64Data);
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                const byteArray = new Uint8Array(byteNumbers);
                const blob = new Blob([byteArray], { type: 'application/pdf' });

                // Create temporary URL for the Blob
                const url = URL.createObjectURL(blob);

                // Set the href attribute of the download link
                const downloadLink = document.getElementById('downloadLink');
                downloadLink.href = url;
                downloadLink.download = 'document.pdf';

                // Add click event listener to trigger download
                downloadLink.addEventListener('click', () => {
                    // Prevent default anchor behavior
                    event.preventDefault();
                    // Trigger download
                    window.location.href = url;
                });
            } catch (error) {
                console.error("An error occurred while processing Base64 data:", error);
            }
        }
    </script>

    <!-- End dwonload pdf file -->

@endpush
