@extends('layouts.dca.template')
@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />


@push('style') 
    <link href="{{asset('dca/asset/css/perlindungantambahancss.css')}}" rel="stylesheet')}}">

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
@endpush


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

@include('layouts.dca.menu')
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">

            @include('sweetalert::alert')  

            <div class="row">
               <div class="col-md-1">
                    <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">
                    <div class="front-left">
                        @include('layouts.dca.left_front')
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">
                        

                        <h2 class="mb30"><b>{{__('messages.perlindungan_tambahan')}} </b></h2>

                            <fieldset class="mb10">
                                <table width="100%" style="line-height: 30px !important; color: #0155a2!important">
                                    @php
                                        $imageData = base64_decode($inCompany->logo);
                                        $imageSrc = 'data:image/png;base64,' . base64_encode($imageData);
                                    @endphp


                                    <tr>
                                        <td colspan="3" align="center">
                                            <img src="{{ $imageSrc }}" alt="Base64 Image" class="img img-responsive" style="max-width: 100pt !important">
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>

                            <fieldset class="mb10">
                                <table width="100%">
                                    <tr>
                                        <div class="card">
                                            <div class="card-body">
                                                <label class="control-label" for="name"> Perlindungan Cermin Depan/Belakang </label>
                                                <br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label color-blue" for="inlineRadio1">Ya</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label color-blue" for="inlineRadio2">Tidak</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </tr>
                                </table>
                            </fieldset>


                            <fieldset class="mb10">
                                <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                   
                                    <tr>
                                        <div class="card">
                                            <div class="card-body">
                                                <label class="control-label" for="name"> Pemandu Tambahan </label>
                                                <br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label" for="inlineRadio1">Ya</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </tr>

                                </table>
                            </fieldset>


                            <fieldset class="mb10">
                                <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                   
                                    <tr>
                                        <div class="card">
                                            <div class="card-body">
                                                <label class="control-label" for="name"> Perlindungan Tambahan Bencana Khas (termasuk Banjir) </label>
                                                <br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions3" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label" for="inlineRadio1">Ya</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions3" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </tr>

                                </table>
                            </fieldset>



                            <fieldset class="mb30">
                                <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                   
                                    <tr>
                                        <div class="card">
                                            <div class="card-body">
                                                <label class="control-label" for="name"> Perlindungan Kemalangan Peribadi (PA) </label>
                                                <br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions4" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label" for="inlineRadio1">Ya</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions4" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </tr>

                                </table>
                            </fieldset>


                            <fieldset class="mb30">
                                <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                   
                                    <tr>
                                        <div class="card">
                                            <div class="card-body">
                                                <label class="control-label" for="name"> Roadtax </label>
                                                <br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions5" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label" for="inlineRadio1">Ya</label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions5" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                                  </div>
                                            </div>
                                        </div>
                                    </tr>

                                </table>
                            </fieldset>


                           
                            <form method="post" action="{{route('pay-option.store')}}" enctype='multipart/form-data' class="smart-form client-form mb30 mt30"  id='smart-form-register'>
                                {{csrf_field()}}


                                <input type="text" hidden name="id" value="{{$id}}">

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Jenis Perlindungan:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="KOMPREHENSIF">
                                    </div>
                                </div>


                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Premium Asas:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 674.83" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">SST:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 53.99" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Duti Setem:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 10.00" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <hr>

                                <div class="col-lg-12 col-md-12 mb30">
                                    <div class="form-group">
                                        <label class="control-label" for="name">Premium Perlu Dibayar</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="premium_perlu_dibayar" size="55" class="form-control" value="RM 738.82" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 mb30">
                                    <div class="form-group">
                                        <label class="control-label" for="name">{{__('messages.mode_pembayaran')}} </label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="flexRadioDefault1" value="online">
                                            <label class="form-check-label" for="flexRadioDefault1" style="color: #0155a2!important">
                                               
                                                {{__('messages.online_payment')}}
                                                
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="flexRadioDefault2" checked value="pembiayaan">
                                            <label class="form-check-label" for="flexRadioDefault2" style="color: #0155a2!important">
                                                
                                                {{__('messages.pembiayaan')}}
                                            
                                            </label>
                                        </div>
                                         
                                    </div>
                                </div>


                                    
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <input type='checkbox' id='terms'  name='terms' value='1' required="required" ><b style="color: #0155a2!important"> Saya bersetuju dengan <a href="#" align='right' id="term" ><u>Terma dan Syarat</u></a></b><br>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <!-- <a align='right'  class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>-->
                                        <button class="btn btn-lg btn-primary" id="openModalBtn" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"> Submit </button>
                                        
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal Login -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    ...
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
                </div>
            </div>
    <!-- End Modal Login -->

@endsection



@push('addjs')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
    <script>
        $(document).ready(function() {
          $("#openModalBtn").click(function() {
            if (!$('#terms').is(':checked')) {
              alert('Please agree to the terms and conditions.');
              event.stopPropagation(); // Stop the event from propagating
                return false; // Prevent the default action
            } else {
              $("#exampleModal").modal('show');
            }
          });
        });
    </script>


    <!-- <script>
        $(document).ready(function(){
            $("#term").click(function(){
                $('#termModal').modal('show');
            });
        });
    </script> -->

    
@endpush
