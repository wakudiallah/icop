@extends('layouts.dca.template')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    .table>:not(caption)>*>* {
        color: #0155a2 !important;
    }
</style>
   
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    @include('layouts.dca.menu')
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
            @include('sweetalert::alert')  
            <div class="row">
               <div class="col-md-1">
                    <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">
                    <div class="front-left">
                        @include('layouts.dca.left_front')
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">
                        

                        <h2 class="mb30"><b>  Selamat Datang   </b></h2>


                            <fieldset class="mb30">
                                <table class="table table-bordered table-hover" border='1' style="font-size: 13px!important;text-align: center!important;margin-left: -8px !important; color: #0155a2 !important">
                                    <thead>
                                        <tr>
                                            <th valign="middle" style="width: 13%; text-align: center!important"><b>Nombor Register</b></th>
                                            <th style="text-align: center!important"><b>Jumlah Pembiayaan</b></th>
                                            <th style="text-align: center!important"><b>Status</b></th>
                                            <th style="text-align: center!important"> <b >Aksi</b></th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>20240424118</td>
                                            <td>RM XXXX</td>
                                            <td><div class="badge badge-pill badge-warning mb-1">New</div></td>
                                            <td align="center">  <a align='right' id="submit_upload" class="btn btn-xs btn-primary"><b><i class="fa fa-send"></i> Detail</b></a></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </fieldset>

                           
                        </div>
                    </div>
                </div>
            </div>

<!-- Modal -->
<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">TERMA DAN SYARAT</h4>
            </div>
            <div class="modal-body custom-scroll terms-body" style="padding: 36px!important">
                {{-- @include('home.terma') --}}
                </b></h3></div>   
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                   
                    <h4 class="modal-title" id="myModalLabel">Account Verification</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'form/uploaddoc', 'enctype' => 'multipart/form-data']) !!}
                        <fieldset>
                            <div class="form-body">   
                                <div class="form-group">
                                    <font color='black'>{{trans('mbsb.ic')}}</font>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                     <!--<input type="hidden" name="id_branch" id="id_branch" onClick="checkBranch()">-->
                                    <input type="icnumber" id="icnumber" maxlength='12' minlength='12' class="form-control" name="icnumber" value=""  required onKeyPress="if(this.value.length==12) return false;"  onkeypress="return isNumberKey(event)" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46) )" >
                                    <input type="hidden" id="fullname" name="fullname" value="$pra->fullname"  readonly >
                                    <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra" readonly >
                                    <input type='hidden' id='status_penyelesaian' name="debt_consolidation" value='0'/>
                                    <input type="hidden" name="referral_code" id="referral_code">
                                </div>
                            </div>
                        </fieldset>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-bs-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                Submit
                            </button>
                        </div>
                    {!! Form::close() !!}   
                </div>
            </div>
        </div>
    </div>
   
@include('pages.frontend.praaplication.upload_js')


   

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="state"]').on('change', function() {
              
                var stateID = $(this).val();
                 var wilayah    = $('#wilayah').val();
                if(stateID) {
                    $.ajax({
                        url:  "<?php  print url('/'); ?>/branch/"+stateID,

                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                             $('select[name="city"]').append("<option value=''>choose</option>");
                            $.each(data, function(key, value) {

                                $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                             
                            });


                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });
        });
    </script>
    <?php for ($x = 1; $x <= 15; $x++) {  ?>

<script>
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
        $('#fileupload{{$x}}').fileupload({
            url: url,
            dataType: 'json',
            success: function ( data) {
                var text = $('#documentx{{$x}}').val();
                var fi =  data.file;
                if(fi =='error'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='ext'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='size'){
                    alert("Please upload file below {{config('dca.max_file_mb')}} MB");
                }
            else{
                $("#document{{$x}}a").hide();
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                alert('Successfully upload '+text+' document');
                window.location.href = window.location.href;
            }
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>

<?php } ?>

    <script>
        function myFunction() {
            alert("Please Upload Mandatory Documents");
        }
    </script>
    <script type="text/javascript">
        var select = document.getElementById('branch');
        var input = document.getElementById('id_branch');
        select.onchange = function() {
            input.value = select.value;
        }
    </script>

 @endsection

@push('addjs')
    <script>
        $(document).ready(function(){
            $("#term").click(function(){
                $('#termModal').modal('show');
            });
        });
    </script>

    <script type="text/javascript">
        $( "#branch" ).change(function() { 
        var _token      = $('#token').val();
        var id_pra      = $("#id_praapplication").val();
        var id_branch   = $("#branch").val();

        $.ajax({
            type: "POST",
            data: {  _token : _token, id_pra : id_pra, id_branch : id_branch},
            url: "{{ url('/select_branch') }}",
            });
         });
    </script>
     <script>
        $(document).ready(function() {
            $("#state").select2();   
        });
        $(document).ready(function() {
            $("#branch").select2();   
        });
    </script>
@endpush
