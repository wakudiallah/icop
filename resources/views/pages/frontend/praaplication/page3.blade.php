@extends('layouts.dca.template')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <style type="text/css">
        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/materialicons/v50/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
        }

        .accordion-button:not(.collapsed) {
            background-color: #fff !important;
        }
    </style>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

@include('layouts.dca.menu')
    <!-- services -->
    <div class="">   <!-- container disable -->
        <div class="content">
            @include('sweetalert::alert')  
            <div class="row">
               <div class="col-md-1">
                    <img src="{{ url('/asset/img/logo_bank.png') }}" class="img-mbsb-xs d-block d-sm-none">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 left-mobile-use">
                    <div class="front-left">
                        @include('layouts.dca.left_front')
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 mb30 front-right">
                    <div class="consultantion-form">
                        

                        <h2 class="mb20"><b>{{__('messages.insuran_kenderaan')}} </b></h2>

                        <div class="accordion mb30" id="accordionExample">
                            <div class="accordion-item">
                              <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  
                                    <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                        @php
                                            $imageData = base64_decode($inCompany->logo);
                                            $imageSrc = 'data:image/png;base64,' . base64_encode($imageData);
                                        @endphp


                                        <tr>
                                            <td colspan="3" align="center">
                                                <img src="{{ $imageSrc }}" alt="Base64 Image" class="img img-responsive" style="max-width: 100pt !important">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="45%"  align="right">
                                                <a href="{{$inPolicy->product_disclosure}}" target="_blank">
                                                <label class="try control-label" for="name"  style="text-align: right !important" >Product Disclosure</label>
                                                </a>
                                            </td>
                                            <td width="5%"  align="left"></td>
                                            <td> 
                                                <a href="{{$inPolicy->policy_wording}}" target="_blank">
                                                <label class="try control-label" for="name" text-align="right">Policy Wording</label>
                                            </td>
                                        </tr>
                                    </table>

                                </button>
                              </h2>
                              <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                        <tr>
                                            <td colspan="3">
                                                <ul>
                                                    <li> PERCUMA Khidmat Tunda Kecemasan 24-jam sehingga jarak 150km</li>
                                                    <li> PERCUMA 1 Pemandu Tambahan</li>
                                                    <li> PERCUMA 24-jam Bantuan Tuntutan</li>
                                                    <li> Insurans Kereta Terbaik 2018</li>
                                                    <li> Kelulusan Segera bagi Nota Perlindungan</li>
                                                    <li> Sesuai bagi Kegunaan E-hailing</li>
                                                </ul>
                                            
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </div>
                              </div>
                            </div>



                            <div class="accordion-item">
                              <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                        @php
                                            $imageData = base64_decode($inCompany2->logo);
                                            $imageSrc = 'data:image/png;base64,' . base64_encode($imageData);
                                        @endphp


                                        <tr>
                                            <td colspan="3" align="center">
                                                <img src="{{ $imageSrc }}" alt="Base64 Image" class="img img-responsive" style="max-width: 100pt !important">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="45%"  align="right">
                                                <a href="{{$inPolicy2->product_disclosure}}" target="_blank">
                                                <label class="try control-label" for="name"  style="text-align: right !important" >Product Disclosure</label>
                                                </a>
                                            </td>
                                            <td width="5%"  align="left"></td>
                                            <td> 
                                                <a href="{{$inPolicy2->policy_wording}}" target="_blank">
                                                <label class="try control-label" for="name" text-align="right">Policy Wording</label>
                                            </td>
                                        </tr>
                                    </table>
                                </button>
                              </h2>
                              <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <table width="100%" style="line-height: 30px !important;color: #0155a2!important">
                                        <tr>
                                            <td colspan="3">
                                                <ul>
                                                    <li> PERCUMA Khidmat Tunda Kecemasan 24-jam sehingga jarak 150km</li>
                                                    <li> PERCUMA 1 Pemandu Tambahan</li>
                                                    <li> PERCUMA 24-jam Bantuan Tuntutan</li>
                                                    <li> Insurans Kereta Terbaik 2018</li>
                                                    <li> Kelulusan Segera bagi Nota Perlindungan</li>
                                                    <li> Sesuai bagi Kegunaan E-hailing</li>
                                                </ul>
                                            
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </div>
                              </div>
                            </div>
                        </div>

                          
                        



                            <form class="smart-form client-form mb30" id='smart-form-register'>
                        
                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Premium Asas:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 674.83" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">SST:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 53.99" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Duti Setem:</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 10.00" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                <hr>

                                <div class="col-lg-12 col-md-12" >
                                    <div class="form-group">
                                        <label class="control-label" for="name">Premium Perlu Dibayar</label>
                                        <input type="text" id="ref_code"  onkeyup="this.value = this.value.toUpperCase()" name="ref_code" size="55" class="form-control" value="RM 738.82" align='right' style="text-align: right !important">
                                    </div>
                                </div>

                                

                                    
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <!-- <input type='checkbox'  id='terms'  name='terms' value='1' required="required" ><b style="color: #0155a2!important"> I agree with the <a href="#" align='right' id="term" ><u>Terms and conditions</u></a></b><br> -->
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        
                                        <a href="{{url('icop31/'.$id)}}" align='right' id="submit_upload" class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>
            </div>

<!-- Modal -->
<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">TERMA DAN SYARAT</h4>
            </div>
            <div class="modal-body custom-scroll terms-body" style="padding: 36px!important">
                {{-- @include('home.terma') --}}
                </b></h3></div>   
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                   
                    <h4 class="modal-title" id="myModalLabel">Account Verification</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'form/uploaddoc', 'enctype' => 'multipart/form-data']) !!}
                        <fieldset>
                            <div class="form-body">   
                                <div class="form-group">
                                    <font color='black'>{{trans('mbsb.ic')}}</font>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                     <!--<input type="hidden" name="id_branch" id="id_branch" onClick="checkBranch()">-->
                                    <input type="icnumber" id="icnumber" maxlength='12' minlength='12' class="form-control" name="icnumber" value=""  required onKeyPress="if(this.value.length==12) return false;"  onkeypress="return isNumberKey(event)" onkeydown="return ( event.ctrlKey || event.altKey 
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9) 
                                    || (event.keyCode>34 && event.keyCode<40) 
                                    || (event.keyCode==46) )" >
                                    <input type="hidden" id="fullname" name="fullname" value="$pra->fullname"  readonly >
                                    <input type="hidden" id="id_praapplication" name="id_praapplication" value="$pra->id_pra" readonly >
                                    <input type='hidden' id='status_penyelesaian' name="debt_consolidation" value='0'/>
                                    <input type="hidden" name="referral_code" id="referral_code">
                                </div>
                            </div>
                        </fieldset>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-bs-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                Submit
                            </button>
                        </div>
                    {!! Form::close() !!}   
                </div>
            </div>
        </div>
    </div>
   
@include('pages.frontend.praaplication.upload_js')


   

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="state"]').on('change', function() {
              
                var stateID = $(this).val();
                 var wilayah    = $('#wilayah').val();
                if(stateID) {
                    $.ajax({
                        url:  "<?php  print url('/'); ?>/branch/"+stateID,

                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                             $('select[name="city"]').append("<option value=''>choose</option>");
                            $.each(data, function(key, value) {

                               

                                $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                             
                            });


                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });
        });
    </script>
    <?php for ($x = 1; $x <= 15; $x++) {  ?>

<script>
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
        $('#fileupload{{$x}}').fileupload({
            url: url,
            dataType: 'json',
            success: function ( data) {
                var text = $('#documentx{{$x}}').val();
                var fi =  data.file;
                if(fi =='error'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='ext'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='size'){
                    alert("Please upload file below {{config('dca.max_file_mb')}} MB");
                }
            else{
                $("#document{{$x}}a").hide();
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                $("#a{{$x}}").val(data.file);
                alert('Successfully upload '+text+' document');
                window.location.href = window.location.href;
            }
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>

<?php } ?>

    <script>
        function myFunction() {
            alert("Please Upload Mandatory Documents");
        }
    </script>
    <script type="text/javascript">
        var select = document.getElementById('branch');
        var input = document.getElementById('id_branch');
        select.onchange = function() {
            input.value = select.value;
        }
    </script>

 @endsection

@push('addjs')
    <script>
        $(document).ready(function(){
            $("#term").click(function(){
                $('#termModal').modal('show');
            });
        });
    </script>

    <script type="text/javascript">
        $( "#branch" ).change(function() { 
        var _token      = $('#token').val();
        var id_pra      = $("#id_praapplication").val();
        var id_branch   = $("#branch").val();

        $.ajax({
            type: "POST",
            data: {  _token : _token, id_pra : id_pra, id_branch : id_branch},
            url: "{{ url('/select_branch') }}",
            });
         });
    </script>
     <script>
        $(document).ready(function() {
            $("#state").select2();   
        });
        $(document).ready(function() {
            $("#branch").select2();   
        });
    </script>
@endpush
