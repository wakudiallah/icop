@extends('frontend.template')

@push('style')
  
 <style>
  .form-control{
    color: black !important;
  }
  </style>

@endpush

@section('main-content')


<!-- contact section -->

  <section class="contact_section layout_padding">

    @include('sweetalert::alert')
    
    <div class="container">
      <div class="heading_container">
        <h2>
          Vehicle Details
        </h2>
      </div>
      <div class="row">
        <div class="col-md-12">

          <form action="{{route('quote.store')}}" method="post" enctype='multipart/form-data'>
            {{csrf_field()}}

            <div>
                <label for="exampleInputEmail1">Vehicle Number</label>
                <input type="text" name="vehicle" value="{{old('vehicle')}}" placeholder="Vehicle Number" class="form-control" style="text-transform: uppercase"/>
                @error('vehicle')
                  <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div>
                <label for="exampleInputEmail1">IC Number</label>
                <input type="text" name="ic" value="{{old('ic')}}" placeholder="IC Number" class="form-control" style="text-transform: uppercase"/>
                @error('ic')
                  <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div>
                <label for="exampleInputEmail1">Police IC / Army IC (if applicable)</label>
                <input type="text" name="ic2" value="{{old('ic2')}}" placeholder="Police IC / Army IC (if applicable)" class="form-control" style="text-transform: uppercase"/>
                @error('ic2')
                  <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div>
                <label for="exampleInputEmail1">E-Hailing</label>
                <select class="form-control" aria-label="Default select example" name="ehailing">
                  <option value="" selected disabled hidden>- Please Select -</option>
                  @foreach($ehailing as $ehailing)
                  <option value="{{$ehailing->id}}">{{$ehailing->ehailing}}</option>
                  @endforeach
                </select>
                @error('ehailing')
                  <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <hr>
            <div>
                <label for="exampleInputEmail1">Email</label>
                <input type="email" value="{{old('email')}}" name="email" placeholder="Email" class="form-control"/>
                @error('email')
                  <span class="text-danger">{{$message}}</span>
                @enderror
              </div>

            <div>
                <label for="exampleInputEmail1">Whatsapp Number</label>
                <input type="text" name="whatsapp" value="{{old('whatsapp')}}" placeholder="Whatsapp Number" class="form-control" style="text-transform: uppercase"/>
                @error('whatsapp')
                  <span class="text-danger">{{$message}}</span>
                @enderror
              </div>


            <div>
                <label for="exampleInputEmail1">Postcode</label>
                <input type="text" name="postcode" value="{{old('postcode')}}" placeholder="Postcode" class="form-control" style="text-transform: uppercase"/>
                @error('postcode')
                  <span class="text-danger">{{$message}}</span>
                @enderror
              </div>


            <div class="d-flex ">
              <button type="submit" class="btn btn-success"> Get Quote </button>
              <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </section>

  <!-- end contact section -->


  @endsection


  @push('scripts')

  @endpush