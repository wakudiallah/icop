@extends('frontend.template')

@section('main-content')

<!-- contact section -->

  <section class="contact_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          Motor Details
        </h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form action="">
            <div>
                <label for="exampleInputEmail1">Vehicle Number</label>
                <input type="text" placeholder="Vehicle Number" class="form-control"/>
            </div>
            <div>
                <label for="exampleInputEmail1">IC Number</label>
                <input type="text" placeholder="IC Number" class="form-control"/>
            </div>
            <div>
                <label for="exampleInputEmail1">Police IC / Army IC (if applicable)</label>
                <input type="email" placeholder="Police IC / Army IC (if applicable)" class="form-control" />
            </div>

            <div>
                <label for="exampleInputEmail1">Owner's Name</label>
                <input type="email" placeholder="Owner's Name" class="form-control" />
            </div>

            <hr>
            <div>
                <label for="exampleInputEmail1">Email</label>
                <input type="email" placeholder="Email" class="form-control" />
            </div>

            <div>
                <label for="exampleInputEmail1">Whatsapp Number</label>
                <input type="email" placeholder="Whatsapp Number" class="form-control" />
            </div>


            <div>
                <label for="exampleInputEmail1">Postcode</label>
                <input type="email" placeholder="Postcode" class="form-control" />
            </div>


            
            <div class="d-flex ">
              <button>
                Get Quote
              </button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </section>

  <!-- end contact section -->


  @endsection