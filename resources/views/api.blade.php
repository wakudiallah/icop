@extends('layouts.app')

@section('title', 'Param GST')

@push('style')
    
     <!-- datatable -->
     <link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.css" />

@endpush


@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Param GST </h1>
                <div class="section-header-breadcrumb">
                    
                    <div class="breadcrumb-item">{{ Breadcrumbs::render('param-gst-edit') }}</div>
                </div>
            </div>

            <div class="section-body">
                <!-- <h2 class="section-title">Table</h2>
                <p class="section-lead">Example of some Bootstrap table components.</p> -->
               
              
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="card-header-form">
                                   
                                </div>
                            </div>
                            <div class="card-body p-10">
                                <div class="table-responsive">
                                    
                                    <form method="post" action="{{url('apitest')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">Generate Token</button>
                                        </div>

                                    </form>


                                    <form method="post" action="{{url('getvehicleinfo')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">getvehicleinfo</button>
                                        </div>

                                    </form>



                                    <form method="post" action="{{url('getvehiclemake')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">getvehiclemake</button>
                                        </div>

                                    </form>


                                    <form method="post" action="{{url('getvehiclemodel')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">getvehiclemodel</button>
                                        </div>

                                    </form>

                                    <hr>

                                    
                                    <form method="post" action="{{url('calculatepremiumCreate')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                           <button class="btn btn-success" type="submit">Create quotation</button>
                                        </div>

                                    </form>
                                    


                                    <form method="post" action="{{url('calculatepremium')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">calculatepremium</button>
                                        </div>

                                    </form>

                                    
                                    <a href="{{url('calculatepremium')}}" target="_blank" class="btn btn-primary">calculatepremium GET </a>


                                    <form method="post" action="{{url('calculatepremiumREF')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">calculatepremiumREF</button>
                                        </div>

                                    </form>


                                    <hr>

                                    
                                    <form method="post" action="{{url('issuecovernote')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 

                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">issuecovernote</button>
                                        </div>

                                    </form>


                                    <form method="post" action="{{url('issuepolicy')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">issuepolicy</button>
                                        </div>

                                    </form>



                                    <form method="post" action="{{url('getjpjstatus')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 

                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">getjpjstatus</button>
                                        </div>
                                    </form>




                                    <form method="post" action="{{url('downloadpolicyschedule')}}" enctype='multipart/form-data' target="_blank">
                                        @csrf 


                                        <div class="form-group mb-3">
                                          <!-- <button type="reset" class="btn btn-warning">Reset</button> -->
                                           <button class="btn btn-success" type="submit">downloadpolicyschedule</button>
                                        </div>

                                    </form>







                                    
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/components-table.js') }}"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

@endpush
