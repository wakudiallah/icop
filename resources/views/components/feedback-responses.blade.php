<x-tlayout>
    <h1>Customer Feedback Responses</h1>
    <ul>
        @foreach($feedbackResponses as $feedback)
        <li><span>Customer phone:</span> {{ $feedback->phone }},<br><br> <span>Feedback Response:</span>
        {{ $feedback->response }}</li>
        @endforeach
    </ul>
</x-tlayout>