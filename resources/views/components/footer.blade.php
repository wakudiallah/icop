<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2024 - <?php echo date("Y"); ?> <div class="bullet"></div> Powered By Masamart
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>
