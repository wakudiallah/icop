<x-tlayout>
    <form method="POST" action="{{ url('/feedback') }}">
        @csrf
        <h1>Send SMS feedback request to customers</h1>
        <label for="phone">Phone:</label>
        <textarea name="phones" id="phone" placeholder="A comma-separated list of customer phone numbers..."></textarea>

        <label for="message">Feedback request message:</label>
        <textarea name="message" id="message" placeholder="Enter a feedback request message for your customers..."></textarea>
        <button type="submit">Submit</button>
    </form>
</x-tlayout>