<aside id="sidebar-wrapper">
        <div class="main-sidebar sidebar-style-2">
        <div class="sidebar-brand">
            <a href="{{route('home.index') }}">{{config('insurance.system_name')}}</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{route('home.index') }}">Icop</a>
        </div>
        <ul class="sidebar-menu">
            @can('dashboard')
            <li class="menu-header">Dashboard</li>

            <li class="{{ Request::is('home') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('home.index') }}"><i class="fas fa-fire">
                    </i> <span> Dashboard </span>
                </a>
            </li>
            @endcan

            <!-- 
            <li class="nav-item dropdown">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li class='{{ Request::is('dashboard-general-dashboard') ? 'active' : '' }}'>
                        <a class="nav-link"
                            href="{{ url('dashboard-general-dashboard') }}">General Dashboard</a>
                    </li>
                </ul>
            </li>
            -->

            @can('praapplication-list')
            <li class="menu-header">Submissions</li>
            
            <li class="nav-item dropdown {{ Request::is('pra-*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-pencil-ruler"></i><span>Pra Application</span></a>
                <ul class="dropdown-menu">

                    <li class="{{ Request::is('pra-finance*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('pra-finance.index') }}"> Financing 
                        </a>
                    </li>

                    <li class="{{ Request::is('pra-apps*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('pra-apps.index') }}">Online Payment 
                        </a>
                    </li>
                    
                </ul>
            </li>
            @endcan

            
            @can('app-processing-list')
            <li class="menu-header">Processing</li>
            <li class="{{ Request::is('app-processing*') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('app-processing.index') }}"><i class="fas fa-check"></i>
                     <span> App Processing </span>
                </a>
            </li>
            @endcan


            @can('app-smsakad-list')
            <li class="menu-header">Akad</li>
            <li class="{{ Request::is('app-smsakad*') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('app-smsakad.index') }}"><i class="fas fa-envelope"></i>
                     <span> App Sms Akad </span>
                </a>
            </li>
            @endcan


            
            <li class="menu-header">Issue Covernote</li>
            <li class="{{ Request::is('app-issuecovernote*') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('app-issuecovernote.index') }}"><i class="fas fa-file-text"></i>
                     <span> App Issue Covernote </span>
                </a>
            </li>
            @can('app-issuecovernote-list')
            @endcan

            


            <li class="menu-header">Parameter</li>

            <li class="nav-item dropdown {{ Request::is('param-*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Parameter</span></a>
                <ul class="dropdown-menu">
                    
                    @can('param-type-insurance-list')
                    <li class="{{ Request::is('param-type-insurance') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-type-insurance.index') }}"> Param Type Insurance
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-branch-list')
                    <li class="{{ Request::is('param-branch*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-branch.index') }}"> Param Branch 
                        </a>
                    </li>
                    @endcan


                    @can('param-protection-type-list')
                    <li class="{{ Request::is('param-protection-type*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-protection-type.index') }}"> Param Protection Type 
                        </a>
                    </li>
                    @endcan


                    @can('param-status-app-list')
                    <li class="{{ Request::is('param-status-app*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-status-app.index') }}"> Param Status App 
                        </a>
                    </li>
                    @endcan
                    

                    @can('param-employment-status-list')
                    <li class="{{ Request::is('param-employment-status*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-employment-status.index') }}"> Param Employment Status 
                        </a>
                    </li>
                    @endcan
        
                    @can('param-employment-type-list')
                    <li class="{{ Request::is('param-employment-type*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-employment-type.index') }}"> Param Employment Type
                        </a>
                    </li>
                    @endcan
        
                    @can('param-occupation-list')
                    <li class="{{ Request::is('param-occupation*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-occupation.index') }}"> Param Occupation 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-payment-mode-list')
                    <li class="{{ Request::is('param-payment-mode*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-payment-mode.index') }}">Param Payment Mode 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-reject-reason-list')
                    <li class="{{ Request::is('param-reject-reason*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-reject-reason.index') }}"> Param Reject Reason 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-bank-name-list')
                    <li class="{{ Request::is('param-bank-name*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-bank-name.index') }}"> Param Bank Name 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-takaful-provider-list')
                    <li class="{{ Request::is('param-takaful-provider*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-takaful-provider.index') }}"> Param Takaful Provider 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-manfaat-tambahan-list')
                    <li class="{{ Request::is('param-manfaat-tambahan*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-manfaat-tambahan.index') }}">Param Manfaat Tambahan
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-interest-rate-list')
                    <li class="{{ Request::is('param-interest-rate*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-interest-rate.index') }}"> Param Interest Rate
                        </a>
                    </li>
                    @endcan
                    
                
                    @can('param-sst-list')
                    <li class="{{ Request::is('param-sst*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-sst.index') }}"> Param SST 
                        </a>
                    </li>
                    @endcan
                    
                     
                    @can('param-gst-list')
                    <li class="{{ Request::is('param-gst*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-gst.index') }}">Param GST 
                        </a>
                    </li>
                    @endcan
        
        
                    @can('param-state-list')
                    <li class="{{ Request::is('param-state*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-state.index') }}"> Param State 
                        </a>
                    </li>
                    @endcan
        
                    @can('param-postcode-list')
                    <li class="{{ Request::is('param-postcode*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-postcode.index') }}"> Param Postcode 
                        </a>
                    </li>
                    @endcan

                    @can('param-duti-setem-list')
                    <li class="{{ Request::is('param-duti-setem*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-duti-setem.index') }}"> Param Duti Setem 
                        </a>
                    </li>
                    @endcan

                    @can('param-email-system-list')
                    <li class="{{ Request::is('param-email-system*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-email-system.index') }}"> Param Email System 
                        </a>
                    </li>
                    @endcan

                    @can('param-email-for-list')
                    <li class="{{ Request::is('param-email-for*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-email-for.index') }}"> Param Email For 
                        </a>
                    </li>
                    @endcan


                    @can('param-insurance-company-list')
                    <li class="{{ Request::is('param-insurance-company*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-insurance-company.index') }}"> Param Insurance Company
                        </a>
                    </li>
                    @endcan


                    @can('param-insurance-policy-list')
                    <li class="{{ Request::is('param-insurance-policy*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-insurance-policy.index') }}"> Param Insurance Policy
                        </a>
                    </li>
                    @endcan

                    @can('param-insurance-promo-list')
                    <li class="{{ Request::is('param-insurance-promo*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-insurance-promo.index') }}"> Param Insurance Promo
                        </a>
                    </li>
                    @endcan


                    @can('param-region-lkm-list')
                    <li class="{{ Request::is('param-region-lkm*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-region-lkm.index') }}"> Param Region LKM 
                        </a>
                    </li>
                    @endcan
                    


                    @can('param-ownership-lkm-list')
                    <li class="{{ Request::is('param-ownership-lkm*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-ownership-lkm.index') }}"> Param Car / Ownership LKM
                        </a>
                    </li>
                    @endcan


                    @can('param-roadtax-lkm-list')
                    <li class="{{ Request::is('param-roadtax-lkm*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('param-roadtax-lkm.index') }}"> Param Roadtax LKM
                        </a>
                    </li>
                    @endcan
                    
        
        
                </ul>
            </li>


            <li class="menu-header">Setup</li>

            <li class="nav-item dropdown {{ Request::is('setup-*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-cog"></i><span>Setup</span></a>
                <ul class="dropdown-menu">

                    @can('setup-maintainable-emails-list')
                    <li class="{{ Request::is('setup-maintainable-emails*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-maintainable-emails.index') }}"> Setup Maintanable Email 
                        </a>
                    </li>
                    @endcan


                    @can('setup-monthly-installment-list')
                    <li class="{{ Request::is('setup-monthly-installment*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-monthly-installment.index') }}"> Setup Monthly Installment 
                        </a>
                    </li>
                    @endcan


                    @can('setup-document-list')
                    <li class="{{ Request::is('setup-document*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-document.index') }}"> Setup Document 
                        </a>
                    </li>
                    @endcan


                    @can('setup-smsakad-list')
                    <li class="{{ Request::is('setup-smsakad*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-smsakad.index') }}"> Setup Sms Akad 
                        </a>
                    </li>
                    @endcan
                    

                    @can('setup-zurichcart-list')
                    <li class="{{ Request::is('setup-zurich-cart*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-zurich-cart.index') }}"> Setup Zurich Cart 
                        </a>
                    </li>
                    @endcan
                    

                    @can('setup-bpacharge-list')
                    <li class="{{ Request::is('setup-bpacharge*') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('setup-bpacharge.index') }}"> Setup BPA Charge 
                        </a>
                    </li>
                    @endcan

                </ul>
            </li>


            @can('audit-trail-list')
            <li class="{{ Request::is('audit-trail') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ url('audit-trail') }}"><i class="fas fa-pencil-ruler">
                    </i> <span> Audit </span>
                </a>
            </li>
            @endcan


            @can('calculate-roadtax-view')
            <li class="{{ Request::is('calculate-roadtax') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ url('calculate-roadtax') }}"><i class="fas fa-pencil-ruler">
                    </i> <span> Calculate Roadtax (LKM) </span>
                </a>
            </li>
            @endcan
            

            <li class="menu-header">API</li>
            <li class="nav-item dropdown {{ Request::is('zurich-*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"><i class="fas fa-cog"></i><span>Zurich API</span></a>
                <ul class="dropdown-menu">

                    
                    <li class="{{ Request::is('zurich-token*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-token') }}"> Token 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-getvehicleinfo*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-getvehicleinfo') }}"> Get Vehicle Info 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-getvehiclemake*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-getvehiclemake') }}"> Get Vehicle Make 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-getvehiclemodel*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-getvehiclemodel') }}"> Get Vehicle Model 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-calculatepremium*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-calculatepremium') }}"> Calculate Premium 
                        </a>
                    </li>


                    <li class="{{ Request::is('zurich-issuecovernote*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-issuecovernote') }}"> Issue Covernote 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-issuepolicy*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-issuepolicy') }}"> Issue Policy 
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-getjpjstatus*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-getjpjstatus') }}"> Get jpj status
                        </a>
                    </li>

                    <li class="{{ Request::is('zurich-downloadpolicyschedule*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-downloadpolicyschedule') }}"> Download Policy Schedule 
                        </a>
                    </li>


                    <li class="{{ Request::is('zurich-postcode*') ? 'active' : '' }}">
                        <a class="nav-link" target="_blank"
                            href="{{ url('zurich-postcode') }}"> Postcode 
                        </a>
                    </li>



                </ul>
            </li>


            <li class="{{ Request::is('api-test') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ url('api-test') }}"><i class="fas fa-pencil-ruler">
                    </i> <span> API Test </span>
                </a>
            </li>


            <li class="menu-header">Report</li>
            

            <li class="menu-header">Starter</li>
            
            @can('role-list')
            <li class="{{ Request::is('roles') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('roles.index') }}"><i class="fas fa-pencil-ruler">
                    </i> <span> Role </span>
                </a>
            </li>
            
            @endcan

            @can('permission-list')
            <li class="{{ Request::is('permissions') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('permissions.index') }}"><i class="fas fa-pencil-ruler">
                    </i> <span>Permission</span>
                </a>
            </li>
            @endcan

            @can('user-list')
            <li class="{{ Request::is('users') ? 'active' : '' }}">
                <a class="nav-link"
                    href="{{ route('users.index') }}"><i class="fas fa-pencil-ruler">
                    </i> <span>User</span>
                </a>
            </li>
            @endcan
            
        </ul>


        

        <div class="hide-sidebar-mini mt-4 mb-4 p-3">
            <a href="https://getstisla.com/docs"
                class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
            </a>
        </div>
    </aside>
</div>
