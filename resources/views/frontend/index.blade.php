<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="google-site-verification" content="XHJAWe662Xw-_9c9cPSX-9az8rHfgjVY5H4ptLZej2o" />

	<title>Icop | Homepage</title>

	<!-- Basic style -->
	@stylesheets('core')
	<link type="text/css" href="{{ asset('frontend/css/main.css')}}" rel="stylesheet">
		<link type="text/css" href="{{ asset('frontend/color/default.css')}}" rel="stylesheet">

	<!--Page scripts -->

	<!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


	<!-- Page style -->
	@yield('jasny')

	<!-- ICOP CSS -->
	@stylesheets('icop')

	<!-- Page style -->
	@yield('page-style')
	

	<!-- #FAVICONS -->
	<link rel="shortcut icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{ url('/img/favicon/favicon.ico')}}" type="image/x-icon">

	<!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>

<body class="menu-on-top" data-spy="scroll">

    <div id="wrapper">

		<div class="container">
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger visible-xs visible-sm">
					<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
					<nav class="gn-menu-wrapper">
			<div class="gn-scroller">
				<ul class="gn-menu">

					<li><a href="{{ url('/')}}" class="gn-icon gn-icon-earth">Utama</a></li>
					@if (!(Auth::user())) 
						@elseif(!empty(Auth::user()->role_id=='4'))
						<li><a href="{{ url('/coop/home') }}" class="gn-icon gn-icon-pictures">Coop</a></li>
					@endif
					@if (!(Auth::user())) 
						@elseif(!empty(Auth::user()->role_id=='2'))
						<li ><a href="{{ url('/admin') }}" class="gn-icon gn-icon-pictures">Admin</a></li>
					@endif
					@if (!(Auth::user())) 
						@elseif(!empty(Auth::user()->role_id=='6'))
						<li ><a href="{{ url('/angkasa/home') }}" class="gn-icon gn-icon-pictures">Angkasa</a></li>
					@endif
						<li><a href="{{URL::to('page/about-us')}}" class="gn-icon gn-icon-videos">Mengenai Kami</a></li>
						<li><a href="{{ url('/product/product')}}" class="gn-icon gn-icon-photoshop">Produk</a></li>
						<li><a href="{{ url('/page/contact-us') }}" class="gn-icon gn-icon-help">Hubungi Kami</a></li>
					@if (!(Auth::user()))
						<li ><a href="{{  url('/daftar') }}" class="gn-icon gn-icon-article" style="color: red">Pendaftaran Anggota</a></li>
						<li ><a href="{{  url('cooperative/register') }}" class="gn-icon gn-icon-article" style="color: red">Pendaftaran Koperasi</a></li>
					@endif
					@if (!(Auth::user())) 
						@elseif(!empty(Auth::user()->role_id=='3'))
						<li ><a href="{{url('/applicant/mystatus')}}" class="gn-icon gn-icon-archive">Status</a></li>
						<li><a href="{{ url('/applicant/profil')}}" class="gn-icon gn-icon-pictures">Profil</a></li>
					@endif
					@if (!(Auth::user()))
						<li><a href="{{ url('/auth/login') }}" class="gn-icon gn-icon-cog">Daftar Masuk</a></li>
					@else
						<li><a href="{{ url('/logout')}}" onclick='return confirm("Apakah Anda Ingin Keluar?")' >Daftar Keluar</a></li>

					@endif
					
				</ul>
			</div><!-- /gn-scroller -->
			</nav>
			</li>
			
			<li class="hidden-xs hidden-sm">
				<a href="{{ url('/')}}"><img src="{{url('/img/icopangkasa.png')}}" width="95px" height="58px"></a>
			</li>
			
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='4'))
					<li class="hidden-xs hidden-sm"><a href="{{ url('/coop/home') }}">{{trans('menu/menu.coop')}}</a></li>
			@endif
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='2'))
					<li class="hidden-xs hidden-sm"><a href="{{ url('/admin') }}">Admin</a></li>
			@endif
			@if (!(Auth::user())) 
				@elseif(!empty(Auth::user()->role_id=='6'))
				<li class="hidden-xs hidden-sm"><a href="{{ url('/angkasa/home') }}">Angkasa</a></li>
			@endif
				<li class="hidden-xs hidden-sm"><a href="{{URL::to('page/about-us')}}">{{trans('menu/menu.about')}}</a></li>
				<li class="hidden-xs hidden-sm"><a href="{{ url('/product/product')}}">{{trans('menu/menu.product')}}</a></li>
				<li class="hidden-xs hidden-sm"><a href="{{ url('/page/contact-us') }}">Hubungi Kami</a></li>
			@if (!(Auth::user()))
				<li class="hidden-xs hidden-sm" style="color: red"><a href="{{ url('/daftar') }}" style="color: red">Pendaftaran Anggota</a></li>
				<li class="hidden-xs hidden-sm" style="color: red"><a href="{{ url('cooperative/register') }}" style="color: blue">{{trans('menu/menu.register_coop')}}</a></li>
			@endif
			
			@if (!(Auth::user())) 
			@elseif(!empty(Auth::user()->role_id=='3'))
				<li class="hidden-xs hidden-sm"><a href="{{ url('/applicant/mystatus')}}">{{trans('menu/menu.status')}}</a></li>
				<li class="hidden-xs hidden-sm"><a href="{{ url('/applicant/profil')}}">{{trans('menu/menu.profile')}}</a></li>
			@endif
			
				<li>			
					@if (!(Auth::user()))
						<a href="{{ url('/auth/login') }}">{{trans('menu/menu.login')}}</a>
					@else
						<li class="hidden-xs hidden-sm"><a href="{{ url('/logout') }}" onclick='return confirm("Apakah Anda Ingin Keluar?")'>{{trans('menu/menu.logout')}}</a></li>
					@endif
				</li>
			</div><!-- /.container -->




	
		<div class="content">
			

				@section('page-style')
					@stylesheets('revolution')
					@stylesheets('nivo')
				@stop

				@section('content')
				<link rel="stylesheet" href="{{ asset('frontend/dist/remodal.css')}}">
				
				
				<link rel="stylesheet" href="{{ asset('frontend/dist/remodal-default-theme.css')}}">
				<style>
					.remodal-bg.with-red-theme.remodal-is-opening,
					.remodal-bg.with-red-theme.remodal-is-opened {
					filter: none;
					}

					.remodal-overlay.with-red-theme {
					background-color: #f44336;
					}

					.remodal.with-red-theme {
					background: #fff;
					}
				</style>
				<style type="text/css">
				.upper { text-transform: uppercase; }
				</style>
					
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#myModal").modal('show');
					});
				</script>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0';
				fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
					<!--<section id="home" class="intro">
					<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><img src="{{url('/img/icopangkasa.png')}}" width="10%" height="10%"><b style="color:#2a1a93;font-weight: bold">	ICOP ANGKASA </b></h4>
						</div>
						<div class="modal-body">
						
							<div class="container">
								<div class="row">
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
										<div class="section-heading">
											<h2 class="text-center" style="  font-family:  Arial; font-weight: bold; font-size: 20px">Tentang Kami</h2>
											<p class="text-justify" style="  font-family:  Arial; font-weight: bold; font-size: 17px">
												Program <b>ICOPANGKASA.COM.MY </b> (ICOP) merupakan inovasi ANGKASA bertujuan memberi nilai tambah perkhidmatan kepada Koperasi Ahli dan memberi manfaat secara langsung kepada anggota-anggota Koperasi seluruh negara. Sistem ini dibangunkan khusus untuk menjalinkan kerjasama bersama Koperasi Koperasi diMalaysia. Melalui ICOP, jalinan persefahaman antara ANGKASA, rakan strategik dan Syarikat-syarikat Insurans/Takaful telah dimeteri bagi membolehkan Koperasi dan anggota-anggota Koperasi membeli insuran kenderaan bermotor serta memperbaharui cukai jalan diatas talian dengan mudah, cepat dan selesa. Selain dari itu ICOP juga akan menawarkan lain-lain produk insuran yang menarik secara pakej istimewa dan berperingkat untuk Koperasi dan anggota Koperasi. Icopangkasa.com.my, diurus tadbir oleh Marketing Sdn Bhd sebagai rakan Teknologi dan strategik ANGKASA
											</p>
										</div>
									</div>
								</div>
									<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<p class="text-justify" style="  font-family:  Arial; font-weight: bold; font-size: 17px">
											Angkatan Koperasi Kebangsaan Malaysia Berhad atau ringkasnya ANGKASA adalah merupakan badan puncak gerakan koperasi yang diiktiraf oleh kerajaan untuk mewakili Pergerakan Koperasi Malaysia diperingkat kebangsaan dan antarabangsa. ANGKASA melaksanakan program transformasi koperasi melalui penyertaannya dalam projekprojek ekonomi berimpak tinggi. ANGKASA juga menjalankan perkhidmatan terasnya yang menyediakan perkhidmatan potongan gaji kepada kakitangan kerajaan, koperasi badan berkanun, kelab, koperasi, kesatuan dan syarikat GLC.
										</p>
									</div>


									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

										<div class="well">
											<div id="myCarousel" class="carousel fade">
												<ol class="carousel-indicators">
													<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
													<li data-target="#myCarousel" data-slide-to="1" class=""></li>
													<li data-target="#myCarousel" data-slide-to="2" class=""></li>
												</ol>

												<div class="carousel-inner">

													<div class="item active">
														<img src="{{asset ('frontend/img/demo/a1.jpg')}}" alt="">
														<div class="carousel-caption caption-right">
															
															
														</div>
													</div>

													<div class="item">
														<img src="{{asset ('frontend/img/demo/a2.jpg')}}" alt="">
														<div class="carousel-caption caption-left">
															
														</div>
													</div>

													<div class="item">
														<img src="{{asset ('frontend/img/demo/a3.jpg')}}" alt="">
														<div class="carousel-caption">
															
														</div>
													</div>

												<a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
												<a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
											</div>
										</div>
									</div>
							</div>
								</div>
						<div class="modal-footer">
						@if (!(Auth::user())) <a href="{{URL::to('daftar')}}"><button class="btn btn-success">Daftar</button></a> @endif <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</section>-->

					

					
				
					
					</div>
					</div>
				</div>
					
						<div class="slogan">

							<!--
							#################################
							- THEMEPUNCH BANNER -
							#################################
							-->
						<div class="tp-banner-container">
									<div class="tp-banner" >
										<ul>
											<!-- SLIDE  -->
											<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-thumb="img/slider/slide1-prev.jpg"  data-saveperformance="on"  data-title="Intro Slide">
												<!-- MAIN IMAGE -->
												<img src="img/slider/dummy.png"  alt="slidebg1" data-lazyload="img/slider/slidebg1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
												<!-- LAYERS -->

												<!-- LAYER NR. 1 -->
													<div class="tp-caption lft customout rs-parallaxlevel-0"
											data-x="775"
											data-y="99"
											data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
											data-speed="700"
											data-start="1550"
											data-easing="Power3.easeInOut"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											style="z-index: 2;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/motor-cycle.jpg">
											</div>

												
											<!-- LAYER NR. 2 -->
											<!--<div class="tp-caption lft customout rs-parallaxlevel-0"
											data-x="564"
											data-y="96"
											data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
											data-speed="700"
											data-start="1400"
											data-easing="Power3.easeInOut"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											style="z-index: 3;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/third-party.jpg">
											</div>-->


												<!-- LAYER NR. 3 -->
												<div class="tp-caption lft customout rs-parallaxlevel-0"
												data-x="480"
												data-y="99"
												data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="700"
												data-start="1100"
												data-easing="Power3.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												style="z-index: 5;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/compre.jpg">
												</div>

												<!-- LAYER NR. 4 -->
												<div class="tp-caption grey_heavy_72 skewfromrightshort tp-resizeme rs-parallaxlevel-0"
													data-x="197"
													data-y="154" 
													data-speed="500"
													data-start="2250"
													data-easing="Power3.easeInOut"
													data-splitin="chars"
													data-splitout="none"
													data-elementdelay="0.1"
													data-endelementdelay="0.1"
													style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">						</div>

												<!-- LAYER NR. 5 -->
												<div class="tp-caption customin rs-parallaxlevel-0"
													data-x="86"
													data-y="184" 
													data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
													data-speed="500"
													data-start="2000"
													data-easing="Power3.easeInOut"
													data-elementdelay="0.1"
													data-endelementdelay="0.1"
													style="z-index: 6;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/redbg.png">
												</div>

												<!-- LAYER NR. 6 -->
												<div class="tp-caption black_heavy_60 skewfromleftshort tp-resizeme rs-parallaxlevel-0"
													data-x="-2"
													data-y="133" 
													data-speed="500"
													data-start="1850"
													data-easing="Power3.easeInOut"
													data-splitin="chars"
													data-splitout="none"
													data-elementdelay="0.1"
													data-endelementdelay="0.1"
													style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">Perlindungan
												</div>

												<!-- LAYER NR. 7 -->
												<div class="tp-caption white_heavy_40 customin tp-resizeme rs-parallaxlevel-0"
													data-x="98"
													data-y="187" 
													data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
													data-speed="500"
													data-start="2050"
													data-easing="Power3.easeInOut"
													data-splitin="none"
													data-splitout="none"
													data-elementdelay="0.1"
													data-endelementdelay="0.1"
													style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Apa
												</div>

												<!-- LAYER NR. 8 -->
												<div class="tp-caption grey_regular_18 customin tp-resizeme rs-parallaxlevel-0"
												data-x="78"
												data-y="318"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="500"
												data-start="2600"
												data-easing="Power3.easeInOut"
												data-splitin="none"
												data-splitout="none"
												data-elementdelay="0.05"
												data-endelementdelay="0.1"
												style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
													<div style="text-align:center;  font-family:  Arial; font-weight: bold; font-size: 17px">
														Kereta  anda    adalah  aset yang   amat  
														<br/> 
														penting dalam  kehidupan seharian.
														<br/>
														Anda   terpaksa   membelanjakan   
														<br/>
														beribu - ribu  ringgit dan
														<br />
														mengenepikan   keperluan  yang lain 
														<br/>
														untuk   memiliki   nya.
														<br /><br />
														Kami menawarkan perlindungan 
														<br>
														kerosakan dari kemalangan, kebakaran
														<br>
														dan kecurian  termasuk tanggungan 
														<br>
														kepada  pihak  ketiga  seperti 
														<br>
														kecederaan atau kematian dan kerosakan 
														<br>
														kepada kereta mereka.
													</div>
												</div>

												<!-- LAYER NR. 9 -->
												<div class="tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0"
												data-x="58"
												data-y="238"
												data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
												data-speed="500"
												data-start="2350"
												data-easing="Back.easeOut"
												data-splitin="none"
												data-splitout="none"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">yang anda perlukan?
													
												</div>

												<!-- LAYER NR. 10 -->
												<div class="tp-caption customin rs-parallaxlevel-0"
												data-x="6"
												data-y="290"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="300"
												data-start="2500"
												data-easing="Power3.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												style="z-index: 11;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/greyline.png">
												</div>

												<!-- LAYER NR. 11 -->
												<div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
												data-x="73"
												data-y="502"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="500"
												data-start="2900"
												data-easing="Power3.easeInOut"
												data-splitin="none"
												data-splitout="none"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-linktoslide="next"
												style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;">
													
												</div>

												<!-- LAYER NR. 12 -->
												<div class="tp-caption arrowicon customin rs-parallaxlevel-0"
												data-x="303"
												data-y="526"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="300"
												data-start="3200"
												data-easing="Power3.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-linktoslide="next"
												style="z-index: 13;">
													<div class=" rs-slideloop" 			data-easing="Power3.easeInOut"
													data-speed="0.5"
													data-xs="-5"
													data-xe="5"
													data-ys="0"
													data-ye="0"
													>
														<img src="img/slider/dummy.png" alt="" data-ww="18" data-hh="11" data-lazyload="img/slider/doublearrow2.png">
													</div>
												</div>
											</li>
											<!-- SLIDE  -->
											<li data-transition="slideleft" data-slotamount="7" data-masterspeed="2000" data-thumb="img/slider/banner11.jpg" data-delay="10000"  data-saveperformance="on"  data-title="Benefit">
												<!-- MAIN IMAGE -->
											<!-- LAYER NR. 1 -->
													<div class="tp-caption lfb rs-parallaxlevel-9"
											data-x="center" data-hoffset="-10"
											data-y="bottom" data-voffset="-20"
											data-speed="1500"
											data-start="2400"
											data-easing="Power4.easeInOut"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											data-endspeed="300"
											style="z-index: 2;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/banner11.jpg" style="width: 1242px; height: 600px;">
											</div>

												<!-- LAYERS -->

											

												<!-- LAYER NR. 13 
												<div class="tp-caption arrowicon customin fadeout rs-parallaxlevel-10"
												data-x="880"
												data-y="454"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="300"
												data-start="5200"
												data-easing="Power3.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="300"
												style="z-index: 14;"><img src="img/slider/dummy.png" alt="" data-ww="17" data-hh="17" data-lazyload="img/slider/check.png">
												</div>-->
											</li>
											<li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="img/slider/slide3-prev.jpg"  data-saveperformance="on"  data-title="Coop Integration">
											<img src="img/slider/dummy.png"  alt="slidebg1" data-lazyload="img/slider/coop-friendly.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
											<!-- LAYERS -->

											<!-- LAYER NR. 1 -->
											<div class="tp-caption lfb rs-parallaxlevel-9"
											data-x="center" data-hoffset="-40"
											data-y="bottom" data-voffset="-10"
											data-speed="1500"
											data-start="2400"
											data-easing="Power4.easeInOut"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											data-endspeed="300"
											style="z-index: 2;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/coop-checklist.png">
											</div>

											<!-- LAYER NR. 2 -->
											<div class="tp-caption customin rs-parallaxlevel-1"
											data-x="515"
											data-y="331"
											data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
											data-speed="500"
											data-start="4400"
											data-easing="Power4.easeInOut"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											data-endspeed="300"
											style="z-index: 3;">
												<div class=" rs-pulse" 			data-easing="Power4.easeInOut"
												data-speed="0.5"
												data-zoomstart="0.75"
												data-zoomend="1"
												>
													<img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/pulse1.png">
												</div>
											</div>

												<!-- LAYER NR. 4 -->
												<div class="tp-caption lfb rs-parallaxlevel-9"
												data-x="693"
												data-y="191"
												data-speed="1500"
												data-start="2900"
												data-easing="Power4.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="300"
												style="z-index: 5;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/iphone.png">
												</div>

												<!-- LAYER NR. 5 -->
												<!--<div class="tp-caption black_heavy_70 customin randomrotateout tp-resizeme rs-parallaxlevel-5"
												data-x="315"
												data-y="40"
												data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
												data-speed="500"
												data-start="1400"
												data-easing="Power3.easeInOut"
												data-splitin="chars"
												data-splitout="none"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="600"
												style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
													
												</div>-->

												<!-- LAYER NR. 6 -->
												<div class="tp-caption customin randomrotateout rs-parallaxlevel-7"
												data-x="434"
												data-y="98"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="300"
												data-start="1900"
												data-easing="Power3.easeInOut"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="600"
												style="z-index: 7;"><img src="img/slider/dummy.png" alt="" data-lazyload="img/slider/largegreen.png">
												</div>

												<!-- LAYER NR. 7 -->
												<div class="tp-caption light_heavy_70 customin randomrotateout tp-resizeme rs-parallaxlevel-7"
												data-x="448"
												data-y="106"
												data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
												data-speed="300"
												data-start="2200"
												data-easing="Power3.easeInOut"
												data-splitin="none"
												data-splitout="none"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="600"
												style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">
													ICOP
												</div>

												<!-- LAYER NR. 8 -->
												<div class="tp-caption black_bold_40 skewfromrightshort randomrotateout tp-resizeme rs-parallaxlevel-6"
												data-x="619"
												data-y="177"
												data-speed="500"
												data-start="2500"
												data-easing="Power3.easeInOut"
												data-splitin="chars"
												data-splitout="none"
												data-elementdelay="0.1"
												data-endelementdelay="0.1"
												data-endspeed="600"
												style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
													MESRA PENGGUNA
												</div>
											</li>
										</ul>
										<div class="tp-bannertimer"></div>
									</div>
								</div>

							</div>
						</section>
						<!-- /Section: INTRO -->

					<section id="product" class="home-section text-center">
						<div class="heading-about marginbot-10">
							<div class="container">
								<div class="row">
									<div class="col-lg-8 col-lg-offset-2">
										<div class="section-heading" class="upper">
											<h2>PRODUK KAMI</h2>
											<h3><strong><span style="color: #c82506">Perlindungan</span> <span style="color: #57aaf9">apa yang anda</span> <span style="color: #00882b">perlukan?</span></strong></h3>
											<p style="color: #57aaf9">
												<strong >Pelbagai produk yang ditawarkan! Pakej dengan ciri ciri yang anda perlukan!</strong>
											</p>
										</div>
									</div>
								</div>
							</div><!-- /.container -->
						</div> <!-- /.heading-about -->

						<div class="container">

							<div class="row">
								
								<div class="col-xs-12 col-sm-6 col-md-4" class="upper">
									<div class="panel panel-success pricing-big">
										<div class="panel-body">
											<a href="{{URL::to('daftar')}}"><img src="{{ url('/img/car.png')}}" alt="" width="40%"></a>
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Kereta</h3>
											<h4>
												<a href="{{URL::to('daftar')}}"><strong>Mohon Sekarang</strong></a>
											</h4>
										</div>
									</div>
								</div>
								<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
							<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
							<div>
								<h2 id="modal1Title" style="  font-family:  Arial; font-weight: bold; font-size: 20px" >INSURAN KERETA</h2>
								<p id="modal1Desc"  style="font-family:  Arial; font-weight: bold; font-size: 20px">
					Sila Pilih Jenis Perlindungan
					</p>
				</div>
				<br>
				<!--<a href="{{URL::to('daftar')}}"><button class="remodal-confirm"><b style="  font-family:  Arial; font-weight: bold; font-size: 15px">Komprehensif</b></button></a>
				<a href="{{url('/pihak_ketiga')}}"><button class="remodal-cancel"><b style="  font-family:  Arial; font-weight: bold; font-size: 15px">Pihak Ketiga</b></button></a>-->
				</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="panel panel-danger pricing-big">
										<div class="panel-body">
											<img src="{{ url('/img/home.png')}}" alt="" width="40%">
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Kebakaran dan Banjir</h3>
											<h4 class="text-danger"><strong>Akan Datang</strong></h4>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="panel panel-danger pricing-big">
										<div class="panel-body">
											<img src="{{ url('/img/theft.png')}}" alt="" width="40%">
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Isi Rumah</h3>
											<h4 class="text-danger"><strong>Akan Datang</strong></h4>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="panel panel-danger pricing-big">
										<div class="panel-body">
											<img src="{{ asset('frontend/images/incident.png')}}" alt="" width="40%">
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Kemalangan Diri</h3>
											<h4 class="text-danger"><strong>Akan Datang</strong></h4>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="panel panel-danger pricing-big">
										<div class="panel-body">
											<img src="{{ asset('frontend/images/travel.png')}}" alt="" width="40%">
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Perjalanan</h3>
											<h4 class="text-danger"><strong>Akan Datang</strong></h4>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="panel panel-danger pricing-big">
										<div class="panel-body">
											<img src="{{ asset('frontend/images/medical.png')}}" alt="" width="40%">
										</div>
										<div class="panel-footer text-align-center">
											<h3 class="panel-title">Perubatan</h3>
											<h4 class="text-danger"><strong>Akan Datang</strong></h4>
										</div>
									</div>
								</div>

							</div>

						</div>
					</section><!-- /#product -->

					<!--<section id="premi-calc" class="home-section text-center bg-gray">
						<div class="heading-about marginbot-10" class="upper">
							<div class="container">
								<div class="row">
									<div class="col-lg-8 col-lg-offset-2">
										<div class="section-heading">
											<h2>Kalkulator Premium</h2>
										</div>
										<div class="team boxed-grey">
											<p>
												<strong>Mengira premium atau jumlah diinsuranskan dan menyediakan ilustrasi beneffit. </ Strong>. Menggunakan rangkaian kami alat interaktif dan kalkulator premium insurans hayat untuk merancang matlamat kewangan anda. Alat ini akan membantu dalam mengukur matlamat anda.
											</p>
											<p>
												<a href="{{URL::to('page/calculator')}}">
													<span class="label label-danger"><strong>Mulai</strong></span>
												</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>-->

					<section id="promotion" class="home-section text-center">
						<div class="heading-about marginbot-10" class="upper">
							<div class="container">
								<div class="row">
									<div class="col-lg-8 col-lg-offset-2">
										<div class="section-heading">
											<h2>Promosi</h2>
											<p>
												<b>Kami tidak mempunyai apa-apa promosi yang aktif lagi. Melanggan dengan kami untuk memastikan anda adalah yang pertama dimaklumkan dengan promosi hebat kami.</b>
											</p>
											<p>
												@if (Session::has('message'))
					

					<div class="alert adjusted alert-info fade in">
					<button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa-lg fa fa-exclamation"></i>
					<strong>{{ Session::get('message') }}</strong> 
					</div>
							
							@endif     
												<form class="form-horizontal" action="{{ url('/promotion')}}" method="POST">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<div class="row">
														<fieldset>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="input-group">
																	<input type="email" class="form-control" placeholder="Email" required="">
																	<div class="input-group-btn">
																		<button id="subscribe" class="btn btn-sm btn-success" type="submit"><strong>Melanggan email</strong></button>
																	</div>
																</div>
															</div>
														</fieldset>
													</div>
												</form>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="container">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="service-box">
										<h1><strong>Content not available yet</strong></h1>
									</div>
								</div>
							</div>
						</div>-->
					</section><!-- /#promotion -->

					<section id="contact-us" class="home-section text-center bg-gray">
						<div class="heading-contact marginbot-10" class="upper">
							<div class="container">
								<div class="row">
									<div class="col-lg-8 col-lg-offset-2">
										<div class="section-heading">
											<h2>Hubungi Kami</h2>
											<p>
												<span align="justify">
													<b>Kami beroperasi dan memberi perkhidmatan berdasarkan kepercayaan. Ini akan menjadi kenyataan melalui sokongan dan pengalaman anda berurus niaga dengan kami. Setiap komen anda sama ada positif atau pun negatif akan diberi perhatian sepenuhnya demi meningkatkan perkhidmatan kami.</b>
												</span>
											</p>
											<p>
												<b>Anda mempunyai apa-apa soalan atau bantuan?<br>
												<span align="justify">
													Pasukan pengurusan kami mempunyai kepakaran dan pengalaman untuk memastikan yang anda menerima maklumat yang anda perlukan, daripada soalan lazim atau pun soalan yang kompleks. Sila mendaftar maklumat anda untuk dihubungi.</b>
												</span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-md-offset-2" class="upper">
									<div class="boxed-grey">
										<form method="POST"  id="contact_form" class="smart-form client-form" action="{{ url('/contactus')}}" >
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="name"> Nama</label>
														<input type="text" class="form-control" id="name" placeholder="Nama" required="required" name="name"/>
													</div>
													<div class="form-group">
														<label for="email"> Alamat email</label>
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span> </span>
															<input type="email" class="form-control" id="email" placeholder="Alamat email" required="required" name="email" />
														</div>
													</div>
													<div class="form-group">
														<label for="subject"> Perkara</label>
														<select id="subject" name="perkara" class="form-control" required="required">
															<option value="">Sila pilih:</option>
															<option value="service">Perkhidmatan Pelanggan</option>
															<option value="suggestions">Cadangan</option>
															<option value="product">Sokongan Produk</option>
														</select>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="name"> Mesej</label>
														<textarea name="mesej" id="message" class="form-control" rows="9" cols="25" required="required" placeholder="Mesej"></textarea >
													</div>
												</div>
												<div class="col-md-12">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<button type="submit" class="btn btn-skin pull-right" id="btnContactUs">Hantar Mesej</button>
												</div>
											</div>
										</form>
									</div>

									<div class="widget-contact row" class="upper">
										<div class="col-lg-6">
											<address style="text-align:justify;  font-family:  Arial; font-weight: bold; font-size: 14px">
												<strong>icopangkasa.com.my<strong>
												<br>
												No. 22-3 D Wangsa Block,
												<br>
												3A Jalan Wangsa Delima 10, 
												<br>
												53300, Kuala Lumpur
												<br>
												<abbr title="Phone">Telefon:</abbr> +(603) 4142 2475
											</address>
										</div>

										<div class="col-lg-6">
											<address style="text-align:justify;  font-family:  Arial; font-weight: bold; font-size: 14px">
												<strong>E-mel</strong>
												<br>
												<!--<a href="mailto:#">icopangkasa@gmail.com</a>
												<br>
												<a href="mailto:#">info@icopangkasa.com.my</a>
												<br />-->
												<a href="mailto:#" style="color: blue;font-style: underline">support@icopangkasa.com.my</a>
											</address>

										</div>
									</div>
								</div>

							</div>

						</div>
					</section><!-- /#contact-us -->

					<div class="bottom-content" class="upper">
						<div class="container custom-container text-center">
							<h2>Kami sentiasa memberi anda keselesaan dan yang terbaik untuk memohon perlindungan insuran.</h2>
							<h3><strong>Terima kasih kerana mempercayai kami</strong></h3>
						</div>
					</div> <!-- /#bottom-content -->
				@stop

				@section('page-script')
					@javascripts('nivo')
					@javascripts('themepunch')
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery('.overlay').removeClass('open');
							jQuery('#motor-insurance-apply, #motor-insurance-close, #motor-compre').click(function() {
								jQuery('.overlay').toggleClass('open');
							});

							jQuery('.tp-banner').show().revolution({
								dottedOverlay : "none",
								delay : 8000,
								startwidth : 1170,
								startheight : 700,
								hideThumbs : 200,

								thumbWidth : 100,
								thumbHeight : 50,
								thumbAmount : 5,

								navigationType : "bullet",
								navigationArrows : "solo",
								navigationStyle : "preview4",

								touchenabled : "on",
								onHoverStop : "off",

								swipe_velocity : 0.7,
								swipe_min_touches : 1,
								swipe_max_touches : 1,
								drag_block_vertical : false,

								parallax : "mouse",
								parallaxBgFreeze : "on",
								parallaxLevels : [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

								keyboardNavigation : "off",

								navigationHAlign : "center",
								navigationVAlign : "bottom",
								navigationHOffset : 0,
								navigationVOffset : 20,

								soloArrowLeftHalign : "left",
								soloArrowLeftValign : "center",
								soloArrowLeftHOffset : 20,
								soloArrowLeftVOffset : 0,

								soloArrowRightHalign : "right",
								soloArrowRightValign : "center",
								soloArrowRightHOffset : 20,
								soloArrowRightVOffset : 0,

								shadow : 0,
								fullWidth : "off",
								fullScreen : "on",

								spinner : "spinner4",

								stopLoop : "off",
								stopAfterLoops : -1,
								stopAtSlide : -1,

								shuffle : "off",

								autoHeight : "off",
								forceFullWidth : "off",

								hideThumbsOnMobile : "off",
								hideNavDelayOnMobile : 1500,
								hideBulletsOnMobile : "off",
								hideArrowsOnMobile : "off",
								hideThumbsUnderResolution : 0,

								hideSliderAtLimit : 0,
								hideCaptionAtLimit : 0,
								hideAllCaptionAtLilmit : 0,
								startWithSlide : 0,
								fullScreenOffsetContainer : ""
							});

							//nivo lightbox
							$('.gallery-item a').nivoLightbox({
								effect: 'fadeScale',                             // The effect to use when showing the lightbox
								theme: 'default',                           // The lightbox theme to use
								keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
								clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
								onInit: function(){},                       // Callback when lightbox has loaded
								beforeShowLightbox: function(){},           // Callback before the lightbox is shown
								afterShowLightbox: function(lightbox){},    // Callback after the lightbox is shown
								beforeHideLightbox: function(){},           // Callback before the lightbox is hidden
								afterHideLightbox: function(){},            // Callback after the lightbox is hidden
								onPrev: function(element){},                // Callback when the lightbox gallery goes to previous item
								onNext: function(element){},                // Callback when the lightbox gallery goes to next item
								errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
							});

						});
						$.scrollUp();
					</script>

					<script>
				// window.REMODAL_GLOBALS = {
				//   NAMESPACE: 'remodal',
				//   DEFAULTS: {
				//     hashTracking: true,
				//     closeOnConfirm: true,
				//     closeOnCancel: true,
				//     closeOnEscape: true,
				//     closeOnOutsideClick: true,
				//     modifier: ''
				//   }
				// };
				</script>




				<script src="{{ asset('front/dist/remodal.js')}}"></script>
				<!-- Events -->
				<script>
				$(document).on('opening', '.remodal', function () {
					console.log('opening');
				});

				$(document).on('opened', '.remodal', function () {
					console.log('opened');
				});

				$(document).on('closing', '.remodal', function (e) {
					console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
				});

				$(document).on('closed', '.remodal', function (e) {
					console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
				});

				$(document).on('confirmation', '.remodal', function () {
					console.log('confirmation');
				});

				$(document).on('cancellation', '.remodal', function () {
					console.log('cancellation');
				});

				//  Usage:
				//  $(function() {
				//
				//    // In this case the initialization function returns the already created instance
				//    var inst = $('[data-remodal-id=modal]').remodal();
				//
				//    inst.open();
				//    inst.close();
				//    inst.getState();
				//    inst.destroy();
				//  });

				//  The second way to initialize:
				$('[data-remodal-id=modal2]').remodal({
					modifier: 'with-red-theme'
				});
				</script>

				@stop


		</div> <!-- End Content -->

	
		
		<footer>
			<div class="icop footer-content clearfix">
				<div class="container custom-container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12">
							<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Syarikat</h3>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/about-us')}}">Mengenai Kami</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/faq')}}">Soalan Lazim</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/contact-us')}}">Hubungi Kami</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/tnc')}}">Terma dan Syarat</a></p>
							</div>
							<div class="fb-share-button" data-href="https://www.icopangkasa.com.my/public/daftar" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.icopangkasa.com.my%2Fpublic%2Fdaftar&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12">
							<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Produk</h3>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/product/motor')}}">Kereta</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Kebakaran dan Banjir</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Isi Rumah</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Kemalangan Diri</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Perjalanan</a></p>
							</div>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Perubatan</a></p>
							</div>
						</div>
						<!--@if (!(Auth::user()))
						<div class="col-md-3 col-sm-6 col-xs-12">
							<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Koperasi</h3>
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('cooperative/register')}}">Pendaftaran Koperasi</a></p>
							</div>
						</div>
						@endif-->
						<div class="col-md-3 col-sm-6 col-xs-12">
							<h3 style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Peralatan</h3>
							
							<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/insurance')}}">Perbezaan Insuran</a></p>
							</div>
							<!--<div class="recent-post clearfix">
								<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; "><a href="{{ url('/page/underconstruction')}}">Mengaktifkan Lindungan</a></p>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<p style="font-family:  Helvetica; font-weight: bold; font-size: 16px; ">Icop Angkasa © {{ date('Y') }} - icopangkasa.com.my</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=48242215"></script>-->
	



		<div class="overlay overlay-hugeinc">
			<button id="motor-insurance-close" type="button" class="overlay-close">Close</button>
			<div class="container container-table">
				<div class="row vertical-center-row">
					<div class="col-lg-6 col-offset-lg-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
						<div class="widget-body">
							<form id="login-form" class="smart-form" novalidate="novalidate">
								<header>
									<h2><strong>Create new account or sign-in.</strong></h2>
									<small>If you dont have any account yet, fill in your email and password, then you are good to go</small>
								</header>
								<fieldset>
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
									</section>
									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div>
									</section>
								</fieldset>
								<footer>
									<a href="applicant/registration" class="btn btn-primary">Sign in</a>
								</footer>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>



    </div>






	<!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
   

</body>
</html>