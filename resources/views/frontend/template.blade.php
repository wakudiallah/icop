<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <link rel="icon" type="image/png" href="{{ asset('favicon/favicon.ico') }}">

  <title>{{config('insurance.system_name')}}</title>

  <!-- slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="{{ asset('front/css/bootstrap.css') }}" />
  <!-- font awesome style -->
  <link rel="stylesheet" type="text/css" href="{{ asset('front/css/font-awesome.min.css') }}" />

  <!-- Custom styles for this template -->
  <link href="{{ asset('front/css/style.css') }}" rel="stylesheet" />
  <!-- responsive style -->
  <link href="{{ asset('front/css/responsive.css') }}" rel="stylesheet" />

  
  @stack('style')

</head>

<body>
  <div class="hero_area">
    
    <!-- header section strats -->
    <header class="header_section">
      <!--
      <div class="header_top">
        <div class="container-fluid">
          <div class="contact_nav">
            <a href="">
              <i class="fa fa-phone" aria-hidden="true"></i>
              <span>
                
              </span>
            </a>
            <a href="">
              <i class="fa fa-envelope" aria-hidden="true"></i>
              <span>
                Email : demo@gmail.com
              </span>
            </a>
          </div>
        </div>
      </div>  -->


      <div class="header_bottom">
        <div class="float-right">
          
          <form action="{{route('language.switch')}}" method="post">
            @csrf
            <select name="language" onchange="this.form.submit()" id="" class="btn btn-sm">
              

              <option value="en" {{ app()->getLocale() === 'en' ? 'selected' : '' }}>
                🇬🇧 ENG
              </option>

              <option value="bm" {{ app()->getLocale() === 'bm' ? 'selected' : '' }}>
                🇲🇾 BM
            </option>
              
            </select>
          </form>
        </div>

        <div class="container-fluid">
          <nav class="navbar navbar-expand-lg custom_nav-container ">
            <a class="navbar-brand" href="{{url('/')}}">
              
              <span>
               
              <img src="{{ asset('front/images/icon.png') }}" alt="" class="img img-responsive" height="10%" width="10%">

              </span>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class=""> </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ">

                <li class="nav-item {{ request()->is('/') ? 'active' : '' }}">
                  <a class="nav-link" href="{{url('/')}}">{{__('messages.home')}} <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item {{ request()->is('about') ? 'active' : '' }}">
                  <a class="nav-link" href="{{url('about')}}"> {{__('messages.about')}}  </a>
                </li>

                <li class="nav-item {{ request()->is('roadtax-lkm') ? 'active' : '' }}">
                  <a class="nav-link" href="{{url('roadtax-lkm')}}"> {{__('messages.calculate_roadtax')}} </a>
                </li>

               
                
                <!-- <li class="nav-item">
                  <a class="btn btn-primary nav-link" href="{{-- url('/login') --}}" style="color: #ffff"><b>  {{__('messages.login')}} </b></a>
                </li> -->

              

              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>
    <!-- end header section -->
  </div>



  @yield('main-content')
  


  <!-- footer section -->
  <footer class="footer_section">
    <div class="container">
      <p>
        &copy; <span id="displayDateYear"></span> All Rights Reserved By
        Masamart
      </p>
    </div>
  </footer>
  <!-- footer section -->



  <script src="{{ asset('front/js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ asset('front/js/bootstrap.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <script src="{{ asset('front/js/custom.js') }}"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap"></script>
  <!-- End Google Map -->

  @stack('scripts')


</body>

</html>