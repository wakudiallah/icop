<?php

return [
    
    'insurance' => 'Insuran',
    'language' => 'Bahasa Melayu',

    'home' => 'Laman Utama',
    'about' => 'Mengenai Kami',
    'login' => 'Masuk',
    'calculate_roadtax' => 'Kira Roadtax',


    'car_insurance' => 'Insuran Kereta',
    'motor_insurance' => 'Insuran Motor',

    'moto' => 'Perlindungan Takaful Kenderaan dengan Pembiayaan pada kadar 0%',
    'hash' => 'Dapatkan tawaran sebutharga Anda sekarang!',


    /* Part System */
    'welcome' => 'Selamat Datang ke ICOP', 
    'vehicle' => 'Nombor Pendaftaran Kenderaan',
    'mykad' => 'Nombor MyKad',
    'postcode' => 'Poskod', 


    'maklumat_kenderaan' => 'Maklumat Kendaraan',
    'nombor_pendaftaran_kenderaan' => 'Nombor Pendaftaran Kenderaan',
    'nombor_chasis' => 'Nombor Chasis',
    'nombor_enjin' => 'Nombor Enjin',
    'tahun_pembuatan' => 'Tahun Pembuatan',
    'brand' => 'Brand',
    'model' => 'Model',
    'varian' =>  'Variant',
    'series' => 'Series',
    'ncd' => 'NCD',
    'kapasiti_kenderaan' => 'Kapasiti Kenderaan',


    'nama_penuh' => 'Nama Penuh',
    'email' => 'E-mel',
    'phone' => 'Telepon Bimbit',
    'alamat1' => 'Alamat (Baris 1)',
    'alamat2' => 'Alamat (Baris 2)',
    'marital_status' => 'Status Perkahwinan',
    'poskod_lokasi' => 'Poskod (Lokasi Kenderaan)',
    'poskod' => 'Poskod',
    'negri' => 'Negri',
    'region' => 'Wilayah',

    'saya_akui_bahawa' => 'Saya akui bahawa maklumat di atas adalah benar dan tepat',
    'saya_bersetuju' => 'Saya bersetuju dengan ',
    'terma_dan_syarat' => 'Terma dan Syarat',


    'insuran_kenderaan' => 'Insuran Kenderaan', 

    'perlindungan_tambahan' => 'Perlindungan Tambahan',

    'mode_pembayaran' => 'Payment Mode',
    'online_payment' => 'Online Payment',
    'pembiayaan' => 'Pembiayaan',


     /* Slect Insurance */
     'basic_premium' => 'Premium Asas',
     'sst' => 'SST',
     'duti_setem' => 'Duti Setem',
     'gross_premium' => 'Premium Kotor',
     'premium_perlu_dibayar' => 'Jumlah Perlu Dibayar',
     'rebate' => 'Rebat',
    'add_on' => 'Tambahan',
    'require_image' => 'Perlindungan insurance mungkin memerlukan gambar kendaran.',


     /* Additional Protection */
    'create_new_account' => 'Buat Akaun Baharu',
    'password' => 'Kata Laluan',
    'confirm_password' => 'Sahkan Kata Laluan',

    'ya' => 'Ya',
    'tidak' => 'Tidak',

    'perlindungan_cermin' => 'Perlindungan Cermin Depan/Belakang',
    'pemandu_tambahan' => 'Pemandu Tambahan',
    'perlindungan_bencana_khas' => 'Perlindungan Tambahan Bencana Khas (termasuk Banjir)',
    'perlindungan_kemalangan_pribadi' => 'Perlindungan Kemalangan Peribadi (PA)',
    'roadtax' => 'Roadtax', 

    'jenis_perlindungan' => 'Jenis Perlindungan',
    'iagree_blacklist' => 'Saya mengesankan saya tidak diseranai hitam oleh JPJ/PDRM',
    


    /* Chose Monthly installment */
    'pembayaran_pembiayaan' => 'Pembayaran Pembiayaan',
    'note_download_bpa' => 'Sila muat turun borang BPA 1/79, isi dan kemudian muat naik borang BPA 1/79',
    'note_schema' => 'Jadual bayaran di atas adalah untuk tujuan ilustrasi sahaja.',


    'bulan' => 'Bulan',

    'skim_potongan' => 'Skim Potongan', 
    'jumlah_pembiayaan' => 'Jumlah Pembiayaan',
    'angsur_bulanan' => 'Angsur Bulanan',
    'pilih' => 'Pilih', 


    'pay_slip' => 'Slip Gaji',
    'BPA1_79' => 'BPA 1/79',


     /* Welcoming */
     'reg_no' => 'Nombor Register',
     'status' => 'Status Sekarang', 
     'aksi' => 'Action',
     'activity' => 'Activities',
     'greeting_selamat_datang' => 'Selamat Datang',



 
    /* Button */
    'continue' => 'Seterusnya',

    ];

    /*
    {{__('messages.')}}
    */