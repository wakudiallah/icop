<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Icop Data
    |--------------------------------------------------------------------------
    */


    'namapenuh' => 'as per IC',
    'emailaddress' => 'email address',
    'address1' => 'address (first line)',
    'address2' => 'address (second line)',



    'fa' => 'Financial Advisor',
    'branch' => 'Branch',
    'salutation' => 'SALUTATION',
    'name' => 'FULL NAME',
    'ic' => 'NRIC',
    'handphone' => 'MOBILE NUMBER',
    'emp' => 'EMPLOYMENT CATEGORY',
    'employer' => 'EMPLOYER NAME',
    'income' => 'MONTHLY INCOME (RM)',
    'deduction' => 'TOTAL MONTHLY DEDUCTION (RM)',
    'package' => 'PERSONAL FINANCING-i PACKAGE',
    'financing' => 'FINANCING AMOUNT (RM)',
    'hello' => 'Assalamualaikum and Hello!',
    'welcome' => "Welcome to MBSB Bank's Online Application Form",
    'easy' => "Now it's easy to apply for a Personal Financing-i",
    'ref' => 'REFERENCE NUMBER',
    'branch' => 'PREFERRED BRANCH',
    'status' => 'STATUS',
    'history' => 'HISTORY',
    'submit' => 'SUBMITTED DATE',
    'no' => 'NO.',
    'assign_to' => 'ASSIGN TO',
    'assign_by' => 'ASSIGN BY',
    'action' => 'ACTION',
    'download' => 'DOWNLOAD',
    'tat' => 'TAT',
    'status_dca' => 'Status DCA',
    'status_waps' => 'Status WAPS',
    'tooltip_name' => 'as per NRIC',
    'tooltip_vehicle' => 'as per your Vehicle Number',
    'tooltip_ic' => "(Please key in without '-' e.g 831110901112)",
    'tooltip_handphone' => "(Please key in without '-' e.g 014xxxxxx)",
    'tooltip_homephone' => "(Please key in without '-' e.g 03xxxxxx)",
    'tooltip_fax' => "(Please key in without '-' e.g 03xxxxxx)",
    'tooltip_income' => '(Please key in Total Basic Salary + Allowance + Other income)',
    'tooltip_deduction' => '(Please key in Total Deduction in Pay Slip Only)',
    'tooltip_upload' => 'Only pdf,png,jpeg,jpg formats are allowed - Maximum file is 5 MB',
    'state' => 'STATE',
    'select_state' => '--SELECT STATE--',
    'select_branch' => '--SELECT PREFERRED BRANCH--',
    'branch' => 'PREFERRED BRANCH',
    'tenure' => 'TENURE',
    'installment'=>'MAXIMUM INSTALLMENT (RM)',
    'total_income'=>'TOTAL INCOME (RM)',
    'max_financing'=>'FINANCING AMOUNT UP TO (RM)',
    'monthly_installment'=>'MONTHLY INSTALLMENT',
    'choose'=>'CHOOSE',
    'financing_amount'=>'FINANCING AMOUNT',
    'category' => 'CATEGORY',
    'package_r' => 'PACKAGE',
    'generate'=>'GENERATE',
    'latest_payslip'=>'LATEST PAYSLIP',
    'status_1'=>'',
    'status_2'=>'PENDING ASSIGNED',
    'status_3'=>'PENDING DOCUMENTS VERIFICATION',
    'status_4'=>'CUSTOMER READY FILL UP FORM',
    'status_5'=>'PENDING VERIFICATION',
    'status_6'=>'SUMBITTED TO WAPS',
    'status_7'=>'FINANCING AMOUNT',
    'status_99'=>'DOCUMENTS REJECTED BY FA',
    'status_8'=>'REJECTED',

    'glan'=>'PENDING WAPS',
    'accp'=>'ACCEPTED-WAPS',
    'decl'=>'DECLINE-WAPS',
    'apva'=>'APPROVED WITH VARIATION',
    'rejm'=>'REJECTED-WAPS',

    'rejection_remark'=>'REJECTION REMARK',
    'submitted_waps'=>'SUBMITTED TO WAPS',
    'assigned_date'=>'ASSIGNED DATE',
    'date_reassign'=>'DATE ASSIGN / REASSIGN',
    'ref_code'=>'REFERRAL CODE',
    'survey'=>'SURVEY DATA',
    'branch_fa'=>'BRANCH (FA)',
    'total_tat'=>'TOTAL TAT COUNT',
    'app_date'=>'APPLICATION DATE',
    'app_time'=>'APPLICATION TIME',
    'referral_flag'=>'REFERRAL FLAG',
    'flag_direct'=>'DIRECT CUSTOMER',
    'flag_referral'=>'REFERRAL CUSTOMER',

    'survey_question'=>'WHERE DO YOU KNOW ABOUT MFAST?',
    'ref_name'=>'REFERRAL NAME',
    'ref_code'=>'REFERRAL CODE',
    'gender'=>'GENDER',
    'reg_number'=>'MBSB Bank Berhad (716122-P).',
    'reserved'=>'All Rights Reserved.',

    'referral'     => [
        'title'         => 'Please',
        'slide'         => '',
        'nonIndividual'      => [
            'type'         => 'Are You',
            'comp_name'    => 'Company Name',
            'reg_number'   => 'Company Registration No',
            'cp'           => 'Contact Person',
            'desc'         => 'Non Individual'
        ],
        'individual'         => [
            'comp_name'    => 'Company Name',
            'reg_number'   => 'Company Registration No',
            'cp'           => 'Contact Person',
            'desc'         => 'Individual'
        ],
        'fa'               => [
            'id'           => 'FA - Staff ID',
            'name'         => 'FA - Staff Name',
        ],
        'staff'            => [
            'type'         => "Are you MBSB Bank's Staff?",
            'id'           => 'Staff ID',
            'div'          => 'Division',
            'dept'         => 'Department',
            'designation'  => 'designation',
            'email'        => 'Office Email Address',
        ],
        'mobile'           => 'Contact Number (Mobile)',
        'office'           => 'Contact Number (Office)',
        'account_no'       => 'MBSB Account Number',

    ],
    'tooltip_ref' => '- Please enter your referer code.<br>
- Combination of letters and numbers',

    'email'       => 'Email Address',

];
