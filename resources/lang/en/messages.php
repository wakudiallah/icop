<?php

return [
    
    'insurance' => 'Insurance',
    'language' => 'English',

    'home' => 'Home',
    'about' => 'About',
    'login' => 'Login',
    'calculate_roadtax' => 'Calculate Roadtax',

    'car_insurance' => 'Car Insurance',
    'motor_insurance' => 'Motor Insurance',

    'moto' => 'Vehicle Takaful Coverage with 0% Financing',
    'hash' => 'Get your quote now!',


    /* Part System */
    'welcome' => 'Welcome to ICOP',

    'vehicle' => 'Vehicle Number', 
    'mykad' => 'NRIC',
    'postcode' => 'Postcode', 


    'maklumat_kenderaan' => 'Vehicle Information',
    'nombor_pendaftaran_kenderaan' => 'Vehicle Registration Number',
    'nombor_chasis' => 'Chassis Number',
    'nombor_enjin' => 'Engine Number',
    'tahun_pembuatan' => 'Year Of Manufacture',
    'brand' => 'Brand',
    'model' => 'Model',
    'varian' =>  'Variant',
    'series' => 'Series',
    'ncd' => 'NCD',
    'kapasiti_kenderaan' => 'Vehicle Capacity',

    'nama_penuh' => 'Full Name',
    'email' => 'E-mail',
    'phone' => 'Phone',
    'alamat1' => 'Address (Line 1)',
    'alamat2' => 'Address (Line 2)',
    'marital_status' => 'Marital Status',
    'poskod_lokasi' => 'Postcode (Vehicle Location)',
    'poskod' => 'Postcode',
    'negri' => 'State',
    'region' => 'Region',
    

    'saya_akui_bahawa' => 'I admit that the information above is true and accurate',
    'saya_bersetuju' => 'I agree to the ',
    'terma_dan_syarat' => 'Terms and Conditions',

    
    'insuran_kenderaan' => 'Vehicle Insurance', 

    'perlindungan_tambahan' => 'Additional Protection',

    'mode_pembayaran' => 'Payment Mode',
    'online_payment' => 'Online Payment',
    'pembiayaan' => 'Financing',



    'pembayaran_pembiayaan' => 'Personal Financing',
    'skim_potongan' => 'Deduction Scheme', 
    'jumlah_pembiayaan' => 'Total Financing',
    'angsur_bulanan' => 'Monthly Installments',
    'pilih' => 'Choose', 


    /* Slect Insurance */
    'basic_premium' => 'Basic Contribution',
    'sst' => 'SST',
    'duti_setem' => 'Stamp Duty',
    'gross_premium' => 'Gross Premium',
    'rebate' => 'Rebate',
    'premium_perlu_dibayar' => 'Total Payable',
    'add_on' => 'Add On',

    'require_image' => 'Insurance coverage may require vehicle images.',


    /* Additional Protection */
    'create_new_account' => 'Create New Account',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',

    'inclusion_of_special_perils' => 'Inclusion of Special Perils',
    'windscreens' => 'Cover For Windscreens, Windows and Sunroof',
    'riot' => 'Strike Riot & Civil Commotion',
    'legal_of_passenger' => 'Legal Liability Of Passengers For Negligence Coverage',
    'legal_to_passenger' => 'Legal Liability to Passengers',
    'pa' => 'PA Plus',
    'all_driver' => 'All Driver',
    'towing' => 'Towing and Cleaning Due To Water Demage',
    'cart' => 'CART',
    'roadtax' => 'Roadtax',

    'register_no' => 'Register Number',
    'iagree_blacklist' => 'I am impressed that I am not blacklisted by JPJ/PDRM',

    

    'ya' => 'Yes',
    'tidak' => 'No',
    
    'perlindungan_cermin' => 'Windscreen Protection Front/Rear ',
    'pemandu_tambahan' => 'Additional Drivers',
    'perlindungan_bencana_khas' => 'Additional Special Disaster Cover (including Flood)',
    'perlindungan_kemalangan_pribadi' => 'Personal Accident Cover (PA)',
    'roadtax' => 'Roadtax', 

    'jenis_perlindungan' => 'Types of Protection',
    

    /* Chose Monthly installment */
    'pembayaran_pembiayaan' => 'Financing Payments',
    'note_download_bpa' => 'Please download the BPA 1/79 form, fill it in and then upload the BPA 1/79 form',
    'note_schema' => 'Payment tables above are for illustration purposes only',

    'bulan' => 'Month',

    'pay_slip' => 'Pay Slip',
    'BPA1_79' => 'BPA 1/79',


    /* Welcoming */
    'reg_no' => 'Register Number',
    'status' => 'Current Status', 
    'aksi' => 'Action',
    'submitted_at' => 'Submitted at',  //
    'activity' => 'Activities',
    'greeting_selamat_datang' => 'Welcome',
    



    /* Button */
    'continue' => 'Continue',



    ];