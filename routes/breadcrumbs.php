<?php

use App\Models\Praapplication;
use App\Models\ParamTypeInsurance;
use BabDev\Breadcrumbs\Contracts\BreadcrumbsGenerator;
use BabDev\Breadcrumbs\Contracts\BreadcrumbsManager;
use Illuminate\Support\Facades\Route;

/*
 * You may use either the Breadcrumbs facade or the $breadcrumbs variable in this file,
 * both of which reference the `BreadcrumbsManager` service
 */

/** @var $breadcrumbs BreadcrumbsManager */



// Home
Breadcrumbs::for('home', static function (BreadcrumbsGenerator $trail): void {
    $trail->push('Dashboard', route('home.index'));
});




// Home > About
Breadcrumbs::for('pra-application', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Pra Application', route('pra-apps.index'));
});


Breadcrumbs::for('pra-smsakad', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Application SMS Akad', route('app-smsakad.index'));
});


// Home > Param Branch
Breadcrumbs::for('param-branch', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Branch', route('param-branch.index'));
});

// Home > Param Branch > [Create]
Breadcrumbs::for('param-branch-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-branch');
    $trail->push('Create', route('param-branch.create'));
});
Breadcrumbs::for('param-branch-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-branch');
    $trail->push('Edit');
});



Breadcrumbs::for('param-employment-status', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Employment Status', route('param-employment-status.index'));
});

// Home > Param Branch > [Create]
Breadcrumbs::for('param-employment-status-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-employment-status');
    $trail->push('Create', route('param-employment-status.create'));
});
Breadcrumbs::for('param-employment-status-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-employment-status');
    $trail->push('Edit');
});



Breadcrumbs::for('param-employment-type', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Employment Type', route('param-employment-type.index'));
});

// Home > Param Branch > [Create]
Breadcrumbs::for('param-employment-type-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-employment-type');
    $trail->push('Create', route('param-employment-type.create'));
});
Breadcrumbs::for('param-employment-type-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-employment-type');
    $trail->push('Edit');
});



Breadcrumbs::for('param-occupation', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Occupation', route('param-occupation.index'));
});

Breadcrumbs::for('param-occupation-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-occupation');
    $trail->push('Create', route('param-occupation.create'));
});
Breadcrumbs::for('param-occupation-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-occupation');
    $trail->push('Edit');
});



Breadcrumbs::for('param-payment-mode', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Payment Mode', route('param-payment-mode.index'));
});

Breadcrumbs::for('param-payment-mode-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-payment-mode');
    $trail->push('Create', route('param-payment-mode.create'));
});
Breadcrumbs::for('param-payment-mode-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-payment-mode');
    $trail->push('Edit');
});



Breadcrumbs::for('param-reject-reason', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Reject Reason', route('param-reject-reason.index'));
});

Breadcrumbs::for('param-reject-reason-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-reject-reason');
    $trail->push('Create', route('param-reject-reason.create'));
});
Breadcrumbs::for('param-reject-reason-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-reject-reason');
    $trail->push('Edit');
});



Breadcrumbs::for('param-bank-name', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Bank Name', route('param-bank-name.index'));
});

Breadcrumbs::for('param-bank-name-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-bank-name');
    $trail->push('Create', route('param-bank-name.create'));
});

Breadcrumbs::for('param-bank-name-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-bank-name');
    $trail->push('Edit');
});



Breadcrumbs::for('param-takaful-provider', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Takaful Provider', route('param-takaful-provider.index'));
});

Breadcrumbs::for('param-takaful-provider-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-takaful-provider');
    $trail->push('Create', route('param-takaful-provider.create'));
});
Breadcrumbs::for('param-takaful-provider-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-takaful-provider');
    $trail->push('Edit');
});



Breadcrumbs::for('param-manfaat-tambahan', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Manfaat Tambahan', route('param-manfaat-tambahan.index'));
});

Breadcrumbs::for('param-manfaat-tambahan-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-manfaat-tambahan');
    $trail->push('Create', route('param-manfaat-tambahan.create'));
});
Breadcrumbs::for('param-manfaat-tambahan-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-manfaat-tambahan');
    $trail->push('Edit');
});



Breadcrumbs::for('param-interest-rate', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Interest Rate', route('param-interest-rate.index'));
});

Breadcrumbs::for('param-interest-rate-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-interest-rate');
    $trail->push('Create', route('param-interest-rate.create'));
});

Breadcrumbs::for('param-interest-rate-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-interest-rate');
    $trail->push('Edit');
});




Breadcrumbs::for('param-sst', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param SST', route('param-sst.index'));
});

Breadcrumbs::for('param-sst-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-sst');
    $trail->push('Create', route('param-sst.create'));
});

Breadcrumbs::for('param-sst-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-sst');
    $trail->push('Edit');
});



Breadcrumbs::for('param-gst', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param GST', route('param-gst.index'));
});

Breadcrumbs::for('param-gst-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-gst');
    $trail->push('Create', route('param-gst.create'));
});

Breadcrumbs::for('param-gst-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-gst');
    $trail->push('Edit');
});


Breadcrumbs::for('param-type-insurance', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Type Insurance', route('param-type-insurance.index'));
});

Breadcrumbs::for('param-type-insurance-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-type-insurance');
    $trail->push('Create', route('param-type-insurance.create'));
});

Breadcrumbs::for('param-type-insurance-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-type-insurance');
    $trail->push('Edit');
});



Breadcrumbs::for('param-region-lkm', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Region LKM', route('param-region-lkm.index'));
});

Breadcrumbs::for('param-region-lkm-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-region-lkm');
    $trail->push('Create', route('param-region-lkm.create'));
});

Breadcrumbs::for('param-region-lkm-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-region-lkm');
    $trail->push('Edit');
});



Breadcrumbs::for('param-roadtax-lkm', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Roadtax LKM', route('param-roadtax-lkm.index'));
});

Breadcrumbs::for('param-roadtax-lkm-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-roadtax-lkm');
    $trail->push('Create', route('param-roadtax-lkm.create'));
});

Breadcrumbs::for('param-roadtax-lkm-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-roadtax-lkm');
    $trail->push('Edit');
});



Breadcrumbs::for('param-ownership-lkm', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Ownership LKM', route('param-ownership-lkm.index'));
});

Breadcrumbs::for('param-ownership-lkm-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-ownership-lkm');
    $trail->push('Create', route('param-ownership-lkm.create'));
});

Breadcrumbs::for('param-ownership-lkm-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-ownership-lkm');
    $trail->push('Edit');
});



Breadcrumbs::for('roles', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Roles', route('roles.index'));
});

Breadcrumbs::for('roles-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('roles');
    $trail->push('Create', route('roles.create'));
});

Breadcrumbs::for('roles-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('roles');
    $trail->push('Show');
});

Breadcrumbs::for('roles-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('roles');
    $trail->push('Edit');
});


//Audit Trail
Breadcrumbs::for('audit-trail', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Audit Trail', route('audit-trail'));
});



//User
Breadcrumbs::for('users', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Users', route('users.index'));
});

Breadcrumbs::for('users-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('users');
    $trail->push('Create', route('users.create'));
});

Breadcrumbs::for('users-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('users');
    $trail->push('Show');
});

Breadcrumbs::for('users-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('users');
    $trail->push('Edit');
});


//Permission
Breadcrumbs::for('permissions', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('permissions');
    $trail->push('Permissions', route('permissions.index'));
});

Breadcrumbs::for('permissions-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('permissions');
    $trail->push('Create', route('permissions.create'));
});

Breadcrumbs::for('permissions-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('permissions');
    $trail->push('Show');
});

Breadcrumbs::for('permissions-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('permissions');
    $trail->push('Edit');
});


//state
Breadcrumbs::for('param-state', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param State', route('param-state.index'));
});

Breadcrumbs::for('param-state-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-state');
    $trail->push('Create', route('param-state.create'));
});

Breadcrumbs::for('param-state-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-state');
    $trail->push('Show');
});

Breadcrumbs::for('param-state-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-state');
    $trail->push('Edit');
});


//Param Postcode
Breadcrumbs::for('param-duti-setem', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Duti Setem', route('param-duti-setem.index'));
});

Breadcrumbs::for('param-duti-setem-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-duti-setem');
    $trail->push('Create', route('param-duti-setem.create'));
});

Breadcrumbs::for('param-duti-setem-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-duti-setem');
    $trail->push('Show');
});

Breadcrumbs::for('param-duti-setem-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-duti-setem');
    $trail->push('Edit');
});


Breadcrumbs::for('param-insurance-company', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Insurance Company', route('param-insurance-company.index'));
});

Breadcrumbs::for('param-insurance-company-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-company');
    $trail->push('Create', route('param-insurance-company.create'));
});

Breadcrumbs::for('param-insurance-company-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-company');
    $trail->push('Show');
});

Breadcrumbs::for('param-insurance-company-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-company');
    $trail->push('Edit');
});



Breadcrumbs::for('param-insurance-policy', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Insurance Policy', route('param-insurance-policy.index'));
});

Breadcrumbs::for('param-insurance-policy-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-policy');
    $trail->push('Create', route('param-insurance-policy.create'));
});

Breadcrumbs::for('param-insurance-policy-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-policy');
    $trail->push('Show');
});

Breadcrumbs::for('param-insurance-policy-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-policy');
    $trail->push('Edit');
});



Breadcrumbs::for('param-insurance-promo', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Insurance Promo', route('param-insurance-promo.index'));
});

Breadcrumbs::for('param-insurance-promo-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-promo');
    $trail->push('Create', route('param-insurance-promo.create'));
});

Breadcrumbs::for('param-insurance-promo-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-promo');
    $trail->push('Show');
});

Breadcrumbs::for('param-insurance-promo-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-insurance-promo');
    $trail->push('Edit');
});




Breadcrumbs::for('param-postcode', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Postcode', route('param-postcode.index'));
});

Breadcrumbs::for('param-postcode-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-postcode');
    $trail->push('Create', route('param-postcode.create'));
});

Breadcrumbs::for('param-postcode-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-postcode');
    $trail->push('Show');
});

Breadcrumbs::for('param-postcode-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-postcode');
    $trail->push('Edit');
});


Breadcrumbs::for('param-protection-type', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Protection Type', route('param-protection-type.index'));
});

Breadcrumbs::for('param-protection-type-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-protection-type');
    $trail->push('Create', route('param-protection-type.create'));
});

Breadcrumbs::for('param-protection-type-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-protection-type');
    $trail->push('Show');
});

Breadcrumbs::for('param-protection-type-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-protection-type');
    $trail->push('Edit');
});



Breadcrumbs::for('param-status-app', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Status App', route('param-status-app.index'));
});

Breadcrumbs::for('param-status-app-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-status-app');
    $trail->push('Create', route('param-status-app.create'));
});

Breadcrumbs::for('param-status-app-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-status-app');
    $trail->push('Show');
});

Breadcrumbs::for('param-status-app-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-status-app');
    $trail->push('Edit');
});




Breadcrumbs::for('param-ehailing', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Ehailing', route('param-ehailing.index'));
});

Breadcrumbs::for('param-ehailing-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-ehailing');
    $trail->push('Create', route('param-ehailing.create'));
});

Breadcrumbs::for('param-ehailing-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-ehailing');
    $trail->push('Show');
});

Breadcrumbs::for('param-ehailing-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-ehailing');
    $trail->push('Edit');
});


//Param Email System
Breadcrumbs::for('param-email-system', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Email System', route('param-email-system.index'));
});

Breadcrumbs::for('param-email-system-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-system');
    $trail->push('Create', route('param-email-system.create'));
});

Breadcrumbs::for('param-email-system-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-system');
    $trail->push('Show');
});

Breadcrumbs::for('param-email-system-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-system');
    $trail->push('Edit');
});



Breadcrumbs::for('param-email-for', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Param Email For', route('param-email-for.index'));
});

Breadcrumbs::for('param-email-for-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-for');
    $trail->push('Create', route('param-email-for.create'));
});

Breadcrumbs::for('param-email-for-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-for');
    $trail->push('Show');
});

Breadcrumbs::for('param-email-for-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('param-email-for');
    $trail->push('Edit');
});





Breadcrumbs::for('setup-maintainable-emails', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Maintanable Email', route('setup-maintainable-emails.index'));
});

Breadcrumbs::for('setup-maintainable-emails-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-maintainable-emails');
    $trail->push('Create', route('setup-maintainable-emails.create'));
});

Breadcrumbs::for('setup-maintainable-emails-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-maintainable-emails');
    $trail->push('Show');
});

Breadcrumbs::for('setup-maintainable-emails-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-maintainable-emails');
    $trail->push('Edit');
});



Breadcrumbs::for('setup-monthly-installment', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Monthly Installment', route('setup-monthly-installment.index'));
});

Breadcrumbs::for('setup-monthly-installment-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-monthly-installment');
    $trail->push('Create', route('setup-monthly-installment.create'));
});

Breadcrumbs::for('setup-monthly-installment-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-monthly-installment');
    $trail->push('Show');
});

Breadcrumbs::for('setup-monthly-installment-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-monthly-installment');
    $trail->push('Edit');
});





Breadcrumbs::for('setup-document', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Document', route('setup-document.index'));
});

Breadcrumbs::for('setup-document-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-document');
    $trail->push('Create', route('setup-document.create'));
});

Breadcrumbs::for('setup-document-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-document');
    $trail->push('Show');
});

Breadcrumbs::for('setup-document-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-document');
    $trail->push('Edit');
});



Breadcrumbs::for('setup-bpacharge', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Bpa Charge', route('setup-bpacharge.index'));
});

Breadcrumbs::for('setup-bpacharge-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-bpacharge');
    $trail->push('Create', route('setup-bpacharge.create'));
});

Breadcrumbs::for('setup-bpacharge-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-bpacharge');
    $trail->push('Show');
});

Breadcrumbs::for('setup-bpacharge-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-bpacharge');
    $trail->push('Edit');
});




Breadcrumbs::for('setup-smsakad', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Sms Akad', route('setup-smsakad.index'));
});

Breadcrumbs::for('setup-smsakad-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-smsakad');
    $trail->push('Create', route('setup-smsakad.create'));
});

Breadcrumbs::for('setup-smsakad-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-smsakad');
    $trail->push('Show');
});

Breadcrumbs::for('setup-smsakad-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-smsakad');
    $trail->push('Edit');
});



Breadcrumbs::for('setup-zurichcart', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('home');
    $trail->push('Setup Zurich Cart', route('setup-zurich-cart.index'));
});

Breadcrumbs::for('setup-zurichcart-create', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-zurichcart');
    $trail->push('Create', route('setup-zurich-cart.create'));
});

Breadcrumbs::for('setup-zurichcart-show', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-zurichcart');
    $trail->push('Show');
});

Breadcrumbs::for('setup-zurichcart-edit', static function (BreadcrumbsGenerator $trail): void {
    $trail->parent('setup-zurichcart');
    $trail->push('Edit');
});








// Home > Blog > [Category]
Breadcrumbs::for('category', static function (BreadcrumbsGenerator $trail, Category $category): void {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});


// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', static function (BreadcrumbsGenerator $trail, Post $post): void {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});