<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\DashboardController;
use Laravel\Fortify\Http\Controllers\PasswordResetLinkController;
use Laravel\Fortify\Http\Controllers\NewPasswordController;
use App\Http\Controllers\PraaplicationController;
use App\Http\Controllers\PraapplicationInsuranceController;
use App\Http\Controllers\InsuranceApiController;
use App\Http\Controllers\CustomerFeedbackController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
/*660807106863
SU242B */

Route::get('/ictest','App\Http\Controllers\TestController@ictest');


Route::post('/apitest','App\Http\Controllers\ApiController@store');
Route::post('/getvehicleinfo','App\Http\Controllers\ApiController@getvehicleinfo');
Route::post('/getvehiclemake','App\Http\Controllers\ApiController@getvehiclemake');
Route::post('/getvehiclemodel','App\Http\Controllers\ApiController@getvehiclemodel');
Route::post('/calculatepremium','App\Http\Controllers\ApiController@calculatepremium');
Route::get('/calculatepremium','App\Http\Controllers\ApiController@calculatepremiumGet');
Route::post('/calculatepremiumCreate','App\Http\Controllers\ApiController@calculatepremiumCreate');
Route::post('/calculatepremiumREF','App\Http\Controllers\ApiController@calculatepremiumREF');
Route::post('/issuecovernote','App\Http\Controllers\ApiController@issuecovernote');
Route::post('/issuepolicy','App\Http\Controllers\ApiController@issuepolicy');
Route::post('/getjpjstatus','App\Http\Controllers\ApiController@getjpjstatus');


Route::post('/downloadpolicyschedule','App\Http\Controllers\ApiController@downloadpolicyschedule');

Route::get('/api-test','App\Http\Controllers\ApiController@index');
Route::get('/zurich-token','App\Http\Controllers\ZurichApiController@token');
Route::get('/zurich-getvehicleinfo','App\Http\Controllers\ZurichApiController@getvehicleinfo');
Route::get('/zurich-getvehiclemake','App\Http\Controllers\ZurichApiController@getvehiclemake');
Route::get('/zurich-getvehiclemodel','App\Http\Controllers\ZurichApiController@getvehiclemodel');
Route::get('/zurich-calculatepremium','App\Http\Controllers\ZurichApiController@calculatepremium');
Route::get('/zurich-issuecovernote','App\Http\Controllers\ZurichApiController@issuecovernote');
Route::get('/zurich-issuepolicy','App\Http\Controllers\ZurichApiController@issuepolicy');
Route::get('/zurich-getjpjstatus','App\Http\Controllers\ZurichApiController@getjpjstatus');
Route::get('/zurich-downloadpolicyschedule','App\Http\Controllers\ZurichApiController@downloadpolicyschedule');
Route::get('/zurich-postcode','App\Http\Controllers\ZurichApiController@postcode');
Route::get('/get-postcode-details/{postcode}','App\Http\Controllers\ZurichApiGetPostcodeController@getPostcodeDetails');
Route::get('/get-postcode-zurich','App\Http\Controllers\ZurichApiGetPostcodeController@getPostcodeDetails');





Route::get('/', function () {
   
    return view('pages.frontend.index');
});


Route::get('/about', function () {
   
    return view('pages.frontend.about');
});


Route::get('/roadtax-lkm','App\Http\Controllers\FrontHomeController@roadtax');


Route::get('/icop-login', function () {
   
    return view('pages.auth.login');
});


Route::get('testSst/{uuid}','App\Http\Controllers\PraapplicationInsuranceController@additionalProtection');



Route::get('/maklumat-kenderaan', function () {
    return view('pages.frontend.apps-icop.quote-vehicle');
});


Route::get('/select-insurance', function () {
    return view('pages.frontend.apps-icop.select-insurance');
});


Route::get('/additional-protection', function () {
    return view('pages.frontend.apps-icop.additional-protection');
});




Route::get('/welcoming-customer', function () {
    return view('pages.frontend.apps-icop.welcoming-customer');
});


Route::get('/apps-icop', function () {
    return view('pages.frontend.apps-icop.index')->name('apps-icop');
});

Route::get('/user-login','App\Http\Controllers\LoginUserController@index');

Route::get('/car-insurance','App\Http\Controllers\PraaplicationController@index');
//Route::post('/result','App\Http\Controllers\PraaplicationController@store');
Route::post('/result','App\Http\Controllers\PraapplicationInsuranceController@store');
Route::get('/quote/{uuid}', [PraapplicationInsuranceController::class, 'show'])->name('quote.show');
Route::post('/chose-insurance-result','App\Http\Controllers\PraapplicationInsuranceController@choseInsurance');

Route::get('/chose-insurance/{uuid}', [PraapplicationInsuranceController::class, 'choseInsuranceShow'])->name('chose-insurance.show');
Route::post('/chose-add-protection','App\Http\Controllers\PraapplicationInsuranceController@choseAdditional');
Route::get('/additional-protection/{uuid}', [PraapplicationInsuranceController::class, 'additionalProtection'])->name('additional-protection.show');


Route::post('/summary/{uuid}', [PraapplicationInsuranceController::class, 'summary']);
Route::post('/update-totalFinancing/{uuid}', [PraapplicationInsuranceController::class, 'updateFinancing'])->name('update-totalFinancing');

Route::get('/summary/{uuid}', [PraapplicationInsuranceController::class, 'getsummary'])->name('summary.show');

Route::get('/detail-financing/{uuid}', [PraapplicationInsuranceController::class, 'detailFinancing']);

Route::post('/financing','App\Http\Controllers\PraapplicationInsuranceController@financing');
Route::get('/financing/{uuid}','App\Http\Controllers\PraapplicationInsuranceController@getFinancing');


//Route::post('/trigger-api', [PraapplicationInsuranceController::class, 'codeAddProtection']);
Route::post('/strike-api/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotation']);
Route::post('/windscreen-api/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationWindscreen']);
Route::post('/legal-liability/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationLegal']);
Route::post('/legal-liability-passenger/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationLegalPassenger']);
Route::post('/inclusion-special-perils/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationPerils']);
Route::post('/private-hire-car/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationPrivateHire']);
Route::post('/cart/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationCart']);
Route::post('/all-driver/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationAllDriver']);
Route::post('/pa-plus/{plan}/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationPaPlus']);
Route::post('/towing-cleaning/{plan}/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationTowing']);

Route::post('/test-api/{uuid}', [PraapplicationInsuranceController::class, 'InsuranceTest']);


//No 
Route::post('/pa-plus-no/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationPaPlusNo']);
Route::post('/towing-cleaning-no/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationTowingNo']);
Route::post('/strike-no/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationStrikeNo']);
Route::post('/cart-no/{uuid}', [PraapplicationInsuranceController::class, 'zurichQuotationCartNo']);




//Yes Push to API  (New To API Server)
Route::post('/strike-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverStrike']);
Route::post('/legal-liability-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverLegalLiability']);
Route::post('/legal-liability-psg-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverLegalLiabilityPsg']);
Route::post('/pa-plus-api-yes/{plan}/{uuid}', [PraapplicationInsuranceController::class, 'coverPaPlus']);
Route::post('/inclusion-perils-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverPerils']);
Route::post('/all-driver-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverAllDriver']);
Route::post('/towing-api-yes/{plan}/{uuid}', [PraapplicationInsuranceController::class, 'coverTowing']);
Route::post('/cart-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverCart']);
Route::post('/windscreen-api-yes/{uuid}', [PraapplicationInsuranceController::class, 'coverWindscreen']);
Route::post('/roadtax-yes/{uuid}', [PraapplicationInsuranceController::class, 'Roadtax']);


//No Push to API (New To API Server)
Route::post('/strike-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverStrikeNo']);
Route::post('/legal-liability-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverLegalLiabilityNo']);
Route::post('/legal-liability-psg-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverLegalLiabilityPsgNo']);
Route::post('/pa-plus-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverPaPlusNo']);
Route::post('/inclusion-perils-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverPerilsNo']);
Route::post('/all-driver-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverAllDriverNo']);
Route::post('/towing-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverTowingNo']);
Route::post('/cart-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverCartNo']);
Route::post('/windscreen-api-no/{uuid}', [PraapplicationInsuranceController::class, 'coverWindscreenNo']);
Route::post('/roadtax-no/{uuid}', [PraapplicationInsuranceController::class, 'RoadtaxNo']);



Route::get('/test-additional/{uuid}', [InsuranceApiController::class, 'addCoverage'])->name('additional-protection.api');


Route::resource('quote','App\Http\Controllers\PraaplicationController');

//Route::post('/chose-insurance-result','App\Http\Controllers\PraaplicationController@choseInsurance');
//Route::post('/chose-add-protection','App\Http\Controllers\PraaplicationController@choseAdditional');

Route::post('/add-driver', 'App\Http\Controllers\PraapplicationAddDriverController@store')->name('add.driver');
Route::delete('/driver/{id}', 'App\Http\Controllers\PraapplicationAddDriverController@destroy')->name('delete.driver');
Route::get('/drivers/{id}', 'App\Http\Controllers\PraapplicationAddDriverController@getDrivers')->name('get.drivers');



Route::get('document/{filename}', function ($filename) {
    $path = storage_path('app/document/' . $filename);

    return response()->file($path);
})->name('documents.show');




Route::get('logout/user','App\Http\Controllers\LoginUserController@logout');


/* ======= Upload Document ===*/
Route::post('upload-ic/{id}','App\Http\Controllers\PraaplicationController@uploadIc');
Route::post('upload-payslip/{id}','App\Http\Controllers\PraaplicationController@uploadPayslip');
Route::post('upload-bpa/{id}','App\Http\Controllers\PraaplicationController@uploadBpa');


/* ============ Check Document Upload ============ */
Route::post('check-upload/{id}','App\Http\Controllers\PraapplicationDocumentController@checkUpload');

Route::post('check-payslip/{id}','App\Http\Controllers\PraapplicationDocumentController@checkPayslipUpload');
Route::post('check-bpa/{id}','App\Http\Controllers\PraapplicationDocumentController@checkBpaUpload');




Route::resource('fr','App\Http\Controllers\FrontHomeController');
Route::get('front','App\Http\Controllers\FrontHomeController@show');
Route::resource('daftar','App\Http\Controllers\FrontQuoteController');


/* Car Praapplication */
Route::resource('car-quote','App\Http\Controllers\CarPraapplicationController');



/* Motor Praapplication */
Route::resource('motor-quote','App\Http\Controllers\MotorPraapplicationController');


Route::get('icop3/{id}','App\Http\Controllers\PraaplicationController@selectInsurance');
Route::get('icop31/{id}','App\Http\Controllers\PraaplicationController@selectPerlindungan');

Route::get('option/{id}','App\Http\Controllers\PraaplicationController@optionMonth')->name('option-month');




Route::resource('pay-option','App\Http\Controllers\PaymentController');
Route::any('callback/{id}', 'App\Http\Controllers\PaymentController@callback');
Route::any('failed/{id}', 'App\Http\Controllers\PaymentController@failed');


Route::get('/home-user', function () {
   
    return view('pages.frontend.praaplication.home');
});


Route::get('/icop4', function () {
    //return view('welcome');
    return view('pages.frontend.praaplication.page4');
});


Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);


Route::get('/frontend', function () {
    return view('frontend.index');
});


Route::post('/language-switch', [App\Http\Controllers\LanguageController::class, 'languageSwitch'])->name('language.switch');


Route::middleware(['auth'])->group(function () {
    
    
    Route::post('/dashboard-customer','App\Http\Controllers\PraaplicationController@dashboardCust');
    Route::get('/dashboard-customer','App\Http\Controllers\PraaplicationController@dashboardCust')->name('dashboard.customer');



    //Test Pusher
    Route::get('testnotif', [App\Http\Controllers\DashboardController::class, 'testnotif']);

    //Private Pusher
    Route::get('privateChanel', [App\Http\Controllers\DashboardController::class, 'privateChanel']);
    

    
    Route::get('/check-details/{uuid}','App\Http\Controllers\ZurichApiDetailController@index');
    Route::get('/generate-quote/{uuid}','App\Http\Controllers\Backend\PraaplicationController@generate');
    Route::get('/issue-covernote/{uuid}','App\Http\Controllers\Backend\PraaplicationController@issueCovernote');
    Route::get('/jpj-status/{uuid}','App\Http\Controllers\Backend\PraaplicationController@jpjStatus');

    


    Route::resource('home', 'App\Http\Controllers\DashboardController', ['names' => [
        'index' => 'home.index',
        'update' => 'home.update',
    ]]);

    Route::resource('pra-apps','App\Http\Controllers\Backend\PraaplicationController', ['names' => [
        'index' => 'pra-apps.index',
        'update' => 'pra-apps.update',
    ]]);

    Route::resource('pra-finance','App\Http\Controllers\Backend\PraaplicationFinanceController', ['names' => [
        'index' => 'pra-finance.index',
        'update' => 'pra-finance.update',
        'store' => 'pra-finance.store',
    ]]);


    Route::resource('app-processing','App\Http\Controllers\ApplicationProcessingController', ['names' => [
        'index' => 'app-processing.index',
        'update' => 'app-processing.update',
    ]]);

    Route::resource('app-smsakad','App\Http\Controllers\ApplicationSmsAkadController', ['names' => [
        'index' => 'app-smsakad.index',
        'update' => 'app-smsakad.update',
    ]]);

    Route::get('sms/send/{uuid}', [App\Http\Controllers\ApplicationSmsAkadController::class, 'sendSms']);
    //Route::get('/feedback/receive', [App\Http\Controllers\ApplicationSmsAkadController::class, 'receiveFeedbackx']);

    Route::get('/feedback', function () { return view('components.customer-feedback'); })->name('CustomerFeedback');
    Route::post('/feedback', [CustomerFeedbackController::class, 'submitForm']);
    Route::get('/feedback/receive', [CustomerFeedbackController::class, 'receiveFeedback']);
    Route::get('/feedback/responses', [CustomerFeedbackController::class, 'showFeedbackResponses']);

    Route::resource('app-issuecovernote','App\Http\Controllers\ApplicationIsseCovernoteController', ['names' => [
        'index' => 'app-issuecovernote.index',
        'update' => 'app-issuecovernote.update',
    ]]);




    Route::resource('param-branch','App\Http\Controllers\ParamBranchController', ['names' => [
        'index' => 'param-branch.index',
        'create' => 'param-branch.create',
        'update' => 'param-branch.update',
        'edit' => 'param-branch.edit',
    ]]);
    Route::post('param-branch/status','App\Http\Controllers\ParamBranchController@status');



    Route::resource('param-status-app','App\Http\Controllers\ParamStatusApplicationController', ['names' => [
        'index' => 'param-status-app.index',
        'create' => 'param-status-app.create',
        'update' => 'param-status-app.update',
        'edit' => 'param-status-app.edit',
    ]]);
    Route::post('param-status-app/status','App\Http\Controllers\ParamStatusApplicationController@status');

    

    Route::resource('param-protection-type','App\Http\Controllers\ParamProtectionTypeController', ['names' => [
        'index' => 'param-protection-type.index',
        'create' => 'param-protection-type.create',
        'update' => 'param-protection-type.update',
        'edit' => 'param-protection-type.edit',
    ]]);
    Route::post('param-protection-type/status','App\Http\Controllers\ParamProtectionTypeController@status');


    Route::resource('param-insurance-company','App\Http\Controllers\ParamInsuranceCompanyController', ['names' => [
        'index' => 'param-insurance-company.index',
        'create' => 'param-insurance-company.create',
        'update' => 'param-insurance-company.update',
        'edit' => 'param-insurance-company.edit',
    ]]);
    Route::post('param-insurance-company/status','App\Http\Controllers\ParamInsuranceCompanyController@status');



    Route::resource('param-insurance-policy','App\Http\Controllers\ParamInsurancePolicyController', ['names' => [
        'index' => 'param-insurance-policy.index',
        'create' => 'param-insurance-policy.create',
        'update' => 'param-insurance-policy.update',
        'edit' => 'param-insurance-policy.edit',
    ]]);
    Route::post('param-insurance-policy/status','App\Http\Controllers\ParamInsurancePolicyController@status');


    Route::resource('param-insurance-promo','App\Http\Controllers\ParamInsurancePromoController', ['names' => [
        'index' => 'param-insurance-promo.index',
        'create' => 'param-insurance-promo.create',
        'update' => 'param-insurance-promo.update',
        'edit' => 'param-insurance-promo.edit',
    ]]);
    Route::post('param-insurance-promo/status','App\Http\Controllers\ParamInsurancePromoController@status');


    Route::resource('param-region-lkm','App\Http\Controllers\ParamRegionlkmController', ['names' => [
        'index' => 'param-region-lkm.index',
        'create' => 'param-region-lkm.create',
        'update' => 'param-region-lkm.update',
        'edit' => 'param-region-lkm.edit',
    ]]);
    Route::post('param-region-lkm/status','App\Http\Controllers\ParamRegionlkmController@status');


    Route::resource('param-ownership-lkm','App\Http\Controllers\ParamOwnershipController', ['names' => [
        'index' => 'param-ownership-lkm.index',
        'create' => 'param-ownership-lkm.create',
        'update' => 'param-ownership-lkm.update',
        'edit' => 'param-ownership-lkm.edit',
    ]]);
    Route::post('param-ownership-lkm/status','App\Http\Controllers\ParamOwnershipController@status');


    Route::resource('param-roadtax-lkm','App\Http\Controllers\ParamRoadtaxController', ['names' => [
        'index' => 'param-roadtax-lkm.index',
        'create' => 'param-roadtax-lkm.create',
        'update' => 'param-roadtax-lkm.update',
        'edit' => 'param-roadtax-lkm.edit',
    ]]);
    Route::post('param-roadtax-lkm/status','App\Http\Controllers\ParamRoadtaxController@status');



    Route::resource('param-ehailing','App\Http\Controllers\ParamEhailingController', ['names' => [
        'index' => 'param-ehailing.index',
        'create' => 'param-ehailing.create',
        'update' => 'param-ehailing.update',
        'edit' => 'param-ehailing.edit',
    ]]);
    Route::post('param-ehailing/status','App\Http\Controllers\ParamEhailingController@status');

    

    Route::resource('param-employment-type','App\Http\Controllers\ParamEmploymentTypeController', ['names' => [
        'index' => 'param-employment-type.index',
        'create' => 'param-employment-type.create',
        'update' => 'param-employment-type.update',
        'edit' => 'param-employment-type.edit',
    ]]);
    Route::post('param-employment-type/status','App\Http\Controllers\ParamEmploymentTypeController@status');

    Route::resource('param-employment-status','App\Http\Controllers\ParamEmploymentStatusController', ['names' => [
        'index' => 'param-employment-status.index',
        'create' => 'param-employment-status.create',
        'update' => 'param-employment-status.update',
        'edit' => 'param-employment-status.edit',
    ]]);
    Route::post('param-employment-status/status','App\Http\Controllers\ParamEmploymentStatusController@status');


    Route::resource('param-occupation','App\Http\Controllers\ParamOccupationController', ['names' => [
        'index' => 'param-occupation.index',
        'create' => 'param-occupation.create',
        'update' => 'param-occupation.update',
        'edit' => 'param-occupation.edit',
    ]]);
    Route::post('param-occupation/status','App\Http\Controllers\ParamOccupationController@status');


    Route::resource('param-payment-mode','App\Http\Controllers\ParamPaymentModeController', ['names' => [
        'index' => 'param-payment-mode.index',
        'create' => 'param-payment-mode.create',
        'update' => 'param-payment-mode.update',
        'edit' => 'param-payment-mode.edit',
    ]]);
    Route::post('param-payment-mode/status','App\Http\Controllers\ParamPaymentModeController@status');

    
    Route::resource('param-reject-reason','App\Http\Controllers\ParamRejectReasonController', ['names' => [
        'index' => 'param-reject-reason.index',
        'create' => 'param-reject-reason.create',
        'update' => 'param-reject-reason.update',
        'edit' => 'param-reject-reason.edit',
    ]]);
    Route::post('param-reject-reason/status','App\Http\Controllers\ParamRejectReasonController@status');


    Route::resource('param-bank-name','App\Http\Controllers\ParamBankNameController', ['names' => [
        'index' => 'param-bank-name.index',
        'create' => 'param-bank-name.create',
        'update' => 'param-bank-name.update',
        'edit' => 'param-bank-name.edit',
    ]]);
    Route::post('param-bank-name/status','App\Http\Controllers\ParamBankNameController@status');


    Route::resource('param-takaful-provider','App\Http\Controllers\ParamTakafulProviderController', ['names' => [
        'index' => 'param-takaful-provider.index',
        'create' => 'param-takaful-provider.create',
        'update' => 'param-takaful-provider.update',
        'edit' => 'param-takaful-provider.edit',
    ]]);
    Route::post('param-takaful-provider/status','App\Http\Controllers\ParamTakafulProviderController@status');


    Route::resource('param-manfaat-tambahan','App\Http\Controllers\ParamManfaatTambahanController', ['names' => [
        'index' => 'param-manfaat-tambahan.index',
        'create' => 'param-manfaat-tambahan.create',
        'update' => 'param-manfaat-tambahan.update',
        'edit' => 'param-manfaat-tambahan.edit',
    ]]);
    Route::post('param-manfaat-tambahan/status','App\Http\Controllers\ParamManfaatTambahanController@status');


    Route::resource('param-interest-rate','App\Http\Controllers\ParamInterestRateController', ['names' => [
        'index' => 'param-interest-rate.index',
        'create' => 'param-interest-rate.create',
        'update' => 'param-interest-rate.update',
        'edit' => 'param-interest-rate.edit',
    ]]);
    Route::post('param-interest-rate/status','App\Http\Controllers\ParamInterestRateController@status');



    Route::resource('param-sst','App\Http\Controllers\ParamSstController', ['names' => [
        'index' => 'param-sst.index',
        'create' => 'param-sst.create',
        'update' => 'param-sst.update',
        'edit' => 'param-sst.edit',
    ]]);
    Route::post('param-sst/status','App\Http\Controllers\ParamSstController@status');


    Route::resource('param-gst','App\Http\Controllers\ParamGstController', ['names' => [
        'index' => 'param-gst.index',
        'create' => 'param-gst.create',
        'update' => 'param-gst.update',
        'edit' => 'param-gst.edit',
    ]]);
    Route::post('param-gst/status','App\Http\Controllers\ParamGstController@status');



    Route::resource('param-type-insurance','App\Http\Controllers\ParamTypeInsuranceController', ['names' => [
        'index' => 'param-type-insurance.index',
        'create' => 'param-type-insurance.create',
        'update' => 'param-type-insurance.update',
        'edit' => 'param-type-insurance.edit',
    ]]);
    Route::post('param-type-insurance/status','App\Http\Controllers\ParamTypeInsuranceController@status');


    Route::resource('param-duti-setem','App\Http\Controllers\ParamDutiSetemController', ['names' => [
        'index' => 'param-duti-setem.index',
        'create' => 'param-duti-setem.create',
        'update' => 'param-duti-setem.update',
        'edit' => 'param-duti-setem.edit',
    ]]);
    Route::post('param-duti-setem/status','App\Http\Controllers\ParamDutiSetemController@status');


    Route::resource('param-state','App\Http\Controllers\StateController', ['names' => [
        'index' => 'param-state.index',
        'create' => 'param-state.create',
        'update' => 'param-state.update',
        'edit' => 'param-state.edit',
    ]]);
    Route::post('param-state/status','App\Http\Controllers\StateController@status');


    Route::resource('param-email-system','App\Http\Controllers\ParamEmailSystemController', ['names' => [
        'index' => 'param-email-system.index',
        'create' => 'param-email-system.create',
        'update' => 'param-email-system.update',
        'edit' => 'param-email-system.edit',
    ]]);
    Route::post('param-email-system/status','App\Http\Controllers\ParamEmailSystemController@status');


    Route::resource('param-email-for','App\Http\Controllers\ParamEmailForController', ['names' => [
        'index' => 'param-email-for.index',
        'create' => 'param-email-for.create',
        'update' => 'param-email-for.update',
        'edit' => 'param-email-for.edit',
    ]]);
    Route::post('param-email-for/status','App\Http\Controllers\ParamEmailForController@status');
   
    
    Route::resource('calculate-roadtax','App\Http\Controllers\CalculateRoadTaxController');


    //Route::resource('postcode','App\Http\Controllers\PostcodeController');
    Route::get('param-postcode/{param_postcode}/edit', 'App\Http\Controllers\PostcodeController@edit')->name('param-postcode.edit');

    Route::resource('param-postcode','App\Http\Controllers\PostcodeController', ['names' => [
        'index' => 'param-postcode.index',
        'create' => 'param-postcode.create',
        'update' => 'param-postcode.update',
        'edit' => 'param-postcode.edit',
    ]]);
    Route::post('param-postcode/status','App\Http\Controllers\PostcodeController@status');
    //Route::get('param-postcode','App\Http\Controllers\PostcodeController@index')->name('param-postcode.index');

 
    


    /* ==================================== Setup =============================== */

    
    Route::resource('setup-maintainable-emails','App\Http\Controllers\SetupMaintainableEmailController', ['names' => [
        'index' => 'setup-maintainable-emails.index',
        'create' => 'setup-maintainable-emails.create',
        'update' => 'setup-maintainable-emails.update',
        'edit' => 'setup-maintainable-emails.edit',
    ]]);
    Route::post('setup-maintainable-emails/status','App\Http\Controllers\SetupMaintainableEmailController@status');

   
    Route::resource('setup-monthly-installment','App\Http\Controllers\SetupMonthlyInstallmentController', ['names' => [
        'index' => 'setup-monthly-installment.index',
        'create' => 'setup-monthly-installment.create',
        'update' => 'setup-monthly-installment.update',
        'edit' => 'setup-monthly-installment.edit',
    ]]);
    Route::post('setup-monthly-installment/status','App\Http\Controllers\SetupMonthlyInstallmentController@status');

    
       
    Route::resource('setup-document','App\Http\Controllers\SetupDocumentController', ['names' => [
        'index' => 'setup-document.index',
        'create' => 'setup-document.create',
        'update' => 'setup-document.update',
        'edit' => 'setup-document.edit',
    ]]);
    Route::post('setup-document/status','App\Http\Controllers\SetupDocumentController@status');
    

    Route::resource('setup-smsakad','App\Http\Controllers\SetupSmsAkadController', ['names' => [
        'index' => 'setup-smsakad.index',
        'create' => 'setup-smsakad.create',
        'update' => 'setup-smsakad.update',
        'edit' => 'setup-smsakad.edit',
    ]]);
    Route::post('setup-smsakad/status','App\Http\Controllers\SetupSmsAkadController@status');
  
    
    Route::resource('setup-zurich-cart','App\Http\Controllers\SetupZurichCartController', ['names' => [
        'index' => 'setup-zurich-cart.index',
        'create' => 'setup-zurich-cart.create',
        'update' => 'setup-zurich-cart.update',
        'edit' => 'setup-zurich-cart.edit',
    ]]);
    Route::post('setup-zurich-cart/status','App\Http\Controllers\SetupZurichCartController@status');
    

    Route::resource('setup-bpacharge','App\Http\Controllers\SetupBpaChargeController', ['names' => [
        'index' => 'setup-bpacharge.index',
        'create' => 'setup-bpacharge.create',
        'update' => 'setup-bpacharge.update',
        'edit' => 'setup-bpacharge.edit',
    ]]);
    Route::post('setup-bpacharge/status','App\Http\Controllers\SetupBpaChargeController@status');
  

    /* ==================================== End Setup =============================== */




    Route::resource('users','App\Http\Controllers\UserController', ['names' => [
        'index' => 'users.index',
        'create' => 'users.create',
        'update' => 'users.update',
        'edit' => 'users.edit',
    ]]);
    Route::post('users/status','App\Http\Controllers\UserController@status');
    //Route::resource('users','App\Http\Controllers\UserController');

    Route::resource('permissions','App\Http\Controllers\PermissionController', ['names' => [
        'index' => 'permissions.index',
        'create' => 'permissions.create',
        'update' => 'permissions.update',
        'edit' => 'permissions.edit',
    ]]);
    Route::post('permissions/status','App\Http\Controllers\PermissionController@status');
    //Route::resource('permissions','App\Http\Controllers\PermissionController');

    Route::resource('roles','App\Http\Controllers\RoleController');
    

    Route::get('/audit-trail','App\Http\Controllers\AuditTrailController@index')->name('audit-trail');
    Route::post('audit-trail/destroy/{id}','App\Http\Controllers\AuditTrailController@destroy');



});



