<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;

class TestController extends Controller
{
    //
    public function ictest()
    {
        $ic = '930524011100';

        // Extract year, month, and day
        $year = substr($ic, 0, 2);
        $month = substr($ic, 2, 2);
        $day = substr($ic, 4, 2);

        // Determine the full year based on current date context
        $currentYear = (int)date('Y');
        $currentYearShort = (int)substr($currentYear, 2, 2);
        $century = ($year <= $currentYearShort) ? '20' : '19';
        $fullYear = $century . $year;

        //'22/12/1998',

        $dateOfBirth = $day.'/'.$month.'/'.$fullYear;

        $dobFormat = $fullYear . '-' . $month . '-' . $day;

        // Convert date of birth to a DateTime object
        $dobDate = new DateTime($dobFormat);
        $currentDate = new DateTime();  // Current date


        // Calculate the age
        $age = $currentDate->diff($dobDate)->y;

        // Output date of birth for debugging
        dd($age);



        dd($dateOfBirth);

        

    }

}
