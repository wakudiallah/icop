<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Praaplication;
use App\Models\ParamProtectionType;
use App\Models\Postcode;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePolicy;
use App\Models\PerlindunganTambahan;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\SetupDocument;
use Illuminate\Support\Str;
use Hash;
use Auth;

class PraapplicationDocumentController extends Controller
{
    
    //
    public function checkUpload($uuid)
    {
        $checkIc = ApplicationDocument::where('uuid', $uuid)
                ->whereNotNull('nric')
                ->first();
        
        $checkPayslip = ApplicationDocument::where('uuid', $uuid)
                ->whereNotNull('payslip')
                ->first();
        
        $checkBpa179 = ApplicationDocument::where('uuid', $uuid)
                ->whereNotNull('bpa179')
                ->first();


        if($checkIc){
            $ic_status = true;
        }else{
            $ic_status = false;
        }

        if($checkPayslip){
            $payslip_status = true;
        }else{
            $payslip_status = false;
        }

        if($checkBpa179){
            $bpa_status = true;
        }else{
            $bpa_status = false;
        }


        //dd($ic_status);

        return response()->json([
            'nric' => $ic_status,
            'payslip' => $payslip_status,
            'checkBpa179' => $bpa_status,
        ]);
        
    }





}
