<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEmailFor;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamEmailForController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-email-for-list|param-email-for-create|param-email-for-edit|param-email-for-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-email-for-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-email-for-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-email-for-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamEmailFor::get();
        
        return view('pages.backend.paramemailfor.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramemailfor.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'email_for'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamEmailFor::create($data);

        if($status){
            alert()->success('success','Successfully added param email for');
        } 
        else{
            alert()->error('error','Error occurred while adding param email for');
        }
        return redirect()->route('param-email-for.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamEmailFor::find($id);
        
        if(!$data){

            alert()->error('error','Param email for found');
        }
       
        return view('pages.backend.paramemailfor.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamEmailFor::find($id);

        $this->validate($request,[
            'email_for'=>'required|max:80',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param email for successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-email-for.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamEmailFor::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param email for successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}

