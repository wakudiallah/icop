<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInsuranceCompany;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamInsuranceCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-insurance-company-list|param-insurance-company-create|param-insurance-company-edit|param-insurance-company-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-insurance-company-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-insurance-company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-insurance-company-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        $data = ParamInsuranceCompany::get();
        
        return view('pages.backend.paraminsurancecompany.index', compact('data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paraminsurancecompany.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'insurance_product'=>'required|max:80',
            'company'=>'required|max:150',
            'register_company'=>'nullable|max:150',
            'logo'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'status'=>'required',
        ]);


        if($request->file('logo')){
            $logo = base64_encode(file_get_contents($request->file('logo')->path()));
        }

        $data=$request->all();
        $data['logo'] = $logo;

        $status = ParamInsuranceCompany::create($data);

        if($status){
            alert()->success('success','Successfully added param insurance company');
        } 
        else{
            alert()->error('error','Error occurred while adding param insurance company');
        }
        return redirect()->route('param-insurance-company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamInsuranceCompany::find($id);
        
        if(!$data){

            alert()->error('error','Param insurance company found');
        }
       
        return view('pages.backend.paraminsurancecompany.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamInsuranceCompany::find($id);

        $this->validate($request,[
            'insurance_product'=>'required|max:80',
            'company'=>'required|max:150',
            'register_company'=>'nullable|max:150',
            'logo'=>'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'status' => 'required|in:1,0',
        ]);

        if($request->file('logo')){
            $logo = base64_encode(file_get_contents($request->file('logo')->path()));
        }

        $data=$request->all();
        $data['logo'] = $logo;
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param insurance company successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-insurance-company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamInsuranceCompany::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param insurance company successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
