<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\Praaplication;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\ApplicationSelectInsurance;
use App\Models\ApplicationAdditionalProtect;
use App\Models\ApplicationFinancing;
use App\Models\ApplicationGajiPendapatan;
use App\Models\ApplicationGajiPotongan;
use App\Models\ApplicationDsr;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\CheckApplicationDoc;
use Auth;

class ApplicationProcessingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:app-processing-list|app-processing-create|app-processing-edit|app-processing-delete', ['only' => ['index','show']]);
        $this->middleware('permission:app-processing-create', ['only' => ['create','store']]);
        $this->middleware('permission:app-processing-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:app-processing-delete', ['only' => ['destroy', 'status']]);
    }


    public function index(){

        
        $data = Praaplication::orderBy('id','DESC')
                ->where('pay_status', 'F')  
                ->where('app_status', 'DC')              
                ->get();

        
        return view('pages.backend.appprocessing.index', compact('data'));
    }


    public function show($uuid){

        $data = Praaplication::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();
        
        $doc = ApplicationDocument::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();

        $gaji = ApplicationGajiPendapatan::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();

        $pot = ApplicationGajiPotongan::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();

        $dsr = ApplicationDsr::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();

        $checkDoc = CheckApplicationDoc::where('uuid', $uuid)->orderBy('id','DESC')
                ->first();

                
        return view('pages.backend.appprocessing.show', compact('uuid', 'doc', 'data', 'gaji', 'pot', 'dsr', 'checkDoc'));
    }

    
    public function store(Request $request){
        

        $uuid = $request->uuid;
        $id = $request->id;
        $status = $request->status;
        $type = $request->type;

        /*
            id 1 = NRIC :
            id 2 = Playslip;
            id 3 = bpa 179;
            id 4 eligibility calculation;
        */

        $checkApp = CheckApplicationDoc::where('uuid', $uuid)->orderBy('id', 'desc')->first();

        if($checkApp){
            if($id == 1){
                $checkApp->update([
                    'check_nirc' => $status
                ]);
            }elseif($id == 2){
                $checkApp->update([
                    'check_payslip' => $status
                ]);
            }elseif($id == 3){
                $checkApp->update([
                    'check_bpa179' => $status
                ]);
            }elseif($id == 4){
                $checkApp->update([
                    'check_eligibility_calculation' => $status
                ]);
            }

        }else{

            if($id == 1){
                CheckApplicationDoc::create([
                    'uuid' => $uuid,
                    'check_nirc' => $status
                ]);
            }elseif($id == 2){
                CheckApplicationDoc::create([
                    'uuid' => $uuid,
                    'check_payslip' => $status
                ]);
                
            }elseif($id == 3){
                CheckApplicationDoc::create([
                    'uuid' => $uuid,
                    'check_bpa179' => $status
                ]);
                
            }elseif($id == 4){
                CheckApplicationDoc::create([
                    'uuid' => $uuid,
                    'check_eligibility_calculation' => $status
                ]);

            }
            
        }

    }


    public function update(Request $request, $uuid){

        $data = Praaplication::where('uuid', $uuid)->update([
            'app_status' => 'AK'    //Akad
        ]);

        return redirect()->route('app-processing.index');
                
    }






}
