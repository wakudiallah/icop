<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SetupBpaCharge;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SetupBpaChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-bpacharge-list|setup-bpacharge-create|setup-bpacharge-edit|setup-bpacharge-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-bpacharge-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-bpacharge-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-bpacharge-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        //
        $data = SetupBpaCharge::get();
        
        return view('pages.backend.setupbpacharge.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.backend.setupbpacharge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'bpa_charges'=>'required|max:200',
            'status'=>'required',
        ]);
        $data=$request->all();


        $status = SetupBpaCharge::create($data);


        if($status){
            alert()->success('success','Successfully added Setup Bpa Charge');
        } 
        else{
            alert()->error('error','Error occurred while adding Setup  Bpa Charge');
        }
        return redirect()->route('setup-bpacharge.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        //
        $data = SetupBpaCharge::find($id);
        
        if(!$data){

            alert()->error('error','Setup bpacharge not found');
        }
       
        return view('pages.backend.setupbpacharge.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = SetupBpaCharge::find($id);

        $this->validate($request,[
            'bpa_charges'=>'required|max:200',
            'status'=>'required',
        ]);
        $data=$request->all();
       
        $status = $brand->update($data);

        if($status){
            
            alert()->success('success','Setup bpacharge successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-bpacharge.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupBpaCharge::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup bpacharge successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
