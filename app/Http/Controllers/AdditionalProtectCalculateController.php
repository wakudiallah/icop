<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceExtraCover;
use App\Models\InsuranceBasicPremium;
use DB;

class AdditionalProtectCalculateController extends Controller
{
    //

    
    public function calculateProtect($uuid)
    {
        
        $extraCover = InsuranceExtraCover::where('uuid', $uuid)
                        ->select('extCoverPrem')
                        ->where('status', 1)
                        ->sum('extCoverPrem');

                        
        //$intExtra = intval($extraCover);

        $basicPrem = InsuranceBasicPremium::where('uuid', $uuid)
                    ->select('totMtrPrem', 'stampDutyAmt', 'gst_Pct', 'totBasicNetAmt')
                    ->first();

        
        $totalSst = ($extraCover + $basicPrem->totBasicNetAmt) * $basicPrem->gst_Pct / 100;
        $totalSst = number_format($totalSst, 2);
       
        $totalCalculte = $extraCover + $basicPrem->totBasicNetAmt + $basicPrem->stampDutyAmt + $totalSst;

        // /dd($totalCalculte);
        $updInsBasic = InsuranceBasicPremium::where('uuid', $uuid)
                        ->update(['ttlPayablePremium' => $totalCalculte]);

       
        $calculate = [
            'extraCover' => $extraCover,
            'payable' => $totalCalculte,
            'totalSst' => $totalSst
        ];
                    
        return $calculate;

    }


}
