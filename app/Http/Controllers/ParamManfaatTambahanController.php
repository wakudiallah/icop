<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamManfaatTambahan;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamManfaatTambahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-manfaat-tambahan-list|param-manfaat-tambahan-create|param-manfaat-tambahan-edit|param-manfaat-tambahan-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-manfaat-tambahan-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-manfaat-tambahan-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-manfaat-tambahan-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamManfaatTambahan::get();
        
        return view('pages.backend.parammanfaattambahan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.parammanfaattambahan.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'manfaat_tambahan'=>'string|required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamManfaatTambahan::create($data);

        if($status){
            alert()->success('success','Successfully added param manfaat tambahan');
        } 
        else{
            alert()->error('error','Error occurred while adding param manfaat tambahan');
        }
        return redirect()->route('param-manfaat-tambahan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamManfaatTambahan::find($id);
        
        if(!$data){

            alert()->error('error','Param bank name not found');
        }
       
        return view('pages.backend.parammanfaattambahan.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamManfaatTambahan::find($id);

        $this->validate($request,[
            'manfaat_tambahan'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param manfaat tambahan successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-manfaat-tambahan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamManfaatTambahan::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param manfaat tambahan successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
