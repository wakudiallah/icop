<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInterestRate;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;


class ParamInterestRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-interest-rate-list|param-interest-rate-create|param-interest-rate-edit|param-interest-rate-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-interest-rate-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-interest-rate-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-interest-rate-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamInterestRate::get();
        
        return view('pages.backend.paraminterestrate.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paraminterestrate.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'interest_rate'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamInterestRate::create($data);

        if($status){
            alert()->success('success','Successfully added param interest rate');
        } 
        else{
            alert()->error('error','Error occurred while adding param interest rate');
        }
        return redirect()->route('param-interest-rate.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamInterestRate::find($id);
        
        if(!$data){

            alert()->error('error','Param interest rate not found');
        }
       
        return view('pages.backend.paraminterestrate.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamInterestRate::find($id);

        $this->validate($request,[
            'interest_rate'=>'required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param interest rate successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-interest-rate.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamInterestRate::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param interest rate successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
