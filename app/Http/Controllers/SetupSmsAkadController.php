<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SetupSmsAkad;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SetupSmsAkadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-smsakad-list|setup-smsakad-create|setup-smsakad-edit|setup-smsakad-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-smsakad-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-smsakad-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-smsakad-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        //
        $data = SetupSmsAkad::get();
        
        return view('pages.backend.setupsmsakad.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.backend.setupsmsakad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'opening'=>'required|max:200',
            'sms_akad'=>'required|max:1000',
            'closing'=>'required|max:200',
            'text_basic_contribution' =>'required|max:200',
            'text_caj_perkhidmatan' =>'required|max:200',
            'status'=>'required',
        ]);
        $data=$request->all();


        $status = SetupSmsAkad::create($data);


        if($status){
            alert()->success('success','Successfully added Setup smsakad');
        } 
        else{
            alert()->error('error','Error occurred while adding Setup smsakad');
        }
        return redirect()->route('setup-smsakad.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        //
        $data = SetupSmsAkad::find($id);
        
        if(!$data){

            alert()->error('error','Setup smsakad not found');
        }
       
        return view('pages.backend.setupsmsakad.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = SetupSmsAkad::find($id);

        $this->validate($request,[
            'opening'=>'required|max:200',
            'sms_akad'=>'required|max:1000',
            'closing'=>'required|max:200','text_basic_premium' =>'required|max:200',
            'text_basic_contribution' =>'required|max:200',
            'text_caj_perkhidmatan' =>'required|max:200',
            
            'status'=>'required',
        ]);
        $data=$request->all();
       
        $status = $brand->update($data);

        if($status){
            
            alert()->success('success','Setup smsakad successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-smsakad.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupSmsAkad::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup smsakad successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
