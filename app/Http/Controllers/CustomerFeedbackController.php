<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Models\CustomerFeedback;

class CustomerFeedbackController extends Controller
{
    protected $accountSid;
    protected $authToken;
    protected $twilioNumber;
    protected $twilioClient;

    public function __construct()
    {
        $this->accountSid = env('TWILIO_SID');
        $this->authToken = env('TWILIO_TOKEN');
        $this->twilioNumber = env('TWILIO_FROM');
        $this->twilioClient = new Client($this->accountSid, $this->authToken);
    }

    public function submitForm(Request $request)
    {
        $validatedData = $request->validate([
            'phones' => 'required',
            'message' => 'required',
        ]);

        $phones = explode(',', $validatedData['phones']);

        foreach ($phones as $phone) {
            try {
                $this->twilioClient->messages->create(
                    $phone,
                    [
                        'from' => $this->twilioNumber,
                        'body' => $validatedData['message']
                    ]
                );
            } catch (\Exception $e) {

                dd($e);
                return back()->withInput()->withErrors(['error' => 'Failed to send SMS.']);
            }
        }
        return redirect()->back()->with('success', 'SMS sent successfully.');
    }

    public function showFeedbackResponses()
    {
        $feedbackResponses = CustomerFeedback::all();
        return view('components.feedback-responses', ['feedbackResponses' => $feedbackResponses]);
    }

    public function receiveFeedback(Request $request)
    {
        $from = $request->From;
        $body = $request->Body;
        
        CustomerFeedback::create([
            'phone' => $from,
            'response' => $body
        ]);
        
        $this->twilioClient->messages->create(
            $from,
            [
                'from' => $this->twilioNumber,
                'body' => "Thank you for your feedback."
            ]
        );
        
        return response("Thank you for your feedback.");
    }


}