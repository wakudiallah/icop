<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamBranch;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-branch-list|param-branch-create|param-branch-edit|param-branch-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-branch-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-branch-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-branch-delete', ['only' => ['destroy', 'status']]);
    }

    
    public function index()
    {
        $data = ParamBranch::get();
        
        return view('pages.backend.parambranch.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('pages.backend.parambranch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'branch'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamBranch::create($data);

        if($status){
            alert()->success('success','Successfully added param branch');
        }
        else{
            alert()->error('error','Error occurred while adding param branch');
        }
        return redirect()->route('param-branch.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParamBranch::find($id);
        
        if(!$data){

            alert()->error('error','ParamBranch not found');
        }
       
        return view('pages.backend.parambranch.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ParamBranch::find($id);

        $this->validate($request,[
            'branch'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','ParamBranch successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-branch.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamBranch::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','ParamBranch successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
