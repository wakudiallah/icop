<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamDutiSetem;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamDutiSetemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-duti-setem-list|param-duti-setem-create|param-duti-setem-edit|param-duti-setem-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-duti-setem-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-duti-setem-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-duti-setem-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamDutiSetem::get();
        
        return view('pages.backend.paramdutisetem.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramdutisetem.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'duti_setem'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamDutiSetem::create($data);

        if($status){
            alert()->success('success','Successfully added param duti setem');
        } 
        else{
            alert()->error('error','Error occurred while adding param duti setem');
        }
        return redirect()->route('param-duti-setem.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamDutiSetem::find($id);
        
        if(!$data){

            alert()->error('error','Param duti setem found');
        }
       
        return view('pages.backend.paramdutisetem.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamDutiSetem::find($id);

        $this->validate($request,[
            'duti_setem'=>'required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param duti setem successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-duti-setem.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamDutiSetem::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param duti setem successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}

