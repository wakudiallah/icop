<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\Praaplication;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\PartnerApiAccount;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ZurichApiCalculatePremium_AllowableCartAmount;
use App\Models\ZurichApiCalculatePremium_AllowableCartDay;
use App\Models\ZurichAllowableCourtesyCarDay;
use App\Models\ZurichApiCalculate_AllowableExtensionCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableKeyReplacement;
use App\Models\ZurichApiCalculatePremium_AllowablePaBasicUnit;
use App\Models\ZurichApiCalculatePremium_AllowablePaExtendedCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableVoluntaryExcess;
use App\Models\ZurichApiCalculatePremium_AllowableWaterDamageCoverage;
use App\Models\ZurichApiCalculatePremium_CarReplacementDay;
use App\Models\ZurichApiCalculatePremium_ErrorDisplay;
use App\Models\ZurichApiCalculatePremium_paCVehicleDetail;
use App\Models\ZurichApiCalculatePremium_premiumDetail;
use App\Models\ZurichApiCalculatePremium_quotationInfo;
use App\Models\ZurichApiCalculatePremium_extraCoverData;
use App\Models\ZurichApiCalculatePremium_allowableCourtesyCar;
use App\Models\ZurichApiIssueCovernote;
use App\Models\ZurichApiGetjpjstatus;
use App\Models\ZurichApiGetvehiclemake;
use App\Models\ZurichApiGetvehiclemodel;
use App\Models\ZurichApiPostcode;
use Auth;
use DateTime;
use Exception;
use DateTimeZone;
use Hash;
use App\Events\NewContentNotification;
use Pusher\Pusher;
use Illuminate\Support\Str;



class PraaplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:praapplication-list|praapplication-create|praapplication-edit|praapplication-delete', ['only' => ['index','show']]);
        $this->middleware('permission:praapplication-create', ['only' => ['create','store']]);
        $this->middleware('permission:praapplication-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:praapplication-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        $data = Praaplication::orderBy('id','DESC')
                ->where('pay_status', 'OL')                
                ->get();

        
        return view('pages.backend.praapplication.index', compact('data'));
    }

    public function finance()
    {
        $data = Praaplication::orderBy('id','DESC')
                ->where('pay_status', 'F')  
                ->get();

        return view('pages.backend.praapplication.index', compact('data'));
    }


    public function show($id)
    {
        $data = Praaplication::orderBy('id','DESC')
                ->where('pay_status', 'F')  
                ->get();

             

        return view('pages.backend.praapplication.index', compact('data'));
    }


    public function generate($uuid)
    {
      
        $zurichQuoteInfo = ZurichApiCalculatePremium_quotationInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $zurichvariant = ZurichApiGetVehicleInfoVariant::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $vehicleInfo = ZurichApiGetVehicleInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $pra = Praaplication::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $postcode = ZurichApiPostcode::where('uuid', $uuid)->orderBy('id', 'Desc')->first();

        //Abisi delete .00
        $abisiMarketValue = $zurichvariant->marketValue;
        $abisiX = str_replace(".00", "", $abisiMarketValue);
        $abisi = (int) $abisiX;

        //effDate
        $formatted_effDate = DateTime::createFromFormat('Y-m-d', $vehicleInfo->nxtPolEffDate)->format('d/m/Y');

        //expDate
        $formatted_expDate = DateTime::createFromFormat('Y-m-d', $vehicleInfo->nxtPolExpDate)->format('d/m/Y');

        //Dob 
        $dob = DateTime::createFromFormat('Y-m-d', $cif->dob)->format('d/m/Y');
        
        $params = [
            'uuid' => $uuid, 
            'quotationNo' => $zurichQuoteInfo->quotationNo,
            'newId' => $pra->reg_no, 
            'ic' => $pra->ic, 
            'vehicleNo' => $pra->vehicle, 
            'email' => $pra->email, 
            'fullname' => $cif->fullname, 
            'phone' => $cif->phone, 
            'address1' => $cif->address1, 
            'address2' => $cif->address2, 
            'state' => $postcode->stateCode, 
            'dateOfBirth' => $dob,
            'gender' => $cif->gender,
            'age' => $cif->age,
            'uom' => $zurichvariant->uom,
            'capacity' => $zurichvariant->capacity,
            'engineNo' => $vehicleInfo->vehEngineNo,
            'chassisNo' => $vehicleInfo->vehChassisNo,
            'abisi' => $abisi,
            'yearOfMake' => $vehicleInfo->vehMakeYear,
            'make' => $vehicleInfo->vehMake,
            'model' => $zurichvariant->vehModelCode,
            'noOfPassenger' => $zurichvariant->vehSeat,
            'effDate' => $formatted_effDate,
            'expDate' => $formatted_expDate,
            'postCode' => $cif->postcode,
            'regionCode' => $postcode->regioncode,
            'state' => $postcode->stateCode,
            'country' => $postcode->countryCode,
            
        ];

        //dd($params);
        
        try{
            $response = app('App\Http\Controllers\ZurichApiController')->calculatepremiumGenerateQuote($params);
            
            alert()->success('success','Successfully sent to partner');
            
            return back();

        }catch (Exception $e) {

            dd($e);

            alert()->error('error','Error occurred while sent to partner');

            return back()->withInput()->with('error', 'error 303 : error sent to partner');

        }
    }


    public function issueCovernote($uuid)
    {
        
        $zurichQuoteInfo = ZurichApiCalculatePremium_quotationInfo::where('uuid', $uuid)->orderBy('id', 'DESC')->first();
        $pra = Praaplication::where('uuid', $uuid)->orderBy('id', 'DESC')->first();

        $params = [
            "transactionReferenceNo" => "T".$pra->$pra,
            "quotationNumber" => $zurichQuoteInfo->quotationNo,
            "emailTo" => "wakudiallah05@gmail.com",
        ];

        //dd()
       
       

        try{
            $response = app('App\Http\Controllers\ZurichApiController')->issuecovernote($params);
            
            alert()->success('success','Successfully issue covernote');

            //$response
            
            return back();

        }catch (Exception $e) {

            dd($e);

            alert()->error('error','Error occurred while sent to partner');

            return back()->withInput()->with('error', 'error 303 : error sent to partner');

        }
    }


    public function jpjStatus($uuid)
    {
        try{
            $response = app('App\Http\Controllers\ZurichApiController')->getjpjstatus($params);
            
            alert()->success('success','Successfully issue covernote');

            //$response
            
            return back();

        }catch (Exception $e) {

            dd($e);

            alert()->error('error','Error occurred while sent to partner');

            return back()->withInput()->with('error', 'error 303 : error sent to partner');

        }
    }






}
