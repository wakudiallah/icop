<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\Praaplication;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\ApplicationSelectInsurance;
use App\Models\ApplicationAdditionalProtect;
use App\Models\ApplicationFinancing;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ApplicationDsr;
use App\Models\ApplicationGajiPotongan;
use App\Models\ApplicationGajiPendapatan;
use App\Models\ApplicationRemark;
use App\Models\SetupBpaCharge;
use App\Models\InsuranceExtraCover;
use Auth;

class PraaplicationFinanceController extends Controller
{
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index()
    {
        $data = Praaplication::orderBy('id','DESC')
                ->where('pay_status', 'F')  
                ->where('app_status', 'NW')              
                ->get();

        

        
        return view('pages.backend.praapplicationfinance.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'remark'=>'required|max:200',
        ]);

        $uuid = $request->uuid;
        $gaji_pokok = $request->gaji_pokok ? str_replace(',', '', $request->gaji_pokok) : null;
        $imbuhan_tetap_perumahan = $request->imbuhan_tetap_perumahan ? str_replace(',', '', $request->imbuhan_tetap_perumahan) : null;
        $imbuhan_tetap_khidmat_awam = $request->imbuhan_tetap_khidmat_awam ? str_replace(',', '', $request->imbuhan_tetap_khidmat_awam) : null;
        $bantuan_sara_hidup = $request->bantuan_sara_hidup ? str_replace(',', '', $request->bantuan_sara_hidup) : null;
        $bayaran_insentif_tugas_khas = $request->bayaran_insentif_tugas_khas ? str_replace(',', '', $request->bayaran_insentif_tugas_khas) : null;
        $perkhidmatan_kritikal = $request->perkhidmatan_kritikal ? str_replace(',', '', $request->perkhidmatan_kritikal) : null;
        $elaun_tetap_lain_lain = $request->elaun_tetap_lain_lain ? str_replace(',', '', $request->elaun_tetap_lain_lain) : null;
        $insentif = $request->insentif ? str_replace(',', '', $request->insentif) : null;
        $penanggungan_kerja = $request->penanggungan_kerja ? str_replace(',', '', $request->penanggungan_kerja) : null;
        $jumlah_pendapatan = $request->jumlah_pendapatan ? str_replace(',', '', $request->jumlah_pendapatan) : null;
        $percentage = $request->percentage ? str_replace(',', '', $request->percentage) : null;
        $max_eligibility = $request->max_eligibility ? str_replace(',', '', $request->max_eligibility) : null;
        $available_deduction = $request->available_deduction ? str_replace(',', '', $request->available_deduction) : null;
        $jml_bersih = $request->jml_bersih ? str_replace(',', '', $request->jml_bersih) : null;


        $pcb = $request->pcb ? str_replace(',', '', $request->pcb) : null;
        $perkeso = $request->perkeso ? str_replace(',', '', $request->perkeso) : null;
        $cukai_pendapatan = $request->cukai_pendapatan ? str_replace(',', '', $request->cukai_pendapatan) : null;
        $pinjaman_perumahan = $request->pinjaman_perumahan ? str_replace(',', '', $request->pinjaman_perumahan) : null;
        $zakat = $request->zakat ? str_replace(',', '', $request->zakat) : null;
        $lemabaga_tabung_haji = $request->lemabaga_tabung_haji ? str_replace(',', '', $request->lemabaga_tabung_haji) : null;
        $pembiayaan_perumahan_lain_lain = $request->pembiayaan_perumahan_lain_lain ? str_replace(',', '', $request->pembiayaan_perumahan_lain_lain) : null;
        $angkasa = $request->angkasa ? str_replace(',', '', $request->angkasa) : null;
        $koperasi = $request->koperasi ? str_replace(',', '', $request->koperasi) : null;
        $piutang_gaji_elaun = $request->piutang_gaji_elaun ? str_replace(',', '', $request->piutang_gaji_elaun) : null;
        $angkasa_bukan_pinjaman = $request->angkasa_bukan_pinjaman ? str_replace(',', '', $request->angkasa_bukan_pinjaman) : null;
        $yir = $request->yir ? str_replace(',', '', $request->yir) : null;
        $yir2 = $request->yir2 ? str_replace(',', '', $request->yir2) : null;

        $jumlah_potongan = $request->jumlah_potongan ? str_replace(',', '', $request->jumlah_potongan) : null;


        $dsr_baru_persen = $request->dsr_percent ? str_replace(',', '', $request->dsr_percent) : null;
        $dsr_baru = $request->dsr ? str_replace(',', '', $request->dsr) : null;

        
        $apDsr = ApplicationDsr::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $gajiPot = ApplicationGajiPotongan::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $gaji = ApplicationGajiPendapatan::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $remark = ApplicationRemark::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        

        if($gaji){
                $gaji->update([
                        'gaji_pokok' => $gaji_pokok,
                        'imbuhan_tetap_perumahan' => $imbuhan_tetap_perumahan,
                        'imbuhan_tetap_khidmat_awam' => $imbuhan_tetap_khidmat_awam,
                        'bantuan_sara_hidup' => $bantuan_sara_hidup,
                        'bayaran_insentif_tugas_khas' => $bayaran_insentif_tugas_khas,
                        'perkhidmatan_kritikal' => $perkhidmatan_kritikal,
                        'elaun_tetap_lain_lain' => $elaun_tetap_lain_lain,
                        'insentif' => $insentif,
                        'penanggungan_kerja' => $penanggungan_kerja,
                        'jumlah_pendapatan' => $jumlah_pendapatan,

                        'percentage' => $percentage,
                        'max_eligibility' => $max_eligibility,
                        'available_deduction' => $available_deduction,
                        'jml_bersih' => $jml_bersih,
                ]);
        }else{
                ApplicationGajiPendapatan::create([
                        'uuid' => $uuid,
                        'gaji_pokok' => $gaji_pokok,
                        'imbuhan_tetap_perumahan' => $imbuhan_tetap_perumahan,
                        'imbuhan_tetap_khidmat_awam' => $imbuhan_tetap_khidmat_awam,
                        'bantuan_sara_hidup' => $bantuan_sara_hidup,
                        'bayaran_insentif_tugas_khas' => $bayaran_insentif_tugas_khas,
                        'perkhidmatan_kritikal' => $perkhidmatan_kritikal,
                        'elaun_tetap_lain_lain' => $elaun_tetap_lain_lain,
                        'insentif' => $insentif,
                        'penanggungan_kerja' => $penanggungan_kerja,
                        'jumlah_pendapatan' => $jumlah_pendapatan,

                        'percentage' => $percentage,
                        'max_eligibility' => $max_eligibility,
                        'available_deduction' => $available_deduction,
                        'jml_bersih' => $jml_bersih,
                ]);
        }


        if($gajiPot){
                $gajiPot->update([
                        'pcb' => $pcb,
                        'perkeso' => $perkeso,
                        'cukai_pendapatan' => $cukai_pendapatan,
                        'pinjaman_perumahan' => $pinjaman_perumahan,
                        'zakat' => $zakat,
                        'lemabaga_tabung_haji' => $lemabaga_tabung_haji,
                        'pembiayaan_perumahan_lain_lain' => $pembiayaan_perumahan_lain_lain,
                        'angkasa' => $angkasa,
                        'koperasi' => $koperasi,
                        'piutang_gaji_elaun' => $piutang_gaji_elaun,
                        'angkasa_bukan_pinjaman' => $angkasa_bukan_pinjaman,
                        'yir' => $yir,
                        'yir2' => $yir2,
                        'jumlah_potongan' => $jumlah_potongan,
                ]);
        }else{
                ApplicationGajiPotongan::create([
                        'uuid' => $uuid,
                        'pcb' => $pcb,
                        'perkeso' => $perkeso,
                        'cukai_pendapatan' => $cukai_pendapatan,
                        'pinjaman_perumahan' => $pinjaman_perumahan,
                        'zakat' => $zakat,
                        'lemabaga_tabung_haji' => $lemabaga_tabung_haji,
                        'pembiayaan_perumahan_lain_lain' => $pembiayaan_perumahan_lain_lain,
                        'angkasa' => $angkasa,
                        'koperasi' => $koperasi,
                        'piutang_gaji_elaun' => $piutang_gaji_elaun,
                        'angkasa_bukan_pinjaman' => $angkasa_bukan_pinjaman,
                        'yir' => $yir,
                        'yir2' => $yir2,
                        'jumlah_potongan' => $jumlah_potongan,
                        
                ]);
        }


        if($apDsr){
                $apDsr->update([
                        'dsr_baru_persen' => $dsr_baru_persen,
                        'dsr_baru' => $dsr_baru,
                ]);
        }else{
                ApplicationDsr::create([
                        'uuid' => $uuid,
                        'dsr_baru_persen' => $dsr_baru_persen,
                        'dsr_baru' => $dsr_baru,
                ]);
        }


        if($remark){
                $remark->update([
                        'r_praapplication' => $request->remark,
                ]);
        }else{
                ApplicationRemark::create([
                        'uuid' => $uuid,
                        'r_praapplication' => $request->remark,
                ]);
        }

        $apDsr = Praaplication::where('uuid', $uuid)->update([
                'app_status' => 'DC',
        ]);



        //ApplicationGajiPendapatan::create

        if($gaji){
                alert()->success('success','Successfully added Praapplication');
        } 
        else{
                alert()->error('error','Error occurred while adding Praapplication');
        }

        return redirect()->route('pra-finance.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        
        $pra = Praaplication::join('insurance_basic_details','praaplications.uuid', '=' ,'insurance_basic_details.uuid')
                ->join('insurance_basic_premia', 'insurance_basic_premia.uuid', '=', 'praaplications.uuid')
                ->join('application_customer_infos', 'application_customer_infos.uuid', '=', 'praaplications.uuid')
                ->join('insurance_extra_covers', 'insurance_extra_covers.uuid', '=', 'praaplications.uuid')
                ->join('application_financings', 'application_financings.uuid', '=', 'praaplications.uuid')
                ->join('application_documents', 'application_documents.uuid', '=', 'praaplications.uuid')
                ->orderBy('praaplications.id', 'DESC') 
                ->select('praaplications.reg_no', 'praaplications.vehicle', 'praaplications.ic', 'praaplications.email',
                        'insurance_basic_details.transType', 'insurance_basic_details.productCode', 'insurance_basic_details.effDate', 'insurance_basic_details.expDate', 'insurance_basic_details.yearOfMake', 'insurance_basic_details.make', 'insurance_basic_details.model', 'insurance_basic_details.capacity', 'insurance_basic_details.uom', 'insurance_basic_details.dateOfBirth', 'insurance_basic_details.age', 'insurance_basic_details.gender', 'insurance_basic_details.maritalSts', 'insurance_basic_details.occupation', 'insurance_basic_details.chassisNo', 'insurance_basic_details.engineNo',
                        'insurance_basic_premia.stampDutyAmt', 'insurance_basic_premia.gst_Pct', 'insurance_basic_premia.ncdAmt', 'insurance_basic_premia.totBasicNetAmt',
                        'application_customer_infos.fullname', 'application_customer_infos.phone', 'application_customer_infos.address1', 'application_customer_infos.address2', 'application_customer_infos.dob', 'application_customer_infos.age', 'application_customer_infos.marital_status', 'application_customer_infos.gender', 'application_customer_infos.postcode', 'application_customer_infos.state',
                        'insurance_extra_covers.extCoverCode', 'insurance_extra_covers.extCoverPrem',
                        'application_financings.deduc_schema', 'application_financings.monthly_installment',
                        'application_documents.nric', 'application_documents.payslip', 'application_documents.bpa179')
                ->where('praaplications.uuid', $uuid) 
                ->where('insurance_extra_covers.extCoverCode', 'XX')
                ->where('praaplications.app_status', 'NW') //is New App 
                ->where('pay_status', 'F')             
                ->first();
        
        
        $doc = ApplicationDocument::where('uuid', $uuid)
                ->orderBy('id', 'DESC')               
                ->first();



        $extraCover = InsuranceExtraCover::where('uuid', $uuid)
                ->where('status', 1)
                ->get();

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);

        $totExtraCover = $calcuAddProtect['extraCover'];
        $payable = $calcuAddProtect['payable'];
        $totalSst = $calcuAddProtect['totalSst'];


        $addProtect = ApplicationAdditionalProtect::where('uuid', $uuid)
                ->orderBy('id', 'DESC')               
                ->first(); 

        
        $bpaFee = SetupBpaCharge::orderBy('id', 'Desc')->first();

       
        if($payable > 1000){
                $processFee = $payable * ( 5 / 100 );
                
        }else{
                $processFee = 50;
        }

        $bpaCharge = $bpaFee->bpa_charges;
        $totalCalculateMonthly = ($payable + $processFee) / $pra->deduc_schema;
        $monthlyInstallment = $totalCalculateMonthly + ($totalCalculateMonthly * ($bpaFee->bpa_charges/100)) ;
        
        
        
        /*--- YANG LAMA calculate Monthly Installment  ----*/
        /* if($payable > 1000){
                $processFee = 100;

        }else{
                $processFee = 50;
        }

        $bpaCharge = ($payable + $processFee) * ($bpaFee->bpa_charges/100) * $pra->deduc_schema / 12;
        $totalCalculateMonthly = $payable + $processFee + $bpaCharge;
        $monthlyInstallment =  $totalCalculateMonthly / $pra->deduc_schema;  */
        /*--- End calculate Monthly Installment ----*/

        

        $finacing = ApplicationFinancing::where('uuid', $uuid)->update([
            
            'after_cal_monthly_installment' => $monthlyInstallment,
            'processing_fee' => $processFee,
            'bpa_charges' => $bpaCharge
            
        ]);


        return view('pages.backend.praapplicationfinance.show', compact('pra', 'uuid', 
                         'addProtect', 'extraCover', 'doc',
                        'totExtraCover', 'payable',  'totalSst', 'bpaFee', 'bpaCharge',  'totalCalculateMonthly',
                        'monthlyInstallment', 'processFee'    
                ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
