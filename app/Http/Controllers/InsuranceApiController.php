<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use App\Models\PartnerApiAccount;
use App\Models\InsuranceGetVehicleInfo;
use App\Models\InsuranceBasicPremium;
use App\Models\InsuranceBasicDetail;
use App\Models\InsuranceExtraCover;
use App\Models\InsuranceAllowableExtraCover;
use App\Models\InsuranceExtraCoverData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class InsuranceApiController extends Controller
{
    //
    private $token;

    public function WeatherForecast()
    {
        $client = new Client();
        $url = 'https://sasnets.com/insurance_api/WeatherForecast';
        $headers = [
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess',
        ];

        try {
            $response = $client->request('GET', $url, [
                'headers' => $headers
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody();
            $data = json_decode($body, true);

            return $data;

        } catch (\Exception $e) {
            // Handle exceptions or errors
            return response()->json([
                'error' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }


    }


    
    public function GetVehicleInfo($vehicle, $icFormat, $uuid)
    {
        $client = new Client();
        $url = 'https://sasnets.com/insurance_api/api/Zurich/GetVehicleInfo';


        $headers = [
            'accept' => 'text/plain',
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess',
            'Content-Type' => 'application/json',
        ];

        // Prepare the data to be sent in the POST request
        $requestData = [
            'id' => 'string',
            'vehNo' => $vehicle,
            'nric' => $icFormat,
        ];

      
        try {
            // Make the POST request with headers and JSON body
            $response = $client->post($url, [
                'headers' => $headers,
                'json' => $requestData
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody();
            $data = json_decode($body, true);


            //dd($data);

            
            // Process the response data as needed
            $getVehicleInfo = [
                'eId' => $data['data']['eId'] ?? null,
                'vehRegNo' => $data['data']['vehRegNo'] ?? null,
                'vehChassisNo' => $data['data']['chasisNo'] ?? null,
                'vehEngineNo' => $data['data']['engineNo'] ?? null,
                'vehMakeYear' => $data['data']['yearMake'] ?? null,
                'brand' => $data['data']['brand'] ?? null,
                'model' => $data['data']['model'] ?? null,
                'variant' => $data['data']['variant'] ?? null,
                'engineSize' => $data['data']['vehCapacity'] ?? null,
                'cc' => $data['data']['uom'] ?? null,
                'ncd' => $data['data']['ncd'] ?? null,
                'listVehInfoVariant' => $data['data']['listVehInfoVariant'] ?? null
            ];


            return $getVehicleInfo;

        } catch (\Exception $e) {

            
            // Handle exceptions or errors
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            //dd($e);
            return response()->json([
                'error' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }

    }


    public function basicPremium($params){
        
        $client = new Client();
        $url = 'https://sasnets.com/insurance_api/api/Zurich/GetCalculatePremiumForClient';

        // Prepare the data to send in the POST request
        
        //dd($params['dateOfBirth']);
        
        $requestData = [
            'eId' => $params['eId'],
            'nric' => $params['nric'],
            'fullName' => $params['fullName'],
            'hpNo' => $params['hpNo'],
            'email' => $params['email'],
            'maritalStatus' => $params['maritalStatus'],
            'gender' => $params['gender'],
            'dateOfBirth' => $params['dateOfBirth'],
            'add1' => $params['add1'],
            'add2' => $params['add2'],
            //'add3' => $params['add3'],
            'postCode' => $params['postCode'],
            //'city' => $params['eId'],
            'state' => $params['state'],
            //'country' => $params['eId']
        ];

        // Set the headers
        $headers = [
            'accept' => 'application/json',
            'Content-Type' => 'application/json',
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
        ];

        try {
            // Send the POST request
            $response = $client->post($url, [
                'headers' => $headers,
                'json' => $requestData
            ]);

            // Process the response
            $statusCode = $response->getStatusCode();
            $body = $response->getBody();
            $data = json_decode($body, true);


            //dd($data);
            
            //dd($allowableExtCvrArray);

            //dd($data['data']['basicDetails']['engineNo']);   ???
            //dd($data['data']['basicDetails']['logBookNo']); ??
            //dd($data['data']['basicDetails']['insNationality']);
            //dd($data['data']['basicDetails']['mobileNo']);
            //dd($data['data']['basicDetails']['address']);

            //dd($data['data']['basicDetails']['address']);


            $dateOfBirthInput = $data['data']['basicDetails']['dateOfBirth'] ?? null;
            $effDateInput = $data['data']['basicDetails']['effDate'] ?? null;
            $expDateInput = $data['data']['basicDetails']['expDate'] ?? null;
            $transType = $data['data']['basicDetails']['transType'] ?? null;

            //dd($effDateInput);

            if ($dateOfBirthInput) {
                
                $dateOfBirth = Carbon::createFromFormat('d/m/Y', $dateOfBirthInput)->format('Y-m-d');
            } else {
                
                $dateOfBirth = null;
            }


            if ($effDateInput) {
                
                $effDate = Carbon::createFromFormat('d/m/Y', $effDateInput)->format('Y-m-d');
            } else {
                
                $effDate = null;
            }


            if ($expDateInput) {
                
                $expDate = Carbon::createFromFormat('d/m/Y', $expDateInput)->format('Y-m-d');
            } else {
                
                $expDate = null;
            }


            InsuranceBasicDetail::updateOrCreate(
                ['uuid' => $params['uuid']],
                [
                    'transType' => $transType,
                    'vehicleNo' => $data['data']['basicDetails']['vehicleNo'] ?? null,
                    'productCode' => $data['data']['basicDetails']['productCode'] ?? null,
                    'coverType' => $data['data']['basicDetails']['coverType'] ?? null,
                    'ciCode' => $data['data']['basicDetails']['ciCode'] ?? null,
                    'effDate' => $effDate,
                    'expDate' => $expDate,
                    'reconInd' => $data['data']['basicDetails']['reconInd'] ?? null,
                    'yearOfMake' => $data['data']['basicDetails']['yearOfMake'] ?? null,
                    'make' => $data['data']['basicDetails']['make'] ?? null,
                    'model' => $data['data']['basicDetails']['model'] ?? null,
                    'capacity' => $data['data']['basicDetails']['capacity'] ?? null,
                    'uom' => $data['data']['basicDetails']['uom'] ?? null,
                    'engineNo' => $data['data']['basicDetails']['engineNo'] ?? null,
                    'chassisNo' => $data['data']['basicDetails']['chassisNo'] ?? null,
                    'logBookNo' => $data['data']['basicDetails']['logBookNo'] ?? null,
                    'regLoc' => $data['data']['basicDetails']['regLoc'] ?? null,
                    'regionCode' => $data['data']['basicDetails']['regionCode'] ?? null,
                    'noOfPassenger' => $data['data']['basicDetails']['noOfPassenger'] ?? null,
                    'noOfDrivers' => $data['data']['basicDetails']['noOfDrivers'] ?? null,
                    'insIndicator' => $data['data']['basicDetails']['insIndicator'] ?? null,
                    'name' => $data['data']['basicDetails']['name'] ?? null,
                    'insNationality' => $data['data']['basicDetails']['insNationality'] ?? null,
                    'newIC' => $data['data']['basicDetails']['newIC'] ?? null,
                    'othersID' => $data['data']['basicDetails']['othersID'] ?? null,
                    'dateOfBirth' => $dateOfBirth,
                    'age' => $data['data']['basicDetails']['age'] ?? null,
                    'gender' => $data['data']['basicDetails']['gender'] ?? null,
                    'maritalSts' => $data['data']['basicDetails']['maritalSts'] ?? null,
                    'occupation' => $data['data']['basicDetails']['occupation'] ?? null,
                    'mobileNo' => $data['data']['basicDetails']['mobileNo'] ?? null,
                    'address' => $data['data']['basicDetails']['address'] ?? null,
                    'postCode' => $data['data']['basicDetails']['postCode'] ?? null,
                    'state' => $data['data']['basicDetails']['state'] ?? null,
                    'country' => $data['data']['basicDetails']['country'] ?? null,
                    'sumInsured' => $data['data']['basicDetails']['sumInsured'] ?? null,
                    'abisi' => $data['data']['basicDetails']['abisi'] ?? null,
                    'chosen_SI_Type' => $data['data']['basicDetails']['chosen_SI_Type'] ?? null,
                    'avInd' => $data['data']['basicDetails']['avInd'] ?? null,
                    'pacInd' => $data['data']['basicDetails']['pacInd'] ?? null,
                    'pacUnit' => $data['data']['basicDetails']['pacUnit'] ?? null,
                    'all_Driver_Ind' => $data['data']['basicDetails']['all_Driver_Ind'] ?? null
                ]
            );


            InsuranceBasicPremium::updateOrCreate(
                ['uuid' => $params['uuid']],
                [
                    'success' => $data['data']['success'] ?? null,
                    'errorMsg' => $data['data']['errorMsg'] ?? null,
                    'quotationNo' => $data['data']['quotationNo'] ?? null,
                    'totBasicNetAmt' => $data['data']['totBasicNetAmt'] ?? null,
                    'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                    'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                    'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                    'rebatePct' => $data['data']['rebatePct'] ?? null,
                    'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                    'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                    'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                    'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                    'ncd_Pct' => $data['data']['ncd_Pct'] ?? null,
                    'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                ]
            );

            
            // Assuming $params is the array containing the 'respParamFromGetQuotationVM'
            $allowableExtCvrArray = $data['data']['respParamFromGetQuotationVM']['allowableExtCvr'] ?? null;

            if (!is_null($allowableExtCvrArray)) {
                foreach ($allowableExtCvrArray as $extCvrData) {
                    InsuranceAllowableExtraCover::updateOrCreate(
                        [
                            'ext_Cvr_Code' => $extCvrData['ext_Cvr_Code'] ?? null, // Assuming 'uuid' is the unique identifier
                        ],
                        [
                            'uuid' => $params['uuid'],
                            'ext_Cvr_Desc' => $extCvrData['ext_Cvr_Desc'] ?? null,
                            'applicable_Ind' => $extCvrData['applicable_Ind'] ?? null,
                            'compulsory_Ind' => $extCvrData['compulsory_Ind'] ?? null,
                            'recommended_Ind' => $extCvrData['recommended_Ind'] ?? null,
                            'endorsement_Ind' => $extCvrData['endorsement_Ind'] ?? null,
                            'eC_Input_Type' => $extCvrData['eC_Input_Type'] ?? null,
                        ]
                    );
                }
            }


            // Return the response data
            $returnCalculate = [

                'basicPrem' => $data['data']['totBasicNetAmt'] ?? null,
                'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                'rebatePct' => $data['data']['rebatePct'] ?? null,
                'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                'ncd_Pct' => $data['data']['ncdAmt'] ?? null,
                'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                
            ];

            return $returnCalculate;


        } catch (\Exception $e) {
            // Handle any errors

            //dd($e);
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return response()->json([
                'error' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }
    }



    public function addCoverage($params)
    {   

       $uuid = $params['uuid'];

       if (isset($params['sumInsured'])) {
            $sumInsured = (int) $params['sumInsured'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $sumInsured = 0;
        }

        if (isset($params['noofunit'])) {
            $noofunit = $params['noofunit'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $noofunit = 0;
        }

        if (isset($params['unitDay'])) {
            $unitDay = $params['unitDay'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $unitDay = 0;
        }

        if (isset($params['unitAmount'])) {
            $unitAmount = $params['unitAmount'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $unitAmount = 0;
        }

        


        $client = new Client();
        $url = 'https://sasnets.com/insurance_api/api/Zurich/checkextracovercharges';

       //dd($params['extCovCode']);
        $requestData = [
            'quotationNo' => $params['quotationNo'],
            'reqGetCalculatePremimumForClientVM' => [
                "eId" =>  $params['eId'],
                "nric" => $params['nric'], // Static value for now
                "fullName" => $params['fullName'],
                "hpNo" => $params['hpNo'],
                "email" => $params['email'],
                "maritalStatus" => $params['maritalStatus'], // M (married)
                "gender" => $params['gender'],
                "dateOfBirth" => $params['dateOfBirth'], // Static date format "YYYY-MM-DD"
                "add1" => $params['add1'],
                "add2" => $params['add2'],
                "add3" => $params['add3'], // Same as add2
                "postCode" => $params['postCode'],
                "city" => $params['city'], // Static for now, change if dynamic
                "state" => $params['state'], // Static for now, change if dynamic
                "country" => $params['country'], // Static for now, change if needed
            ],
            'extraCoverDetails' => [
                'extraCoverData' => [
                    [
                        'quotationNo' => "",
                        'chassisNo' => "",
                        'extCovCode' => $params['extCovCode'], 
                        'unitDay' => $unitDay,
                        'unitAmount' => $unitAmount,
                        'effDate' => '', // Empty for now
                        'expDate' => '', // Empty for now
                        'sumInsured' => $sumInsured,
                        'noOfUnit' => $noofunit,
                        "extCovDesc" => "" // Empty for now
                    ],

                ]
            ]
        ];

        $headers = [
            'accept' => 'application/json',
            'Content-Type' => 'application/json',
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
        ];


        try {
            // Send the POST request
            $response = $client->post($url, [
                'headers' => $headers,
                'json' => $requestData
            ]);

            // Process the response
            $statusCode = $response->getStatusCode();
            $body = $response->getBody();
            $data = json_decode($body, true);

            

            //dd($data['data'][0]['eId']);
            
            $extCoverPrem = $data['data'][0]['extCoverPrem'] ?? null;
            $extCoverCode = $data['data'][0]['extCoverCode'] ?? null;
            $extCoverSumInsured = $data['data'][0]['extCoverSumInsured'] ?? null;

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            if ($extCoverSumInsured !== null) {
                // Remove commas and convert to float
                $extCoverSumInsured = floatval(str_replace(',', '', $extCoverSumInsured));
            }


            //InsuranceExtraCover
            if (!is_null($extCoverCode)) {
                InsuranceExtraCover::updateOrCreate(
                    [
                        'uuid' => $uuid,
                        'extCoverCode' => $extCoverCode
                    ],
                    [
                        'extCoverSumInsured' => $extCoverSumInsured,
                        'extCoverPrem' => $extCoverPrem,
                        'compulsory_Ind' => $data['data'][0]['compulsory_Ind'] ?? null,
                        'status' => 1,
                    ]
                );

                InsuranceGetVehicleInfo::where('uuid', $uuid)->update(['eId' => $data['data'][0]['eId']] );
            }


            //insuranceExtraCoverData
            if (!is_null($extCoverCode)) {
                InsuranceExtraCoverData::updateOrCreate(
                    [
                        'uuid' => $uuid,
                        'extCovCode' => $extCoverCode
                    ],
                    [
                        'unitDay' => $unitDay,
                        'unitAmount' => $unitAmount,
                        'effDate' => '',
                        'expDate' => '',
                        'sumInsured' => $sumInsured,
                        'noOfUnit' => $noofunit,
                        'extCovDesc' => ''
                    ]
                );

            }


            


            $returnCalculate = [

                'extCode' => $extCoverCode,
                'premium' => $extCoverPrem,
                
            ];

            //dd($returnCalculate);
            return $returnCalculate;

            

        } catch (\Exception $e) {
            // Handle any errors

            dd($e);
            Log::error('addCoverage:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return response()->json([
                'error' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }

    }



    public function GetFinalPremium($params)
    {   

       $uuid = $params['uuid'];

       if (isset($params['sumInsured'])) {
            $sumInsured = (int) $params['sumInsured'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $sumInsured = 0;
        }

        if (isset($params['noofunit'])) {
            $noofunit = $params['noofunit'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $noofunit = 0;
        }

        if (isset($params['unitDay'])) {
            $unitDay = $params['unitDay'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $unitDay = 0;
        }

        if (isset($params['unitAmount'])) {
            $unitAmount = $params['unitAmount'];
        } else {
            // Handle the case when $params['sumInsured'] does not exist
            $unitAmount = 0;
        }

        $coverData = InsuranceExtraCoverData::where('insurance_extra_covers.uuid', $uuid)
        ->join('insurance_extra_covers', 'insurance_extra_cover_data.extCovCode', '=', 'insurance_extra_covers.extCoverCode')
        ->where('insurance_extra_covers.status', 1)
        ->select('insurance_extra_cover_data.extCovCode', 'insurance_extra_cover_data.unitDay', 'insurance_extra_cover_data.unitAmount',  'insurance_extra_cover_data.effDate', 'insurance_extra_cover_data.expDate', 'insurance_extra_cover_data.sumInsured', 'insurance_extra_cover_data.noOfUnit')
        ->get();
    
        $client = new Client();
        $url = 'https://sasnets.com/insurance_api/api/Zurich/GetFinalPremium';
        
        // Build the base request data
        $requestData = [
            'quotationNo' => $params['quotationNo'],
            'reqGetCalculatePremimumForClientVM' => [
                "eId" =>  $params['eId'],
                "nric" => $params['nric'],
                "fullName" => $params['fullName'],
                "hpNo" => $params['hpNo'],
                "email" => $params['email'],
                "maritalStatus" => $params['maritalStatus'],
                "gender" => $params['gender'],
                "dateOfBirth" => $params['dateOfBirth'],
                "add1" => $params['add1'],
                "add2" => $params['add2'],
                "add3" => $params['add3'],
                "postCode" => $params['postCode'],
                "city" => $params['city'],
                "state" => $params['state'],
                "country" => $params['country'],
            ],
            'extraCoverDetails' => [
                'extraCoverData' => []  // Start with an empty array
            ]
        ];
        
        // Iterate over $coverData and build the extraCoverData array
        foreach ($coverData as $cover) {
            $requestData['extraCoverDetails']['extraCoverData'][] = [
                'chassisNo' => "",    // Set appropriate values or leave empty
                'extCovCode' => $cover->extCovCode,  // Using the value from $coverData
                'unitDay' => $cover->unitDay,  
                'unitAmount' => $cover->unitAmount,  
                'effDate' => $cover->effDate,  
                'expDate' => $cover->expDate,  
                'sumInsured' => $cover->sumInsured,  
                'noOfUnit' => $cover->noOfUnit,  
                "extCovDesc" => ""  // Set appropriate values or leave empty
            ];
        }
        
        // Define the request headers
        $headers = [
            'accept' => 'application/json',
            'Content-Type' => 'application/json',
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
        ];
        
        try {
            // Send the POST request
            $response = $client->post($url, [
                'headers' => $headers,
                'json' => $requestData
            ]);
        
            // Process the response
            $statusCode = $response->getStatusCode();
            $body = $response->getBody();
            $data = json_decode($body, true);
        
            dd($data);
        
        } catch (\Exception $e) {
            // Handle any errors
            Log::error('addCoverage:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            return response()->json([
                'error' => 'An error occurred: ' . $e->getMessage()
            ], 500);
        }

    }
    
    




}
