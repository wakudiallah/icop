<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Audit;
use Spatie\Permission\Models\Permission;


class AuditTrailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:audit-trail-list|audit-trail-create|audit-trail-edit|audit-trail-delete', ['only' => ['index','show']]);
        $this->middleware('permission:audit-trail-create', ['only' => ['create','store']]);
        $this->middleware('permission:audit-trail-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:audit-trail-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        $audit = Audit::orderBy('id','DESC')->get();
        
        return view('pages.backend.audits.index', compact('audit'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



    public function destroy($id)
    {
        
        
        $brand=Audit::find($id);
        
        if($brand){
            $status=$brand->delete();
            

            if($status){
                //request()->session()->flash('success','Audit successfully deleted');
                alert()->success('success','Audit successfully deleted');
            }
            else{
                //request()->session()->flash('error','Error, Please try again');
                alert()->error('error','Error, Please try again');
            }
            return redirect()->back();
            
        }
        else{
            
            alert()->error('error','Audit not found');
            return redirect()->back();
        }
    }



}
