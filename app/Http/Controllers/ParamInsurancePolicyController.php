<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInsurancePolicy;
use App\Models\ParamInsuranceCompany;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamInsurancePolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-insurance-policy-list|param-insurance-policy-create|param-insurance-policy-edit|param-insurance-policy-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-insurance-policy-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-insurance-policy-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-insurance-policy-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        $data = ParamInsurancePolicy::get();
        
        return view('pages.backend.paraminsurancepolicy.index', compact('data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //

        $insCompany = ParamInsuranceCompany::where('status', 1)->get();

        return view('pages.backend.paraminsurancepolicy.create', compact('insCompany'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'insurance_company_id'=>'required|max:80',
            'product_disclosure'=>'required',
            'disclosure_published'=>'nullable|max:150',
            'policy_wording'=>'required',
            'policy_published'=>'nullable|max:150',
            'status'=>'required',
        ]);

        $data=$request->all();

        $status = ParamInsurancePolicy::create($data);

        if($status){
            alert()->success('success','Successfully added param insurance policy');
        } 
        else{
            alert()->error('error','Error occurred while adding param insurance policy');
        }
        return redirect()->route('param-insurance-policy.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamInsurancePolicy::find($id);
        $insCompany = ParamInsuranceCompany::where('status', 1)->get();
        
        if(!$data){

            alert()->error('error','Param insurance policy found');
        }
       
        return view('pages.backend.paraminsurancepolicy.edit')
        ->with('data',$data)
        ->with('insCompany',$insCompany);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamInsurancePolicy::find($id);

        $this->validate($request,[
            'insurance_company_id'=>'required|max:80',
            'product_disclosure'=>'required',
            'disclosure_published'=>'nullable|max:150',
            'policy_wording'=>'required',
            'policy_published'=>'nullable|max:150',
            'status' => 'required|in:1,0',
        ]);

       

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param insurance policy successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-insurance-policy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamInsurancePolicy::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param insurance policy successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
