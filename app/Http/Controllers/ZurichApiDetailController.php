<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Carbon\Carbon;
use App\Models\PartnerApiAccount;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ZurichApiCalculatePremium_AllowableCartAmount;
use App\Models\ZurichApiCalculatePremium_AllowableCartDay;
use App\Models\ZurichAllowableCourtesyCarDay;
use App\Models\ZurichApiCalculate_AllowableExtensionCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableKeyReplacement;
use App\Models\ZurichApiCalculatePremium_AllowablePaBasicUnit;
use App\Models\ZurichApiCalculatePremium_AllowablePaExtendedCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableVoluntaryExcess;
use App\Models\ZurichApiCalculatePremium_AllowableWaterDamageCoverage;
use App\Models\ZurichApiCalculatePremium_CarReplacementDay;
use App\Models\ZurichApiCalculatePremium_ErrorDisplay;
use App\Models\ZurichApiCalculatePremium_paCVehicleDetail;
use App\Models\ZurichApiCalculatePremium_premiumDetail;
use App\Models\ZurichApiCalculatePremium_quotationInfo;
use App\Models\ZurichApiCalculatePremium_extraCoverData;
use App\Models\ZurichApiCalculatePremium_allowableCourtesyCar;
use App\Models\ZurichApiCalculatePremium_AllowableExtCvr;
use App\Models\ZurichApiIssueCovernote;
use App\Models\ZurichApiGetjpjstatus;
use App\Models\ZurichApiGetvehiclemake;
use App\Models\ZurichApiGetvehiclemodel;

use App\Models\ZurichApiPostcode;

class ZurichApiDetailController extends Controller
{
    //
    public function index($uuid)
    {
        
       
        
        $vehInfo = ZurichApiGetVehicleInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();
        $vehInfovariant = ZurichApiGetVehicleInfoVariant::where('uuid', $uuid)->get();

        $a = ZurichApiCalculatePremium_AllowableCartAmount::where('uuid', $uuid)->get();
        $b = ZurichApiCalculatePremium_AllowableCartDay::where('uuid', $uuid)->get();
        $c = ZurichApiCalculatePremium_AllowableKeyReplacement::where('uuid', $uuid)->get();
        $d = ZurichApiCalculatePremium_AllowablePaBasicUnit::where('uuid', $uuid)->get();
        $e = ZurichApiCalculatePremium_AllowablePaExtendedCoverage::where('uuid', $uuid)->get();
        $f = ZurichApiCalculatePremium_AllowableVoluntaryExcess::where('uuid', $uuid)->get();
        $g = ZurichApiCalculatePremium_AllowableWaterDamageCoverage::where('uuid', $uuid)->get();
        $h = ZurichApiCalculatePremium_CarReplacementDay::where('uuid', $uuid)->get();
        $i = ZurichApiCalculatePremium_ErrorDisplay::where('uuid', $uuid)->get();
        $j = ZurichApiCalculatePremium_paCVehicleDetail::where('uuid', $uuid)->get();
        $k = ZurichApiCalculatePremium_premiumDetail::where('uuid', $uuid)->get();
        $l = ZurichApiCalculatePremium_quotationInfo::where('uuid', $uuid)->get();
        $m = ZurichApiCalculatePremium_extraCoverData::where('uuid', $uuid)->get();
        $n = ZurichApiCalculatePremium_allowableCourtesyCar::where('uuid', $uuid)->get();
        $o = ZurichApiCalculatePremium_AllowableExtCvr::where('uuid', $uuid)->get();

        
        
        return view('pages.backend.praapplicationDetails.index', compact('uuid', 'vehInfo', 'vehInfovariant', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'));
    }





}
