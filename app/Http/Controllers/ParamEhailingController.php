<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEhailing;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamEhailingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-ehailing-list|param-ehailing-create|param-ehailing-edit|param-ehailing-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-ehailing-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-ehailing-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-ehailing-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamEhailing::get();
        
        return view('pages.backend.paramehailing.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramehailing.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'ehailing'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamEhailing::create($data);

        if($status){
            alert()->success('success','Successfully added param gst');
        } 
        else{
            alert()->error('error','Error occurred while adding param ehailing');
        }
        return redirect()->route('param-ehailing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamEhailing::find($id);
        
        if(!$data){

            alert()->error('error','Param ehailing found');
        }
       
        return view('pages.backend.paramehailing.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamEhailing::find($id);

        $this->validate($request,[
            'ehailing'=>'required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param ehailing successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-ehailing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamEhailing::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param ehailing successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
