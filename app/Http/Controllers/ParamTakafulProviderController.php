<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamTakafulProvider;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamTakafulProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-takaful-provider-list|param-takaful-provider-create|param-takaful-provider-edit|param-takaful-provider-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-takaful-provider-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-takaful-provider-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-takaful-provider-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamTakafulProvider::get();
        
        return view('pages.backend.paramtakafulprovider.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramtakafulprovider.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'takaful_provider'=>'string|required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamTakafulProvider::create($data);

        if($status){
            alert()->success('success','Successfully added param takaful provider');
        } 
        else{
            alert()->error('error','Error occurred while adding param takaful provider');
        }
        return redirect()->route('param-takaful-provider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamTakafulProvider::find($id);
        
        if(!$data){

            alert()->error('error','Param takaful provider not found');
        }
       
        return view('pages.backend.paramtakafulprovider.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamTakafulProvider::find($id);

        $this->validate($request,[
            'takaful_provider'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param takaful provider successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-takaful-provider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamTakafulProvider::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param takaful provider successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
