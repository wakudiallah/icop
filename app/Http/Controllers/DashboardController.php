<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\Events\NewContentNotification;
use Pusher\Pusher;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->user_id = Auth::user()->id;
        $this->middleware('permission:dashboard', ['only' => ['index']]);
         /*$this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy', 'status']]);*/
    }


    public function index()
    {
        //$data = User::get();

        if(Auth::user()->role == 5){    //Company Pembiayaan

            return view('pages.backend.dashboard.comp-dashboard'); 

        }else{


            return view('pages.backend.dashboard.dashboard'); 

        }
        
    }

    public function testnotif()
    {
        $dataSend = [
            'message' => "New Application Submition",
            'from' => "Dial"
        ];
        
        event(new NewContentNotification($dataSend));
    }


    public function privateChanel()
    {

        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );

        $pusher = new Pusher(
            'acf4bdadcc5bac4824d5',
            '7bff4ae40954ca99818a',
            '1822123',
            $options
        );

        $recipientIds = ['2', '4'];    //Role Super Admin & Admin

        $dataSend = [
            'message' => "New Application Submitted",
            'from' => "Dial"
        ];

        foreach($recipientIds as $recipientId){

            $pusher->trigger("private.$recipientId" , 'my-event', $dataSend);

        }
        

    }




}
