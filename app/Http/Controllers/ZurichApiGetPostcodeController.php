<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Praaplication;
use App\Models\ParamProtectionType;
use App\Models\Postcode;
use App\Models\ZurichApiPostcode;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePolicy;
use App\Models\PerlindunganTambahan;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\SetupDocument;
use App\Models\PartnerApiAccount;
use Illuminate\Support\Str;
use Hash;
use App\Events\NewContentNotification;
use Pusher\Pusher;
use Illuminate\Support\Facades\Http;

class ZurichApiGetPostcodeController extends Controller
{
    //

    public function getPostcodeDetails(Request $request)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        $token = $data->token;

        $postcode = $request->input('postcode');
        $uuid = $request->input('uuid');

        //dd($token);

        $client = new Client();

        $headers = [
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess',
        ];

        try {
            
            $response = $client->request('GET', 'https://sasnets.com/insurance_api/api/Zurich/GetInsPostCode/'.$postcode, [
                'headers' => $headers
               
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody['stateCode']);

            ZurichApiPostcode::create([
                'uuid' => $uuid,
                'stateCode' => $decodedBody['stateCode'],
                'stateName' => $decodedBody['stateName'],
                'regioncode' => $decodedBody['regioncode'],
                'countryCode' => $decodedBody['countryCode'],
                'countryName' => $decodedBody['countryName'],
            ]);

            

            //return response()->json($decodedBody['meta']['respCode']);
            
            //return response()->json($decodedBody['data']['stateName']);
            return response()->json(['state' => $decodedBody['stateName']], 200);
            //dd($decodedBody['data']['stateName']);
        } catch (\Exception $e) {

            //dd($e);

            return response()->json(['error' => 'Unable to fetch state'], 500);
       }


       
        
    }


}
