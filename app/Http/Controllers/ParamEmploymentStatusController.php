<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEmploymentType;
use App\Models\ParamEmploymentStatus;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamEmploymentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-employment-status-list|param-employment-status-create|param-employment-status-edit|param-employment-status-delete', ['only' => ['index','show']]);
         $this->middleware('permission:param-employment-status-create', ['only' => ['create','store']]);
         $this->middleware('permission:param-employment-status-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:param-employment-status-delete', ['only' => ['destroy', 'status']]);
    }
    
     public function index()
    {
        $data = ParamEmploymentStatus::get();
        
        return view('pages.backend.paramemploymentstatus.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('pages.backend.paramemploymentstatus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'employment_status'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamEmploymentStatus::create($data);

        if($status){
            alert()->success('success','Successfully added param emploment status');
        }
        else{
            alert()->error('error','Error occurred while adding param emploment status');
        }
        return redirect()->route('param-employment-status.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParamEmploymentStatus::find($id);
        
        if(!$data){

            alert()->error('error','ParamEmployment Status not found');
        }
       
        return view('pages.backend.paramemploymentstatus.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ParamEmploymentStatus::find($id);

        $this->validate($request,[
            'employment_status'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param Employment Status successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-employment-status.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamEmploymentStatus::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param Employment Status successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
