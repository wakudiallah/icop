<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEmailSystem;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamEmailSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-email-system-list|param-email-system-create|param-email-system-edit|param-email-system-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-email-system-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-email-system-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-email-system-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamEmailSystem::get();
        
        return view('pages.backend.paramemailsystem.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramemailsystem.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'email'=>'required|max:80|email|string|unique:param_email_systems',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamEmailSystem::create($data);

        if($status){
            alert()->success('success','Successfully added param email systems');
        } 
        else{
            alert()->error('error','Error occurred while adding param email systems');
        }
        return redirect()->route('param-email-system.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamEmailSystem::find($id);
        
        if(!$data){

            alert()->error('error','Param email systems found');
        }
       
        return view('pages.backend.paramemailsystem.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamEmailSystem::find($id);

        $this->validate($request,[
            'email'=>'required|max:80|email|string|unique:param_email_systems,email,'.$id,
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param email systems successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-email-system.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamEmailSystem::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param email systems successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
