<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class LoginUserController extends Controller
{
    public function logout(Request $request) {
        
        Auth::logout();

        return redirect('car-insurance');
    }


    public function index(Request $request) {

        if(!Auth::user()){

            return view('pages.frontend.login.index');
        }
        else{

            return redirect('dashboard-customer');
        }


    }


    public function login(Request $request) {

        
    }



}
