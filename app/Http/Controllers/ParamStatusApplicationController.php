<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamStatusApplication;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;


class ParamStatusApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-status-app-list|param-status-app-create|param-status-app-edit|param-status-app-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-status-app-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-status-app-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-status-app-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamStatusApplication::get();
        
        return view('pages.backend.paramstatusapp.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramstatusapp.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'code_status_app'=>'required|max:40|unique:param_status_applications',
            'status_app'=>'required|max:100',
            'label'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamStatusApplication::create($data);

        if($status){
            alert()->success('success','Successfully added param status app');
        } 
        else{
            alert()->error('error','Error occurred while adding param status app');
        }
        return redirect()->route('param-status-app.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamStatusApplication::find($id);
        
        if(!$data){

            alert()->error('error','Param status app not found');
        }
       
        return view('pages.backend.paramstatusapp.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamStatusApplication::find($id);

        $this->validate($request,[
            'code_status_app'=>'required|max:40|unique:param_status_applications,code_status_app,'.$id,
            'status_app'=>'required|max:100',
            'label'=>'required|max:80',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param status app successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-status-app.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamStatusApplication::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param status app successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}

