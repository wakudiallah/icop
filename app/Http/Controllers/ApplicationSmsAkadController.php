<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\Praaplication;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\ApplicationSelectInsurance;
use App\Models\ApplicationAdditionalProtect;
use App\Models\ApplicationFinancing;
use App\Models\ApplicationGajiPendapatan;
use App\Models\ApplicationGajiPotongan;
use App\Models\SetupBpaCharge;
use App\Models\ApplicationDsr;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\CheckApplicationDoc;
use App\Models\SetupSmsAkad;
use App\Models\ApplicationSmsAkad;
use App\Models\CustomerFeedback;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Twilio\Rest\Client;
use Auth;

class ApplicationSmsAkadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:app-smsakad-list|app-smsakad-create|app-smsakad-edit|app-smsakad-delete', ['only' => ['index','show']]);
        $this->middleware('permission:app-smsakad-create', ['only' => ['create','store']]);
        $this->middleware('permission:app-smsakad-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:app-smsakad-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        //
        $data = Praaplication::orderBy('updated_at','DESC')
                ->where('pay_status', 'F')  
                ->where('app_status', 'AK')              
                ->get();

        
        return view('pages.backend.appsmsakad.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  

    public function store(Request $request)
    {
        


    }

    public function sendSms($uuid)
    {
        
        $smsAkad = SetupSmsAkad::where('status', 1)->orderBy('id', 'Desc')->first();
        $stupbpaCharge = SetupBpaCharge::where('status', 1)->orderBy('id', 'Desc')->first();
        
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();
        $financing = ApplicationFinancing::join('insurance_basic_premia', 'insurance_basic_premia.uuid', '=', 'application_financings.uuid')
                    ->select('application_financings.total_payable', 'application_financings.deduc_schema', 'application_financings.after_cal_monthly_installment', 'application_financings.processing_fee', 'application_financings.bpa_charges', 'application_financings.total_financing',
                            'insurance_basic_premia.rebateAmt', 'insurance_basic_premia.stampDutyAmt', 'insurance_basic_premia.totBasicNetAmt')
                    ->where('application_financings.uuid', $uuid)
                    ->first();
        
        // Use strip_tags to remove HTML tags
        $cleanText = strip_tags($smsAkad->sms_akad);
        
        // You might also want to trim any extra spaces or HTML entities like &nbsp;
        $cleanText = trim($cleanText);
        $cleanText = str_replace("&nbsp;", " ", $cleanText);

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);

        $extraCover = $calcuAddProtect['extraCover'];
        $payable = $calcuAddProtect['payable'];
        $totalSst = $calcuAddProtect['totalSst'];
        
        
        $receiverNumber = $cif->phone; // Replace with the recipient's phone number
        //$receiverNumber = "+60142449179";

        if (strpos($receiverNumber, '0') === 0) {
            $receiverNumber = '+60' . substr($receiverNumber, 1);
        }

        /*$message = $smsAkad->opening . "\n"."\n" . $smsAkad->text_basic_contribution . config('insurance.currency') ." " . $payable. "\n".  
                    $smsAkad->text_caj_perkhidmatan .config('insurance.currency')." ".  $financing->processing_fee. "\n".
                    $smsAkad->text_bpa_charges."(".$stupbpaCharge->bpa_charges."%)".": ".config('insurance.currency')." ".$financing->bpa_charges."\n". 
                    $smsAkad->text_jumlah_pembiayaan. config('insurance.currency')." ". $financing->total_financing."\n". 
                    $smsAkad->text_bayaran_balik. config('insurance.currency')." ". $financing->after_cal_monthly_installment. " sebulan". "\n". "\n".
                    
                    $cleanText; // Replace with your desired message*/

        $message = $smsAkad->opening . "\n"."\n" . 
                    "i. Sumbangan / Premium :" . config('insurance.currency') ." " . $payable. "\n".
                    "ii. Ansuran :" . $financing->deduc_schema ." bulan". "\n".
                    "iii. Fi Pemprosesan :". config('insurance.currency'). " ".$financing->processing_fee.   "\n".
                    "iv. Fi BPA :" ." ".$stupbpaCharge->bpa_charges."% " ."\n".
                    "Potongan Gaji Bulanan : " . config('insurance.currency') ." ". $financing->after_cal_monthly_installment. "\n"."\n".
                    $cleanText; // Replace with your desired message

        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');
        $fromNumber = env('TWILIO_FROM');

        try {
            $client = new Client($sid, $token);
            $client->messages->create($receiverNumber, [
                'from' => $fromNumber,
                'body' => $message
            ]);

            ApplicationSmsAkad::create([
                'uuid' => $uuid,
                'send_status' => true,
                'send_time' => now(),
                'message_send' => $message
                
            ]);

            alert()->success('success','Successfully sms akad');
               
            return redirect()->route('app-smsakad.index');

        } catch (Exception $e) {

            return response()->json(['success' => false, 'message' => $e->getMessage()]);

        }
    }

    public function receiveFeedbackx(Request $request)
    {
        $from = $request->From;
        $body = $request->Body;

        $this->twilioClient->messages->create(
            $from,
            [
                'from' => $this->twilioNumber,
                'body' => "Thank you for your feedback."
            ]
        );

        ApplicationSmsAkad::create([
        
            'uuid' => "uuid",
            'reply_status' => true,
            'reply_time' => now(),
            'message_reply' => $message
            
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        
        $smsAkad = SetupSmsAkad::where('status', 1)->orderBy('id', 'Desc')->first();

        $cif = ApplicationCustomerInfo::where('uuid', $id)->first();
        $financing = ApplicationFinancing::join('insurance_basic_premia', 'insurance_basic_premia.uuid', '=', 'application_financings.uuid')
                    ->select('application_financings.total_payable', 'application_financings.deduc_schema', 'application_financings.monthly_installment',
                            'insurance_basic_premia.rebateAmt', 'insurance_basic_premia.stampDutyAmt', 'insurance_basic_premia.totBasicNetAmt')
                    ->where('application_financings.uuid', $id)
                    ->first();

        // Use strip_tags to remove HTML tags
        $cleanText = strip_tags($smsAkad->sms_akad);

        // You might also want to trim any extra spaces or HTML entities like &nbsp;
        $cleanText = trim($cleanText);
        $cleanText = str_replace("&nbsp;", " ", $cleanText);


        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($id);

        $extraCover = $calcuAddProtect['extraCover'];
        $payable = $calcuAddProtect['payable'];
        $totalSst = $calcuAddProtect['totalSst'];


        // URL and parameters
        $url = config('apiIcop.url');
        $username = config('apiIcop.username');
        $password = config('apiIcop.password');
        $phoneNumber = $cif->phone;
        $message = $smsAkad->opening . "\n" . $smsAkad->text_basic_premium . config('insurance.currency') . $financing->totBasicNetAmt. "\n".
                    $smsAkad->text_rebate .config('insurance.currency').  $financing->rebateAmt. "\n".
                    $smsAkad->text_add_on.config('insurance.currency'). $extraCover . "\n". 
                    $smsAkad->text_sst. config('insurance.currency'). $totalSst . "\n".
                    $smsAkad->text_dutty_stamp . config('insurance.currency'). $financing->stampDutyAmt ."\n".
                    $smsAkad->text_payable . config('insurance.currency'). $payable . "\n".
                    $cleanText;
        $type = config('apiIcop.type'); // SMS type
        $sendid = config('apiIcop.sendid');



        // Initialize Guzzle client
        $client = new Client();

        // Send the request
        try {
            $response = $client->request('GET', $url, [
                'query' => [
                    'un' => $username,
                    'pwd' => $password,
                    'dstno' => $phoneNumber,
                    'msg' => $message,
                    'type' => $type,
                    'sendid' => $sendid,
                ]
            ]);

            // Check the status code
            if ($response->getStatusCode() == 200) {
                // Handle successful response
                
                alert()->success('success','Successfully sms akad');
               
                return redirect()->route('app-smsakad.index');

            } else {
                // Handle non-200 responses
                return response()->json(['success' => false, 'message' => 'Failed to send SMS.']);
            }
        } catch (\Exception $e) {
            // Handle exceptions

            dd($e);
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    public function showFeedbackResponses()
    {
        $feedbackResponses = CustomerFeedback::all();
        return view('components.feedback-responses', ['feedbackResponses' => $feedbackResponses]);
    }
    
    
     public function edit($id)
    {
        //
    }


    public function receiveFeedback(Request $request)
    {
        $from = $request->From;
        $body = $request->Body;
        
        CustomerFeedback::create([
            'phone' => $from,
            'response' => $body
        ]);
        
        $this->twilioClient->messages->create(
            $from,
            [
                'from' => $this->twilioNumber,
                'body' => "Thank you for your feedback."
            ]
        );
        
        return response("Thank you for your feedback.");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
