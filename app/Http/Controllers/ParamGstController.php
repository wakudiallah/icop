<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamGst;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamGstController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-gst-list|param-gst-create|param-gst-edit|param-gst-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-gst-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-gst-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-gst-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamGst::get();
        
        return view('pages.backend.paramgst.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramgst.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'gst'=>'required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamGst::create($data);

        if($status){
            alert()->success('success','Successfully added param gst');
        } 
        else{
            alert()->error('error','Error occurred while adding param gst');
        }
        return redirect()->route('param-gst.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamGst::find($id);
        
        if(!$data){

            alert()->error('error','Param gst found');
        }
       
        return view('pages.backend.paramgst.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamGst::find($id);

        $this->validate($request,[
            'gst'=>'required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param gst successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-gst.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamGst::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param gst successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
