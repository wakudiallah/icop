<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SetupDocument;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class SetupDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-document-list|setup-document-create|setup-document-edit|setup-document-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-document-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-document-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-document-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = SetupDocument::get();
        
        return view('pages.backend.setupdocument.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.setupdocument.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'code'=>'required|unique:setup_documents,code|max:80|min:2',
            'name'=>'required|max:200',
            'document'=>'required|mimes:pdf',
            'status'=>'required',
        ]);
        $data=$request->all();

        $file = $request->file('document');
        $name = $request->code;


        //Ensure the documents directory exists
        $documentsPath = storage_path('app/document');
        if(!file_exists($documentsPath)) {
            mkdir($documentsPath, 0777, true);
        }

    
        // Generate a new filename with extension
        $date = Carbon::now()->format('Y-m-d_H:i:s'); // Get today's date
        $newFilename = $date . '_' . $name . '.' . $file->getClientOriginalExtension();

        //$path = $request->file('document')->store('document');
        $path = $file->storeAs('document', $newFilename);
        //$request->file('document')->move(public_path('document'), $newFilename);


        $data['document'] = $newFilename;

        $status = SetupDocument::create($data);


        if($status){
            alert()->success('success','Successfully added Setup document');
        } 
        else{
            alert()->error('error','Error occurred while adding Setup document');
        }
        return redirect()->route('setup-document.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = SetupDocument::find($id);
        
        if(!$data){

            alert()->error('error','Setup document not found');
        }
       
        return view('pages.backend.setupdocument.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = SetupDocument::find($id);

        $this->validate($request,[
            'code'=>'required|max:80|min:2|unique:setup_documents,code,'.$id,
            'name'=>'required|max:200',
            'document'=>'nullable|mimes:pdf',
            'status' => 'required|in:1,0',
        ]);

        //$data = $request->except('document');

        if ($request->hasFile('document')) {
            $file = $request->file('document');
            $name = $request->code;

            // Generate a new filename with extension
            $date = Carbon::now()->format('Y-m-d H:i:s'); // Get today's date
            $newFilename = $date . '_' . $name . '.' . $file->getClientOriginalExtension();

            $path = $file->storeAs('document', $newFilename);

            $data['document'] = $newFilename;
            
        } else {
            // If no file uploaded, remove the 'document' key from $data to prevent it from being updated
            $data = $request->except('document');
        }

       
        $status = $brand->update($data);

        if($status){
            
            alert()->success('success','Setup document successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-document.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupDocument::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup document successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}

