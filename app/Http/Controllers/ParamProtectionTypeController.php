<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamProtectionType;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamProtectionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-protection-type-list|param-protection-type-create|param-protection-type-edit|param-protection-type-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-protection-type-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-protection-type-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-protection-type-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamProtectionType::get();
        
        return view('pages.backend.paramprotectiontype.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.paramprotectiontype.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'protection_type'=>'required|max:100',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamProtectionType::create($data);

        if($status){
            alert()->success('success','Successfully added param protection type');
        } 
        else{
            alert()->error('error','Error occurred while adding param protection type');
        }
        return redirect()->route('param-protection-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamProtectionType::find($id);
        
        if(!$data){

            alert()->error('error','Param protection type found');
        }
       
        return view('pages.backend.paramprotectiontype.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamProtectionType::find($id);

        $this->validate($request,[
            'protection_type'=>'required|max:100',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param protection type successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-protection-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamProtectionType::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param protection type successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
