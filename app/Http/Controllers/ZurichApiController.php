<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\PartnerApiAccount;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ZurichApiCalculatePremium_AllowableCartAmount;
use App\Models\ZurichApiCalculatePremium_AllowableCartDay;
use App\Models\ZurichAllowableCourtesyCarDay;
use App\Models\ZurichApiCalculate_AllowableExtensionCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableKeyReplacement;
use App\Models\ZurichApiCalculatePremium_AllowablePaBasicUnit;
use App\Models\ZurichApiCalculatePremium_AllowablePaExtendedCoverage;
use App\Models\ZurichApiCalculatePremium_AllowableVoluntaryExcess;
use App\Models\ZurichApiCalculatePremium_AllowableWaterDamageCoverage;
use App\Models\ZurichApiCalculatePremium_CarReplacementDay;
use App\Models\ZurichApiCalculatePremium_ErrorDisplay;
use App\Models\ZurichApiCalculatePremium_paCVehicleDetail;
use App\Models\ZurichApiCalculatePremium_premiumDetail;
use App\Models\ZurichApiCalculatePremium_quotationInfo;
use App\Models\ZurichApiCalculatePremium_AllowableExtCvr;
use App\Models\ZurichApiCalculatePremium_extraCoverData;
use App\Models\ZurichApiCalculatePremium_allowableCourtesyCar;
use App\Models\ZurichApiIssueCovernote;
use App\Models\ZurichApiGetjpjstatus;
use App\Models\ZurichApiGetvehiclemake;
use App\Models\ZurichApiGetvehiclemodel;
use App\Models\ZurichApiPostcode;


use App\Models\ZurichApiDownloadPolicySchedule;




class ZurichApiController extends Controller
{
    //

    private $name;
    private $nric;



    public function __construct()
    {
        $this->name = "Wakudiallah";
        $this->ic = "660807-10-6863";

        
    }


    public function token()
    {
        //
        $currentDate = Carbon::now()->format('Y:m:d H:i:s');

        
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $password = $data->password;


        if($data->expired < $currentDate){

            $client = new Client();

            try { 
                $response = $client->request('POST', config('apiIcop.zurich_token'), [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'json' => [
                        'userName' => $userName,
                        'password' => $password
                    ]
                ]);
    
                $statusCode = $response->getStatusCode();
                $body = $response->getBody()->getContents();
    
                $decodedBody = json_decode($body, true);
    
                //dd($decodedBody);
                $token = $decodedBody['data']['token'];
                $expiration = $decodedBody['data']['expiration'];
    
                $datetime = Carbon::parse($expiration);
                $formattedDatetime = $datetime->format('Y:m:d H:i:s');
    
                PartnerApiAccount::where('partner_code', '01')->update([
                    'token' => $token,
                    'expired' => $formattedDatetime
                ]);
                
                return $decodedBody;
    
    
            } catch (RequestException $e) {

                Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

                if ($e->hasResponse()) {
                    $errorResponse = $e->getResponse();
                    $errorStatusCode = $errorResponse->getStatusCode();
                    $errorBody = $errorResponse->getBody()->getContents();
    
                    echo "Error status code: $errorStatusCode\n";
                    echo "Error response body: $errorBody\n";
                } else {
                    echo "Error: " . $e->getMessage() . "\n";
                }
            }

        }

        //dd($decodedBody);


        //return view('pages.backend.setupdocument.index', compact('data'));
    }



    public function getvehicleinfo($vehicle, $icFormat, $uuid)
    {

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;


        $client = new Client();
        try {

            $response = $client->request('POST', config('apiIcop.zurich_api_getVehicleInfo'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    "agentCode" => $userName,
                    "vehNo" => $vehicle,
                    "id" => $icFormat
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);


            foreach ($decodedBody['data']['nvicData'] as $nvicData) {
                ZurichApiGetVehicleInfoVariant::firstOrCreate(
                    ['uuid' => $uuid], // Check if a record with this UUID exists
                    [   // If it doesn't exist, create with these attributes
                        'nvic' => $nvicData['nvic'],
                        'vehModelCode' => $nvicData['vehModelCode'],
                        'vehModel' => $nvicData['vehModel'],
                        'vehSeat' => $nvicData['vehSeat'],
                        'vehTransType' => $nvicData['vehTransType'],
                        'marketValue' => $nvicData['marketValue'],
                        'capacity' => $nvicData['capacity'],
                        'vehFuelType' => $nvicData['vehFuelType'],
                        'variant' => $nvicData['variant'],
                        'vehBody' => $nvicData['vehBody'],
                        'uom' => $nvicData['uom'],
                        'vehVariantDesc' => $nvicData['vehVariantDesc'],
                    ]
                );
            }
            
            
            if (!empty($decodedBody['data']['polEffDate'])) {

                $polEffDate = Carbon::createFromFormat('dmY', $decodedBody['data']['polEffDate'])->format('Y-m-d');

            } else {

                $polEffDate = Null;

            }

            $polExpDateCheck = $decodedBody['data']['polExpDate'] ?? null;
            $nxtNCDEffDateCheck = $decodedBody['data']['nxtNCDEffDate'] ?? null;
            $nxtPolEffDateCheck = $decodedBody['data']['nxtPolEffDate'] ?? null;
            $nxtPolExpDateCheck = $decodedBody['data']['nxtPolExpDate'] ?? null;

            $polExpDate = $polExpDateCheck ? Carbon::createFromFormat('dmY', $decodedBody['data']['polExpDate'])->format('Y-m-d') : null;
            $nxtNCDEffDate = $nxtNCDEffDateCheck ? Carbon::createFromFormat('dmY', $decodedBody['data']['nxtNCDEffDate'])->format('Y-m-d') : null;
            $nxtPolEffDate = $nxtPolEffDateCheck ? Carbon::createFromFormat('dmY', $decodedBody['data']['nxtPolEffDate'])->format('Y-m-d') : null;
            $nxtPolExpDate = $nxtPolExpDateCheck ? Carbon::createFromFormat('dmY', $decodedBody['data']['nxtPolExpDate'])->format('Y-m-d') : null;
            

            ZurichApiGetVehicleInfo::create([
                'uuid' => $uuid,
                'vehRegNo' => $decodedBody['data']['vehRegNo'],
                'vehClass' => $decodedBody['data']['vehClass'],
                'vehMake' => $decodedBody['data']['vehMake'],
                'vehMakeDesc' => $decodedBody['data']['vehMakeDesc'],
                'vehMakeYear' => $decodedBody['data']['vehMakeYear'],
                'vehUse' => $decodedBody['data']['vehUse'],
                'vehEngineNo' => $decodedBody['data']['vehEngineNo'],
                'vehChassisNo' => $decodedBody['data']['vehChassisNo'],
                'polEffDate' => $polEffDate,
                'polExpDate' => $polExpDate,
                'builtType' => $decodedBody['data']['builtType'],
                'ncdPct' => $decodedBody['data']['ncdPct'],
                'nxtNCDEffDate' => $nxtNCDEffDate,
                'ncdStatus' => $decodedBody['data']['ncdStatus'],
                'ismNCDRespCode' => $decodedBody['data']['ismNCDRespCode'],
                'ismVIXRespCode' => $decodedBody['data']['ismVIXRespCode'],
                'nxtPolEffDate' => $nxtPolEffDate,
                'nxtPolExpDate' => $nxtPolExpDate,
                'curEnqDate' => $decodedBody['data']['curEnqDate'],
                'vehVIXModelCode' => $decodedBody['data']['vehVIXModelCode'],
                'vixNVIC' => $decodedBody['data']['vixNVIC'],
                'preInsCode' => $decodedBody['data']['preInsCode'],
                'vixPreInsCode' => $decodedBody['data']['vixPreInsCode'],
                'vixVehClass' => $decodedBody['data']['vixVehClass'],
                'vixCoverType' => $decodedBody['data']['vixCoverType'],
                'vixPreInsDesc' => $decodedBody['data']['vixPreInsDesc'],
                'coverType' => $decodedBody['data']['coverType'],
            ]);


            $getVehicleInfo = [
                'vehRegNo' => $decodedBody['data']['vehRegNo'] ?? null,
                'vehChassisNo' => $decodedBody['data']['vehChassisNo'] ?? null,
                'vehEngineNo' => $decodedBody['data']['vehEngineNo'] ?? null,
                'vehMakeYear' => $decodedBody['data']['vehMakeYear'] ?? null,
                'brand' => $decodedBody['data']['vehMakeDesc'] ?? null,
                'model' => $decodedBody['data']['nvicData'][0]['vehModel'] ?? null,
                'variant' => $decodedBody['data']['nvicData'][0]['variant'] ?? null,
                'engineSize' => $decodedBody['data']['nvicData'][0]['capacity'] ?? null,
                'cc' => $decodedBody['data']['nvicData'][0]['uom'] ?? null,
                'ncd' => $decodedBody['data']['ncdPct'] ?? null,

            ];

            return $getVehicleInfo;


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }

    }


    public function getvehiclemake()
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;
        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/vix/getvehiclemake', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    
                        "agentCode" => $userName,
                        "productCode" => "PZ01",
                        "filterKey" => ""
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            foreach($decodedBody['data']['make'] as $make ){
                ZurichApiGetvehiclemake::create([
                    'sdfMakeID' => $make['sdfMakeID'],
                    'description' => $make['description']
                ]);
            }
            


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    public function getvehiclemodel()
    {
        
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;
        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/vix/getvehiclemodel', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    
                        "agentCode"=> $userName,
                        "productCode"=> "PC01",
                        "makeYear"=> "2010",
                        "make"=> "05"
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            foreach($decodedBody['data']['model'] as $model ){
                ZurichApiGetvehiclemodel::create([
                    'sdfVehModelID' =>  $model['sdfVehModelID'],
                    'description' =>  $model['description'],
                    'misDes' =>  $model['misDes'],
                    'bodyType' =>  $model['bodyType'],
                    'capacityFrom' =>  $model['capacityFrom'],
                    'capacityTo' =>  $model['capacityTo'],
                    'uom' =>  $model['uom'],
                    'ism' =>  $model['ism'],
                    'wScreenSI' =>  $model['wScreenSI'],
                    'noofPassenger' =>  $model['noofPassenger'],
                    'make' =>  $model['make'],
                    'nvic' =>  $model['nvic'],
                    'variant' =>  $model['variant'],
                    'marketValue' =>  $model['marketValue'],
                    'vehTransType' =>  $model['vehTransType'],
                    'vehFuelType' =>  $model['vehFuelType'],
                    'vehVariantDesc' =>  $model['vehVariantDesc']
                ]);
            }
            


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    

    public function calculatepremium($params)
    {
        //dd($params['abisi']);
       

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

      
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');
        
        $client = new Client();

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => $params['newId'],
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            'quotationNo' => '',  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $params['vehicleNo'],
                            'productCode' => 'PZ01',  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => 'V-FT', //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => 'MX1', //MX1 - Private Use Only (CI code)
                            'effDate' => $params['effDate'],
                            'expDate' => $params['expDate'],
                            'reconInd' => 'N', //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $params['yearOfMake'],
                            'make' => $params['make'],
                            'model' => $params['model'],
                            'capacity' => $params['capacity'],
                            'uom' => $params['uom'],
                            'engineNo' => $params['engineNo'],
                            'chassisNo' => $params['chassisNo'],
                            'logBookNo' => 'ERTGRET253', //??
                            'regLoc' => 'L', //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $params['noOfPassenger'],
                            'noOfDrivers' => 1,
                            'insIndicator' => 'P', //Insured Type P – Personal, C- Company
                            'name' => $params['fullname'],
                            'insNationality' => 'L', //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $params['ic'],
                            'dateOfBirth' => $params['dateOfBirth'],
                            'age' => $params['age'], 
                            'gender' => $params['gender'],
                            'maritalSts' => $params['maritalSts'],  //M ??????????????
                            'occupation' => '99',  //99 - Others
                            'mobileNo' => $params['phone'],
                            'address' => $params['address1'] . $params['address2'],
                            'postCode' => $params['postCode'],
                            'regionCode' => $params['regionCode'],
                            'state' => $params['state'],
                            'country' => $params['country'],
                            'sumInsured' =>  $params['sumInsured'], //Vehicle Sum Insured in RM ???
                            'abisi' => $params['abisi'], //Market value Sum Insured
                            'chosen_SI_Type' => 'REC_SI',  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => 'N', //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => 'N', //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => 0,  //??????????????????????????
                            'all_Driver_Ind' => 'N' //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ]
                    ]
                ]
            ]);

            

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);
            //dd($decodedBody);





            //$payload = $decodedBody['data']['allowable_CART_Amount'][0]['carT_Amount'];
            //dd('A');
            //Log::info('Request Payload:', $payload);

            //dd($decodedBody['data']['premiumDetails'][0]['annual_NCD_Amount']);

            //dd($decodedBody['data']['allowable_CART_Amount'][0]['carT_Amount]);

            /* ===============  Disable dari sini ====================== */
            
            foreach($decodedBody['data']['allowable_CART_Amount'] as $allowable_CART_Amount ){
                ZurichApiCalculatePremium_AllowableCartAmount::create([
                    'uuid' => $params['uuid'], 
                    'carT_Amount' => $allowable_CART_Amount['carT_Amount'],
                    'carT_Amount_Ind' => $allowable_CART_Amount['carT_Amount_Ind']
                ]);
            }

            foreach($decodedBody['data']['allowable_CART_Days'] as $allowable_CART_Days ){
                ZurichApiCalculatePremium_AllowableCartAmount::create([
                    'uuid' => $params['uuid'], 
                    'carT_Days' => $allowable_CART_Days['carT_Days'],
                    'carT_Days_Ind' => $allowable_CART_Days['carT_Days_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_Courtesy_Car'] as $allowable_Courtesy_Car ){
                ZurichApiCalculatePremium_allowableCourtesyCar::create([
                    'uuid' => $params['uuid'], 
                    'courtesy_Car_Days' => $allowable_Courtesy_Car['courtesy_Car_Days'],
                    'courtesy_Car_Days_Ind' => $allowable_Courtesy_Car['courtesy_Car_Days_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_Ext_Cvr'] as $allowable_Ext_Cvr ){
                ZurichApiCalculatePremium_allowableCourtesyCar::create([
                    'uuid' => $params['uuid'], 
                    "ext_Cvr_Code" =>  $allowable_Ext_Cvr['ext_Cvr_Code'],
                    "ext_Cvr_Desc" =>  $allowable_Ext_Cvr['ext_Cvr_Desc'],
                    "applicable_Ind" =>  $allowable_Ext_Cvr['applicable_Ind'],
                    "compulsory_Ind" =>  $allowable_Ext_Cvr['compulsory_Ind'],
                    "recommended_Ind" =>  $allowable_Ext_Cvr['recommended_Ind'],
                    "endorsement_Ind" =>  $allowable_Ext_Cvr['endorsement_Ind'],
                    "eC_Input_Type" =>  $allowable_Ext_Cvr['eC_Input_Type']
                ]);
            }

            foreach($decodedBody['data']['allowable_Key_Replacement'] as $allowable_Key_Replacement ){
                ZurichApiCalculatePremium_AllowableKeyReplacement::create([
                    'uuid' => $params['uuid'], 
                    'key_Replacement_SI' => $allowable_Key_Replacement['key_Replacement_SI'],
                    'key_Replacement_SI_Ind' => $allowable_Key_Replacement['key_Replacement_SI_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_PA_Basic_Unit'] as $allowable_PA_Basic_Unit ){
                ZurichApiCalculatePremium_AllowablePaBasicUnit::create([
                    'uuid' => $params['uuid'], 
                    'pA_Basic_Unit' => $allowable_PA_Basic_Unit['pA_Basic_Unit'],
                    'pA_Basic_Unit_Ind' => $allowable_PA_Basic_Unit['pA_Basic_Unit_Ind'],
                    'pA_Basic_SI' => $allowable_PA_Basic_Unit['pA_Basic_SI']
                ]);
            }


            foreach($decodedBody['data']['allowable_PA_Ext_Cvr'] as $allowable_PA_Ext_Cvr ){
                ZurichApiCalculatePremium_AllowablePaExtendedCoverage::create([
                    'uuid' => $params['uuid'], 
                    "paC_Ext_Cvr_Code" =>  $allowable_PA_Ext_Cvr['paC_Ext_Cvr_Code'],
                    "paC_Ext_Cvr_Desc" =>  $allowable_PA_Ext_Cvr['paC_Ext_Cvr_Desc'],
                    "ind" =>  $allowable_PA_Ext_Cvr['ind'],
                    "min_Units" =>  $allowable_PA_Ext_Cvr['min_Units'],
                    "max_Units" =>  $allowable_PA_Ext_Cvr['max_Units'],
                    "additional_Insured_Person_Ind" =>  $allowable_PA_Ext_Cvr['additional_Insured_Person_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_VolExcess'] as $allowable_VolExcess ){
                ZurichApiCalculatePremium_AllowableVoluntaryExcess::create([
                    'uuid' => $params['uuid'], 
                    'voluntary_Excess' => $allowable_VolExcess['voluntary_Excess'],
                    'voluntary_Excess_Ind' => $allowable_VolExcess['voluntary_Excess_Ind']
                ]);
            }

            foreach($decodedBody['data']['allowable_Water_Damage'] as $allowable_Water_Damage ){
                ZurichApiCalculatePremium_AllowableWaterDamageCoverage::create([
                    'uuid' => $params['uuid'], 
                    'water_Damage_SI' => $allowable_Water_Damage['water_Damage_SI'],
                    'water_Damage_SI_Ind' => $allowable_Water_Damage['water_Damage_SI_Ind']
                ]);
            }


            foreach($decodedBody['data']['car_Replacement_Days'] as $car_Replacement_Days ){
                ZurichApiCalculatePremium_CarReplacementDay::create([
                    'uuid' => $params['uuid'], 
                    'car_Replacement_Ind' => $car_Replacement_Days['car_Replacement_Ind'],
                    'car_Replacement_Days' => $car_Replacement_Days['car_Replacement_Days']
                ]);
            }


            foreach($decodedBody['data']['error_Display'] as $error_Display ){
                ZurichApiCalculatePremium_ErrorDisplay::create([
                    'uuid' => $params['uuid'], 
                    "error_Code" =>  $error_Display['error_Code'],
                    "error_Desc" =>  $error_Display['error_Desc'],
                    "severity" =>  $error_Display['severity'],
                    "remarks" =>  $error_Display['remarks'],
                    "warning_Ind" =>  $error_Display['warning_Ind']
                ]);
            }


            foreach($decodedBody['data']['paC_Vehicle_Details'] as $paC_Vehicle_Details ){
                ZurichApiCalculatePremium_paCVehicleDetail::create([
                    'uuid' => $params['uuid'], 
                    "vehicle_Number" =>  $paC_Vehicle_Details['vehicle_Number'],
                    "no_of_Seats" =>  $paC_Vehicle_Details['no_of_Seats'],
                    "is_Policyholder" =>  $paC_Vehicle_Details['is_Policyholder'],
                    "paC_SI" =>  $paC_Vehicle_Details['paC_SI'],
                    "paC_Annual_Basic_Premium" =>  $paC_Vehicle_Details['paC_Annual_Basic_Premium'],
                    "paC_Annual_Additional_Seats_Premium" =>  $paC_Vehicle_Details['paC_Annual_Additional_Seats_Premium'],
                    "paC_Benefit_Schedule_Code" =>  $paC_Vehicle_Details['paC_Benefit_Schedule_Code'],
                    "paC_Medical_Plan_Entry_Code" =>  $paC_Vehicle_Details['paC_Medical_Plan_Entry_Code']
                ]);
            }

            //$table->double('shipping_cost', 8, 2)->nullable();
            //dd($decodedBody['data']['extraCoverData']);
            //dd($decodedBody['data']['premiumDetails']['totBasicNetAmt']);

            

            foreach($decodedBody['data']['quotationInfo'] as $quotationInfo ){
                ZurichApiCalculatePremium_quotationInfo::create([
                    'uuid' => $params['uuid'], 
                    "quotationNo" => $quotationInfo['quotationNo'],
                    "ncdMsg" => $quotationInfo['ncdMsg'],
                    "ncdPct" => $quotationInfo['ncdPct']
                
                ]);
            }

            foreach($decodedBody['data']['allowable_Ext_Cvr'] as $allowable_Ext_Cvr ){
                ZurichApiCalculatePremium_AllowableExtCvr::create([
                    
                    'uuid' => $params['uuid'], 
                    "ext_Cvr_Code" => $allowable_Ext_Cvr['ext_Cvr_Code'],
                    "ext_Cvr_Desc" => $allowable_Ext_Cvr['ext_Cvr_Desc'],
                    "applicable_Ind" => $allowable_Ext_Cvr['applicable_Ind'],
                    "compulsory_Ind" => $allowable_Ext_Cvr['compulsory_Ind'],
                    "recommended_Ind" => $allowable_Ext_Cvr['recommended_Ind'],
                    "endorsement_Ind" => $allowable_Ext_Cvr['endorsement_Ind'],
                    "eC_Input_Type" => $allowable_Ext_Cvr['eC_Input_Type'],
                
                ]);
            }

            if (isset($decodedBody['data']['extraCoverData']) && is_array($decodedBody['data']['extraCoverData'])) {
                foreach ($decodedBody['data']['extraCoverData'] as $extraCoverData) {
                    ZurichApiCalculatePremium_extraCoverData::create([
                        'uuid' => $params['uuid'], 
                        "extCoverCode" => $extraCoverData['extCoverCode'],
                        "extCoverSumInsured" => $extraCoverData['extCoverSumInsured'],
                        "extCoverPrem" => $extraCoverData['extCoverPrem'],
                        "compulsory_Ind" => $extraCoverData['compulsory_Ind']
                    ]);
                }
            }

            /* ===============  Disable sampai sini ====================== */

            foreach($decodedBody['data']['premiumDetails'] as $premiumDetails ){
                ZurichApiCalculatePremium_premiumDetail::create([
                    'uuid' => $params['uuid'], 
                    "excessType" => $premiumDetails['excessType'],
                    "volExcessTypeApplied" => $premiumDetails['volExcessTypeApplied'],
                    "excessAmt" => $premiumDetails['excessAmt'],
                    "volExcessAmt" => $premiumDetails['volExcessAmt'],
                    "totExcessAmt" => $premiumDetails['totExcessAmt'],
                    "package_Applicable" => $premiumDetails['package_Applicable'],
                    "package_Prem" => $premiumDetails['package_Prem'],
                    "package_Selected" => $premiumDetails['package_Selected'],
                    "vehicle_Model_Code_Out" => $premiumDetails['vehicle_Model_Code_Out'],
                    "annual_BasePremAdjustment" => $premiumDetails['annual_BasePremAdjustment'],
                    "annual_Schedule_Premium" => $premiumDetails['annual_Schedule_Premium'],
                    "annual_Loading_Amount" => $premiumDetails['annual_Loading_Amount'],
                    "annual_Tuition_Purposes_Amount" => $premiumDetails['annual_Tuition_Purposes_Amount'],
                    "annual_Tariff_Premium" => $premiumDetails['annual_Tariff_Premium'],
                    "annual_All_Riders" => $premiumDetails['annual_All_Riders'],
                    "annual_NCD_Amount" => $premiumDetails['annual_NCD_Amount'],
                    "annual_Voluntary_Excess_Amount" => $premiumDetails['annual_Voluntary_Excess_Amount'],
                    "annual_Total_Extra_Cover_Premium" => $premiumDetails['annual_Total_Extra_Cover_Premium'],
                    "annual_Renewal_Bonus_Amount" => $premiumDetails['annual_Renewal_Bonus_Amount'],
                    "basicAnnualPrem" => $premiumDetails['basicAnnualPrem'],
                    "annual_Act_BasePremAdjustment" => $premiumDetails['annual_Act_BasePremAdjustment'],
                    "annual_Act_Schedule_Premium" => $premiumDetails['annual_Act_Schedule_Premium'],
                    "annual_Act_Loading_Amount" => $premiumDetails['annual_Act_Loading_Amount'],
                    "annual_Act_Tuition_Purposes_Amount" => $premiumDetails['annual_Act_Tuition_Purposes_Amount'],
                    "annual_Act_Tariff_Premium" => $premiumDetails['annual_Act_Tariff_Premium'],
                    "annual_Act_All_Riders" => $premiumDetails['annual_Act_All_Riders'],
                    "annual_Act_NCD_Amount" => $premiumDetails['annual_Act_NCD_Amount'],
                    "annual_Act_Voluntary_Excess_Amount" => $premiumDetails['annual_Act_Voluntary_Excess_Amount'],
                    "annual_Act_Total_Extra_Cover_Premium" => $premiumDetails['annual_Act_Total_Extra_Cover_Premium'],
                    "annual_Act_Renewal_Bonus_Amount" => $premiumDetails['annual_Act_Renewal_Bonus_Amount'],
                    "annual_Act_Premium" => $premiumDetails['annual_Act_Premium'],
                    "basicNetAct" => $premiumDetails['basicNetAct'],
                    "actPrem" => $premiumDetails['actPrem'],
                    "basicNetNonAct" => $premiumDetails['basicNetNonAct'],
                    "nonActPrem" => $premiumDetails['nonActPrem'],
                    "commAmt" => $premiumDetails['commAmt'],
                    "commPct" => $premiumDetails['commPct'],
                    "gstOnCommAmt" => $premiumDetails['gstOnCommAmt'],
                    "nettPrem" => $premiumDetails['nettPrem'],
                    "basic_Premium_Adjustment" => $premiumDetails['basic_Premium_Adjustment'],
                    "basic_Premium_Adjustment_Pct" => $premiumDetails['basic_Premium_Adjustment_Pct'],
                    "schedule_Premium_0" => $premiumDetails['schedule_Premium_0'],
                    "basicPrem" => $premiumDetails['basicPrem'],
                    "trailerPremium" => $premiumDetails['trailerPremium'],
                    "loadAmt" => $premiumDetails['loadAmt'],
                    "trailer_Loading_Amount" => $premiumDetails['trailer_Loading_Amount'],
                    "loadPct" => $premiumDetails['loadPct'],
                    "trlLoadingPer" => $premiumDetails['trlLoadingPer'],
                    "tuitionLoadAmt" => $premiumDetails['tuitionLoadAmt'],
                    "trailerTuitionLoadAmt" => $premiumDetails['trailerTuitionLoadAmt'],
                    "tuitionLoadPct" => $premiumDetails['tuitionLoadPct'],
                    "trailerTuitionLoadPer" => $premiumDetails['trailerTuitionLoadPer'],
                    "allRiderAmt" => $premiumDetails['allRiderAmt'],
                    "ncdAmt" => $premiumDetails['ncdAmt'],
                    "volExcessDiscAmt" => $premiumDetails['volExcessDiscAmt'],
                    "volExcessDiscPct" => $premiumDetails['volExcessDiscPct'],
                    "totBasicNetAmt" => $premiumDetails['totBasicNetAmt'],
                    "totExtCoverPrem" => $premiumDetails['totExtCoverPrem'],
                    "rnwBonusAmt" => $premiumDetails['rnwBonusAmt'],
                    "rnwBonusPct" => $premiumDetails['rnwBonusPct'],
                    "basic_Premium" => $premiumDetails['basic_Premium'],
                    "rebateAmt" => $premiumDetails['rebateAmt'],
                    "rebatePct" => $premiumDetails['rebatePct'],
                    "grossPrem" => $premiumDetails['grossPrem'],
                    "gsT_Amt" => $premiumDetails['gsT_Amt'],
                    "gsT_Pct" => $premiumDetails['gsT_Pct'],
                    "ssT_Amt" => $premiumDetails['ssT_Amt'],
                    "ssT_Pct" => $premiumDetails['ssT_Pct'],
                    "stampDutyAmt" => $premiumDetails['stampDutyAmt'],
                    "totMtrPrem" => $premiumDetails['totMtrPrem'],
                    "ptV_Amt" => $premiumDetails['ptV_Amt'],
                    "ttlPayablePremium" => $premiumDetails['ttlPayablePremium'],
                    "paC_Max_Additional_Vehicle" => $premiumDetails['paC_Max_Additional_Vehicle'],
                    "paC_MultiProductAmt" => $premiumDetails['paC_MultiProductAmt'],
                    "paC_MultiProductPct" => $premiumDetails['paC_MultiProductPct'],
                    "paC_RebateAmt" => $premiumDetails['paC_RebateAmt'],
                    "paC_RebatePct" => $premiumDetails['paC_RebatePct'],
                    "paC_GSTAmt" => $premiumDetails['paC_GSTAmt'],
                    "paC_GSTPct" => $premiumDetails['paC_GSTPct'],
                    "paC_StampDuty" => $premiumDetails['paC_StampDuty'],
                    "paC_TotPrem" => $premiumDetails['paC_TotPrem'],
                    "paC_CommAmt" => $premiumDetails['paC_CommAmt'],
                    "paC_CommPct" => $premiumDetails['paC_CommPct'],
                    "paC_GstOnCommAmt" => $premiumDetails['paC_GstOnCommAmt'],
                    "paC_SumInsured" => $premiumDetails['paC_SumInsured'],
                    "paC_GrossPrem" => $premiumDetails['paC_GrossPrem'],
                    "paC_NettPrem" => $premiumDetails['paC_NettPrem'],
                    "paC_MinPrem" => $premiumDetails['paC_MinPrem'],
                    "paC_Prem" => $premiumDetails['paC_Prem'],
                    "paC_AddPrem" => $premiumDetails['paC_AddPrem'],
                    "paC_AddVehPrem" => $premiumDetails['paC_AddVehPrem'],
                    "pacExtraCover" => $premiumDetails['pacExtraCover'],
                    "renewal_Cap_Ind" => $premiumDetails['renewal_Cap_Ind'],
                    "basic_Without_Ren_Cap" => $premiumDetails['basic_Without_Ren_Cap'],
                    "tariff_Premium_Pure" => $premiumDetails['tariff_Premium_Pure'],
                    "tariff_Premium" => $premiumDetails['tariff_Premium'],
                    "detariff_Premium_Adjustment" => $premiumDetails['detariff_Premium_Adjustment'],
                    "uncapped_Detariff_Premium" => $premiumDetails['uncapped_Detariff_Premium'],
                    "loading_Group" => $premiumDetails['loading_Group'],
                    "detariff_Lower_Bound" => $premiumDetails['detariff_Lower_Bound'],
                    "detariff_Upper_Bound" => $premiumDetails['detariff_Upper_Bound'],
                    "chosen_Vehicle_SI_Min_Deviation" => $premiumDetails['chosen_Vehicle_SI_Min_Deviation'],
                    "chosen_Vehicle_SI_Max_Deviation" => $premiumDetails['chosen_Vehicle_SI_Max_Deviation'],
                    "chosen_Vehicle_SI_Lower_Bound" => $premiumDetails['chosen_Vehicle_SI_Lower_Bound'],
                    "chosen_Vehicle_SI_Upper_Bound" => $premiumDetails['chosen_Vehicle_SI_Upper_Bound'],
                    "loading_Overwrite_Ind" => $premiumDetails['loading_Overwrite_Ind'],
                    "piaM_Blacklist_Veh_No_Ind" => $premiumDetails['piaM_Blacklist_Veh_No_Ind'],
                    "comp_BL_Veh_No_Ind" => $premiumDetails['comp_BL_Veh_No_Ind'],
                    "comp_BL_Client_Ind" => $premiumDetails['comp_BL_Client_Ind'],
                    "abI_Check_Ind" => $premiumDetails['abI_Check_Ind'],
                    "allowable_Motor_Rebate_Ind" => $premiumDetails['allowable_Motor_Rebate_Ind'],
                    "allowable_PAC_Rebate_Ind" => $premiumDetails['allowable_PAC_Rebate_Ind'],
                    "calculate_Method" => $premiumDetails['calculate_Method'],
                    "motor_Min_Prem_Ind" => $premiumDetails['motor_Min_Prem_Ind'],
                    "abI_Adopt_Ind" => $premiumDetails['abI_Adopt_Ind'],
                    "motor_Levy_Per" => $premiumDetails['motor_Levy_Per'],
                    "motor_Levy_Amount" => $premiumDetails['motor_Levy_Amount'],
                    "recommended_Vehicle_SI_Type" => $premiumDetails['recommended_Vehicle_SI_Type'],
                    "manual_Benchmark_SI_Display" => $premiumDetails['manual_Benchmark_SI_Display'],
                    "manual_Benchmark_SI_Text" => $premiumDetails['manual_Benchmark_SI_Text'],
                    "manual_Benchmark_SI_Editable" => $premiumDetails['manual_Benchmark_SI_Editable'],
                    "vehicle_Use_Code" => $premiumDetails['vehicle_Use_Code'],
                    "aV_SI_Value" => $premiumDetails['aV_SI_Value'],
                    "rec_SI_Value" => $premiumDetails['rec_SI_Value'],
                    "rec_SI_Text" => $premiumDetails['rec_SI_Text'],
                    "veh_SI_Editable" => $premiumDetails['veh_SI_Editable'],
                    "quote_Exp_Date" => $premiumDetails['quote_Exp_Date'],
                    "vehicle_Class" => $premiumDetails['vehicle_Class'],
                    "aV_SI_Lower_Bound" => $premiumDetails['aV_SI_Lower_Bound'],
                    "aV_SI_Upper_Bound" => $premiumDetails['aV_SI_Upper_Bound'],
                    "mV_SI_Lower_Bound" => $premiumDetails['mV_SI_Lower_Bound'],
                    "mV_SI_Upper_Bound" => $premiumDetails['mV_SI_Upper_Bound'],
                    "isM_Cover_Type" => $premiumDetails['isM_Cover_Type'],
                    "ncD_Class" => $premiumDetails['ncD_Class'],
                    "ncD_Applicable_Ind" => $premiumDetails['ncD_Applicable_Ind'],
                    "ddlAgreeValue" => $premiumDetails['ddlAgreeValue'],
                    "rec_Vehicle_SI" => $premiumDetails['rec_Vehicle_SI'],
                    "paC_SI_Per_Seat" => $premiumDetails['paC_SI_Per_Seat'],
                    "total_ZDA_Premium" => $premiumDetails['total_ZDA_Premium'],
                    "zdA_Add_On" => $premiumDetails['zdA_Add_On'],
                    "paC_Type_Offered" => $premiumDetails['paC_Type_Offered'],
                    "paC_Desc" => $premiumDetails['paC_Desc'],
                    "paC_Max_Units" => $premiumDetails['paC_Max_Units'],
                    "paC_Min_Units" => $premiumDetails['paC_Min_Units'],
                    "rec_PAC_Min_Units" => $premiumDetails['rec_PAC_Min_Units'],
                    "paC_Compulsory_Ind" => $premiumDetails['paC_Compulsory_Ind'],
                    "paC_Type_Applicable" => $premiumDetails['paC_Type_Applicable'],
                    "wakalah_Commission_Mtr_Pct" => $premiumDetails['wakalah_Commission_Mtr_Pct'],
                    "wakalah_Commission_Mtr_Amt" => $premiumDetails['wakalah_Commission_Mtr_Amt'],
                    "wakalah_Expense_Mtr_Pct" => $premiumDetails['wakalah_Expense_Mtr_Pct'],
                    "wakalah_Expense_Mtr_Amt" => $premiumDetails['wakalah_Expense_Mtr_Amt'],
                    "wakalah_Fee_Mtr_Pct" => $premiumDetails['wakalah_Fee_Mtr_Pct'],
                    "wakalah_Fee_Mtr_Amt" => $premiumDetails['wakalah_Fee_Mtr_Amt'],
                    "wakalah_Commission_PA_Pct" => $premiumDetails['wakalah_Commission_PA_Pct'],
                    "wakalah_Commission_PA_Amt" => $premiumDetails['wakalah_Commission_PA_Amt'],
                    "wakalah_Expense_PA_Pct" => $premiumDetails['wakalah_Expense_PA_Pct'],
                    "wakalah_Expense_PA_Amt" => $premiumDetails['wakalah_Expense_PA_Amt'],
                    "wakalah_Fee_PA_Pct" => $premiumDetails['wakalah_Fee_PA_Pct'],
                    "wakalah_Fee_PA_Amt" => $premiumDetails['wakalah_Fee_PA_Amt']
        
                ]);
            }

            
            

            $returnCalculate = [

                'basicPrem' => $decodedBody['data']['premiumDetails'][0]['basicPrem'] ?? null,
                'stampDutyAmt' => $decodedBody['data']['premiumDetails'][0]['stampDutyAmt'] ?? null,
                'ssT_Pct' => $decodedBody['data']['premiumDetails'][0]['ssT_Pct'] ?? null,
                'ssT_Amt' => $decodedBody['data']['premiumDetails'][0]['ssT_Amt'] ?? null,
                "rebateAmt" => $decodedBody['data']['premiumDetails'][0]['rebateAmt'] ?? null,
                "rebatePct" => $decodedBody['data']['premiumDetails'][0]['rebatePct'] ?? null,
                "grossPrem" => $decodedBody['data']['premiumDetails'][0]['grossPrem'] ?? null,
                "ncdAmt" => $decodedBody['data']['premiumDetails'][0]['ncdAmt'] ?? null
                
            ];

            return $returnCalculate;


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            /*dd($e);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }*/
        }


    }

    public function calculatepremiumGenerateQuote($params)
    {

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

       
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => $params['newId'],
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $params['vehicleNo'],
                            'productCode' => 'PZ01',  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => 'V-CO', //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => 'MX1', //MX1 - Private Use Only (CI code)
                            'effDate' => $params['effDate'],
                            'expDate' => $params['expDate'],
                            'reconInd' => 'N', //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $params['yearOfMake'],
                            'make' => $params['make'],
                            'model' => $params['model'],
                            'capacity' => '1798',
                            'uom' => $params['uom'],
                            'engineNo' => $params['engineNo'],
                            'chassisNo' => $params['chassisNo'],
                            'logBookNo' => 'ERTGRET253', //??
                            'regLoc' => 'L', //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $params['noOfPassenger'],
                            'noOfDrivers' => 1,
                            'insIndicator' => 'P', //Insured Type P – Personal, C- Company
                            'name' => $params['fullname'],
                            'insNationality' => 'L', //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $params['ic'],
                            'dateOfBirth' => $params['dateOfBirth'], //DD/MM/YYYY
                            'age' => $params['age'], 
                            'gender' => $params['gender'],
                            'maritalSts' => 'M',  //M 
                            'occupation' => '99',  //99 - Others
                            'mobileNo' => $params['phone'],
                            'address' => $params['address1'] . $params['address2'],
                            'postCode' => $params['postCode'],
                            'regionCode' => $params['regionCode'],
                            'state' => $params['state'],
                            'country' => $params['country'],
                            'sumInsured' => 30000, //Vehicle Sum Insured in RM
                            'abisi' => $params['abisi'], //Market value Sum Insured
                            'chosen_SI_Type' => 'REC_SI',  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => 'N', //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => 'N', //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => 0,  //??????????????????????????
                            'all_Driver_Ind' => 'N' //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                [
                                    'extCovCode' => '101',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '15/8/2024',
                                    'expDate' => '14/8/2025',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '112',
                                    'unitDay' => 50,
                                    'unitAmount' => 7,
                                    'effDate' => '15/8/2024',
                                    'expDate' => '14/8/2025',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '25',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ],
                        'additionalNamedDriverDetails' => [
                            'additionalNamedDriverData' => [
                                [
                                    'ndName' => 'Driver1',
                                    'ndIdentityNo' => 'Asdd123',
                                    'ndDateOfBirth' => '14/12/1998',
                                    'ndGender' => 'M',
                                    'ndMaritalSts' => 'M',
                                    'ndOccupation' => '99',
                                    'ndRelationship' => 'SIS',
                                    'ndNationality' => 'USA'
                                ],
                                [
                                    'ndName' => 'Driver2',
                                    'ndIdentityNo' => 'As43123',
                                    'ndDateOfBirth' => '22/12/1990',
                                    'ndGender' => 'F',
                                    'ndMaritalSts' => 'M',
                                    'ndOccupation' => '99',
                                    'ndRelationship' => 'SIS',
                                    'ndNationality' => 'IND'
                                ]
                            ]
                        ],
                        'pacRiderDetails' => [
                            'pacRiderData' => [
                                'pacRiderNo' => 1,
                                'pacRiderName' => 'PacRDNmae',
                                'pacRiderNewIC' => 'PacRDNewIC',
                                'pacRiderOldIC' => 'PacRDOldIC',
                                'pacRiderDOB' => '27/03/1987',
                                'defaultInd' => 1
                            ]
                        ],
                        'pacExtraCoverDetails' => [
                            'pacExtraCoverData' => [
                                [
                                    'pacCode' => 'R008',
                                    'pacUnit' => 1
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);

            foreach($decodedBody['data']['allowable_Ext_Cvr'] as $allowable_Ext_Cvr ){
                ZurichApiCalculatePremium_AllowableExtCvr::create([
                    
                    'uuid' => $params['uuid'], 
                    "ext_Cvr_Code" => $allowable_Ext_Cvr['ext_Cvr_Code'],
                    "ext_Cvr_Desc" => $allowable_Ext_Cvr['ext_Cvr_Desc'],
                    "applicable_Ind" => $allowable_Ext_Cvr['applicable_Ind'],
                    "compulsory_Ind" => $allowable_Ext_Cvr['compulsory_Ind'],
                    "recommended_Ind" => $allowable_Ext_Cvr['recommended_Ind'],
                    "endorsement_Ind" => $allowable_Ext_Cvr['endorsement_Ind'],
                    "eC_Input_Type" => $allowable_Ext_Cvr['eC_Input_Type'],
                
                ]);
            }


            foreach($decodedBody['data']['allowable_CART_Amount'] as $allowable_CART_Amount ){
                ZurichApiCalculatePremium_AllowableCartAmount::create([
                    'uuid' => $params['uuid'], 
                    'carT_Amount' => $allowable_CART_Amount['carT_Amount'],
                    'carT_Amount_Ind' => $allowable_CART_Amount['carT_Amount_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_CART_Days'] as $allowable_CART_Days ){
                ZurichApiCalculatePremium_AllowableCartAmount::create([
                    'uuid' => $params['uuid'], 
                    'carT_Days' => $allowable_CART_Days['carT_Days'],
                    'carT_Days_Ind' => $allowable_CART_Days['carT_Days_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_Courtesy_Car'] as $allowable_Courtesy_Car ){
                ZurichApiCalculatePremium_allowableCourtesyCar::create([
                    'uuid' => $params['uuid'], 
                    'courtesy_Car_Days' => $allowable_Courtesy_Car['courtesy_Car_Days'],
                    'courtesy_Car_Days_Ind' => $allowable_Courtesy_Car['courtesy_Car_Days_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_Ext_Cvr'] as $allowable_Ext_Cvr ){
                ZurichApiCalculatePremium_allowableCourtesyCar::create([
                    'uuid' => $params['uuid'], 
                    "ext_Cvr_Code" =>  $allowable_Ext_Cvr['ext_Cvr_Code'],
                    "ext_Cvr_Desc" =>  $allowable_Ext_Cvr['ext_Cvr_Desc'],
                    "applicable_Ind" =>  $allowable_Ext_Cvr['applicable_Ind'],
                    "compulsory_Ind" =>  $allowable_Ext_Cvr['compulsory_Ind'],
                    "recommended_Ind" =>  $allowable_Ext_Cvr['recommended_Ind'],
                    "endorsement_Ind" =>  $allowable_Ext_Cvr['endorsement_Ind'],
                    "eC_Input_Type" =>  $allowable_Ext_Cvr['eC_Input_Type']
                ]);
            }

            foreach($decodedBody['data']['allowable_Key_Replacement'] as $allowable_Key_Replacement ){
                ZurichApiCalculatePremium_AllowableKeyReplacement::create([
                    'uuid' => $params['uuid'], 
                    'key_Replacement_SI' => $allowable_Key_Replacement['key_Replacement_SI'],
                    'key_Replacement_SI_Ind' => $allowable_Key_Replacement['key_Replacement_SI_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_PA_Basic_Unit'] as $allowable_PA_Basic_Unit ){
                ZurichApiCalculatePremium_AllowablePaBasicUnit::create([
                    'uuid' => $params['uuid'], 
                    'pA_Basic_Unit' => $allowable_PA_Basic_Unit['pA_Basic_Unit'],
                    'pA_Basic_Unit_Ind' => $allowable_PA_Basic_Unit['pA_Basic_Unit_Ind'],
                    'pA_Basic_SI' => $allowable_PA_Basic_Unit['pA_Basic_SI']
                ]);
            }


            foreach($decodedBody['data']['allowable_PA_Ext_Cvr'] as $allowable_PA_Ext_Cvr ){
                ZurichApiCalculatePremium_AllowablePaExtendedCoverage::create([
                    'uuid' => $params['uuid'], 
                    "paC_Ext_Cvr_Code" =>  $allowable_PA_Ext_Cvr['paC_Ext_Cvr_Code'],
                    "paC_Ext_Cvr_Desc" =>  $allowable_PA_Ext_Cvr['paC_Ext_Cvr_Desc'],
                    "ind" =>  $allowable_PA_Ext_Cvr['ind'],
                    "min_Units" =>  $allowable_PA_Ext_Cvr['min_Units'],
                    "max_Units" =>  $allowable_PA_Ext_Cvr['max_Units'],
                    "additional_Insured_Person_Ind" =>  $allowable_PA_Ext_Cvr['additional_Insured_Person_Ind']
                ]);
            }


            foreach($decodedBody['data']['allowable_VolExcess'] as $allowable_VolExcess ){
                ZurichApiCalculatePremium_AllowableVoluntaryExcess::create([
                    'uuid' => $params['uuid'], 
                    'voluntary_Excess' => $allowable_VolExcess['voluntary_Excess'],
                    'voluntary_Excess_Ind' => $allowable_VolExcess['voluntary_Excess_Ind']
                ]);
            }

            foreach($decodedBody['data']['allowable_Water_Damage'] as $allowable_Water_Damage ){
                ZurichApiCalculatePremium_AllowableWaterDamageCoverage::create([
                    'uuid' => $params['uuid'], 
                    'water_Damage_SI' => $allowable_Water_Damage['water_Damage_SI'],
                    'water_Damage_SI_Ind' => $allowable_Water_Damage['water_Damage_SI_Ind']
                ]);
            }


            foreach($decodedBody['data']['car_Replacement_Days'] as $car_Replacement_Days ){
                ZurichApiCalculatePremium_CarReplacementDay::create([
                    'uuid' => $params['uuid'], 
                    'car_Replacement_Ind' => $car_Replacement_Days['car_Replacement_Ind'],
                    'car_Replacement_Days' => $car_Replacement_Days['car_Replacement_Days']
                ]);
            }


            foreach($decodedBody['data']['error_Display'] as $error_Display ){
                ZurichApiCalculatePremium_ErrorDisplay::create([
                    'uuid' => $params['uuid'], 
                    "error_Code" =>  $error_Display['error_Code'],
                    "error_Desc" =>  $error_Display['error_Desc'],
                    "severity" =>  $error_Display['severity'],
                    "remarks" =>  $error_Display['remarks'],
                    "warning_Ind" =>  $error_Display['warning_Ind']
                ]);
            }


            foreach($decodedBody['data']['paC_Vehicle_Details'] as $paC_Vehicle_Details ){
                ZurichApiCalculatePremium_paCVehicleDetail::create([
                    'uuid' => $params['uuid'], 
                    "vehicle_Number" =>  $paC_Vehicle_Details['vehicle_Number'],
                    "no_of_Seats" =>  $paC_Vehicle_Details['no_of_Seats'],
                    "is_Policyholder" =>  $paC_Vehicle_Details['is_Policyholder'],
                    "paC_SI" =>  $paC_Vehicle_Details['paC_SI'],
                    "paC_Annual_Basic_Premium" =>  $paC_Vehicle_Details['paC_Annual_Basic_Premium'],
                    "paC_Annual_Additional_Seats_Premium" =>  $paC_Vehicle_Details['paC_Annual_Additional_Seats_Premium'],
                    "paC_Benefit_Schedule_Code" =>  $paC_Vehicle_Details['paC_Benefit_Schedule_Code'],
                    "paC_Medical_Plan_Entry_Code" =>  $paC_Vehicle_Details['paC_Medical_Plan_Entry_Code']
                ]);
            }

            //$table->double('shipping_cost', 8, 2)->nullable();

            foreach($decodedBody['data']['premiumDetails'] as $premiumDetails ){
                ZurichApiCalculatePremium_premiumDetail::create([
                    'uuid' => $params['uuid'], 
                    "excessType" => $premiumDetails['excessType'],
                    "volExcessTypeApplied" => $premiumDetails['volExcessTypeApplied'],
                    "excessAmt" => $premiumDetails['excessAmt'],
                    "volExcessAmt" => $premiumDetails['volExcessAmt'],
                    "totExcessAmt" => $premiumDetails['totExcessAmt'],
                    "package_Applicable" => $premiumDetails['package_Applicable'],
                    "package_Prem" => $premiumDetails['package_Prem'],
                    "package_Selected" => $premiumDetails['package_Selected'],
                    "vehicle_Model_Code_Out" => $premiumDetails['vehicle_Model_Code_Out'],
                    "annual_BasePremAdjustment" => $premiumDetails['annual_BasePremAdjustment'],
                    "annual_Schedule_Premium" => $premiumDetails['annual_Schedule_Premium'],
                    "annual_Loading_Amount" => $premiumDetails['annual_Loading_Amount'],
                    "annual_Tuition_Purposes_Amount" => $premiumDetails['annual_Tuition_Purposes_Amount'],
                    "annual_Tariff_Premium" => $premiumDetails['annual_Tariff_Premium'],
                    "annual_All_Riders" => $premiumDetails['annual_All_Riders'],
                    "annual_NCD_Amount" => $premiumDetails['annual_NCD_Amount'],
                    "annual_Voluntary_Excess_Amount" => $premiumDetails['annual_Voluntary_Excess_Amount'],
                    "annual_Total_Extra_Cover_Premium" => $premiumDetails['annual_Total_Extra_Cover_Premium'],
                    "annual_Renewal_Bonus_Amount" => $premiumDetails['annual_Renewal_Bonus_Amount'],
                    "basicAnnualPrem" => $premiumDetails['basicAnnualPrem'],
                    "annual_Act_BasePremAdjustment" => $premiumDetails['annual_Act_BasePremAdjustment'],
                    "annual_Act_Schedule_Premium" => $premiumDetails['annual_Act_Schedule_Premium'],
                    "annual_Act_Loading_Amount" => $premiumDetails['annual_Act_Loading_Amount'],
                    "annual_Act_Tuition_Purposes_Amount" => $premiumDetails['annual_Act_Tuition_Purposes_Amount'],
                    "annual_Act_Tariff_Premium" => $premiumDetails['annual_Act_Tariff_Premium'],
                    "annual_Act_All_Riders" => $premiumDetails['annual_Act_All_Riders'],
                    "annual_Act_NCD_Amount" => $premiumDetails['annual_Act_NCD_Amount'],
                    "annual_Act_Voluntary_Excess_Amount" => $premiumDetails['annual_Act_Voluntary_Excess_Amount'],
                    "annual_Act_Total_Extra_Cover_Premium" => $premiumDetails['annual_Act_Total_Extra_Cover_Premium'],
                    "annual_Act_Renewal_Bonus_Amount" => $premiumDetails['annual_Act_Renewal_Bonus_Amount'],
                    "annual_Act_Premium" => $premiumDetails['annual_Act_Premium'],
                    "basicNetAct" => $premiumDetails['basicNetAct'],
                    "actPrem" => $premiumDetails['actPrem'],
                    "basicNetNonAct" => $premiumDetails['basicNetNonAct'],
                    "nonActPrem" => $premiumDetails['nonActPrem'],
                    "commAmt" => $premiumDetails['commAmt'],
                    "commPct" => $premiumDetails['commPct'],
                    "gstOnCommAmt" => $premiumDetails['gstOnCommAmt'],
                    "nettPrem" => $premiumDetails['nettPrem'],
                    "basic_Premium_Adjustment" => $premiumDetails['basic_Premium_Adjustment'],
                    "basic_Premium_Adjustment_Pct" => $premiumDetails['basic_Premium_Adjustment_Pct'],
                    "schedule_Premium_0" => $premiumDetails['schedule_Premium_0'],
                    "basicPrem" => $premiumDetails['basicPrem'],
                    "trailerPremium" => $premiumDetails['trailerPremium'],
                    "loadAmt" => $premiumDetails['loadAmt'],
                    "trailer_Loading_Amount" => $premiumDetails['trailer_Loading_Amount'],
                    "loadPct" => $premiumDetails['loadPct'],
                    "trlLoadingPer" => $premiumDetails['trlLoadingPer'],
                    "tuitionLoadAmt" => $premiumDetails['tuitionLoadAmt'],
                    "trailerTuitionLoadAmt" => $premiumDetails['trailerTuitionLoadAmt'],
                    "tuitionLoadPct" => $premiumDetails['tuitionLoadPct'],
                    "trailerTuitionLoadPer" => $premiumDetails['trailerTuitionLoadPer'],
                    "allRiderAmt" => $premiumDetails['allRiderAmt'],
                    "ncdAmt" => $premiumDetails['ncdAmt'],
                    "volExcessDiscAmt" => $premiumDetails['volExcessDiscAmt'],
                    "volExcessDiscPct" => $premiumDetails['volExcessDiscPct'],
                    "totBasicNetAmt" => $premiumDetails['totBasicNetAmt'],
                    "totExtCoverPrem" => $premiumDetails['totExtCoverPrem'],
                    "rnwBonusAmt" => $premiumDetails['rnwBonusAmt'],
                    "rnwBonusPct" => $premiumDetails['rnwBonusPct'],
                    "basic_Premium" => $premiumDetails['basic_Premium'],
                    "rebateAmt" => $premiumDetails['rebateAmt'],
                    "rebatePct" => $premiumDetails['rebatePct'],
                    "grossPrem" => $premiumDetails['grossPrem'],
                    "gsT_Amt" => $premiumDetails['gsT_Amt'],
                    "gsT_Pct" => $premiumDetails['gsT_Pct'],
                    "ssT_Amt" => $premiumDetails['ssT_Amt'],
                    "ssT_Pct" => $premiumDetails['ssT_Pct'],
                    "stampDutyAmt" => $premiumDetails['stampDutyAmt'],
                    "totMtrPrem" => $premiumDetails['totMtrPrem'],
                    "ptV_Amt" => $premiumDetails['ptV_Amt'],
                    "ttlPayablePremium" => $premiumDetails['ttlPayablePremium'],
                    "paC_Max_Additional_Vehicle" => $premiumDetails['paC_Max_Additional_Vehicle'],
                    "paC_MultiProductAmt" => $premiumDetails['paC_MultiProductAmt'],
                    "paC_MultiProductPct" => $premiumDetails['paC_MultiProductPct'],
                    "paC_RebateAmt" => $premiumDetails['paC_RebateAmt'],
                    "paC_RebatePct" => $premiumDetails['paC_RebatePct'],
                    "paC_GSTAmt" => $premiumDetails['paC_GSTAmt'],
                    "paC_GSTPct" => $premiumDetails['paC_GSTPct'],
                    "paC_StampDuty" => $premiumDetails['paC_StampDuty'],
                    "paC_TotPrem" => $premiumDetails['paC_TotPrem'],
                    "paC_CommAmt" => $premiumDetails['paC_CommAmt'],
                    "paC_CommPct" => $premiumDetails['paC_CommPct'],
                    "paC_GstOnCommAmt" => $premiumDetails['paC_GstOnCommAmt'],
                    "paC_SumInsured" => $premiumDetails['paC_SumInsured'],
                    "paC_GrossPrem" => $premiumDetails['paC_GrossPrem'],
                    "paC_NettPrem" => $premiumDetails['paC_NettPrem'],
                    "paC_MinPrem" => $premiumDetails['paC_MinPrem'],
                    "paC_Prem" => $premiumDetails['paC_Prem'],
                    "paC_AddPrem" => $premiumDetails['paC_AddPrem'],
                    "paC_AddVehPrem" => $premiumDetails['paC_AddVehPrem'],
                    "pacExtraCover" => $premiumDetails['pacExtraCover'],
                    "renewal_Cap_Ind" => $premiumDetails['renewal_Cap_Ind'],
                    "basic_Without_Ren_Cap" => $premiumDetails['basic_Without_Ren_Cap'],
                    "tariff_Premium_Pure" => $premiumDetails['tariff_Premium_Pure'],
                    "tariff_Premium" => $premiumDetails['tariff_Premium'],
                    "detariff_Premium_Adjustment" => $premiumDetails['detariff_Premium_Adjustment'],
                    "uncapped_Detariff_Premium" => $premiumDetails['uncapped_Detariff_Premium'],
                    "loading_Group" => $premiumDetails['loading_Group'],
                    "detariff_Lower_Bound" => $premiumDetails['detariff_Lower_Bound'],
                    "detariff_Upper_Bound" => $premiumDetails['detariff_Upper_Bound'],
                    "chosen_Vehicle_SI_Min_Deviation" => $premiumDetails['chosen_Vehicle_SI_Min_Deviation'],
                    "chosen_Vehicle_SI_Max_Deviation" => $premiumDetails['chosen_Vehicle_SI_Max_Deviation'],
                    "chosen_Vehicle_SI_Lower_Bound" => $premiumDetails['chosen_Vehicle_SI_Lower_Bound'],
                    "chosen_Vehicle_SI_Upper_Bound" => $premiumDetails['chosen_Vehicle_SI_Upper_Bound'],
                    "loading_Overwrite_Ind" => $premiumDetails['loading_Overwrite_Ind'],
                    "piaM_Blacklist_Veh_No_Ind" => $premiumDetails['piaM_Blacklist_Veh_No_Ind'],
                    "comp_BL_Veh_No_Ind" => $premiumDetails['comp_BL_Veh_No_Ind'],
                    "comp_BL_Client_Ind" => $premiumDetails['comp_BL_Client_Ind'],
                    "abI_Check_Ind" => $premiumDetails['abI_Check_Ind'],
                    "allowable_Motor_Rebate_Ind" => $premiumDetails['allowable_Motor_Rebate_Ind'],
                    "allowable_PAC_Rebate_Ind" => $premiumDetails['allowable_PAC_Rebate_Ind'],
                    "calculate_Method" => $premiumDetails['calculate_Method'],
                    "motor_Min_Prem_Ind" => $premiumDetails['motor_Min_Prem_Ind'],
                    "abI_Adopt_Ind" => $premiumDetails['abI_Adopt_Ind'],
                    "motor_Levy_Per" => $premiumDetails['motor_Levy_Per'],
                    "motor_Levy_Amount" => $premiumDetails['motor_Levy_Amount'],
                    "recommended_Vehicle_SI_Type" => $premiumDetails['recommended_Vehicle_SI_Type'],
                    "manual_Benchmark_SI_Display" => $premiumDetails['manual_Benchmark_SI_Display'],
                    "manual_Benchmark_SI_Text" => $premiumDetails['manual_Benchmark_SI_Text'],
                    "manual_Benchmark_SI_Editable" => $premiumDetails['manual_Benchmark_SI_Editable'],
                    "vehicle_Use_Code" => $premiumDetails['vehicle_Use_Code'],
                    "aV_SI_Value" => $premiumDetails['aV_SI_Value'],
                    "rec_SI_Value" => $premiumDetails['rec_SI_Value'],
                    "rec_SI_Text" => $premiumDetails['rec_SI_Text'],
                    "veh_SI_Editable" => $premiumDetails['veh_SI_Editable'],
                    "quote_Exp_Date" => $premiumDetails['quote_Exp_Date'],
                    "vehicle_Class" => $premiumDetails['vehicle_Class'],
                    "aV_SI_Lower_Bound" => $premiumDetails['aV_SI_Lower_Bound'],
                    "aV_SI_Upper_Bound" => $premiumDetails['aV_SI_Upper_Bound'],
                    "mV_SI_Lower_Bound" => $premiumDetails['mV_SI_Lower_Bound'],
                    "mV_SI_Upper_Bound" => $premiumDetails['mV_SI_Upper_Bound'],
                    "isM_Cover_Type" => $premiumDetails['isM_Cover_Type'],
                    "ncD_Class" => $premiumDetails['ncD_Class'],
                    "ncD_Applicable_Ind" => $premiumDetails['ncD_Applicable_Ind'],
                    "ddlAgreeValue" => $premiumDetails['ddlAgreeValue'],
                    "rec_Vehicle_SI" => $premiumDetails['rec_Vehicle_SI'],
                    "paC_SI_Per_Seat" => $premiumDetails['paC_SI_Per_Seat'],
                    "total_ZDA_Premium" => $premiumDetails['total_ZDA_Premium'],
                    "zdA_Add_On" => $premiumDetails['zdA_Add_On'],
                    "paC_Type_Offered" => $premiumDetails['paC_Type_Offered'],
                    "paC_Desc" => $premiumDetails['paC_Desc'],
                    "paC_Max_Units" => $premiumDetails['paC_Max_Units'],
                    "paC_Min_Units" => $premiumDetails['paC_Min_Units'],
                    "rec_PAC_Min_Units" => $premiumDetails['rec_PAC_Min_Units'],
                    "paC_Compulsory_Ind" => $premiumDetails['paC_Compulsory_Ind'],
                    "paC_Type_Applicable" => $premiumDetails['paC_Type_Applicable'],
                    "wakalah_Commission_Mtr_Pct" => $premiumDetails['wakalah_Commission_Mtr_Pct'],
                    "wakalah_Commission_Mtr_Amt" => $premiumDetails['wakalah_Commission_Mtr_Amt'],
                    "wakalah_Expense_Mtr_Pct" => $premiumDetails['wakalah_Expense_Mtr_Pct'],
                    "wakalah_Expense_Mtr_Amt" => $premiumDetails['wakalah_Expense_Mtr_Amt'],
                    "wakalah_Fee_Mtr_Pct" => $premiumDetails['wakalah_Fee_Mtr_Pct'],
                    "wakalah_Fee_Mtr_Amt" => $premiumDetails['wakalah_Fee_Mtr_Amt'],
                    "wakalah_Commission_PA_Pct" => $premiumDetails['wakalah_Commission_PA_Pct'],
                    "wakalah_Commission_PA_Amt" => $premiumDetails['wakalah_Commission_PA_Amt'],
                    "wakalah_Expense_PA_Pct" => $premiumDetails['wakalah_Expense_PA_Pct'],
                    "wakalah_Expense_PA_Amt" => $premiumDetails['wakalah_Expense_PA_Amt'],
                    "wakalah_Fee_PA_Pct" => $premiumDetails['wakalah_Fee_PA_Pct'],
                    "wakalah_Fee_PA_Amt" => $premiumDetails['wakalah_Fee_PA_Amt']
        
                ]);
            }

            foreach($decodedBody['data']['quotationInfo'] as $quotationInfo ){
                ZurichApiCalculatePremium_quotationInfo::create([
                    'uuid' => $params['uuid'], 
                    "quotationNo" => $quotationInfo['quotationNo'],
                    "ncdMsg" => $quotationInfo['ncdMsg'],
                    "ncdPct" => $quotationInfo['ncdPct']
                
                ]);
            }

            foreach($decodedBody['data']['extraCoverData'] as $extraCoverData ){
                ZurichApiCalculatePremium_extraCoverData::create([
                    'uuid' => $params['uuid'], 
                    "extCoverCode" => $extraCoverData['extCoverCode'],
                    "extCoverSumInsured" => $extraCoverData['extCoverSumInsured'],
                    "extCoverPrem" => $extraCoverData['extCoverPrem'],
                    "compulsory_Ind" => $extraCoverData['compulsory_Ind']
                
                ]);
            }

            foreach($decodedBody['data']['allowable_Ext_Cvr'] as $allowable_Ext_Cvr ){
                ZurichApiCalculatePremium_AllowableExtCvr::create([
                    
                    'uuid' => $params['uuid'], 
                    "ext_Cvr_Code" => $allowable_Ext_Cvr['ext_Cvr_Code'],
                    "ext_Cvr_Desc" => $allowable_Ext_Cvr['ext_Cvr_Desc'],
                    "applicable_Ind" => $allowable_Ext_Cvr['applicable_Ind'],
                    "compulsory_Ind" => $allowable_Ext_Cvr['compulsory_Ind'],
                    "recommended_Ind" => $allowable_Ext_Cvr['recommended_Ind'],
                    "endorsement_Ind" => $allowable_Ext_Cvr['endorsement_Ind'],
                    "eC_Input_Type" => $allowable_Ext_Cvr['eC_Input_Type'],
                
                ]);
            }
            
            
            $returnCalculate = [

                'quotationNo' => $decodedBody['data']['quotationInfo'][0]['quotationNo'] ?? null,
                'ncdMsg' => $decodedBody['data']['quotationInfo'][0]['ncdMsg'] ?? null,
                'ncdPct' => $decodedBody['data']['quotationInfo'][0]['ncdPct'] ?? null,
          
            ];

            return $returnCalculate;
            


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function issuecovernote($params)
    {

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;


        // Current date and time
        $currentDateTime = new \DateTime();

        // Format the date and time
        $formattedDateTime = $currentDateTime->format('Y-M-d h:i:s A');


        $client = new Client();

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_issue_covernote'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    "requestData" => [
                            "participantDetails" => [
                                "transactionReferenceNo" => $params['transactionReferenceNo'],
                                "requestDateTime" => $formattedDateTime
                            ],
                            "issueCoverNoteDetails" => [
                                "agentCode" => $userName,
                                "quotationNumber" => $params['quotationNumber'],
                                "emailTo" => $params['emailTo']
                            ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            foreach($decodedBody['data']['coverNoteInfo'] as $coverNoteInfo ){
                ZurichApiIssueCovernote::create([

                    'coverNoteNo' => $coverNoteInfo['coverNoteNo'],
                    'ncdMsg' => $coverNoteInfo['ncdMsg'],
                    'coverNoteError' => $coverNoteInfo['coverNoteError']

                ]);
            }

            $returnissueCovernote = [

                'basicPrem' => $decodedBody['data']['coverNoteInfo'][0]['coverNoteNo'] ?? null,

            ];

            return $returnissueCovernote;


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function issuepolicy()
    {

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/issuepolicy', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    "RequestData" => [
                        "ParticipantDetails" => [
                            "TransactionReferenceNo" => "20000011",
                            "RequestDateTime" => "2024-07-06T10:36:39.129Z",
                            "EmailTo" => "wakudiallah05@gmail.com"
                        ],
                        "BasicDetails" => [
                            "QuotationNo" => "MQ240000482843",
                            "FranchiseAgentCode" => "",
                            "AgentCode" => $userName,
                            "TransType" => "B",
                            "VehicleNo" => "LOPLK222",
                            "ProductCode" => "PZ01",
                            "CoverType" => "V-CO",
                            "CICode" => "MX1",
                            "EffDate" => "07/07/2024",
                            "ExpDate" => "08/08/2025",
                            "ReconInd" => "N",
                            "YearOfMake" => 2014,
                            "Make" => "22",
                            "Model" => "22*3418",
                            "Capacity" => "1999",
                            "UOM" => "CC",
                            "EngineNo" => "EWEKLL94d",
                            "ChassisNo" => "PM2OS00df2437l",
                            "LogBookNo" => "NA",
                            "RegLoc" => "L",
                            "RegionCode" => "W",
                            "NoOfPassenger" => 5,
                            "NoOfDrivers" => 1,
                            "InsIndicator" => "P",
                            "Name" => "Wakudiallah",
                            "InsNationality" => "L",
                            "NewIC" => "981212-06-5226",
                            "DateOfBirth" => "22/12/1998",
                            "Age" => 25,
                            "Gender" => "M",
                            "MaritalSts" => "M",
                            "Occupation" => "99",
                            "MobileNo" => "012-3456789",
                            "Address" => "Datok Keramat KL",
                            "PostCode" => "54000",
                            "State" => "6",
                            "Country" => "MAS",
                            "SumInsured" => 29200,
                            "ABISI" => 29200,
                            "Chosen_SI_Type" => "REC_SI",
                            "AVInd" => "N",
                            "PACInd" => "N",
                            "PACUnit" => 0,
                            "All_Driver_Ind" => "N"
                        ],
                        "ExtraCoverDetails" => [
                            "ExtraCoverData" => [
                                [
                                    "ExtCovCode" => "101",
                                    "UnitDay" => 0,
                                    "UnitAmount" => 0,
                                    "EffDate" => "15/4/2017",
                                    "ExpDate" => "14/4/2018",
                                    "SumInsured" => 0
                                ],
                                [
                                    "ExtCovCode" => "112",
                                    "UnitDay" => 50,
                                    "UnitAmount" => 7,
                                    "EffDate" => "",
                                    "ExpDate" => "",
                                    "SumInsured" => 0
                                ],
                                [
                                    "ExtCovCode" => "25",
                                    "UnitDay" => 0,
                                    "UnitAmount" => 0,
                                    "EffDate" => "",
                                    "ExpDate" => "",
                                    "SumInsured" => 0
                                ]
                            ]
                        ],
                        "AdditionalNamedDriverDetails" => [
                            "AdditionalNamedDriverData" => [
                                [
                                    "NdName" => "Driver1",
                                    "NdIdentityNo" => "Asdd123",
                                    "NdDateOfBirth" => "14/12/1998",
                                    "NdGender" => "M",
                                    "NdMaritalSts" => "M",
                                    "NdOccupation" => "99",
                                    "NdRelationship" => "SIS",
                                    "NdNationality" => "USA"
                                ],
                                [
                                    "NdName" => "Driver2",
                                    "NdIdentityNo" => "As43123",
                                    "NdDateOfBirth" => "22/12/1990",
                                    "NdGender" => "F",
                                    "NdMaritalSts" => "M",
                                    "NdOccupation" => "99",
                                    "NdRelationship" => "SIS",
                                    "NdNationality" => "IND"
                                ]
                            ]
                        ],
                        "PacRiderDetails" => [
                            "PacRiderData" => [
                                "PacRiderNo" => 1,
                                "PacRiderName" => "PacRDNmae",
                                "PacRiderNewIC" => "PacRDNewIC",
                                "PacRiderOldIC" => "PacRDOldIC",
                                "PacRiderDOB" => "27/03/1987",
                                "DefaultInd" => 1
                            ]
                        ],
                        "PacExtraCoverDetails" => [
                            "PACExtraCoverData" => [
                                [
                                    "PacCode" => "R008",
                                    "PacUnit" => 1
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            foreach($decodedBody['data']['coverNoteInfo'] as $coverNoteInfo ){
                ZurichApiCalculatePremium_ErrorDisplay::create([
                    "coverNoteNo" =>  $coverNoteInfo['coverNoteNo'],
                    "ncdMsg" =>  $coverNoteInfo['ncdMsg'],
                    "coverNoteError" =>  $coverNoteInfo['coverNoteError']
                ]);
            }
            


        } catch (RequestException $e) {

            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function getjpjstatus($params)
    {

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/getjpjstatus', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    "RequestData" => [
                        "ParticipantDetails" => [
                            "ParticipantCode" => "20",
                            "TransactionReferenceNo" => "20000010",
                            "RequestDateTime" => "2024-07-05T23:00:00.000"
                        ],
                        "JPJStatusDetails" => [
                            "AgentCode" => $userName,
                            "CoverNoteNumber" => "T62966E-24000003"
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            ZurichApiGetjpjstatus::create([
                "coverNoteNo" =>  $decodedBody['data']['jpjInfo']['coverNoteNo'],
                "vehRegNo" =>  $decodedBody['data']['jpjInfo']['vehRegNo'],
                "status" =>  $decodedBody['data']['jpjInfo']['status'],
                "dateTime" =>  $decodedBody['data']['jpjInfo']['dateTime'],
                "jpjStatusError" =>  $decodedBody['data']['jpjInfo']['jpjStatusError']
            ]);
            
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function downloadpolicyschedule()
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/downloadpolicyschedule', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    
                        'requestData' => [
                            'participantDetails' => [
                                'transactionReferenceNo' => '20000008',
                                'requestDateTime' => '2024-07-05T23:00:00.000'
                            ],
                            'downloadCoverNoteDetails' => [
                                'agentCode' => $userName,
                                'coverNoteNumber' => 'T62966E-24000002',
                                'emailTo' => 'wakudiallah05@gmail.com',
                                'returnType' => 4
                            ]
                        ]
                    
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            ZurichApiDownloadPolicySchedule::create([
                "emailStatus" =>  $decodedBody['data']['downloadInfo']['emailStatus'],
                "fileUrl" =>  $decodedBody['data']['downloadInfo']['fileUrl'],
                "fileByte" =>  $decodedBody['data']['downloadInfo']['fileByte']
            ]);
            
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    public function postcode(Request $request)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        

        $client = new Client();

        try {
            $response = $client->request('GET', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/postcode/'.$postcode, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ]
               
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            ZurichApiPostcode::create([
                'uuid' => $uuid,
                'stateCode' => $decodedBody['data']['stateCode'],
                'stateName' => $decodedBody['data']['stateName'],
                'regioncode' => $decodedBody['data']['regioncode'],
                'countryCode' => $decodedBody['data']['countryCode'],
                'countryName' => $decodedBody['data']['countryName'],
            ]);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


}
