<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEmailFor;
use App\Models\SetupMonthlyInstallment;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class SetupMonthlyInstallmentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-monthly-installment-list|setup-monthly-installment-create|setup-monthly-installment-edit|setup-monthly-installment-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-monthly-installment-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-monthly-installment-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-monthly-installment-delete', ['only' => ['destroy', 'status']]);
    }



    public function index()
    {
        $data = SetupMonthlyInstallment::orderBy('installment', 'Asc')->get();
        
        return view('pages.backend.setupmonthlyinstallment.index', compact('data'));
    }


    public function create()
    {
   
        
        return view('pages.backend.setupmonthlyinstallment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'installment'=>'required|integer|min:1|max:2000',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = SetupMonthlyInstallment::create($data);

        if($status){
            alert()->success('success','Successfully added setup Monthly installment');
        }
        else{
            alert()->error('error','Error occurred while adding setup Monthly installment');
        }
        return redirect()->route('setup-monthly-installment.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
  
        $data = SetupMonthlyInstallment::find($id);
        
        if(!$data){

            alert()->error('error','Setup Monthly installment not found');
        }
       
        return view('pages.backend.setupmonthlyinstallment.edit')
            ->with('data',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = SetupMonthlyInstallment::find($id);

        $this->validate($request,[
            
            'installment'=>'required|integer|min:1|max:2000',
            'status'=>'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Setup Monthly installment successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-monthly-installment.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupMonthlyInstallment::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup Monthly installment successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }

}
