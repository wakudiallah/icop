<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamEmploymentType;
use App\Models\ParamEmploymentStatus;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamEmploymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-employment-type-list|param-employment-type-create|param-employment-type-edit|param-employment-type-delete', ['only' => ['index','show']]);
         $this->middleware('permission:param-employment-type-create', ['only' => ['create','store']]);
         $this->middleware('permission:param-employment-type-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:param-employment-type-delete', ['only' => ['destroy', 'status']]);
    }
    
    
     public function index()
    {
        $data = ParamEmploymentType::get();
        
        return view('pages.backend.paramemploymenttype.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.backend.paramemploymenttype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'employment_type'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamEmploymentType::create($data);

        if($status){
            alert()->success('success','Successfully added param employment type');
        }
        else{
            alert()->error('error','Error occurred while adding param employment status');
        }
        return redirect()->route('param-employment-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParamEmploymentType::find($id);
        
        if(!$data){

            alert()->error('error','Param Employment Type not found');
        }
       
        return view('pages.backend.paramemploymenttype.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ParamEmploymentType::find($id);

        $this->validate($request,[
            'employment_type'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param Employment Type successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-employment-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function status(Request $request)
    {
        
        $user = ParamEmploymentType::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param Employment Type successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
