<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApplicationAdditionalProtectAddDriver;

class PraapplicationAddDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDrivers($id)
    {
        //
        $drivers = ApplicationAdditionalProtectAddDriver::where('uuid', $id)->get();
        return response()->json(['drivers' => $drivers]);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        // Validate the request
        $request->validate([
            'driver_name' => 'required|max:200',
            'driver_nric' => 'required|max:200',
            'code_relation' => 'nullable',
            'uuid' => 'nullable',
        ]);

        //Check if the driver already exists
        $exiDriver = ApplicationAdditionalProtectAddDriver::where('driver_nric', $request->driver_nric)->first();

        if($exiDriver){
            return response()->json(['message' => 'Driver already exists'], 400);
        }

        // Create a new driver
        $driver = ApplicationAdditionalProtectAddDriver::create([
            'driver_name' => $request->driver_name,
            'driver_nric' => $request->driver_nric,
            'code_relation' => $request->code_relation,
            'uuid' => $request->uuid,
        ]);

        // Return the newly created driver as JSON
        return response()->json(['driver' => $driver]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        try{
            $driver = ApplicationAdditionalProtectAddDriver::findOrFail($id);
            $driver->delete();

            return response()->json(['success' => true]);

        }catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }
}
