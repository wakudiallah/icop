<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SetupMaintainableEmail;
use App\Models\ParamEmailFor;
use App\Models\ParamEmailSystem;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class SetupMaintainableEmailController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-maintainable-emails-list|setup-maintainable-emails-create|setup-maintainable-emails-edit|setup-maintainable-emails-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-maintainable-emails-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-maintainable-emails-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-maintainable-emails-delete', ['only' => ['destroy', 'status']]);
    }



    public function index()
    {
        $data = SetupMaintainableEmail::get();
        
        return view('pages.backend.setupmaintanableemail.index', compact('data'));
    }


    public function create()
    {
        $emailsystem =ParamEmailSystem::where('status', '1')->get();
        $for =ParamEmailFor::where('status', '1')->get();

        
        return view('pages.backend.setupmaintanableemail.create', compact('emailsystem', 'for'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'email_for_id'=>'required|min:1',
            'email_system_id'=>'required|min:1',
            'greeting'=>'string|required|min:1|max:2000',
            'text_email'=>'string|required|max:2000',
            'closing'=>'string|nullable|max:200',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = SetupMaintainableEmail::create($data);

        if($status){
            alert()->success('success','Successfully added setup maintanable email');
        }
        else{
            alert()->error('error','Error occurred while adding setup maintanable email');
        }
        return redirect()->route('setup-maintainable-emails.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SetupMaintainableEmail::find($id);
        $emailsystem =ParamEmailSystem::where('status', '1')->get();
        $for =ParamEmailFor::where('status', '1')->get();
        
        if(!$data){

            alert()->error('error','Setup maintanable email not found');
        }
       
        return view('pages.backend.setupmaintanableemail.edit')
            ->with('data',$data)
            ->with('emailsystem',$emailsystem)
            ->with('for',$for);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = SetupMaintainableEmail::find($id);

        $this->validate($request,[
            
            'email_for_id'=>'required|min:1',
            'email_system_id'=>'required|min:1',
            'greeting'=>'string|required|min:1|max:2000',
            'text_email'=>'string|required|max:2000',
            'closing'=>'string|nullable|max:200',
            'status'=>'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','setup maintanable email successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-maintainable-emails.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupMaintainableEmail::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup maintanable email successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }

}
