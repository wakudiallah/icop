<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamRegionlkm; 
use App\Models\ParamOwnershiplkm; 

class CalculateRoadTaxController extends Controller
{
    //
    public function index()
    {
        $region = ParamRegionlkm::where('status', 1)->get();
        $owner = ParamOwnershiplkm::where('status', 1)->get();
        
        return view('pages.backend.calculateroadtax.index', compact('region', 'owner'));
    }


    public function store(Request $request)
    {

        $region = $request->region;
        $type = $request->owner;
        $engine = $request->engine;

        $engineData = (int)$engine;


        if ($region == 1) {
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 55;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 90;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.4) + 200;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.5) + 280;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 1) + 380;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 2.5) + 880;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 4.5) + 2130;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 110;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 140;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 180;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.8) + 400;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 1) + 560;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 3) + 760;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 7.5) + 2260;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 13.5) + 6010;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            }
            else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 85;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 100;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 120;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.3) + 300;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.4) + 360;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.8) + 440;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 1.6) + 840;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.6) + 1640;
                }

            }

        }
        else if ($region == 2) {
            if ($type == 1 || $type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 44;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 56;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 72;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.32) + 160;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.25) + 224;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.5) + 274;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 1) + 524;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.35) + 1024;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            }
            else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 42.5;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 50;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 60;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.17) + 165;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.22) + 199;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.44) + 243;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.88) + 463;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.2) + 903;
                }
            }
        }
        else if ($region == 3) {
            // console.log('$region3');
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 27.50;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 35;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 45;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.2) + 100;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.25) + 140;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.5) + 190;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.75) + 440;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 2.25) + 1065;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 55;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 90;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.4) + 200;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.5) + 280;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 1.5) + 380;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 3.75) + 1100;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 6.75) + 3005;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            } else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 42.50;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 50;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.15) + 150;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.2) + 180;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.4) + 220;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.8) + 420;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.8) + 820;
                }
            }
        }
        else if ($region == 4) {
            // console.log('$region4');
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 22;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 28;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 36;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.16) + 80;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.13) + 112;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.25) + 137;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.5) + 262;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.68) + 512;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 22;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 28;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 36;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.16) + 80;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.13) + 112;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.25) + 137;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.5) + 262;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.68) + 512;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            } else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 21.25;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 25;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 30;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.89) + 82;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.61) + 99;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.72) + 121;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.94) + 231;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.10) + 451;
                }
            }
        }
        else {
            
        }

        return response()->json(['roadtax_amount' => $roadtax_amount]);
        
    }


    public function calculateRoadtax($params){

        $region = $params['region'];
        $type = $params['type'];
        $engine = $params['engine'];

        $engineData = (int)$engine;

        
        if ($region == 1) {
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 55;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 90;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.4) + 200;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.5) + 280;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 1) + 380;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 2.5) + 880;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 4.5) + 2130;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 110;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 140;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 180;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.8) + 400;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 1) + 560;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 3) + 760;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 7.5) + 2260;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 13.5) + 6010;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            }
            else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 85;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 100;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 120;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.3) + 300;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.4) + 360;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.8) + 440;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 1.6) + 840;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.6) + 1640;
                }

            }

        }
        else if ($region == 2) {
            if ($type == 1 || $type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 44;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 56;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 72;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.32) + 160;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.25) + 224;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.5) + 274;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 1) + 524;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.35) + 1024;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            }
            else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 42.5;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 50;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 60;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.17) + 165;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.22) + 199;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.44) + 243;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.88) + 463;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.2) + 903;
                }
            }
        }
        else if ($region == 3) {
            // console.log('$region3');
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 27.50;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 35;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 45;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.2) + 100;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.25) + 140;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.5) + 190;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.75) + 440;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 2.25) + 1065;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 55;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 90;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.4) + 200;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.5) + 280;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 1.5) + 380;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 3.75) + 1100;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 6.75) + 3005;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            } else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 42.50;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 50;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 70;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.15) + 150;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.2) + 180;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.4) + 220;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.8) + 420;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.8) + 820;
                }
            }
        }
        else if ($region == 4) {
            // console.log('$region4');
            if ($type == 1) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 22;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 28;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 36;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.16) + 80;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.13) + 112;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.25) + 137;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.5) + 262;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.68) + 512;
                }
            } else if ($type == 2) {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 22;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 28;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 36;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.16) + 80;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.13) + 112;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.25) + 137;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.5) + 262;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 0.68) + 512;
                }
            } else if ($type == 4) {
                if ($engine <= 150) {
                    $roadtax_amount = 2;
                } else if ($engine >= 151 && $engine <= 200) {
                    $roadtax_amount = 30;
                } else if ($engine >= 201 && $engine <= 250) {
                    $roadtax_amount = 50;
                } else if ($engine >= 251 && $engine <= 500) {
                    $roadtax_amount = 100;
                } else if ($engine >= 501 && $engine <= 800) {
                    $roadtax_amount = 250;
                } else if ($engine >= 801) {
                    $roadtax_amount = 350;
                }
            } else {
                if ($engine <= 1000) {
                    $roadtax_amount = 20;
                } else if ($engine >= 1001 && $engine <= 1200) {
                    $roadtax_amount = 21.25;
                } else if ($engine >= 1201 && $engine <= 1400) {
                    $roadtax_amount = 25;
                } else if ($engine >= 1401 && $engine <= 1600) {
                    $roadtax_amount = 30;
                } else if ($engine >= 1601 && $engine <= 1800) {
                    $roadtax_amount = ((($engineData - 1601) + 1) * 0.89) + 82;
                } else if ($engine >= 1801 && $engine <= 2000) {
                    $roadtax_amount = ((($engineData - 1801) + 1) * 0.61) + 99;
                } else if ($engine >= 2001 && $engine <= 2500) {
                    $roadtax_amount = ((($engineData - 2001) + 1) * 0.72) + 121;
                } else if ($engine >= 2501 && $engine <= 3000) {
                    $roadtax_amount = ((($engineData - 2501) + 1) * 0.94) + 231;
                } else if ($engine >= 3001) {
                    $roadtax_amount = ((($engineData - 3001) + 1) * 1.10) + 451;
                }
            }
        }
        else {
            
        }

        

        $roadtax = [
            'roadtax_amount' => $roadtax_amount,
        ];
                    
        return $roadtax;
        
    }

}
