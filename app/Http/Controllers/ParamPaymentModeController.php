<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamPaymentMode;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamPaymentModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct()
     {
         $this->middleware('auth');
         $this->middleware('permission:param-payment-mode-list|param-payment-mode-create|param-payment-mode-edit|param-payment-mode-delete', ['only' => ['index','show']]);
         $this->middleware('permission:param-payment-mode-create', ['only' => ['create','store']]);
         $this->middleware('permission:param-payment-mode-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:param-payment-mode-delete', ['only' => ['destroy', 'status']]);
     }
    
    
    public function index()
    {
        $data = ParamPaymentMode::get();
        
        return view('pages.backend.parampaymentmode.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.backend.parampaymentmode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'payment_mode'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamPaymentMode::create($data);

        if($status){
            alert()->success('success','Successfully added param payment mode');
        }
        else{
            alert()->error('error','Error occurred while adding param payment mode');
        }
        return redirect()->route('param-payment-mode.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamPaymentMode::find($id);
        
        if(!$data){

            alert()->error('error','Param payment mode not found');
        }
       
        return view('pages.backend.parampaymentmode.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamPaymentMode::find($id);

        $this->validate($request,[
            'payment_mode'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param payment mode successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-payment-mode.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamPaymentMode::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param payment mode successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
