<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamBankName;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamBankNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-bank-name-list|param-bank-name-create|param-bank-name-edit|param-bank-name-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-bank-name-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-bank-name-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-bank-name-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        //
        $data = ParamBankName::get();
        
        return view('pages.backend.parambankname.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //
        return view('pages.backend.parambankname.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'bank_name'=>'string|required|max:80',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamBankName::create($data);

        if($status){
            alert()->success('success','Successfully added param bank name');
        } 
        else{
            alert()->error('error','Error occurred while adding param bank name');
        }
        return redirect()->route('param-bank-name.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamBankName::find($id);
        
        if(!$data){

            alert()->error('error','Param bank name not found');
        }
       
        return view('pages.backend.parambankname.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamBankName::find($id);

        $this->validate($request,[
            'bank_name'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param bank name successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-bank-name.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamBankName::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param bank name successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
