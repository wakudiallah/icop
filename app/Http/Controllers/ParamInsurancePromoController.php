<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInsurancePolicy;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePromo; 
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamInsurancePromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-insurance-promo-list|param-insurance-promo-create|param-insurance-promo-edit|param-insurance-promo-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-insurance-promo-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-insurance-promo-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-insurance-promo-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        $data = ParamInsurancePromo::get();
        
        return view('pages.backend.paraminsurancepromo.index', compact('data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     //

        $insCompany = ParamInsuranceCompany::where('status', 1)->get();

        return view('pages.backend.paraminsurancepromo.create', compact('insCompany'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //Termasuk 24-Jam Khidmat Tunda sebanyak RM 200 setiap tahun

    public function store(Request $request)
    {
        //


        $this->validate($request,
        [
            'insurance_product_id'=>'required|max:80',
            'promo'=>'required|max:500',
            'promo_english'=>'nullable',
            'status'=>'required',
        ]);

        $data=$request->all();

        $status = ParamInsurancePromo::create($data);

        if($status){
            alert()->success('success','Successfully added param insurance promo');
        } 
        else{
            alert()->error('error','Error occurred while adding param insurance promo');
        }
        return redirect()->route('param-insurance-promo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamInsurancePromo::find($id);
        $insCompany = ParamInsuranceCompany::where('status', 1)->get();
        
        if(!$data){

            alert()->error('error','Param insurance promo found');
        }
       
        return view('pages.backend.paraminsurancepromo.edit')
        ->with('data',$data)
        ->with('insCompany',$insCompany);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamInsurancePromo::find($id);

        $this->validate($request,[
            
            'insurance_product_id'=>'required|max:80',
            'promo'=>'required|max:500',
            'promo_english'=>'nullable',
            'status' => 'required|in:1,0',

        ]);

       

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param insurance promo successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-insurance-promo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamInsurancePromo::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param insurance promo successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
