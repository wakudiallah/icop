<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\User;
use App\Models\Praaplication;
use App\Models\ParamProtectionType;
use App\Models\Postcode;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePolicy;
use App\Models\PerlindunganTambahan;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\ApplicationSelectInsurance;
use App\Models\ApplicationAdditionalProtect;
use App\Models\ApplicationFinancing;
use App\Models\SetupDocument;
use App\Models\SetupMonthlyInstallment;
use App\Models\PartnerApiAccount;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ZurichApiPostcode;
use App\Models\ParamDutiSetem;
use App\Models\ParamSst;
use App\Models\ZurichMaritalStatus;
use App\Models\ZurichRelationship;
use App\Models\ZurichCoverType;
use Illuminate\Support\Str;
use Hash;
use App\Events\NewContentNotification;
use Pusher\Pusher;
use DateTime;
use Exception;
use DateTimeZone;
use Illuminate\Support\Facades\Log;


use Illuminate\Http\Request;

class PraaplicationController extends Controller
{
    
    public function index()
    {
        
        /*return view('pages.frontend.praaplication.index');*/

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        $userName = $data->userName;
        $token = $data->token;



        return view('pages.frontend.apps-icop.index',  compact('token'));
        
        /*$data = User::get();
        return view('pages.backend.users.index', compact('data')); */
    }


    public function store(Request $request)
    {
        
        
        $this->validate($request,
        [
            'vehicle'=>'string|required',
            'ic'=>'required|nullable',
            'ic2'=>'string|nullable',
            'ehailing'=>'nullable',
            'email'=>'nullable',
            'whatsapp'=>'nullable',
            'postcode'=>'nullable',
        ]);

        $uuid = Str::uuid();
        
        $data = $request->all();
        $data['uuid'] = $uuid;

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        $expired_utc = $data->expired;

        
        //Check Expired then generate new token
        $expired_utc_datetime = new DateTime($expired_utc, new DateTimeZone('UTC'));

        // Convert to Malaysia Time (UTC+8)
        $expired_malaysia_datetime = clone $expired_utc_datetime;
        $expired_malaysia_datetime->setTimezone(new DateTimeZone('Asia/Kuala_Lumpur'));

        // Get the current time in Malaysia Time
        $current_malaysia_datetime = new DateTime('now', new DateTimeZone('Asia/Kuala_Lumpur'));

        

        // Check if the expired time is in the past
        if ($expired_malaysia_datetime < $current_malaysia_datetime) {
            
            try{
                // Generate a new token
                $response = app('App\Http\Controllers\ZurichApiController')->token();

                

            }catch (Exception $e) {

                //dd($e);
                Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

                return back()->withInput()->with('error', '303 : Problem get vahicle info, Pls try again later');

            }
            
        } 

        $vehicle = $request->input('vehicle');
        $ic = $request->input('ic');
        $postcode = $request->input('postcode');

        //
        alert()->success('success','Successfully submitted data');

        return redirect()->route('quote.show', ['uuid' => $uuid, 'vehicle' => $vehicle, 'ic' => $ic, 'postcode' => $postcode]);

                       
    }


    public function show($uuid, Request $request)
    {
       
        $vehicle = $request->query('vehicle');
        $ic = $request->query('ic');
        $postcode = $request->query('postcode');


        $firstPart = substr($ic, 0, 6);
        $secondPart = substr($ic, 6, 2);
        $thirdPart = substr($ic, 8, 4);

        $icFormat = $firstPart . '-' . $secondPart . '-' . $thirdPart;

        
        $zMaritalSts = ZurichMaritalStatus::get();

        $checkPostcode =  Postcode::join('states', 'states.state_code', '=', 'postcodes.state_code')
                        ->where('postcodes.postcode', $postcode)
                        ->select('states.alias_name')
                        ->first();
        
        try{
        
            // Call the method from SecondController
            $response = app('App\Http\Controllers\ZurichApiController')->getvehicleinfo($vehicle, $icFormat, $uuid);
            
            $vehRegNo = $response['vehRegNo'];
            $vehChassisNo = $response['vehChassisNo'];
            $vehEngineNo = $response['vehEngineNo'];
            $vehMakeYear = $response['vehMakeYear'];
            $brand = $response['brand'];
            $model = $response['model'];  //blm ada

        
            $variant = $response['variant'];
            $engineSize = $response['engineSize'];
            $cc = $response['cc'];
            $ncd = $response['ncd'];


            

            $Vehiclevarint = ZurichApiGetVehicleInfoVariant::where('uuid', $uuid)->get();

            //$apiPostcode = app('App\Http\Controllers\ZurichApiController')->postcode($postcode, $uuid);

            return view('pages.frontend.apps-icop.quote-vehicle', compact('uuid', 'vehicle', 'ic', 'postcode', 'checkPostcode', 'vehRegNo', 'vehChassisNo', 'vehEngineNo', 'vehMakeYear', 'brand', 'model', 'variant', 'engineSize', 'cc', 'ncd', 'zMaritalSts', 'Vehiclevarint'));

        }catch (Exception $e) {

            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            //dd($e);

            return back()->withInput()->with('error', '303 : error get vahicle info, Pls try again later');

        }

    }

    

    public function choseInsurance(Request $request)
    {
    
        $uuid = $request->uuid;
        $vehicle = $request->vehicle;

        $insCompany = ParamInsuranceCompany::with('promo')->where('status', 1)->get();


        $lastItem = Praaplication::where('reg_no', 'LIKE', 'CAR%')->orderBy('reg_no', 'desc')->first();
        if ($lastItem) {
            //$lastId = (int)$lastItem->reg_no;
            $lastNumericId = (int) substr($lastItem->reg_no, 3);
            $newNumericId = str_pad($lastNumericId + 1, 8, '0', STR_PAD_LEFT);
        } else {
            $newNumericId = '00000001';
        }
        $newId = 'CAR' . $newNumericId;

        $ic = $request->ic;
        $email = $request->email;
        $fullname = $request->fullname;
        $phone = $request->phone;
        $marital_status = $request->marital_status;
        $address1 = $request->address1;
        $address2 = $request->address2;
        $state = $request->state;
        $vehicle = $request->vehicle;
        $postcode = $request->postcode;

        // Extract year, month, and day
        $year = substr($ic, 0, 2);
        $month = substr($ic, 2, 2);
        $day = substr($ic, 4, 2);

        // Determine the full year based on current date context
        $currentYear = (int)date('Y');
        $currentYearShort = (int)substr($currentYear, 2, 2);
        $century = ($year <= $currentYearShort) ? '20' : '19';
        $fullYear = $century . $year;

        $dateOfBirth = $day.'/'.$month.'/'.$fullYear;


        //*Gender
        $jantina = substr($ic,10, 2); 

        if ($jantina % 2 == 0) {
            $gender =  "F";
        } else {
            $gender =  "M";
        }

        //*Age
        $dobFormat = $fullYear . '-' . $month . '-' . $day;

        // Convert date of birth to a DateTime object
        $dobDate = new DateTime($dobFormat);
        $currentDate = new DateTime();  // Current date

        // Calculate the age
        $age = $currentDate->diff($dobDate)->y;


        $praaplication = Praaplication::where('uuid', $uuid)
                ->select('vehicle', 'ic', 'email')
                ->orderBy('id', 'desc')
                ->first();

        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $cid = ApplicationDocument::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $selectInsurance = ApplicationSelectInsurance::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $addProtect = ApplicationAdditionalProtect::where('uuid',  $uuid)->orderBy('id', 'desc')->first();
        $finance = ApplicationFinancing::where('uuid', $uuid)->orderBy('id', 'desc')->first();


        $relation = ZurichRelationship::get();


        $vehicleInfo = ZurichApiGetVehicleInfo::where('uuid', $uuid)
                ->select('vehMake', 'vehEngineNo', 'vehChassisNo', 'vehMakeYear', 'vehMake', 'nxtPolEffDate', 'nxtPolExpDate')
                ->orderBy('id', 'desc')
                ->first();

        $vehicleInfoVar = ZurichApiGetVehicleInfoVariant::where('uuid', $uuid)
                ->select('vehModelCode', 'capacity', 'uom', 'vehSeat', 'marketValue', 'vehModel')
                ->orderBy('id', 'desc')
                ->first();

        

        //Abisi delete .00
        $abisiMarketValue = $vehicleInfoVar->marketValue;
        $abisiX = str_replace(".00", "", $abisiMarketValue);
        $abisi = (int) $abisiX;

        $percentage = 0.20;
        $sumInsured =  $abisi * (1 + $percentage); //20% dari abisi

        
        //effDate
        $formatted_effDate = DateTime::createFromFormat('Y-m-d', $vehicleInfo->nxtPolEffDate)->format('d/m/Y');

        //expDate
        $formatted_expDate = DateTime::createFromFormat('Y-m-d', $vehicleInfo->nxtPolExpDate)->format('d/m/Y');


        //capacity
        $capacity = (string) $vehicleInfoVar->capacity;


        //ZurichApiPostcode
        $postcodeGet = ZurichApiPostcode::where('uuid', $uuid)
                ->orderBy('id', 'desc')
                ->first();


        if($praaplication){

            $praaplication->update([
                'ic' => $ic,
                'email' => $email,
                'vehicle' => $vehicle
            ]);

        }else{

            Praaplication::create([
                'uuid' => $uuid,
                'reg_no' => $newId,
                'ic' => $ic,
                'email' => $email,
                'vehicle' => $vehicle,
                'app_status' => "NW"
    
            ]);

        }


        if($addProtect){
            $addProtect->update([
                'uuid' => $uuid,
            ]);
        }else{
            ApplicationAdditionalProtect::create([
                'uuid' => $uuid,
            ]);
        }

        
        if($cif){

            $cif->update([
                'fullname' => $fullname,
                'phone' => $phone,
                'marital_status' => $marital_status,
                'address1' => $address1,
                'address2' => $address2,
                'postcode' => $postcode,
                'state' => $state,
            ]);

        }else{
            ApplicationCustomerInfo::create([
                'uuid' => $uuid,
                'fullname' => $fullname,
                'phone' => $phone,
                'address1' => $address1,
                'address2' => $address2,
                'postcode' => $postcode,
                'state' => $state,
                'dob' => $dobFormat,
                'age' => $age,
                'gender' => $gender

            ]);
        }
        
        if($cid){
            $cid->update([
                'uuid' => $uuid,
            ]);
        }else{
            ApplicationDocument::create([
                'uuid' => $uuid,
            ]);
        }

      
        if($finance){
            $cid->update([
                'uuid' => $uuid,
            ]);
        }else{
            ApplicationFinancing::create([
                'uuid' => $uuid,
            ]);
        }

        
        if($selectInsurance){

            $selectInsurance->update([
                'uuid' => $uuid,
            ]);

        }else{

            ApplicationSelectInsurance::create([
                'uuid' => $uuid,
            ]);

        }


        $params = [
            'uuid' => $uuid, 
            'newId' => $newId, 
            'ic' => $ic, 
            'vehicleNo' => $vehicle, 
            'email' => $email, 
            'fullname' => $fullname, 
            'phone' => $phone, 
            'address1' => $address1, 
            'address2' => $address2, 
            'state' => $state, 
            'dateOfBirth' => $dateOfBirth,
            'gender' => $gender,
            'age' => $age,
            'uom' => $vehicleInfoVar->uom,
            'capacity' => $capacity,
            'engineNo' => $vehicleInfo->vehEngineNo,
            'chassisNo' => $vehicleInfo->vehChassisNo,
            'abisi' => 28000, // $abisi, 
            'sumInsured' => 30000, //$abisi + 2000, //$sumInsured,
            'yearOfMake' => $vehicleInfo->vehMakeYear,
            'make' => $vehicleInfo->vehMake,
            'model' => $vehicleInfoVar->vehModelCode,
            'noOfPassenger' => $vehicleInfoVar->vehSeat,
            'effDate' => $formatted_effDate,
            'expDate' => $formatted_expDate,
            'postCode' => $postcode,
            'regionCode' => $postcodeGet->regioncode,
            'state' => $postcodeGet->stateCode,
            'country' => $postcodeGet->countryCode,
            'maritalSts' => $marital_status
            
        ];
        
        //dd($params);

        //Select Insurance and calculate insurance
        try{

            //$basicPremium = 400000;

            $response = app('App\Http\Controllers\ZurichApiController')->calculatepremium($params);

            $dutisetem = ParamDutiSetem::where('status', 1)->orderBy('id', 'Desc')->first();
            $sst = ParamSst::where('status', 1)->orderBy('id', 'Desc')->first();

            //dd($sst);
            //dd($response['basicPrem']);
            
            $basicPremium = $response['basicPrem'];
            $stampDutyAmt = $response['stampDutyAmt'];
            $ssT_Pct = $response['ssT_Pct'];
            $ssT_Amt = $response['ssT_Amt'];
            $rebateAmt = $response['rebateAmt'];
            $rebatePct = $response['rebatePct'];
            $grossPrem = $response['grossPrem'];
            $ncdAmt = $response['ncdAmt'];
            $realBacicPremium = str_replace(',', '', $basicPremium);

            
            return view('pages.frontend.apps-icop.select-insurance', compact('uuid', 'dutisetem', 'sst', 'basicPremium', 'realBacicPremium', 'relation', 'stampDutyAmt', 'ssT_Pct', 'ssT_Amt', 'rebateAmt', 'rebatePct', 'grossPrem', 'ncdAmt', 'insCompany'));

        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            //dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }

    }



    public function choseAdditional(Request $request)
    {
        $uuid = $request->uuid;
        $basic_premium = $request->basic_premium;
        $sst = $request->sst;
        $duti_setem = $request->duti_setem;
        $insurance_id = $request->insurance_id;
        $payable = $request->payable;


        $data = Praaplication::where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();

        $relation = ZurichRelationship::get();
        $coverType = ZurichCoverType::get();

        $checkAccount = User::where('email', $data->email)->exists();

        //Save Aplication Selected Insurance 
        ApplicationSelectInsurance::where('uuid', $uuid)->update([
            'insurance_id' => $insurance_id,
            'basic_premium' => $basic_premium,
            'sst' => $sst,
            'duti_setem' => $duti_setem,
            'payable' => $payable,
        ]);

        $selectedInsurance = ApplicationSelectInsurance::where('uuid', $uuid)
            ->orderBy('id', 'Desc')
            ->first();



        return view('pages.frontend.apps-icop.additional-protection', compact('uuid', 'data', 'cif', 'checkAccount', 'selectedInsurance', 'relation', 'coverType'));
    }


    public function financing(Request $request)
    {
       
        $uuid = $request->uuid;
        $total_payable = $request->total_payable;
        $fullname = $request->fullname;
        $a = $request->a;

        //dd($total_payable);


        $data = ApplicationDocument::where('uuid', $uuid)->first();
        $checkAccount = User::where('email', $request->email_hidden)->exists();
        $checkEmail = User::where('email', $request->email)->exists();

        

        Praaplication::where('uuid', $request->uuid)->update([
            
            'pay_status' => 'F',
            
        ]);


        ApplicationAdditionalProtect::where('uuid', $request->uuid)->update([
            
            'total_payable' => $total_payable,
            'roadtax' => 90,
            
        ]);


        ApplicationFinancing::where('uuid', $request->uuid)->update([
            
            'total_payable' => $total_payable,
            
        ]);



        /*  ========   Check if Guest Account Create is disable ===========  */
        if( Auth::guest() && $checkEmail == false){
            $user = User::create([
                'name' => $request->fullname,
                'email' => $request->email,
                'password' =>  Hash::make($request->password),
                'role' => 6,
                'status' => 1,
            ]);


            if ($user) {
                // Log in the user
                Auth::login($user);
        
                $user->save();
            }

            
            return redirect('financing/'.$uuid);


        } elseif(Auth::guest() && $checkEmail == true){  //just login

            
            $credentials = $request->only('email', 'password');
 
            if (Auth::attempt($credentials)) {
                
                return redirect()->intended('financing/'.$uuid);

            }else{

                return Redirect::back()->withErrors(['msg' => 'The Message']);
            }

        }else{


            return redirect('financing/'.$uuid);

        }
      
        

    }


    public function getFinancing($uuid)
    {

        $data = ApplicationDocument::where('uuid', $uuid)->first();

        $doc = SetupDocument::where('code', 'BPA1')->where('status', 1)->first();
        $monthly = SetupMonthlyInstallment::orderBy('installment', 'Asc')->where('status', 1)->get();

        $finance = ApplicationFinancing::orderBy('id', 'Desc')->first();
        $addProtect = ApplicationAdditionalProtect::orderBy('id', 'Desc')->first();


        //$cif = ApplicationCustomerInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();

        return view('pages.frontend.apps-icop.financing', compact('uuid', 'data', 'doc', 'monthly', 'finance', 'addProtect'));

    }



    public function dashboardCust(Request $request)
    {
        $uuid = $request->uuid;
        $monthly_installment = $request->monthly;
        $schema = $request->schema;

        //dd($monthly_installment);

        $myId = Auth::user()->email;

        $Pra = Praaplication::where('email', $myId)->get();

        //dd($myId);


        $finacing = ApplicationFinancing::where('uuid', $uuid)->update([
            
            'deduc_schema' => $schema,
            'monthly_installment' => $monthly_installment
            
        ]);

      

        $thisApps = Praaplication::where('uuid', $uuid)->first();


        // Check if the form has already been submitted
        $exists = ApplicationDocument::where('uuid', $uuid)
                ->where('nric', Null)
                ->where('payslip', Null)  
                ->where('bpa179', Null)    
                ->exists();

        if ($exists) {
            alert()->error('error','Document not complete');

            return back()->with('error', 'Document not complete');
        }


        try {
            //Pusher Notif For Admin & Super Admin
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );

            $pusher = new Pusher(
                'acf4bdadcc5bac4824d5',
                '7bff4ae40954ca99818a',
                '1822123',
                $options
            );

            $recipientIds = ['2', '4'];    //Role Super Admin & Admin

            $dataSend = [
                'message' => "New application ". $thisApps->reg_no,
                'from' => $thisApps->email
            ];

            foreach($recipientIds as $recipientId){

                $pusher->trigger("private.$recipientId" , 'my-event', $dataSend);

            }
        } catch (\Exception $e) {

        }
        //End Pusher Notif For Admin & Super Admin




        return view('pages.frontend.apps-icop.welcoming-customer', compact('uuid', 'Pra'));
    }






    public function mfast()
    {
        /*$data = User::get();*/
        
        return view('pages.frontend.praaplication.index'); 
    }



    public function selectInsurance($id)
    {   

      

        $inCompany = ParamInsuranceCompany::where('id', 1)->first();
        $inPolicy = ParamInsurancePolicy::where('insurance_company_id', 1)->first();

        $inCompany2 = ParamInsuranceCompany::where('id', 6)->first();
        $inPolicy2 = ParamInsurancePolicy::where('insurance_company_id', 6)->first();


        return view('pages.frontend.praaplication.page3', compact('inCompany', 'inPolicy', 'inCompany2', 'inPolicy2', 'id')); 
    }




    public function selectPerlindungan($id)
    {   

        $inCompany = ParamInsuranceCompany::where('id', 1)->first();
        $inPolicy = ParamInsurancePolicy::where('insurance_company_id', 1)->first();

        return view('pages.frontend.praaplication.page31', compact('inCompany', 'inPolicy', 'id')); 
    }


    public function optionMonth($id)
    {   
      
        $pra = 'a';
        $doc = ApplicationDocument::where('uuid', $id)->first();


        return view('pages.frontend.praaplication.option', compact('pra', 'id', 'doc')); 
    }



    
    public function uploadIc(Request $request, $id)
    {   

        $base64Data = "data:image/png;base64,".base64_encode(file_get_contents($request->file('file')->path()));

        $photo=$request->file("file");
        $imageName=time().'_'.$photo->getClientOriginalName();
        $imageName2=$photo->getClientOriginalName();
       
        $ext = $photo->getClientOriginalExtension();
        $size = $photo->getSize();

        $extension = $ext;
        $size = $size;

        $path = $request->file('file')->store('uploads');

        

        $update = ApplicationDocument::where('uuid',$id)->update(array(
            
            'nric' => $base64Data,
            'sz_ic' => $size,
            'ext_ic' => $extension
        ));
        

        // Determine the file type
        $fileType = 'other';
        $extension = strtolower($request->file('file')->getClientOriginalExtension());
        if(in_array($extension, ['jpg', 'jpeg', 'png'])) {
            $fileType = 'image';
        } elseif($extension === 'pdf') {
            $fileType = 'pdf';
        }

        //shell_exec("chmod -R 775 /storage/$path");


    
        return response()->json([
            'file_path' => asset('storage/' . $path),
            'file_type' => $fileType,
            'base64Data' => $base64Data
        ]);

    }


    public function uploadPayslip(Request $request, $id)
    {   

        $base64Data = "data:image/png;base64,".base64_encode(file_get_contents($request->file('file')->path()));

        $photo=$request->file("file");
        $imageName=time().'_'.$photo->getClientOriginalName();
        $imageName2=$photo->getClientOriginalName();
       
        $ext = $photo->getClientOriginalExtension();
        $size = $photo->getSize();

        $extension = $ext;
        $size = $size;

        $path = $request->file('file')->store('uploads');


        $update = ApplicationDocument::where('uuid',$id)->update(array(
            
            'payslip' => $base64Data,
            'sz_pay' => $size,
            'ext_pay' => $extension
        ));


        // Determine the file type
        $fileType = 'other';
        $extension = strtolower($request->file('file')->getClientOriginalExtension());
        if(in_array($extension, ['jpg', 'jpeg', 'png'])) {
            $fileType = 'image';
        } elseif($extension === 'pdf') {
            $fileType = 'pdf';
        }

        //shell_exec("chmod -R 775 /storage/$path");


    
        return response()->json([
            'file_path' => asset('storage/' . $path),
            'file_type' => $fileType,
            'base64Data' => $base64Data
        ]);

    }


    public function uploadBpa(Request $request, $id)
    {   

        try {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                
                // Check if the uploaded file is an image or PDF
                $fileType = $file->getClientMimeType();
                
                if (strpos($fileType, 'image') !== false) {
                    // If it's an image, encode it as a base64 string
                    $base64Data = "data:image/png;base64," . base64_encode(file_get_contents($file->path()));
                } elseif ($fileType === 'application/pdf') {
                    // If it's a PDF, encode it as a base64 string
                    $base64Data = "data:application/pdf;base64," . base64_encode(file_get_contents($file->path()));
                } else {
                    // Unsupported file type
                    return response()->json(['error' => 'Unsupported file type'], 400);
                }
                
                // Save the base64 encoded data in the database
                $update = ApplicationDocument::where('uuid', $id)->update([
                    'bpa179' => $base64Data,
                    'sz_bpa' => $file->getSize(),
                    'ext_bpa' => $file->getClientOriginalExtension()
                ]);

                // Determine the file type
                $fileType = 'other';
                $extension = strtolower($request->file('file')->getClientOriginalExtension());
                if(in_array($extension, ['jpg', 'jpeg', 'png'])) {
                    $fileType = 'image';
                } elseif($extension === 'pdf') {
                    $fileType = 'pdf';
                }

                return response()->json([
                    'file_type' => $fileType,
                    'base64Data' => $base64Data
                ]);
            }

            return response()->json(['error' => 'File not uploaded'], 400);

        } catch (\Exception $e) {
            Log::error('File upload error: ' . $e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    

    }

    

}
