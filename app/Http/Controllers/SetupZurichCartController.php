<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SetupZurichCart;
use App\Models\ZurichAllowableCartDay;
use App\Models\ZurichAllowableCartAmount;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SetupZurichCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *  
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:setup-zurichcart-list|setup-zurichcart-create|setup-zurichcart-edit|setup-zurichcart-delete', ['only' => ['index','show']]);
        $this->middleware('permission:setup-zurichcart-create', ['only' => ['create','store']]);
        $this->middleware('permission:setup-zurichcart-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:setup-zurichcart-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        //
        $data = SetupZurichCart::get();
        
        return view('pages.backend.setupzurichcart.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $day = ZurichAllowableCartDay::get();
        $amount = ZurichAllowableCartAmount::get();

        return view('pages.backend.setupzurichcart.create', compact('day', 'amount'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            
            'cart_day_id'=>'required',
            'cart_amount_id'=>'required',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = SetupZurichCart::create($data);


        if($status){
            alert()->success('success','Successfully added Setup Zurich cart');
        } 
        else{
            alert()->error('error','Error occurred while adding Setup Zurich cart');
        }
        return redirect()->route('setup-zurich-cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $day = ZurichAllowableCartDay::get();
        $amount = ZurichAllowableCartAmount::get();
        $data = SetupZurichCart::find($id);

        
        if(!$data){

            alert()->error('error','Setup smsakad not found');
        }
       
        return view('pages.backend.setupzurichcart.edit', compact('day', 'amount'))->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = SetupZurichCart::find($id);

        $this->validate($request,[
            'cart_day_id'=>'required',
            'cart_amount_id'=>'required',
            'status'=>'required',
        ]);
        $data=$request->all();
       
        $status = $brand->update($data);

        if($status){
            
            alert()->success('success','Setup zurich cart successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('setup-zurich-cart.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = SetupZurichCart::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Setup zurich Cart successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
