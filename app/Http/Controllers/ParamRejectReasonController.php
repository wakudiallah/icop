<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamRejectReason;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamRejectReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
         $this->middleware('permission:param-reject-reason-list|param-reject-reason-create|param-reject-reason-edit|param-reject-reason-delete', ['only' => ['index','show']]);
         $this->middleware('permission:param-reject-reason-create', ['only' => ['create','store']]);
         $this->middleware('permission:param-reject-reason-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:param-reject-reason-delete', ['only' => ['destroy', 'status']]);
     }


    public function index()
    {
        //
        $data = ParamRejectReason::get();
        
        return view('pages.backend.paramrejectreason.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.backend.paramrejectreason.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        [
            'reject_reason'=>'string|required|max:2000',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamRejectReason::create($data);

        if($status){
            alert()->success('success','Successfully added param reject reason');
        } 
        else{
            alert()->error('error','Error occurred while adding param reject reason');
        }
        return redirect()->route('param-reject-reason.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamRejectReason::find($id);
        
        if(!$data){

            alert()->error('error','Param reject reason not found');
        }
       
        return view('pages.backend.paramrejectreason.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamRejectReason::find($id);

        $this->validate($request,[
            'reject_reason'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param reject reason successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-reject-reason.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function status(Request $request)
    {
        
        $user = ParamRejectReason::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param reject reason successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }

}
