<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInsurancePolicy;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamRegionlkm; 
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamRegionlkmController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-region-lkm-list|param-region-lkm-create|param-region-lkm-edit|param-region-lkm-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-region-lkm-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-region-lkm-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-region-lkm-delete', ['only' => ['destroy', 'status']]);
    }

    public function index()
    {
        $data = ParamRegionlkm::get();
        
        return view('pages.backend.paramregionlkm.index', compact('data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

        return view('pages.backend.paramregionlkm.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //Termasuk 24-Jam Khidmat Tunda sebanyak RM 200 setiap tahun

    public function store(Request $request)
    {
        
        $this->validate($request,
        [
            'region'=>'required|max:80',
            'status'=>'required',
        ]);

        $data=$request->all();

        $status = ParamRegionlkm::create($data);

        if($status){
            alert()->success('success','Successfully added param region promo');
        } 
        else{
            alert()->error('error','Error occurred while adding param region promo');
        }
        return redirect()->route('param-region-lkm.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamRegionlkm::find($id);
        
        if(!$data){

            alert()->error('error','Param insurance promo found');
        }
       
        return view('pages.backend.paramregionlkm.edit')
        ->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamRegionlkm::find($id);

        $this->validate($request,[
            
            'region'=>'required|max:80',
            'status' => 'required|in:1,0',

        ]);

       

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param region successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-region-lkm.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamRegionlkm::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param region successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
