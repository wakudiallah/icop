<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use Spatie\Permission\Models\Permission;

class StateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-state-list|param-state-create|param-state-edit|param-state-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-state-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-state-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-state-delete', ['only' => ['destroy', 'status']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    //  ========================================================
    // $states = State::get();
    // return view('backend.state.index', compact('states'));
    //  --------------------------------------------------------
       $data = State::orderBy('id','ASC')->get();  
       return view('pages.backend.paramstate.index')->with('data',$data);
    //  ========================================================
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.backend.paramstate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'state_code' => 'required|min:1|max:10|unique:states',
            'state' => 'required|min:1|max:30',
            'status' => 'required|min:1',
        ]);
        $input = $request->all();
        $state = State::create($input);

        if($state){
            alert()->success('success','Successfully added param state');
        }
        else{
            alert()->success('error','Error, Please try again');        
        }
        return redirect()->route('param-state.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.backend.paramstate.show',compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=State::findOrFail($id);
        if(!$data){
            
            alert()->error('error','Param State not found');
        }
        return view('pages.backend.paramstate.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $state=State::find($id);
        $this->validate($request,[
            'state_code' => 'required|unique:states,state_code,'.$id,
            'state'=>'string|required', 
            'status'=>'required|in:1,0'
        ]);
        
        $data=$request->all();
        $status=$state->fill($data)->save();

        if($status){
            alert()->success('success','Param state successfully updated');
        }
        else{
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-state.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state=State::find($id);
        if($state){
            $status=$state->delete();
            if($status){
                request()->session()->flash('success','State successfully deleted');
            }
            else{
                request()->session()->flash('error','Error, Please try again');
            }
            return redirect()->route('state.index');
        }
        else{
            request()->session()->flash('error','State not found');
            return redirect()->back();
        }
    }


    public function status(Request $request)
    {
        $user = State::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param state successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
