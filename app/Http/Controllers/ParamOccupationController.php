<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamOccupation;
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamOccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-occupation-list|param-occupation-create|param-occupation-edit|param-occupation-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-occupation-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-occupation-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-occupation-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        $data = ParamOccupation::get();
        
        return view('pages.backend.paramoccupation.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * occupation
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.backend.paramoccupation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'occupation'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamOccupation::create($data);

        if($status){
            alert()->success('success','Successfully added param occupation');
        }
        else{
            alert()->error('error','Error occurred while adding param occupation');
        }
        return redirect()->route('param-occupation.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParamOccupation::find($id);
        
        if(!$data){

            alert()->error('error','Param Occupation not found');
        }
       
        return view('pages.backend.paramoccupation.edit')->with('data',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ParamOccupation::find($id);

        $this->validate($request,[
            'occupation'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param occupation successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-occupation.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamOccupation::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param occupation successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }
}
