<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Auth;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
        //$this->user_id = Auth::user()->id;
        $this->middleware('permission:user-list|user-create|user-edit|  user-delete', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy', 'status']]);
    }


    public function index()
    {
        $data = User::get();
        
        return view('pages.backend.users.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles= Role::get();

        return view('pages.backend.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required|unique:users',
            'password'=>'string|required',
            'role'=>'required',
            'status'=>'required',
        ]);
        
        $data=$request->all();
        $data['password']=Hash::make($request->password);

        if($request->file('photo')){
            $fileName = time().'.'.$request->file('photo')->extension(); 
            $data['photo']=$fileName;
            $request->file('photo')->move(public_path('user'), $fileName);
        }
        

        $status=User::create($data);

        $status->assignRole($request->input('role'));


        if($status){
            
            alert()->success('success','Successfully added user');

        }
        else{
            
            alert()->error('error','Error occurred while adding user');

        }
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = User::find($id);
        $roles= Role::get();
        
        if(!$data){

            alert()->error('error','Users found');
        }
       
        return view('pages.backend.users.edit')
            ->with('data',$data)
            ->with('roles',$roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user=User::findOrFail($id);
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required|email|max:255|unique:users,email,'.$id,
            'role'=>'required',
            'password' => 'nullable|string|min:8',
            'status'=>'required|in:1,0'
        ]);
      
        $data=$request->all();
        

        // Perform validation
        //$validatedData = $request->validate($rules);

        // Update the data
        /*$data = User::findOrFail($id);
        $data->name = $validatedData['name'];
        $data->email = $validatedData['email'];*/

        // Update password if provided
        if ($request->has('password')) {
            $data['password'] = Hash::make($request->password);
        }
        
        $status=$user->fill($data)->save();

        if($status){
            request()->session()->flash('success','Successfully updated');
        }
        else{
            request()->session()->flash('error','Error occured while updating');
        }
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=User::findorFail($id);
        $status=$delete->delete();
        
        if($status){
            //request()->session()->flash('success','User Successfully deleted');
            alert()->success('success','User Successfully deleted');
        }
        else{

            alert()->error('error','There is an error while deleting users');
            
        }

        return redirect()->route('users.index');
    }


    public function status(Request $request)
    {
        
        $user = User::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','User successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
