<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParamInsurancePolicy;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamOwnershiplkm; 
use Spatie\Permission\Models\Permission;
use App\Models\Audit;

class ParamOwnershipController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-ownership-lkm-list|param-ownership-lkm-create|param-ownership-lkm-edit|param-ownership-lkm-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-ownership-lkm-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-ownership-lkm-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-ownership-lkm-delete', ['only' => ['destroy', 'status']]);
    }



    public function index()
    {
        $data = ParamOwnershiplkm::get();
        
        return view('pages.backend.paramownershiplkm.index', compact('data'));
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('pages.backend.paramownershiplkm.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //Termasuk 24-Jam Khidmat Tunda sebanyak RM 200 setiap tahun

    public function store(Request $request)
    {
        
        $this->validate($request,
        [
            'ownership'=>'required|max:80',
            'status'=>'required',
        ]);

        $data=$request->all();

        $status = ParamOwnershiplkm::create($data);

        if($status){
            alert()->success('success','Successfully added param ownership');
        } 
        else{
            alert()->error('error','Error occurred while adding param ownership');
        }
        return redirect()->route('param-ownership-lkm.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = ParamOwnershiplkm::find($id);
        
        if(!$data){

            alert()->error('error','Param ownership not found');
        }
       
        return view('pages.backend.paramownershiplkm.edit')
        ->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = ParamOwnershiplkm::find($id);

        $this->validate($request,[
            
            'ownership'=>'required|max:80',
            'status' => 'required|in:1,0',

        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','Param ownership successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-ownership-lkm.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamOwnershiplkm::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param ownership successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
