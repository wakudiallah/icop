<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Postcode;
use App\Models\State;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\StateController;
use Auth;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PostcodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-postcode-list|param-postcode-create|param-postcode-edit|param-postcode-delete', ['only' => ['index','store']]);
        $this->middleware('permission:param-postcode-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-postcode-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-postcode-delete', ['only' => ['destroy', 'status']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data  = Postcode::select('*');
            

            return Datatables::of($data)
                    ->addIndexColumn()

                    ->editColumn('state_code', function($data){

                        $value =""; 
                        //if(!empty($data->statedesc->state)){
                            $value = $data->statedesc->state;
                        /*}else{
                            $value = "Kosong";
                        }*/
                        
                        return $value; 
                    })


                    ->rawColumns(['state_code'])


                    ->make(true);
        }

        return view('pages.backend.parampostcode.index');
    
    }



    public function indexxxx()
    {

         $postcode  = Postcode::paginate(10);


        return view('pages.backend.postcode.index', compact('postcode'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = State::where('status', 1)->get();
        
        return view('pages.backend.parampostcode.create', compact('state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
            'postcode' => 'required|max:5|unique:postcodes',
            'area' => 'required|min:1|max:70',
            'post_office' => 'required|min:1|max:30',
            'state_code' => 'required',
            'status' => 'required|min:1'
        ]);

    

        $input = $request->all();
        $postcode = Postcode::create($input);

        if($postcode){
            alert()->success('success','Successfully added param postcode');
        }
        else{
            alert()->error('error','Error occurred while adding param postcode');
        }

        return redirect()->route('param-postcode.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postcode=Postcode::findOrFail($id);
        $state = State::where('status', 1)->get();

        return view('pages.backend.parampostcode.edit')->with('postcode',$postcode)
                    ->with('state',$state);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postcode=Postcode::find($id);
            $this->validate($request, [
            'postcode' => 'required|max:5',
            'area' => 'required|min:1|max:70',
            'post_office' => 'required|min:1|max:30',
            'state_code' => 'required',
            'status' => 'required||in:1,0'
        ]);

        $data=$request->all();
        $status=$postcode->fill($data)->save();
        if($postcode){
            alert()->success('success','Param postcode successfully updated');
        }
        else{
            alert()->error('error','Error, Please try again');
        }

        return redirect()->route('param-postcode.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postcode=Postcode::find($id);
        if($postcode){
            $status=$postcode->delete();
            if($status){
                request()->session()->flash('success','Postcode successfully deleted');
            }
            else{
                request()->session()->flash('error','Error, Please try again');
            }
            return redirect()->route('postcode.index');
        }
        else{
            request()->session()->flash('error','Postcode not found');
            return redirect()->back();
        }
    }


    public function status(Request $request)
    {
        $user = Postcode::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','Param postcode successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }


    }

    public function postcode($id)
    {
        $data = Postcode::where('postcode',$id)->with('statedesc')->limit(1)->get();
        return $data;
    }

}
