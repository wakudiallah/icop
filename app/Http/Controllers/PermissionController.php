<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
Use Alert;


use Illuminate\Http\Request;



class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:audit-trail-list|audit-trail-create|audit-trail-edit|audit-trail-delete', ['only' => ['index','show']]);
        //$this->middleware('permission:audit-trail-create', ['only' => ['create','store']]);
        //$this->middleware('permission:audit-trail-edit', ['only' => ['edit','update']]);
        //$this->middleware('permission:audit-trail-delete', ['only' => ['destroy', 'status']]);
    }

    public function index()
    {
        $data = Permission::get();
        
        return view('pages.backend.permissions.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.backend.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name'
        ]);

        //Permission::create($request->only('name'));
        $permissions = [
           'list',
           'create',
           'edit',
           'delete'
           
        ];

        $inputname = $request->name;
     
        foreach ($permissions as $permission) {
             Permission::create(['name' =>$inputname. '-' .$permission, 'guard_name' =>  'web']);
        }

        return redirect()->route('permissions.index')
            ->withSuccess(__('Permission created successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('pages.backend.permissions.edit', [
            'permission' => $permission
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name,'.$permission->id
        ]);

        $permission->update($request->only('name'));

        return redirect()->route('permissions.index')
            ->withSuccess(__('Permission updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();

        

        return redirect()->route('permissions.index');


    }
}
