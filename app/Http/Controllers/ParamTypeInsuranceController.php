<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use App\Models\Audit;
use App\Models\ParamTypeInsurance;

use Illuminate\Http\Request;


class ParamTypeInsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:param-type-insurance-list|param-type-insurance-create|param-type-insurance-edit|param-type-insurance-delete', ['only' => ['index','show']]);
        $this->middleware('permission:param-type-insurance-create', ['only' => ['create','store']]);
        $this->middleware('permission:param-type-insurance-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:param-type-insurance-delete', ['only' => ['destroy', 'status']]);
    }
    
    
    public function index()
    {
        $data = ParamTypeInsurance::get();
        
        return view('pages.backend.paramtypeinsurance.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('pages.backend.paramtypeinsurance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'type_insurance'=>'string|required|max:70',
            'status'=>'required',
        ]);
        $data=$request->all();

        $status = ParamTypeInsurance::create($data);

        if($status){
            alert()->success('success','Successfully added param type insurance');
        }
        else{
            alert()->error('error','Error occurred while adding param type insurance');
        }
        return redirect()->route('param-type-insurance.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParamTypeInsurance::find($id);
        
        if(!$data){

            alert()->error('error','ParamTypeInsurance not found');
        }
       
        return view('pages.backend.paramtypeinsurance.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ParamTypeInsurance::find($id);

        $this->validate($request,[
            'type_insurance'=>'string|required',
            'status' => 'required|in:1,0',
        ]);

        $data=$request->all();
       
        $status=$brand->fill($data)->save();

        if($status){
            
            alert()->success('success','ParamTypeInsurance successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');
        }
        return redirect()->route('param-type-insurance.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        
        $user = ParamTypeInsurance::where('id',$request->id)->update(array(
            //'code' => $request->code, 
            'status' => $request->status
        ));

        if($user){
            
            alert()->success('success','ParamTypeInsurance successfully updated');
        }
        else{
            
            alert()->error('error','Error, Please try again');

        }
    }


}
