<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use App\Models\PartnerApiAccount;


class ApiController extends Controller
{
    

    private $token;


    public function __construct()
    {
        $this->token = config('apiIcop.token');

        
    }


    public function index()
    {
        return view('api');
    }


    public function store(Request $request)
    {

        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3/tokenizer/gettoken', [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'userName' => 'T62966E-000',
                    'password' => 'kl)85g&*UT'
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            $token = $decodedBody['data']['token'];

            dd($token);


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    
    }


    public function getvehicleinfo(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/vix/getvehicleinfo', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    "agentCode" => "T62966E-000",
                    "vehNo" => "WSV173",
                    "id" => "660807-10-6863"
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }



    public function getvehiclemake(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/vix/getvehiclemake', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    
                        "agentCode" => "T62966E-000",
                        "productCode" => "PZ01",
                        "filterKey" => "Volvo"
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }



    public function getvehiclemodel(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/vix/getvehiclemodel', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    
                        "agentCode"=> "T62966E-000",
                        "productCode"=> "PC01",
                        "makeYear"=> "2013",
                        "make"=> "79"
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function calculatepremiumCreate(Request $request)
    {
       
        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/calculatepremium', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => '20088111',
                            'requestDateTime' => '2024-07-30T10:45:23.123'
                        ],
                        'basicDetails' => [
                            //'quotationNo' => '',  //kalau kosong akan create Quotationcode
                            'agentCode' => 'T62966E-000',
                            'transType' => 'B',
                            'vehicleNo' => 'SU242B',
                            'productCode' => 'PZ01',
                            'coverType' => 'V-CO',
                            'ciCode' => 'MX4',  //MX4 Company
                            'effDate' => '07/08/2024',
                            'expDate' => '06/08/2025',
                            'reconInd' => 'N',
                            'yearOfMake' => 2012,
                            'make' => '11',
                            'model' => '11*1028',
                            'capacity' => '1497',
                            'uom' => 'CC',
                            'engineNo' => 'EWEK094',
                            'chassisNo' => 'PM2L252S002107437',
                            'logBookNo' => '',
                            'regLoc' => 'L',
                            'regionCode' => 'W',
                            'noOfPassenger' => 5,
                            'noOfDrivers' => 1,
                            'insIndicator' => 'C',
                            'name' => 'WAKUDIALLAH',
                            'insNationality' => 'L',
                            'othersID' => '660807-10-6863', //Bussiness 
                            'newIC' => '',
                            'dateOfBirth' => '',
                            'age' => 0,
                            'gender' => '',
                            'maritalSts' => '',
                            'occupation' => '',
                            'mobileNo' => '012-3456789',
                            'address' => '20, Dato Keramat',
                            'postCode' => '50150',
                            'state' => '6',
                            'country' => 'MAS',
                            'sumInsured' => 30000,
                            'avInd' => 'N',
                            'pacInd' => 'N',
                            'all_Driver_Ind' => 'N',
                            'abisi' => 28000,
                            'chosen_SI_Type' => 'REC_SI',
                            'pacUnit' => 0,  //ada 2juga
                            
                        ],

                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                [
                                    'extCovCode' => '101',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '15/8/2024',
                                    'expDate' => '14/8/2025',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '112',
                                    'unitDay' => 50,
                                    'unitAmount' => 7,
                                    'effDate' => '15/8/2024',
                                    'expDate' => '14/8/2025',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '25',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ],


                        'pacExtraCoverDetails' => [
                            'pacExtraCoverData' => [
                                [
                                    'pacCode' => 'R008',
                                    'pacUnit' => 1
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    public function calculatepremiumGet()
    {   

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;
        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/calculatepremium', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => '200893',
                            'requestDateTime' => '2024-07-28T10:45:23.123'
                        ],
                        'basicDetails' => [
                            'quotationNo' => '',  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => 'T62966E-000',
                            'transType' => 'B',
                            'vehicleNo' => 'PAU10',
                            'productCode' => 'PZ01',
                            'coverType' => 'V-CO',
                            'ciCode' => 'MX1',
                            'effDate' => '11/09/2024',
                            'expDate' => '12/09/2025',
                            'reconInd' => 'N',
                            'yearOfMake' => 2012,
                            'make' => '11',
                            'model' => '11*1028',
                            'capacity' => '1497',
                            'uom' => 'CC',
                            'engineNo' => 'EWEK094',
                            'chassisNo' => 'PM2L252S002107437',
                            'logBookNo' => 'ERTGRET253',
                            'regLoc' => 'L',
                            'regionCode' => 'W',
                            'noOfPassenger' => 5,
                            'noOfDrivers' => 1,
                            'insIndicator' => 'P',
                            'name' => 'CHE ONN',
                            'insNationality' => 'L',
                            'newIC' => '660807-10-6863', 
                            'dateOfBirth' => '22/12/1998',
                            'age' => 27,
                            'gender' => 'M',
                            'maritalSts' => 'M',
                            'occupation' => '99',
                            'mobileNo' => '012-3456789',
                            'address' => '20, Jalan PJU, Taman A, Petaling Jaya',
                            'postCode' => '54000',
                            'state' => '6',
                            'country' => 'MAS',
                            'sumInsured' => 30000,
                            'abisi' => 28000,
                            'chosen_SI_Type' => 'REC_SI',
                            'avInd' => 'N',
                            'pacInd' => 'N',
                            'pacUnit' => 0,
                            'all_Driver_Ind' => 'N'
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                [
                                    'extCovCode' => '89A', //cermin dak ngaruh  
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 1000
                                ],
                                [
                                    'extCovCode' => '07', //Additional Driver  tapi gak effect
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 1000,
                                    "noofunit"=> 2
                                ],
                                [
                                    'extCovCode' => '200', //PA Basic  ngaruh
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 1000,
                                    "noofunit"=> 4     //1-5   ini berpengaruh
                                ],
                                [
                                    'extCovCode' => '57',  //Special Disaster     ngaruh
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                                
                            ]
                        ],
                        "additionalNamedDriverDetails" => [
                            "additionalNamedDriverData" => [
                                [
                                    "ndName" => "Wakudiallah",
                                    "ndIdentityNo" => "660807106863",
                                    "ndDateOfBirth" => "14/12/1998",
                                    "ndGender" => "M",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "MY"
                                ],
                                [
                                    "ndName" => "Farah Attarine",
                                    "ndIdentityNo" => "930807106860",
                                    "ndDateOfBirth" => "22/12/1990",
                                    "ndGender" => "F",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "MY"
                                ]
                            ]
                        ],


                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);

            /*$params = [
                $basicPrem => $decodedBody['data']['premiumDetails'][0]['basicPrem'],
                $basic_Premium => $decodedBody['data']['premiumDetails'][0]['basic_Premium']
            ];*/
            
            //dd($decodedBody['data']['premiumDetails'][0]['ttlPayablePremium']);
            dd($decodedBody['data']['premiumDetails']);


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function calculatepremium(Request $request)
    {   

        
        
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/calculatepremium', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => '200892',
                            'requestDateTime' => '2024-07-28T10:45:23.123'
                        ],
                        'basicDetails' => [
                            'quotationNo' => '',  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => 'T62966E-000',
                            'transType' => 'B',
                            'vehicleNo' => 'SU242B',
                            'productCode' => 'PZ01',
                            'coverType' => 'V-CO',
                            'ciCode' => 'MX1',
                            'effDate' => '11/09/2024',
                            'expDate' => '12/09/2025',
                            'reconInd' => 'N',
                            'yearOfMake' => 2012,
                            'make' => '11',
                            'model' => '11*1028',
                            'capacity' => '1497',
                            'uom' => 'CC',
                            'engineNo' => 'EWEK094',
                            'chassisNo' => 'PM2L252S002107437',
                            'logBookNo' => 'ERTGRET253',
                            'regLoc' => 'L',
                            'regionCode' => 'W',
                            'noOfPassenger' => 5,
                            'noOfDrivers' => 1,
                            'insIndicator' => 'P',
                            'name' => 'CHE ONN',
                            'insNationality' => 'L',
                            'newIC' => '660807-10-6863', 
                            'dateOfBirth' => '22/12/1998',
                            'age' => 27,
                            'gender' => 'M',
                            'maritalSts' => 'M',
                            'occupation' => '99',
                            'mobileNo' => '012-3456789',
                            'address' => '20, Jalan PJU, Taman A, Petaling Jaya',
                            'postCode' => '54000',
                            'state' => '6',
                            'country' => 'MAS',
                            'sumInsured' => 30000,
                            'abisi' => 28000,
                            'chosen_SI_Type' => 'REC_SI',
                            'avInd' => 'N',
                            'pacInd' => 'N',
                            'pacUnit' => 0,
                            'all_Driver_Ind' => 'N'
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                [
                                    'extCovCode' => '89', //cermin
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 1000
                                ],
                                [
                                    'extCovCode' => '07', //Additional Driver
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 1000,
                                    "noofunit"=> 2
                                ],
                                [
                                    'extCovCode' => '57',  //Special Disaster
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '200', //PA Basic
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0,
                                    'noOfUnit' => 3
                                ]
                                
                            ]
                        ],
                        "additionalNamedDriverDetails" => [
                            "additionalNamedDriverData" => [
                                [
                                    "ndName" => "Wakudiallah",
                                    "ndIdentityNo" => "660807106863",
                                    "ndDateOfBirth" => "14/12/1998",
                                    "ndGender" => "M",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "MY"
                                ],
                                [
                                    "ndName" => "Farah Attarine",
                                    "ndIdentityNo" => "930807106860",
                                    "ndDateOfBirth" => "22/12/1990",
                                    "ndGender" => "F",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "MY"
                                ]
                            ]
                        ],


                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            /*$params = [
                $basicPrem => $decodedBody['data']['premiumDetails'][0]['basicPrem'],
                $basic_Premium => $decodedBody['data']['premiumDetails'][0]['basic_Premium']
            ];*/
            
            dd($decodedBody['data']['premiumDetails'][0]['basic_Premium']);


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }




    public function calculatepremiumREF(Request $request)
    {
        $client = new Client();
        
        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/calculatepremium', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    "requestData" => [
                        "participantDetails" => [
                            "transactionReferenceNo" => "200888",
                            "requestDateTime" => "2024-07-01T10:36:39.129Z"
                        ],
                        "basicDetails" => [
                            "quotationNo" => "",
                            "agentCode" => "T62966E-000",
                            "transType" => "B",
                            "vehicleNo" => "WA823H",
                            "productCode" => "PZ01",
                            "coverType" => "V-CO",
                            "ciCode" => "MX1",
                            "effDate" => "01/07/2024",
                            "expDate" => "01/07/2025",
                            "reconInd" => "N",
                            "yearOfMake" => 2011,
                            "make" => "33",
                            "model" => "33*3315",
                            "capacity" => "1298",
                            "uom" => "CC",
                            "engineNo" => "EWEK094",
                            "chassisNo" => "PM2M602S002025382",
                            "logBookNo" => "NA",
                            "regLoc" => "L",
                            "regionCode" => "W",
                            "noOfPassenger" => 5,
                            "noOfDrivers" => 1,
                            "insIndicator" => "P",
                            "name" => "Shopeeuser",
                            "insNationality" => "L",
                            "newIC" => "671207075585",
                            "dateOfBirth" => "07/12/1967",
                            "age" => 27,
                            "gender" => "M",
                            "maritalSts" => "M",
                            "occupation" => "99",
                            "mobileNo" => "012-3456789",
                            "address" => "20, Jalan PJU, Taman A, Petaling Jaya",
                            "postCode" => "81120",
                            "state" => "JOHOR DARUL TAKZIM",
                            "country" => "MAS",
                            "sumInsured" => 1000000,
                            "abisi" => 16300,
                            "chosen_SI_Type" => "REC_SI",
                            "avInd" => "Y",
                            "pacInd" => "N",
                            "pacUnit" => 0,
                            "all_Driver_Ind" => "N"
                        ],
                        "extraCoverDetails" => [
                            "extraCoverData" => [
                                [
                                    "extCovCode" => "101",
                                    "unitDay" => 0,
                                    "unitAmount" => 0,
                                    "effDate" => "15/04/2017",
                                    "expDate" => "14/04/2018",
                                    "sumInsured" => 0
                                ],
                                [
                                    "extCovCode" => "112",
                                    "unitDay" => 50,
                                    "unitAmount" => 7,
                                    "effDate" => "",
                                    "expDate" => "",
                                    "sumInsured" => 0
                                ],
                                [
                                    "extCovCode" => "25",
                                    "unitDay" => 0,
                                    "unitAmount" => 0,
                                    "effDate" => "",
                                    "expDate" => "",
                                    "sumInsured" => 0
                                ]
                            ]
                        ],
                        "additionalNamedDriverDetails" => [
                            "additionalNamedDriverData" => [
                                [
                                    "ndName" => "Driver1",
                                    "ndIdentityNo" => "Asdd123",
                                    "ndDateOfBirth" => "14/12/1998",
                                    "ndGender" => "M",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "USA"
                                ],
                                [
                                    "ndName" => "Driver2",
                                    "ndIdentityNo" => "As43123",
                                    "ndDateOfBirth" => "22/12/1990",
                                    "ndGender" => "F",
                                    "ndMaritalSts" => "M",
                                    "ndOccupation" => "99",
                                    "ndRelationship" => "SIS",
                                    "ndNationality" => "IND"
                                ]
                            ]
                        ],
                        "pacRiderDetails" => [
                            "pacRiderData" => [
                                "pacRiderNo" => 1,
                                "pacRiderName" => "PacRDNmae",
                                "pacRiderNewIC" => "PacRDNewIC",
                                "pacRiderOldIC" => "PacRDOldIC",
                                "pacRiderDOB" => "27/03/1987",
                                "defaultInd" => 1
                            ]
                        ],
                        "pacExtraCoverDetails" => [
                            "pacExtraCoverData" => [
                                [
                                    "pacCode" => "R008",
                                    "pacUnit" => 1
                                ]
                            ]
                        ]
                    ]
                ]

            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    
    }



    public function issuecovernote(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/issuecovernote', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    "requestData" => [
                        [
                            "participantDetails" => [
                                "transactionReferenceNo" => "T3910000587",
                                "requestDateTime" => "2024-Jul-03 04:28:48 PM"
                            ],
                            "issueCoverNoteDetails" => [
                                "agentCode" => "T62966E-000",
                                "quotationNumber" => "MQ240000481793",
                                "emailTo" => "wakudiallah05@gmail.com"
                            ]
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function issuepolicy(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/issuepolicy', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    "RequestData" => [
                        "ParticipantDetails" => [
                            "TransactionReferenceNo" => "20000008",
                            "RequestDateTime" => "2024-07-06T10:36:39.129Z",
                            "EmailTo" => "wakudiallah05@gmail.com"
                        ],
                        "BasicDetails" => [
                            "QuotationNo" => "MQ240000481793",
                            "FranchiseAgentCode" => "",
                            "AgentCode" => "T62966E-000",
                            "TransType" => "B",
                            "VehicleNo" => "LOPLK222",
                            "ProductCode" => "PZ01",
                            "CoverType" => "V-CO",
                            "CICode" => "MX1",
                            "EffDate" => "07/07/2024",
                            "ExpDate" => "08/08/2025",
                            "ReconInd" => "N",
                            "YearOfMake" => 2014,
                            "Make" => "22",
                            "Model" => "22*3418",
                            "Capacity" => "1999",
                            "UOM" => "CC",
                            "EngineNo" => "EWEKLL94d",
                            "ChassisNo" => "PM2OS00df2437l",
                            "LogBookNo" => "NA",
                            "RegLoc" => "L",
                            "RegionCode" => "W",
                            "NoOfPassenger" => 5,
                            "NoOfDrivers" => 1,
                            "InsIndicator" => "P",
                            "Name" => "Wakudiallah",
                            "InsNationality" => "L",
                            "NewIC" => "981212-06-5226",
                            "DateOfBirth" => "22/12/1998",
                            "Age" => 25,
                            "Gender" => "M",
                            "MaritalSts" => "M",
                            "Occupation" => "99",
                            "MobileNo" => "012-3456789",
                            "Address" => "20, Jalan PJU, Taman A, Petaling Jaya",
                            "PostCode" => "54000",
                            "State" => "6",
                            "Country" => "MAS",
                            "SumInsured" => 29200,
                            "ABISI" => 29200,
                            "Chosen_SI_Type" => "REC_SI",
                            "AVInd" => "N",
                            "PACInd" => "N",
                            "PACUnit" => 0,
                            "All_Driver_Ind" => "N"
                        ],
                        "ExtraCoverDetails" => [
                            "ExtraCoverData" => [
                                [
                                    "ExtCovCode" => "101",
                                    "UnitDay" => 0,
                                    "UnitAmount" => 0,
                                    "EffDate" => "15/4/2017",
                                    "ExpDate" => "14/4/2018",
                                    "SumInsured" => 0
                                ],
                                [
                                    "ExtCovCode" => "112",
                                    "UnitDay" => 50,
                                    "UnitAmount" => 7,
                                    "EffDate" => "",
                                    "ExpDate" => "",
                                    "SumInsured" => 0
                                ],
                                [
                                    "ExtCovCode" => "25",
                                    "UnitDay" => 0,
                                    "UnitAmount" => 0,
                                    "EffDate" => "",
                                    "ExpDate" => "",
                                    "SumInsured" => 0
                                ]
                            ]
                        ],
                        "AdditionalNamedDriverDetails" => [
                            "AdditionalNamedDriverData" => [
                                [
                                    "NdName" => "Driver1",
                                    "NdIdentityNo" => "Asdd123",
                                    "NdDateOfBirth" => "14/12/1998",
                                    "NdGender" => "M",
                                    "NdMaritalSts" => "M",
                                    "NdOccupation" => "99",
                                    "NdRelationship" => "SIS",
                                    "NdNationality" => "USA"
                                ],
                                [
                                    "NdName" => "Driver2",
                                    "NdIdentityNo" => "As43123",
                                    "NdDateOfBirth" => "22/12/1990",
                                    "NdGender" => "F",
                                    "NdMaritalSts" => "M",
                                    "NdOccupation" => "99",
                                    "NdRelationship" => "SIS",
                                    "NdNationality" => "IND"
                                ]
                            ]
                        ],
                        "PacRiderDetails" => [
                            "PacRiderData" => [
                                "PacRiderNo" => 1,
                                "PacRiderName" => "PacRDNmae",
                                "PacRiderNewIC" => "PacRDNewIC",
                                "PacRiderOldIC" => "PacRDOldIC",
                                "PacRiderDOB" => "27/03/1987",
                                "DefaultInd" => 1
                            ]
                        ],
                        "PacExtraCoverDetails" => [
                            "PACExtraCoverData" => [
                                [
                                    "PacCode" => "R008",
                                    "PacUnit" => 1
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }


    public function getjpjstatus(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/getjpjstatus', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    "RequestData" => [
                        "ParticipantDetails" => [
                            "ParticipantCode" => "20",
                            "TransactionReferenceNo" => "20000008",
                            "RequestDateTime" => "2024-07-03T23:00:00.000"
                        ],
                        "JPJStatusDetails" => [
                            "AgentCode" => "T62966E-000",
                            "CoverNoteNumber" => "T62966E-24000003"
                        ]
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }



    public function downloadpolicyschedule(Request $request)
    {
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api.test.zurich.com.my/v1/takaful/insurance/services/v3.1/motorapi/motor/downloadpolicyschedule', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token,
                ],
                'json' => [
                    
                        "agentCode"=> "T62966E-000",
                        "productCode"=> "PC01",
                        "makeYear"=> "2010",
                        "make"=> "05"
                        
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);
            


        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }





}
