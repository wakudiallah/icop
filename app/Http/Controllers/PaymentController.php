<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\User;
use App\Models\Praaplication;
use App\Models\ParamProtectionType;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePolicy;
use App\Models\PerlindunganTambahan;
use App\Models\ApplicationDocument;
use App\Models\InsuranceBasicDetail;
use App\Models\InsuranceBasicPremium;
use App\Models\InsuranceGetVehicleInfo;
use App\Models\ApplicationCustomerInfo;
use App\Models\ZurichApiPostcode;
use Illuminate\Support\Str;
use url;
use GuzzleHttp\Client;
use Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Redirect;
use Hash;
use Carbon\Carbon;

use Illuminate\Http\Request;



class PaymentController extends Controller
{
    
    public function index(Request $request)
    {
        
        $uuid = $request->uuid;
        $pra = Praaplication::where('uuid', $uuid)->first();


        $data = ApplicationDocument::where('uuid', $uuid)->first();
        $checkEmail = User::where('email', $request->email)->exists();

        
        
            $orderNumber = $pra->reg_no;
            $amountNotDot = 738.82;


            $amount = number_format($amountNotDot,2);
            
            $amountNotComma = str_replace(",", "", $amount);  
            $amountNotDot = str_replace(".", "", $amountNotComma);

            
            $callback_url = url('/failed/'.$uuid);  //failed
            $redirect_url = url('/callback/'.$uuid); //success
            
            $client = new Client();
            $res = $client->request('POST', 'https://billplz-sandbox.com/api/v3/bills', [
            
                'auth' => ['087a8a65-c2dc-46bb-b82f-eee48803f9fb', '', 'basic'],
                'form_params' => [
                    'collection_id' => 'gwlamrq53',
                    'email' => 'wakudiallah05@gmail.com',
                    'name' =>  'wakud',
                    'amount'     => $amountNotDot,
                   'callback_url' => $callback_url,
                    'description' => 'Order Number :'.$orderNumber,
                    'redirect_url' => $redirect_url,
                ]
            ]);


            $array = json_decode($res->getBody()->getContents(), true);
            $var_url =  var_export($array['url'],true);



            if( Auth::guest() && $checkEmail == false){
                $user = User::create([
                    'name' => $request->fullname,
                    'email' => $request->email,
                    'password' =>  Hash::make($request->password),
                    'role' => 6,
                    'status' => 1,
                ]);
    
    
                if ($user) {
                    Auth::login($user);
                    $user->save();
                }
    
                $url = substr($var_url, 1, -1);

                return Redirect::to($url);
    
    
            } elseif(Auth::guest() && $checkEmail == true){  //just login
    
    
                $credentials = $request->only('email', 'password');
     
                if (Auth::attempt($credentials)) {

                    $url = substr($var_url, 1, -1);

                    return Redirect::to($url);
                    
                   
    
                }else{
    
                    return Redirect::back()->withErrors(['msg' => 'The Message']);
                }
    
            }

        
    }







    
    public function store(Request $request)
    {
        //dd($request->all()); 
        
          
        
        $uuid = $request->uuid;

        $pra = Praaplication::where('uuid', $uuid)->first();

        $data = ApplicationDocument::where('uuid', $uuid)->first();
        $checkEmail = User::where('email', $request->email)->exists();

       

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
       
        
            $orderNumber = $pra->reg_no;
            $amountNotDot = $calcuAddProtect['payable'];


            $amount = number_format($amountNotDot,2);
            
            $amountNotComma = str_replace(",", "", $amount);  
            $amountNotDot = str_replace(".", "", $amountNotComma);

            
            $callback_url = url('/failed/'.$uuid);  //failed
            $redirect_url = url('/callback/'.$uuid); //success
            
            $client = new Client();
            $res = $client->request('POST', 'https://billplz-sandbox.com/api/v3/bills', [
            
                'auth' => ['087a8a65-c2dc-46bb-b82f-eee48803f9fb', '', 'basic'],
                'form_params' => [
                    'collection_id' => 'gwlamrq53',
                    'email' => 'wakudiallah05@gmail.com',
                    'name' =>  'wakud',
                    'amount'     => $amountNotDot,
                   'callback_url' => $callback_url,
                    'description' => 'Order Number :'.$orderNumber,
                    'redirect_url' => $redirect_url,
                ]
            ]);


            $array = json_decode($res->getBody()->getContents(), true);
            $var_url =  var_export($array['url'],true);


            if( Auth::guest() && $checkEmail == false){
                
                $user = User::create([
                    'name' => $request->fullname,
                    'email' => $request->email,
                    'password' =>  Hash::make($request->password),
                    'role' => 6,
                    'status' => 1,
                ]);

           
                if ($user) {
                    Auth::login($user);
                    $user->save();
                }
    
                $url = substr($var_url, 1, -1);

                return Redirect::to($url);
    
    
            } elseif(Auth::guest() && $checkEmail == true){  //just login
    
    
                $credentials = $request->only('email', 'password');
     
                if (Auth::attempt($credentials)) {

                    $url = substr($var_url, 1, -1);

                    return Redirect::to($url);
                    
                   
    
                }else{
    
                    return Redirect::back()->withErrors(['msg' => 'The Message']);
                }
    
            }else{

                $url = substr($var_url, 1, -1);

                return Redirect::to($url);

            }


        
    }





    public function callback($uuid)
    { 
        
        Praaplication::where('uuid', $uuid)->update([
            
            'pay_status' => 'OL',
            
        ]);


        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');


        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

        ];


        $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->GetFinalPremium($params);


        
        return redirect('dashboard-customer');

    }




    public function failed($id)
    {
      
        dd('failed');

       
        return redirect('/user/user-purchase')->with(['warning' => 'Your Order Failed Payment']);
    }




    public function callbackXX($id)
    { 
       

        return redirect('/dashboard-customer')->with(['success' => 'Thank You, We will immediately process your request']);



        $order = Order::join('order_customer_infos', 'orders.order_number', '=', 'order_customer_infos.order_number')
            ->where('orders.order_number', $id)
            ->select('order_customer_infos.email', 'orders.order_number', 'order_customer_infos.first_name', 'order_customer_infos.last_name', 'orders.id as orderId', 'orders.created_at')
            ->first();




        $setting = Settings::select('logo')
                            ->where('id', 1)
                            ->first();

        $email = ParEmail::where('id', 2)->first();


        $today = $order->created_at;

        $toEmail = $order->email;
        $firstName = $order->first_name;
        $fromEmail = $email->email;


         /*===== Sent Email =======*/
         $dataq = array(
                    'today'=>$today, 
                    'setting' => $setting, 
                    'orderNumber' => $id, 
                    'toCustomerEmail' => $order->email,
                    'toCustomerFirstName' => $order->first_name,
                    'toCustomerLastName' => $order->last_name,
                    'detailPurchase' => $detailPurchase

                    );

        Mail::send('email.orderNotif', $dataq, function($message) use ($toEmail, $firstName, $fromEmail){

             $message->to($toEmail)->subject( config('setting_system.store-name').' - Order Confirmed');
             $message->bcc(['wakudiallah05@gmail.com']);
             $message->from($fromEmail, config('setting_system.store-name') );

          });
       
        /*===== End Sent Email =======*/ 

    }





}
