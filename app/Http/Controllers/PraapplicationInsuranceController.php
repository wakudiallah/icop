<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Praaplication;
use App\Models\ParamProtectionType;
use App\Models\Postcode;
use App\Models\ParamInsuranceCompany;
use App\Models\ParamInsurancePolicy;
use App\Models\PerlindunganTambahan;
use App\Models\ApplicationDocument;
use App\Models\ApplicationCustomerInfo;
use App\Models\ApplicationSelectInsurance;
use App\Models\ApplicationAdditionalProtect;
use App\Models\ApplicationFinancing;
use App\Models\SetupDocument;
use App\Models\ParamRegionlkm;
use App\Models\SetupMonthlyInstallment;
use App\Models\PartnerApiAccount;
use App\Models\ZurichApiGetVehicleInfo;
use App\Models\ZurichApiGetVehicleInfoVariant;
use App\Models\ZurichApiPostcode;
use App\Models\SetupZurichCart;
use App\Models\ParamDutiSetem;
use App\Models\ParamSst;
use App\Models\ZurichMaritalStatus;
use App\Models\ZurichRelationship;
use App\Models\ZurichCoverType;
use App\Models\InsuranceGetVehicleInfo;
use App\Models\InsuranceBasicPremium;
use App\Models\InsuranceBasicDetail;
use App\Models\InsuranceExtraCover;
use App\Models\InsuranceAllowableExtraCover;
use Illuminate\Support\Str;
use Hash;
use App\Events\NewContentNotification;
use Pusher\Pusher;
use DateTime;
use Exception;
use DateTimeZone;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Redirect;


class PraapplicationInsuranceController extends Controller
{
    //
    public function index()
    {
        
        /*return view('pages.frontend.praaplication.index');*/
        
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        $userName = $data->userName;
        $token = $data->token;


        return view('pages.frontend.apps-icop.index',  compact('token'));
        
    }


    public function store(Request $request)
    {
        
        $this->validate($request,
        [
            'vehicle'=>'string|required',
            'ic'=>'required|nullable',
            'ic2'=>'string|nullable',
            'ehailing'=>'nullable',
            'email'=>'nullable',
            'whatsapp'=>'nullable',
            'postcode'=>'nullable',
        ]);

        $uuid = Str::uuid();
        
        $data = $request->all();
        $data['uuid'] = $uuid;
       

        try{
            //Generate a new token
            $response = app('App\Http\Controllers\InsuranceApiController')->WeatherForecast();
  

        }catch (Exception $e) {

            //dd($e);
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return back()->withInput()->with('error', '303 : Problem get vahicle info, Pls try again later');

        }


        $vehicle = $request->input('vehicle');
        $ic = $request->input('ic');
        $postcode = $request->input('postcode');

        alert()->success('success','Successfully submitted data');

        return redirect()->route('quote.show', [
            'quote' => $uuid, 
            'vehicle' => $vehicle, 
            'ic' => $ic, 
            'postcode' => $postcode
        ]);

    }


    public function show($uuid, Request $request)
    {
        $vehicle = $request->query('vehicle');
        $ic = $request->query('ic');
        $postcode = $request->query('postcode');

        $firstPart = substr($ic, 0, 6);
        $secondPart = substr($ic, 6, 2);
        $thirdPart = substr($ic, 8, 4);

        $icFormat = $firstPart . '-' . $secondPart . '-' . $thirdPart;

        $zMaritalSts = ZurichMaritalStatus::get();

        $region = ParamRegionlkm::where('status', 1)->get();


        try{
        
            // Call the method from SecondController
            $response = app('App\Http\Controllers\InsuranceApiController')->GetVehicleInfo($vehicle, $icFormat, $uuid);
            

            $vehRegNo = $response['vehRegNo'];
            $vehChassisNo = $response['vehChassisNo'];
            $vehEngineNo = $response['vehEngineNo'];
            $vehMakeYear = $response['vehMakeYear'];
            $brand = $response['brand'];
            $model = $response['model'];  //blm ada

        
            $variant = $response['variant'];
            $engineSize = $response['engineSize'];
            $cc = $response['cc'];
            $ncd = $response['ncd'];
            $eId = $response['eId'];


            
            $vehicle = InsuranceGetVehicleInfo::updateOrCreate(
                // Condition to check if the record exists
                ['uuid' => $uuid],
                // Data to update or create
                [
                    'eId' => $response['eId'],
                    'vehRegNo' => $response['vehRegNo'],
                    'chasisNo' => $response['vehChassisNo'],
                    'engineNo' => $response['vehEngineNo'],
                    'yearMake' => $response['vehMakeYear'],
                    'brand' => $response['brand'],
                    'model' => $response['model'],  // Add this field to your database table if not already present
                    'variant' => $response['variant'],
                    'vehCapacity' => $response['engineSize'],
                    'uom' => $response['cc'],
                    'ncd' => $response['ncd']
                ]
            );


            //$Vehiclevarint = ZurichApiGetVehicleInfoVariant::where('uuid', $uuid)->get();
            $Vehiclevarint = $response['listVehInfoVariant'];

            //$apiPostcode = app('App\Http\Controllers\ZurichApiController')->postcode($postcode, $uuid);

            return view('pages.frontend.apps-icop.quote-vehicle', compact('uuid', 'vehicle', 'ic', 'postcode', 'vehRegNo', 'vehChassisNo', 'vehEngineNo', 'vehMakeYear', 'brand', 'model', 'variant', 'engineSize', 'cc', 'ncd', 'zMaritalSts', 'Vehiclevarint', 'eId', 'region'));

        }catch (Exception $e) {

            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            //dd($e);

            return back()->withInput()->with('error', '303 : error get vahicle info, Pls try again later');

        }
    }


    public function choseInsurance(Request $request)
    {
        $uuid = $request->uuid;
        $vehicle = $request->vehicle;
        $eId = $request->eId;

        //dd($eId);

        $insCompany = ParamInsuranceCompany::with('promo')->where('status', 1)->get();


        $lastItem = Praaplication::where('reg_no', 'LIKE', 'CAR%')->orderBy('reg_no', 'desc')->first();
        if ($lastItem) {
            //$lastId = (int)$lastItem->reg_no;
            $lastNumericId = (int) substr($lastItem->reg_no, 3);
            $newNumericId = str_pad($lastNumericId + 1, 8, '0', STR_PAD_LEFT);
        } else {
            $newNumericId = '00000001';
        }
        $newId = 'CAR' . $newNumericId;
        
        $ic = $request->ic;
        $email = $request->email;
        $fullname = $request->fullname;
        $phone = $request->phone;
        $marital_status = $request->marital_status;
        $address1 = $request->address1;
        $address2 = $request->address2;
        $state = $request->state;
        $vehicle = $request->vehicle;
        $postcode = $request->postcode;
        $region = $request->region;

        // Extract year, month, and day
        $year = substr($ic, 0, 2);
        $month = substr($ic, 2, 2);
        $day = substr($ic, 4, 2);

         // Determine the full year based on current date context
         $currentYear = (int)date('Y');
         $currentYearShort = (int)substr($currentYear, 2, 2);
         $century = ($year <= $currentYearShort) ? '20' : '19';
         $fullYear = $century . $year;
 
         //$dateOfBirth = $day.'/'.$month.'/'.$fullYear;
         $dateOfBirth = $fullYear.'-'.$month.'-'.$day;
 
 
         //*Gender
         $jantina = substr($ic,10, 2); 
 
         if ($jantina % 2 == 0) {
             $gender =  "F";
         } else {
             $gender =  "M";
         }

          //*Age
        $dobFormat = $fullYear . '-' . $month . '-' . $day;

        // Convert date of birth to a DateTime object
        $dobDate = new DateTime($dobFormat);
        $currentDate = new DateTime();  // Current date

        // Calculate the age
        $age = $currentDate->diff($dobDate)->y;

        $praaplication = Praaplication::where('uuid', $uuid)
                ->select('vehicle', 'ic', 'email')
                ->orderBy('id', 'desc')
                ->first();

        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $cid = ApplicationDocument::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $selectInsurance = ApplicationSelectInsurance::where('uuid', $uuid)->orderBy('id', 'desc')->first();
        $addProtect = ApplicationAdditionalProtect::where('uuid',  $uuid)->orderBy('id', 'desc')->first();
        $finance = ApplicationFinancing::where('uuid', $uuid)->orderBy('id', 'desc')->first();

        $vehicleInfo = InsuranceGetVehicleInfo::where('uuid', $uuid)
        //->select('vehMake', 'vehEngineNo', 'vehChassisNo', 'vehMakeYear', 'vehMake', 'nxtPolEffDate', 'nxtPolExpDate')
        ->orderBy('id', 'desc')
        ->first();


        if($praaplication){

            $praaplication->update([
                'ic' => $ic,
                'email' => $email,
                'vehicle' => $vehicle
            ]);

        }else{

            Praaplication::create([
                'uuid' => $uuid,
                'reg_no' => $newId,
                'ic' => $ic,
                'email' => $email,
                'vehicle' => $vehicle,
                'app_status' => "NW"
    
            ]);

        }

        /*$addProtect = ApplicationAdditionalProtect::updateOrCreate(
            // Condition to check if the record exists
            ['uuid' => $uuid],
            // Data to update or create
            [
                'uuid' => $uuid,
                
            ]
        );*/


        if($cif){

            $cif->update([
                'fullname' => $fullname,
                'phone' => $phone,
                'marital_status' => $marital_status,
                'address1' => $address1,
                'address2' => $address2,
                'postcode' => $postcode,
                'state' => $state,
                'region' => $region
            ]);

        }else{
            ApplicationCustomerInfo::create([
                'uuid' => $uuid,
                'fullname' => $fullname,
                'phone' => $phone,
                'marital_status' => $marital_status,
                'address1' => $address1,
                'address2' => $address2,
                'postcode' => $postcode,
                'state' => $state,
                'dob' => $dobFormat,
                'age' => $age,
                'gender' => $gender,
                'region' => $region

            ]);
        }


        if($cid){
            $cid->update([
                'uuid' => $uuid,
            ]);
        }else{
            ApplicationDocument::create([
                'uuid' => $uuid,
            ]);
        }

      
        if($finance){
            $cid->update([
                'uuid' => $uuid,
            ]);
        }else{
            ApplicationFinancing::create([
                'uuid' => $uuid,
            ]);
        }

        
        if($selectInsurance){

            $selectInsurance->update([
                'uuid' => $uuid,
            ]);

        }else{

            ApplicationSelectInsurance::create([
                'uuid' => $uuid,
            ]);

        }

        //dd($eId);

        $params = [
            'uuid' => $uuid,
            'eId' => $eId, 
            'nric' => $ic,
            'fullName' => $fullname,
            'hpNo' => $phone,
            'email' =>  $email,
            'maritalStatus' => $marital_status,
            'gender' => $gender,
            'dateOfBirth' => $dateOfBirth,
            'add1' => $address1,
            'add2' => $address2,
            //'add3' => 'string',
            'postCode' => $postcode,
            //'city' => 'string',
            'state' => $state,
            //'country' => 'string'
            
        ];

        

        try{

            //$basicPremium = 400000;

            $response = app('App\Http\Controllers\InsuranceApiController')->basicPremium($params);

            
            
            $dutisetem = ParamDutiSetem::where('status', 1)->orderBy('id', 'Desc')->first();
            $sst = ParamSst::where('status', 1)->orderBy('id', 'Desc')->first();

            //dd($sst);
            //dd($response['basicPrem']);
            
            /*$basicPremium = $response['basicPrem'];
            $stampDutyAmt = $response['stampDutyAmt'];
            $ssT_Pct = $response['gst_Pct'];
            $ssT_Amt = $response['gst_Amt'];
            $rebateAmt = $response['rebateAmt'];
            $rebatePct = $response['rebatePct'];
            //$grossPrem = $response['grossPrem'];
            $ncdAmt = $response['ncdAmt'];
            $realBacicPremium = str_replace(',', '', $basicPremium);*/

            
            //return view('pages.frontend.apps-icop.select-insurance', compact('uuid', 'dutisetem', 'sst', 'basicPremium', 'realBacicPremium', 'stampDutyAmt', 'ssT_Pct', 'ssT_Amt', 'rebateAmt', 'rebatePct', 'ncdAmt', 'insCompany'));

            return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }


    }



    public function choseInsuranceShow($uuid)
    {   
        $insCompany = ParamInsuranceCompany::with('promo')->where('status', 1)->get();
        
        $basicPrem = InsuranceBasicPremium::where('uuid', $uuid)->first();


        return view('pages.frontend.apps-icop.select-insurance', compact('uuid', 'basicPrem', 'insCompany'));
    }


    public function choseAdditional(Request $request)
    {
        $uuid = $request->uuid;
        $basic_premium = $request->basic_premium;
        $sst = $request->sst;
        $duti_setem = $request->duti_setem;
        $insurance_id = $request->insurance_id;
        $payable = $request->payable;


        $data = Praaplication::where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();

        $relation = ZurichRelationship::get();
        $coverType = ZurichCoverType::get();

        $checkAccount = User::where('email', $data->email)->exists();

        //Save Aplication Selected Insurance 
        ApplicationSelectInsurance::where('uuid', $uuid)->update([
            'insurance_id' => $insurance_id,
            'basic_premium' => $basic_premium,
            'sst' => $sst,
            'duti_setem' => $duti_setem,
            'payable' => $payable,
        ]);

        $selectedInsurance = ApplicationSelectInsurance::where('uuid', $uuid)
            ->orderBy('id', 'Desc')
            ->first();
        
        
        return redirect()->route('additional-protection.show', ['uuid' => $uuid]);

        //return view('pages.frontend.apps-icop.additional-protection', compact('uuid', 'data', 'cif', 'checkAccount', 'selectedInsurance', 'relation', 'coverType'));
    }


    public function additionalProtection($uuid)
    {
       
        $data = Praaplication::where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();

        $extraCovers = InsuranceExtraCover::where('uuid', $uuid)
            ->whereIn('extCoverCode', ['89A', '25', '72', '100', '200', '57', '01', '202', '112', 'XX'])
            ->select('extCoverCode','extCoverPrem', 'status')
            ->get();

        $sumExtraCover = InsuranceExtraCover::where('uuid', $uuid)
                        ->select('extCoverPrem')
                        ->where('status', 1)
                        ->sum('extCoverPrem');
        
        $intSumExtra = $sumExtraCover;

        $allowExtraCover = InsuranceAllowableExtraCover::where('uuid', $uuid)
                    ->whereIn('ext_Cvr_Code', ['89A', '25', '72', '100', '200', '57', '01', '202', '112'])
                    ->select('ext_Cvr_Code')
                    ->get();
        
        $allow = [];
        foreach ($allowExtraCover as $code) {
            $allow[] = $code->ext_Cvr_Code; // Add the ext_Cvr_Code to the $allow array
        }


        $covers = [];
        $expectedCoverCodes = ['89A', '25', '72', '100', '200', '57', '01', '202', '112', 'XX'];

        foreach ($expectedCoverCodes as $code) {
            $covers[$code] = [
                'status' => 0, // Default status to 0 (not selected)
                'extCoverPrem' => null, // Default premium to null
            ];
        }

        foreach ($extraCovers as $cover) {
            $covers[$cover->extCoverCode] = [
                'status' => $cover->status,
                'extCoverPrem' => $cover->extCoverPrem,
            ];
        }

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);

        $totalSst = $calcuAddProtect['totalSst'];

       
        /*$calculate = [
            'extraCover' => $extraCover,
            'payable' => $totalCalculte,
            'totalSst' => $totalSst
        ];*/

        $relation = ZurichRelationship::get();
        $coverType = ZurichCoverType::get();

        $checkAccount = User::where('email', $data->email)->exists();

        $selectedInsurance = ApplicationSelectInsurance::where('uuid', $uuid)
            ->orderBy('id', 'Desc')
            ->first();
        
        $basicPremium = InsuranceBasicPremium::where('uuid', $uuid)->first();

        $cart = SetupZurichCart::where('status', 1)->get();
        
        return view('pages.frontend.apps-icop.additional-protection', compact('uuid', 'data', 'cif', 'checkAccount',
                     'selectedInsurance', 'relation', 'coverType', 'basicPremium', 'cart', 'covers', 
                     'intSumExtra', 'allow', 'calcuAddProtect', 'totalSst'));
    
    }



    public function codeAddProtection(Request $request)
    {
        // Example API endpoint
        $url = 'https://sasnets.com/insurance_api/api/Zurich/GetFinalPremium';

        // Define headers
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
        ];

        // Extract request data
        $requestData = $request->all();

        try {
            // Send the POST request using Http facade
            $response = Http::withHeaders($headers)->post($url, $requestData);

            // Process the response
            $data = $response->json(); // Decodes the JSON response

            //dd($data);

            // Return the response data
            return response()->json($data);

        } catch (\Exception $e) {
            // Handle exceptions
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }



    public function summary(Request $request, $uuid)
    {

        
        return redirect()->route('summary.show', ['uuid' => $uuid]);
    }



    public function getsummary(Request $request, $uuid)
    {

        $data = Praaplication::where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();

        $basicPremium = InsuranceBasicPremium::where('uuid', $uuid)->first();

        $extraCovers = InsuranceExtraCover::where('uuid', $uuid)
            ->whereIn('extCoverCode', ['89A', '25', '72', '100', '200', '57', '01', '202', '112', 'XX'])
            ->select('extCoverCode','extCoverPrem', 'status')
            ->get();

        $covers = [];

        foreach ($extraCovers as $cover) {
            $covers[$cover->extCoverCode] = [
                'status' => $cover->status,
                'extCoverPrem' => $cover->extCoverPrem,
            ];
        }

        $checkAccount = User::where('email', $data->email)->exists();

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);

        $totalSst = $calcuAddProtect['totalSst'];

        $sumExtraCover = InsuranceExtraCover::where('uuid', $uuid)
                        ->select('extCoverPrem')
                        ->where('status', 1)
                        ->sum('extCoverPrem');
        
        $intSumExtra = $sumExtraCover;


        return view('pages.frontend.apps-icop.summary', compact(
            'uuid', 'data', 'checkAccount', 'checkAccount',
            'cif', 'basicPremium', 'covers', 'intSumExtra', 'totalSst'
        ));

    }


    public function detailFinancing($uuid)
    {

        $data = Praaplication::where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::where('uuid', $uuid)->first();

        $basicPremium = InsuranceBasicPremium::where('uuid', $uuid)->first();

        $extraCovers = InsuranceExtraCover::where('uuid', $uuid)
            ->whereIn('extCoverCode', ['89A', '25', '72', '100', '200', '57', '01', '202', '112', 'XX'])
            ->select('extCoverCode','extCoverPrem', 'status')
            ->get();

        $covers = [];

        foreach ($extraCovers as $cover) {
            $covers[$cover->extCoverCode] = [
                'status' => $cover->status,
                'extCoverPrem' => $cover->extCoverPrem,
            ];
        }

        $checkAccount = User::where('email', $data->email)->exists();

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);

        $totalSst = $calcuAddProtect['totalSst'];

        $sumExtraCover = InsuranceExtraCover::where('uuid', $uuid)
                        ->select('extCoverPrem')
                        ->where('status', 1)
                        ->sum('extCoverPrem');
        
        $intSumExtra = $sumExtraCover;


        return view('pages.frontend.apps-icop.detail-financing', compact(
            'uuid', 'data', 'checkAccount', 'checkAccount',
            'cif', 'basicPremium', 'covers', 'intSumExtra', 'totalSst'
        ));

    }


    public function updateFinancing(Request $request, $uuid){

        $financingData = $request->financingData;
        ApplicationFinancing::where('uuid', $uuid)->update([
            'total_payable' => $financingData
        ]);
    }


    public function financing(Request $request)
    {
       
        $uuid = $request->uuid;
        $total_payable = $request->total_payable;
        $fullname = $request->fullname;
        $a = $request->a;

        //dd($uuid);


        $data = ApplicationDocument::where('uuid', $uuid)->first();
        $checkAccount = User::where('email', $request->email_hidden)->exists();
        $checkEmail = User::where('email', $request->email)->exists();

        

        Praaplication::where('uuid', $uuid)->update([
            
            'pay_status' => 'F',
            
        ]);


       

        ApplicationFinancing::where('uuid', $uuid)->update([
            
            'total_payable' => $total_payable,
            
        ]);



        /*  ========   Check if Guest Account Create is disable ===========  */
        if( Auth::guest() && $checkEmail == false){
            $user = User::create([
                'name' => $request->fullname,
                'email' => $request->email,
                'password' =>  Hash::make($request->password),
                'role' => 6,
                'status' => 1,
            ]);


            if ($user) {
                // Log in the user
                Auth::login($user);
        
                $user->save();
            }

            
            return redirect('financing/'.$uuid);


        } elseif(Auth::guest() && $checkEmail == true){  //just login

            
            $credentials = $request->only('email', 'password');
 
            if (Auth::attempt($credentials)) {
                
                return redirect()->intended('financing/'.$uuid);

            }else{

                return Redirect::back()->withErrors(['msg' => 'The Message']);
            }

        }else{


            return redirect('financing/'.$uuid);

        }
      
        

    }


    public function getFinancing($uuid)
    {

        $data = ApplicationDocument::where('uuid', $uuid)->first();

        $doc = SetupDocument::where('code', 'BPA1')->where('status', 1)->first();
        $monthly = SetupMonthlyInstallment::orderBy('installment', 'Asc')->where('status', 1)->get();

        $finance = ApplicationFinancing::orderBy('id', 'Desc')->first();

        $selectedMonthId = ApplicationFinancing::where('uuid', $uuid)->first();
       

        //$cif = ApplicationCustomerInfo::where('uuid', $uuid)->orderBy('id', 'Desc')->first();

        return view('pages.frontend.apps-icop.financing', compact('uuid', 'data', 
            'doc', 'monthly', 'finance', 'selectedMonthId'));

    }
    


    public function coverLegalLiability(Request $request, $uuid)
    {
       
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '72',
            'noofunit' => '0'
        ];

        

        try{

            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            
            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverLegalLiabilityNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "72",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "72",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverStrike(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '25',
            'noofunit' => '0'
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            //dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverStrikeNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "25",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "25",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverLegalLiabilityPsg(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '100',
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            //dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverLegalLiabilityPsgNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "100",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "100",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }

    public function coverPaPlus(Request $request, $plan, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $planInt = intval($plan);

       // dd($planInt);
        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '200',
            "noOfUnit" => $planInt,
        ];


        try{

            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverPaPlusNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "200",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "200",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverPerils(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '57',
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            //dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverPerilsNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "57",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "57",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverAllDriver(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '01',
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverAllDriverNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "01",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "01",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverTowing(Request $request, $plan, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '202',
            "sumInsured" => $plan,
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverTowingNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "202",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "202",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverCart(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');
        $selectedCartOption = $request->selected_cart_option;

        $zurichCart = DB::table('setup_zurich_carts')
            ->select('zurich_allowable_cart_days.cart_day', 'zurich_allowable_cart_amounts.cart_ammount')
            ->leftJoin('zurich_allowable_cart_amounts', 'setup_zurich_carts.cart_amount_id', '=', 'zurich_allowable_cart_amounts.id')
            ->leftJoin('zurich_allowable_cart_days', 'setup_zurich_carts.cart_day_id', '=', 'zurich_allowable_cart_days.id')
            ->where('setup_zurich_carts.id', $selectedCartOption)
            ->first();

            //dd($zurichCart->cart_day);
        $intDay = (int)$zurichCart->cart_day;
        $intAmount = (int)$zurichCart->cart_ammount;

        

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '112',
            'unitDay' => $intDay,
            'unitAmount' => $intAmount,
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverCartNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "112",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "112",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);


            //dd($calcuAddProtect['extraCover']);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function coverWindscreen(Request $request, $uuid)
    {
        $windscreenValue = $request->windscreenValue;

        $winsV = intval($windscreenValue);

        

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        
        $params = [

            'uuid' => $uuid,
            'quotationNo' => $insPremium->quotationNo,
            "eId" => $getInfo->eId,
            "nric" => $pra->ic, // Static value for now
            "fullName" => $insBasic->name,
            "hpNo" => $cif->phone,
            "email" => $pra->email,
            "maritalStatus" => $insBasic->maritalSts, // M (married)
            "gender" => $insBasic->gender,
            "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
            "add1" => $cif->address1,
            "add2" => $cif->address2,
            "add3" => $cif->address2, // Same as add2
            "postCode" => $insBasic->postCode,
            "city" => $dataPostcode->stateName, // Static for now, change if dynamic
            "state" => $dataPostcode->stateName, // Static for now, change if dynamic
            "country" => $dataPostcode->countryName, // Static for now, change if needed

            "extCovCode" => '89A',
            "sumInsured" => $winsV,
        ];

        

        try{


            $responseExtraCover = app('App\Http\Controllers\InsuranceApiController')->addCoverage($params);
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($responseExtraCover) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseExtraCover['premium'],
                    'extCode' => $responseExtraCover['extCode'],
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function coverWindscreenNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "89A",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "89A",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function Roadtax(Request $request, $uuid)
    {
       
     
        $cif = ApplicationCustomerInfo::select('region')->where('uuid', $uuid)->first();
        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

       
        $extCoverCode = 'XX';

        $params = [

            'uuid' => $uuid,
            'region' => $cif->region,
            'type' => 1,
            'engine' => $insBasic->capacity,

        ];


        try{

            $responseRoadtax = app('App\Http\Controllers\CalculateRoadTaxController')->calculateRoadtax($params);

            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $extCoverCode
                ],
                [
                    'extCoverSumInsured' => 0,
                    'extCoverPrem' => $responseRoadtax['roadtax_amount'],
                    'compulsory_Ind' => null,
                    'status' => 1,
                ]
            );


            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            

            if($calcuAddProtect) {

                return response()->json([
                    'success' => true,
                    'premium' => $responseRoadtax['roadtax_amount'],
                    'extCode' => $extCoverCode,
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                    'totalSst' => $calcuAddProtect['totalSst'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            //return redirect()->route('chose-insurance.show', ['uuid' => $uuid]);


        }catch (Exception $e) {
            
            // Log the exception
            Log::error('Exception caught:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            dd($e->getMessage());
            
            return back()->withInput()->with('error', 'error 303 : error get basic premium');

        }
    }


    public function RoadtaxNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "XX",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "XX",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
                'totalSst' => $calcuAddProtect['totalSst'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }















    public function zurichQuotation(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        // /dd($insBasic);

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '25',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            dd($decodedBody);

            //dd($decodedBody['data']['extraCoverData'][0]['extCoverCode']);
            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;
            $extCoverCode = $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null;

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            //InsuranceExtraCover
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );


            //calculateTotalAddProtection
            
            $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);
            //return $decodedBody;
            //dd($calcuAddProtect);

            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                    'extCode' => $extCoverCode,
                    'status' => true,
                    'totalExtCover' => $calcuAddProtect['extraCover'],
                    'payable' => $calcuAddProtect['payable'],
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }

    public function zurichQuotationStrikeNo(Request $request, $uuid)
    {
        $no = InsuranceExtraCover::updateOrCreate(
            [
                'uuid' => $uuid,
                'extCoverCode' => "25",
            ],
            
            [
                'status' => 0,
            ]
        );

        $calcuAddProtect = app('App\Http\Controllers\AdditionalProtectCalculateController')->calculateProtect($uuid);


        if($no) {

            return response()->json([
                'success' => true,
                'status' => false,
                'extCode' => "25",
                'totalExtCover' => $calcuAddProtect['extraCover'],
                'payable' => $calcuAddProtect['payable'],
            ]);

        }else{

            // In case of failure, return a failure response
            return response()->json([
                'success' => false,
                'message' => 'Failed to calculate premium.',
            ], 400); // 400 Bad Request
            
        }

    }


    public function zurichQuotationLegal(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        // /dd($insBasic);

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '72',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;
            $extCoverCode = $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null;

            
            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );

            //return $decodedBody;

            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                    'extCode' => $extCoverCode,
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function zurichQuotationLegalPassenger(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        // /dd($insBasic);

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '100',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);


            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;
            $extCoverCode = $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null;

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }


            //dd($decodedBody);
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );
           
            //return $decodedBody;
            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                    'extCode' => $extCoverCode,
                    'status' => true,
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }

    public function zurichQuotationPaPlus(Request $request, $plan, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        $planInt = intval($plan);


        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '200',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0,
                                    'noofunit' => $planInt
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;
            $extCoverSumInsured = $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null;
            $extCoverCode = $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null;

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            if ($extCoverSumInsured !== null) {
                // Remove commas and convert to float
                $extCoverSumInsured = floatval(str_replace(',', '', $extCoverSumInsured));
            }


            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $extCoverSumInsured,
                    'extCoverPrem' => $extCoverPrem,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );

            //return $decodedBody;
            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                    'extCode' => $extCoverCode,
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function zurichQuotationPerils(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);



        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '57',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0,
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );
           

            return $decodedBody;
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }

    }


    public function zurichQuotationPrivateHire(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);



        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '57',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0,
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );
           

            return $decodedBody;
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function zurichQuotationTowing(Request $request, $uuid)
    {
        //dd($uuid);
        
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);



        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '202',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0,
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );

           

            return $decodedBody;
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }

    public function zurichQuotationWindscreen(Request $request, $uuid)
    {
        //dd($uuid);
        $windscreenValue = $request->windscreenValue;

        $winsV = intval($windscreenValue);

        //dd($winsV);
        
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);



        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '89A',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => $winsV,
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null);

            //dd($decodedBody);
            $extCoverSumInsured = $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null;
            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;

            if ($extCoverSumInsured !== null) {
                // Remove commas and convert to float
                $extCoverSumInsured = floatval(str_replace(',', '', $extCoverSumInsured));
            }

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $extCoverSumInsured,
                    'extCoverPrem' => $extCoverPrem,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );

            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request

            }

            //return $decodedBody;


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }


    }


    public function zurichQuotationCart(Request $request, $uuid)
    {
        
        $selectedCartOption = $request->selected_cart_option;

        /*$zurichCart = SetupZurichCart::join('')
        where('id', $selectedCartOption)->first();*/

        $zurichCart = DB::table('setup_zurich_carts')
            ->select('zurich_allowable_cart_days.cart_day', 'zurich_allowable_cart_amounts.cart_ammount')
            ->leftJoin('zurich_allowable_cart_amounts', 'setup_zurich_carts.cart_amount_id', '=', 'zurich_allowable_cart_amounts.id')
            ->leftJoin('zurich_allowable_cart_days', 'setup_zurich_carts.cart_day_id', '=', 'zurich_allowable_cart_days.id')
            ->where('setup_zurich_carts.id', $selectedCartOption)
            ->first();

            //dd($zurichCart->cart_day);
        $intDay = (int)$zurichCart->cart_day;
        $intAmount = (int)$zurichCart->cart_ammount;

        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        // /dd($insBasic);

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '112',
                                    'unitDay' => $intDay,
                                    'unitAmount' => $intAmount,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);
            $extCoverPrem = $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null;
            $extCoverSumInsured = $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null;
            $extCoverCode = $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null;

            if ($extCoverPrem !== null) {
                // Remove commas and convert to float
                $extCoverPrem = floatval(str_replace(',', '', $extCoverPrem));
            }

            if ($extCoverSumInsured !== null) {
                // Remove commas and convert to float
                $extCoverSumInsured = floatval(str_replace(',', '', $extCoverSumInsured));
            }

            //InsuranceExtraCover
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $extCoverSumInsured,
                    'extCoverPrem' => $extCoverPrem,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );


            //return $decodedBody;
            if($decodedBody) {

                return response()->json([
                    'success' => true,
                    'premium' => $extCoverPrem,
                    'extCode' => $extCoverCode,
                    'status' => true,
                ]);

            }else{

                // In case of failure, return a failure response
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to calculate premium.',
                ], 400); // 400 Bad Request
                
            }
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
        
        
        
        //dd($uuid);
    }

    public function zurichQuotationCartNo(Request $request, $uuid)
    {

    }


    public function zurichQuotationAllDriver(Request $request, $uuid)
    {
       
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();

        //dd($insBasic->sumInsured);
       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);

        // /dd($insBasic);

        try {
            $response = $client->request('POST', config('apiIcop.zurich_api_create_quotation'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'requestData' => [
                        'participantDetails' => [
                            'transactionReferenceNo' => "CAR2008881",
                            'requestDateTime' => $now
                        ],
                        'basicDetails' => [
                            //'quotationNo' => $params['quotationNo'],  //kalau sdh masuk covernote already generate artinya bisa
                            'agentCode' => $userName,
                            'transType' => 'B',    //New Business/Renewal
                            'vehicleNo' => $insBasic->vehicleNo,
                            'productCode' => $insBasic->productCode,  //PZ01 PRIVATE CAR – Z-DRIVER (Product Code)
                            'coverType' => $insBasic->coverType, //V-CO MOTOR COMPREHENSIVE (Cover Type)
                            'ciCode' => $insBasic->ciCode, //MX1 - Private Use Only (CI code)
                            'effDate' => $effDate,
                            'expDate' => $expDate,
                            'reconInd' => $insBasic->reconInd, //Recon Indicator.  Is Recon vehicle? Y- Yes, N - No
                            'yearOfMake' => $insBasic->yearOfMake,
                            'make' => $insBasic->make,
                            'model' => $insBasic->model,
                            'capacity' => $insBasic->capacity,
                            'uom' => $insBasic->uom,
                            'engineNo' => $insBasic->engineNo,
                            'chassisNo' => $insBasic->chassisNo,
                            'logBookNo' => $insBasic->logBookNo, //??
                            'regLoc' => $insBasic->regLoc, //Local Registration - Vehicle Register Location
                            'all_Driver_Ind' => "Y",
                            'noOfPassenger' => $insBasic->noOfPassenger,
                            'noOfDrivers' => $insBasic->noOfDrivers,
                            'insIndicator' => $insBasic->insIndicator, //Insured Type P – Personal, C- Company
                            'name' => $insBasic->name,
                            'insNationality' => $insBasic->insNationality, //Insured Nationality. L-Local, F -Foreign
                            'newIC' => $insBasic->newIC,
                            'dateOfBirth' => $dateOfBirth, //DD/MM/YYYY
                            'age' => $insBasic->age, 
                            'gender' => $insBasic->gender,
                            'maritalSts' => $insBasic->maritalSts,  //M 
                            'occupation' => $insBasic->occupation,  //99 - Others
                            'mobileNo' => $insBasic->mobileNo,
                            'address' => $insBasic->address,
                            'postCode' => $insBasic->postCode,
                            'regionCode' => $insBasic->regionCode,
                            'state' => $insBasic->state,
                            'country' => $insBasic->country,
                            'sumInsured' => $sumInsured, //Vehicle Sum Insured in RM
                            'abisi' => $abisi, //Market value Sum Insured
                            'chosen_SI_Type' => $insBasic->chosen_SI_Type,  //Rec_SI -Market Value -- AV_SI - Agreed value
                            'avInd' => $insBasic->avInd, //Agree Value Indicator. Y- Agreed Value, N-Non Agreed Value(Market value)
                            'pacInd' => $insBasic->pacInd, //PAC (Personal Auto Cover) Indicator. N-with PAC default
                            'pacUnit' => $insBasic->pacUnit,  //??????????????????????????
                            'all_Driver_Ind' => $insBasic->all_Driver_Ind //All Driver Indicator? Y- Yes, N - No   Ref- All Driver/Authorised driver conditions
                        ],
                        'extraCoverDetails' => [
                            'extraCoverData' => [
                                
                                [
                                    'extCovCode' => '01',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ],
                                [
                                    'extCovCode' => '25',
                                    'unitDay' => 0,
                                    'unitAmount' => 0,
                                    'effDate' => '',
                                    'expDate' => '',
                                    'sumInsured' => 0
                                ]
                            ]
                        ]
                        
                    ]
                ]
            ]);

            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();

            $decodedBody = json_decode($body, true);

            //dd($decodedBody);

            //InsuranceExtraCover
            InsuranceExtraCover::updateOrCreate(
                [
                    'uuid' => $uuid,
                    'extCoverCode' => $decodedBody['data']['extraCoverData'][0]['extCoverCode'] ?? null
                ],
                [
                    'extCoverSumInsured' => $decodedBody['data']['extraCoverData'][0]['extCoverSumInsured'] ?? null,
                    'extCoverPrem' => $decodedBody['data']['extraCoverData'][0]['extCoverPrem'] ?? null,
                    'compulsory_Ind' => $decodedBody['data']['extraCoverData'][0]['compulsory_Ind'] ?? null,
                    'status' => 1,
                ]
            );


            return $decodedBody;
            


        } catch (RequestException $e) {


            Log::error('ZrcApiCtr:', ['error' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse();
                $errorStatusCode = $errorResponse->getStatusCode();
                $errorBody = $errorResponse->getBody()->getContents();

                echo "Error status code: $errorStatusCode\n";
                echo "Error response body: $errorBody\n";
            } else {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
        
        
        
        //dd($uuid);
    }

    

    public function zurichQuotationPaPlusNo(Request $request, $uuid)
    {
        
       
        
        InsuranceExtraCover::updateOrCreate(
            ['uuid' => $uuid],
            [
                'status' => 0,
            ]
        );

    }

    public function zurichQuotationTowingNo(Request $request, $uuid)
    {
        
        
        
        InsuranceExtraCover::updateOrCreate(
            ['uuid' => $uuid],
            
            [
                'status' => 0,
            ]
        );

    }

    public function InsuranceTest(Request $request, $uuid)
    {
        
        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::select('quotationNo')->where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email', 'ic')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();
        $dataPostcode =  ZurichApiPostcode::select('stateName', 'regioncode', 'countryCode', 'countryName')
                        ->where('uuid', $uuid)
                        ->orderBy('id', 'Desc')
                        ->first();

        

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);
        $yearOfMake = intval($insBasic->yearOfMake);

        $noOfPassenger = intval($insBasic->noOfPassenger);

        //dd($cif->state);
        

        // /dd($insBasic);  /api/Zurich/GetUpdateCalculatePremiumForClient


        //try {
            
            $client = new Client();
            $url = 'https://sasnets.com/insurance_api/api/Zurich/checkextracovercharges';

            $requestData = [
                'quotationNo' => $insPremium->quotationNo,
                'reqGetCalculatePremimumForClientVM' => [
                    "eId" => $getInfo->eId,
                    "nric" => $pra->ic, // Static value for now
                    "fullName" => $insBasic->name,
                    "hpNo" => $cif->phone,
                    "email" => $pra->email,
                    "maritalStatus" => $insBasic->maritalSts, // M (married)
                    "gender" => $insBasic->gender,
                    "dateOfBirth" => $insBasic->dateOfBirth, // Static date format "YYYY-MM-DD"
                    "add1" => $cif->address1,
                    "add2" => $cif->address2,
                    "add3" => $cif->address2, // Same as add2
                    "postCode" => $insBasic->postCode,
                    "city" => $dataPostcode->stateName, // Static for now, change if dynamic
                    "state" => $dataPostcode->stateName, // Static for now, change if dynamic
                    "country" => $dataPostcode->countryName, // Static for now, change if needed
                ],
                'extraCoverDetails' => [
                    'extraCoverData' => [
                        [
                            'quotationNo' => "",
                            'chassisNo' => "",
                            'extCovCode' => '25', // Static for now
                            'unitDay' => 0,
                            'unitAmount' => 0,
                            'effDate' => '', // Empty for now
                            'expDate' => '', // Empty for now
                            'sumInsured' => 0,
                            "extCovDesc" => "" // Empty for now
                        ],

                    ]
                ]
            ];
            
            

            $headers = [
                'accept' => 'application/json',
                'Content-Type' => 'application/json',
                'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
            ];
    
            try {
                // Send the POST request
                $response = $client->post($url, [
                    'headers' => $headers,
                    'json' => $requestData
                ]);
    
                // Process the response
                $statusCode = $response->getStatusCode();
                $body = $response->getBody();
                $data = json_decode($body, true);
    
                dd($data);
    
                
                //dd($allowableExtCvrArray);
    
                
    
                //dd($data['data']['basicDetails']['engineNo']);   ???
                //dd($data['data']['basicDetails']['logBookNo']); ??
                //dd($data['data']['basicDetails']['insNationality']);
                //dd($data['data']['basicDetails']['mobileNo']);
                //dd($data['data']['basicDetails']['address']);
    
                //dd($data['data']['basicDetails']['address']);
    
                $dateOfBirthInput = $data['data']['basicDetails']['dateOfBirth'] ?? null;
                $effDateInput = $data['data']['basicDetails']['effDate'] ?? null;
                $expDateInput = $data['data']['basicDetails']['expDate'] ?? null;
                $transType = $data['data']['basicDetails']['transType'] ?? null;
    
                //dd($effDateInput);
    
                if ($dateOfBirthInput) {
                    
                    $dateOfBirth = Carbon::createFromFormat('d/m/Y', $dateOfBirthInput)->format('Y-m-d');
                } else {
                    
                    $dateOfBirth = null;
                }
    
    
                if ($effDateInput) {
                    
                    $effDate = Carbon::createFromFormat('d/m/Y', $effDateInput)->format('Y-m-d');
                } else {
                    
                    $effDate = null;
                }
    
    
                if ($expDateInput) {
                    
                    $expDate = Carbon::createFromFormat('d/m/Y', $expDateInput)->format('Y-m-d');
                } else {
                    
                    $expDate = null;
                }
    
    
                InsuranceBasicDetail::updateOrCreate(
                    ['uuid' => $params['uuid']],
                    [
                        'transType' => $transType,
                        'vehicleNo' => $data['data']['basicDetails']['vehicleNo'] ?? null,
                        'productCode' => $data['data']['basicDetails']['productCode'] ?? null,
                        'coverType' => $data['data']['basicDetails']['coverType'] ?? null,
                        'ciCode' => $data['data']['basicDetails']['ciCode'] ?? null,
                        'effDate' => $effDate,
                        'expDate' => $expDate,
                        'reconInd' => $data['data']['basicDetails']['reconInd'] ?? null,
                        'yearOfMake' => $data['data']['basicDetails']['yearOfMake'] ?? null,
                        'make' => $data['data']['basicDetails']['make'] ?? null,
                        'model' => $data['data']['basicDetails']['model'] ?? null,
                        'capacity' => $data['data']['basicDetails']['capacity'] ?? null,
                        'uom' => $data['data']['basicDetails']['uom'] ?? null,
                        'engineNo' => $data['data']['basicDetails']['engineNo'] ?? null,
                        'chassisNo' => $data['data']['basicDetails']['chassisNo'] ?? null,
                        'logBookNo' => $data['data']['basicDetails']['logBookNo'] ?? null,
                        'regLoc' => $data['data']['basicDetails']['regLoc'] ?? null,
                        'regionCode' => $data['data']['basicDetails']['regionCode'] ?? null,
                        'noOfPassenger' => $data['data']['basicDetails']['noOfPassenger'] ?? null,
                        'noOfDrivers' => $data['data']['basicDetails']['noOfDrivers'] ?? null,
                        'insIndicator' => $data['data']['basicDetails']['insIndicator'] ?? null,
                        'name' => $data['data']['basicDetails']['name'] ?? null,
                        'insNationality' => $data['data']['basicDetails']['insNationality'] ?? null,
                        'newIC' => $data['data']['basicDetails']['newIC'] ?? null,
                        'othersID' => $data['data']['basicDetails']['othersID'] ?? null,
                        'dateOfBirth' => $dateOfBirth,
                        'age' => $data['data']['basicDetails']['age'] ?? null,
                        'gender' => $data['data']['basicDetails']['gender'] ?? null,
                        'maritalSts' => $data['data']['basicDetails']['maritalSts'] ?? null,
                        'occupation' => $data['data']['basicDetails']['occupation'] ?? null,
                        'mobileNo' => $data['data']['basicDetails']['mobileNo'] ?? null,
                        'address' => $data['data']['basicDetails']['address'] ?? null,
                        'postCode' => $data['data']['basicDetails']['postCode'] ?? null,
                        'state' => $data['data']['basicDetails']['state'] ?? null,
                        'country' => $data['data']['basicDetails']['country'] ?? null,
                        'sumInsured' => $data['data']['basicDetails']['sumInsured'] ?? null,
                        'abisi' => $data['data']['basicDetails']['abisi'] ?? null,
                        'chosen_SI_Type' => $data['data']['basicDetails']['chosen_SI_Type'] ?? null,
                        'avInd' => $data['data']['basicDetails']['avInd'] ?? null,
                        'pacInd' => $data['data']['basicDetails']['pacInd'] ?? null,
                        'pacUnit' => $data['data']['basicDetails']['pacUnit'] ?? null,
                        'all_Driver_Ind' => $data['data']['basicDetails']['all_Driver_Ind'] ?? null
                    ]
                );
    
    
                InsuranceBasicPremium::updateOrCreate(
                    ['uuid' => $params['uuid']],
                    [
                        'success' => $data['data']['success'] ?? null,
                        'errorMsg' => $data['data']['errorMsg'] ?? null,
                        'quotationNo' => $data['data']['quotationNo'] ?? null,
                        'totBasicNetAmt' => $data['data']['totBasicNetAmt'] ?? null,
                        'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                        'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                        'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                        'rebatePct' => $data['data']['rebatePct'] ?? null,
                        'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                        'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                        'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                        'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                        'ncd_Pct' => $data['data']['ncd_Pct'] ?? null,
                        'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                    ]
                );
    
    
                // Assuming $params is the array containing the 'respParamFromGetQuotationVM'
                $allowableExtCvrArray = $data['data']['respParamFromGetQuotationVM']['allowableExtCvr'] ?? null;
    
                foreach ($allowableExtCvrArray as $extCvrData) {
                    InsuranceAllowableExtraCover::updateOrCreate(
                        [
                            'ext_Cvr_Code' => $extCvrData['ext_Cvr_Code'] ?? null, // Assuming 'uuid' is the unique identifier
                        ],
                        [
                            'uuid' => $params['uuid'],
                            'ext_Cvr_Desc' => $extCvrData['ext_Cvr_Desc'] ?? null,
                            'applicable_Ind' => $extCvrData['applicable_Ind'] ?? null,
                            'compulsory_Ind' => $extCvrData['compulsory_Ind'] ?? null,
                            'recommended_Ind' => $extCvrData['recommended_Ind'] ?? null,
                            'endorsement_Ind' => $extCvrData['endorsement_Ind'] ?? null,
                            'eC_Input_Type' => $extCvrData['eC_Input_Type'] ?? null,
                        ]
                    );
                }
    
    
                
    
                // Return the response data
                $returnCalculate = [
    
                    'basicPrem' => $data['data']['totBasicNetAmt'] ?? null,
                    'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                    'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                    'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                    'rebatePct' => $data['data']['rebatePct'] ?? null,
                    'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                    'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                    'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                    'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                    'ncd_Pct' => $data['data']['ncdAmt'] ?? null,
                    'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                    
                ];
    
                return $returnCalculate;
    
    
            } catch (\Exception $e) {
                // Handle any errors
    
                dd($e);
    
                return response()->json([
                    'error' => 'An error occurred: ' . $e->getMessage()
                ], 500);
            }


    }


    public function InsuranceTestFinal(Request $request, $uuid)
    {
        $data = PartnerApiAccount::where('partner_code', '01')->first();
        
        $userName = $data->userName;
        $token = $data->token;

        $now = Carbon::now()->format('Y-m-d\TH:i:s.v');

        $client = new Client();

        $insBasic = InsuranceBasicDetail::where('uuid', $uuid)->first();
        $insPremium = InsuranceBasicPremium::where('uuid', $uuid)->first();

        $getInfo = InsuranceGetVehicleInfo::select('eId', 'chasisNo')->where('uuid', $uuid)->first();
        $pra = Praaplication::select('email')->where('uuid', $uuid)->first();
        $cif = ApplicationCustomerInfo::select('address1', 'address2', 'phone', 'state')->first();

        

       
        $dateOfBirth = Carbon::createFromFormat('Y-m-d', $insBasic->dateOfBirth)->format('d/m/Y');
        $effDate = Carbon::createFromFormat('Y-m-d', $insBasic->effDate)->format('d/m/Y');
        $expDate = Carbon::createFromFormat('Y-m-d', $insBasic->expDate)->format('d/m/Y');

        //dd($now);

        $sumInsured = intval($insBasic->sumInsured);
        $abisi = intval($insBasic->abisi);
        $yearOfMake = intval($insBasic->yearOfMake);

        $noOfPassenger = intval($insBasic->noOfPassenger);

        //dd($insPremium->quotationNo);
        

        // /dd($insBasic);  /api/Zurich/GetUpdateCalculatePremiumForClient


        //try {
            

            $client = new Client();
            $url = 'https://sasnets.com/insurance_api/api/Zurich/GetUpdateCalculatePremiumForClient';

            $requestData = [
                'quotationNo' => "MQ240000516960",
                'reqGetCalculatePremimumForClientVM' => [
                    "eId" => $getInfo->eId,
                    "nric" => "931212-34-3354", // Static value for now
                    "fullName" => $insBasic->name,
                    "hpNo" => $cif->phone,
                    "email" => $pra->email,
                    "maritalStatus" => $insBasic->maritalSts, // M (married)
                    "gender" => $insBasic->gender,
                    "dateOfBirth" => "1993-09-09", // Static date format "YYYY-MM-DD"
                    "add1" => $cif->address1,
                    "add2" => $cif->address2,
                    "add3" => $cif->address2, // Same as add2
                    "postCode" => $insBasic->postCode,
                    "city" => "Kuala Lumpur", // Static for now, change if dynamic
                    "state" => "Kuala Lumpur", // Static for now, change if dynamic
                    "country" => "Malaysia", // Static for now, change if needed
                ],
                'extraCoverDetails' => [
                    'extraCoverData' => [
                        [
                            'quotationNo' => "",
                            'chassisNo' => "",
                            'extCovCode' => '25', // Static for now
                            'unitDay' => 0,
                            'unitAmount' => 0,
                            'effDate' => '', // Empty for now
                            'expDate' => '', // Empty for now
                            'sumInsured' => 0,
                            "extCovDesc" => "" // Empty for now
                        ],

                        [
                            'quotationNo' => "",
                            'chassisNo' => "",
                            'extCovCode' => '72', // Static for now
                            'unitDay' => 0,
                            'unitAmount' => 0,
                            'effDate' => '', // Empty for now
                            'expDate' => '', // Empty for now
                            'sumInsured' => 0,
                            "extCovDesc" => "" // Empty for now
                        ],

                        [
                            'quotationNo' => "",
                            'chassisNo' => "",
                            'extCovCode' => '89A', // Static for now
                            'unitDay' => 0,
                            'unitAmount' => 0,
                            'effDate' => '', // Empty for now
                            'expDate' => '', // Empty for now
                            'sumInsured' => 500,
                            "extCovDesc" => "" // Empty for now
                        ]

                    ]
                ]
            ];
            

            
            

            $headers = [
                'accept' => 'application/json',
                'Content-Type' => 'application/json',
                'ApiKey' => 'InsurancePartnerApiKeyForExternalAccess'
            ];
    
            try {
                // Send the POST request
                $response = $client->post($url, [
                    'headers' => $headers,
                    'json' => $requestData
                ]);
    
                // Process the response
                $statusCode = $response->getStatusCode();
                $body = $response->getBody();
                $data = json_decode($body, true);
    
                dd($data);
    
                
                //dd($allowableExtCvrArray);
    
                
    
                //dd($data['data']['basicDetails']['engineNo']);   ???
                //dd($data['data']['basicDetails']['logBookNo']); ??
                //dd($data['data']['basicDetails']['insNationality']);
                //dd($data['data']['basicDetails']['mobileNo']);
                //dd($data['data']['basicDetails']['address']);
    
                //dd($data['data']['basicDetails']['address']);
    
    
                $dateOfBirthInput = $data['data']['basicDetails']['dateOfBirth'] ?? null;
                $effDateInput = $data['data']['basicDetails']['effDate'] ?? null;
                $expDateInput = $data['data']['basicDetails']['expDate'] ?? null;
                $transType = $data['data']['basicDetails']['transType'] ?? null;
    
                //dd($effDateInput);
    
                if ($dateOfBirthInput) {
                    
                    $dateOfBirth = Carbon::createFromFormat('d/m/Y', $dateOfBirthInput)->format('Y-m-d');
                } else {
                    
                    $dateOfBirth = null;
                }
    
    
                if ($effDateInput) {
                    
                    $effDate = Carbon::createFromFormat('d/m/Y', $effDateInput)->format('Y-m-d');
                } else {
                    
                    $effDate = null;
                }
    
    
                if ($expDateInput) {
                    
                    $expDate = Carbon::createFromFormat('d/m/Y', $expDateInput)->format('Y-m-d');
                } else {
                    
                    $expDate = null;
                }
    
    
                InsuranceBasicDetail::updateOrCreate(
                    ['uuid' => $params['uuid']],
                    [
                        'transType' => $transType,
                        'vehicleNo' => $data['data']['basicDetails']['vehicleNo'] ?? null,
                        'productCode' => $data['data']['basicDetails']['productCode'] ?? null,
                        'coverType' => $data['data']['basicDetails']['coverType'] ?? null,
                        'ciCode' => $data['data']['basicDetails']['ciCode'] ?? null,
                        'effDate' => $effDate,
                        'expDate' => $expDate,
                        'reconInd' => $data['data']['basicDetails']['reconInd'] ?? null,
                        'yearOfMake' => $data['data']['basicDetails']['yearOfMake'] ?? null,
                        'make' => $data['data']['basicDetails']['make'] ?? null,
                        'model' => $data['data']['basicDetails']['model'] ?? null,
                        'capacity' => $data['data']['basicDetails']['capacity'] ?? null,
                        'uom' => $data['data']['basicDetails']['uom'] ?? null,
                        'engineNo' => $data['data']['basicDetails']['engineNo'] ?? null,
                        'chassisNo' => $data['data']['basicDetails']['chassisNo'] ?? null,
                        'logBookNo' => $data['data']['basicDetails']['logBookNo'] ?? null,
                        'regLoc' => $data['data']['basicDetails']['regLoc'] ?? null,
                        'regionCode' => $data['data']['basicDetails']['regionCode'] ?? null,
                        'noOfPassenger' => $data['data']['basicDetails']['noOfPassenger'] ?? null,
                        'noOfDrivers' => $data['data']['basicDetails']['noOfDrivers'] ?? null,
                        'insIndicator' => $data['data']['basicDetails']['insIndicator'] ?? null,
                        'name' => $data['data']['basicDetails']['name'] ?? null,
                        'insNationality' => $data['data']['basicDetails']['insNationality'] ?? null,
                        'newIC' => $data['data']['basicDetails']['newIC'] ?? null,
                        'othersID' => $data['data']['basicDetails']['othersID'] ?? null,
                        'dateOfBirth' => $dateOfBirth,
                        'age' => $data['data']['basicDetails']['age'] ?? null,
                        'gender' => $data['data']['basicDetails']['gender'] ?? null,
                        'maritalSts' => $data['data']['basicDetails']['maritalSts'] ?? null,
                        'occupation' => $data['data']['basicDetails']['occupation'] ?? null,
                        'mobileNo' => $data['data']['basicDetails']['mobileNo'] ?? null,
                        'address' => $data['data']['basicDetails']['address'] ?? null,
                        'postCode' => $data['data']['basicDetails']['postCode'] ?? null,
                        'state' => $data['data']['basicDetails']['state'] ?? null,
                        'country' => $data['data']['basicDetails']['country'] ?? null,
                        'sumInsured' => $data['data']['basicDetails']['sumInsured'] ?? null,
                        'abisi' => $data['data']['basicDetails']['abisi'] ?? null,
                        'chosen_SI_Type' => $data['data']['basicDetails']['chosen_SI_Type'] ?? null,
                        'avInd' => $data['data']['basicDetails']['avInd'] ?? null,
                        'pacInd' => $data['data']['basicDetails']['pacInd'] ?? null,
                        'pacUnit' => $data['data']['basicDetails']['pacUnit'] ?? null,
                        'all_Driver_Ind' => $data['data']['basicDetails']['all_Driver_Ind'] ?? null
                    ]
                );
    
    
                InsuranceBasicPremium::updateOrCreate(
                    ['uuid' => $params['uuid']],
                    [
                        'success' => $data['data']['success'] ?? null,
                        'errorMsg' => $data['data']['errorMsg'] ?? null,
                        'quotationNo' => $data['data']['quotationNo'] ?? null,
                        'totBasicNetAmt' => $data['data']['totBasicNetAmt'] ?? null,
                        'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                        'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                        'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                        'rebatePct' => $data['data']['rebatePct'] ?? null,
                        'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                        'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                        'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                        'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                        'ncd_Pct' => $data['data']['ncd_Pct'] ?? null,
                        'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                    ]
                );
    
    
                // Assuming $params is the array containing the 'respParamFromGetQuotationVM'
                $allowableExtCvrArray = $data['data']['respParamFromGetQuotationVM']['allowableExtCvr'] ?? null;
    
                foreach ($allowableExtCvrArray as $extCvrData) {
                    InsuranceAllowableExtraCover::updateOrCreate(
                        [
                            'ext_Cvr_Code' => $extCvrData['ext_Cvr_Code'] ?? null, // Assuming 'uuid' is the unique identifier
                        ],
                        [
                            'uuid' => $params['uuid'],
                            'ext_Cvr_Desc' => $extCvrData['ext_Cvr_Desc'] ?? null,
                            'applicable_Ind' => $extCvrData['applicable_Ind'] ?? null,
                            'compulsory_Ind' => $extCvrData['compulsory_Ind'] ?? null,
                            'recommended_Ind' => $extCvrData['recommended_Ind'] ?? null,
                            'endorsement_Ind' => $extCvrData['endorsement_Ind'] ?? null,
                            'eC_Input_Type' => $extCvrData['eC_Input_Type'] ?? null,
                        ]
                    );
                }
    
    
    
                // Return the response data
                $returnCalculate = [
    
                    'basicPrem' => $data['data']['totBasicNetAmt'] ?? null,
                    'stampDutyAmt' => $data['data']['stampDutyAmt'] ?? null,
                    'gst_Amt' => $data['data']['gst_Amt'] ?? null,
                    'gst_Pct' => $data['data']['gst_Pct'] ?? null,
                    'rebatePct' => $data['data']['rebatePct'] ?? null,
                    'rebateAmt' => $data['data']['rebateAmt'] ?? null,
                    'totMtrPrem' => $data['data']['totMtrPrem'] ?? null,
                    'ttlPayablePremium' => $data['data']['ttlPayablePremium'] ?? null,
                    'ncdAmt' => $data['data']['ncdAmt'] ?? null,
                    'ncd_Pct' => $data['data']['ncdAmt'] ?? null,
                    'quote_Exp_Date' => $data['data']['quote_Exp_Date'] ?? null,
                    
                ];
    
                return $returnCalculate;
        
            } catch (\Exception $e) {
                // Handle any errors
    
                dd($e);
    
                return response()->json([
                    'error' => 'An error occurred: ' . $e->getMessage()
                ], 500);
            }

    }


  


}
