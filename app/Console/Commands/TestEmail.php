<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon;
use DB;
use Mail;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ic:emailtest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $toCustomerEmail = "wakudiallah@netxpert.com.my";
        $toCustomerLastName = "Alin";

        $today = Carbon\Carbon::now();


        $dataq = array(
                'today'=>$today, 
                'orderNumber' => "OXXXX-XXXXXXXX", 
                'toCustomerEmail' => "wakudiallah@netxpert.com.my",
                'toCustomerFirstName' => "Alyn",
                'toCustomerLastName' => "Alyn",
                'toCustomerPhone' => "01638234555",
                'toCustomerAddress' => "Dato Keramat"

                );

                Mail::send('email.test', $dataq, function($message) use ($toCustomerEmail, $toCustomerLastName){

                     $message->to($toCustomerEmail)->subject('Fashionlova - Order Confirmed');
                     //$message->cc(['wakudiallah05@gmail.com']);
                     $message->from('no-reply@fashionlova.com.my','Fashionlova');

                  });
    }
}
