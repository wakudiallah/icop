<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiDownloadPolicySchedule extends Model
{
    use HasFactory;

    //zurich_api_download_policy_schedules
    protected $table = 'zurich_api_download_policy_schedules';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
    
}
