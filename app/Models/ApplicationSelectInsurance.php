<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationSelectInsurance extends Model
{
    use HasFactory;

    //application_select_insurances
    protected $table = 'application_select_insurances';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function insuranceCompany() {
        return $this->belongsTo('App\Models\ParamInsuranceCompany', 'insurance_id', 'id'); 
    }

}


