<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiCalculatePremium_AllowableWaterDamageCoverage extends Model
{
    use HasFactory;

    //zurich_api_calculate_premium__allowable_water_damage_coverages

    protected $table = 'zurich_api_calculate_premium__allowable_water_damage_coverages';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

}
