<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiGetVehicleInfo extends Model
{
    use HasFactory;

    //zurich_api_get_vehicle_infos

    protected $table = 'zurich_api_get_vehicle_infos';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


}


