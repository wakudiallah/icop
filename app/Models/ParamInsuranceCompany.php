<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ParamInsurancePromo;

class ParamInsuranceCompany extends Model
{
    use HasFactory;
    //use \OwenIt\Auditing\Auditable;

    protected $table = 'param_insurance_companies';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function promo(){
        return $this->hasMany(ParamInsurancePromo::class, 'insurance_product_id', 'id' )->where('status', 1);
    }

    public function insurancePolicy() {
        return $this->belongsTo('App\Models\ParamInsurancePolicy', 'id', 'insurance_company_id'); 
    }

}
