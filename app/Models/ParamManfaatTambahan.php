<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParamManfaatTambahan extends Model implements Auditable
{
    use HasFactory;
    //param_manfaat_tambahans

    use \OwenIt\Auditing\Auditable;


    protected $table = 'param_manfaat_tambahans';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


}
