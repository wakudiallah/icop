<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;


    protected $table = 'audits';


    public function audit_user() {
        return $this->belongsTo('App\Models\User', 'user_id','id'); 
    }

}




    

