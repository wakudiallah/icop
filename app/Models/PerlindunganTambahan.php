<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerlindunganTambahan extends Model
{
    use HasFactory;
    //perlindungan_tambahans

    //use \OwenIt\Auditing\Auditable;


    protected $table = 'perlindungan_tambahans';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


}
