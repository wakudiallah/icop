<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationCustomerInfo extends Model
{
    use HasFactory;

    protected $table = 'application_customer_infos';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;




    public function marital() {
        return $this->belongsTo('App\Models\ZurichMaritalStatus','marital_status','code');	
        
    }

    public function region() {
        return $this->belongsTo('App\Models\ParamRegionlkm','id', 'region');	
        
    }


}
