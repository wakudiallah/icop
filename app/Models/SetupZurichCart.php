<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SetupZurichCart extends Model
{
    use HasFactory;

    protected $table = 'setup_zurich_carts';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function cart_amount() {
        return $this->belongsTo('App\Models\ZurichAllowableCartAmount', 'cart_amount_id','id'); 
    }


    public function cart_day() {
        return $this->belongsTo('App\Models\ZurichAllowableCartDay', 'cart_day_id','id'); 
    }

}
