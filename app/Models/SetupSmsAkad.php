<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SetupSmsAkad extends Model implements Auditable
{
    use HasFactory;

    use \OwenIt\Auditing\Auditable;


    protected $table = 'setup_sms_akads';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


}
