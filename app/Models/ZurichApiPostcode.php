<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiPostcode extends Model
{
    use HasFactory;

    //zurich_api_postcodes

    protected $table = 'zurich_api_postcodes';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
