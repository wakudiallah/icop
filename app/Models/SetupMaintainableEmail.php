<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SetupMaintainableEmail extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table = 'setup_maintainable_emails';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function emailfor() {
        return $this->belongsTo('App\Models\ParamEmailFor', 'email_for_id','id'); 
    }

    public function emailsystem() {
        return $this->belongsTo('App\Models\ParamEmailSystem', 'email_system_id','id'); 
    }

}
