<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiGetjpjstatus extends Model
{
    use HasFactory;

    //zurich_api_getjpjstatuses

    protected $table = 'zurich_api_getjpjstatuses';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
    
}
