<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsuranceAllowableExtraCover extends Model
{
    use HasFactory;

    //insurance_allowable_extra_covers
    protected $table = 'insurance_allowable_extra_covers';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
    
}
