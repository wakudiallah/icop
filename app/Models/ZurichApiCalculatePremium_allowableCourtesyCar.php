<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiCalculatePremium_allowableCourtesyCar extends Model
{
    use HasFactory;

    //zurich_api_calculate_premium_allowable_courtesy_cars

    protected $table = 'zurich_api_calculate_premium_allowable_courtesy_cars';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
