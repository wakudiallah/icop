<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postcodetable extends Model
{
    use HasFactory;
    protected $connection = 'mysql2';

    protected $table = 'postcodetables';

    public $timestamps = false;

}
