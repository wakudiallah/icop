<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParamInsurancePolicy extends Model implements Auditable
{
    use HasFactory;
    //
    use \OwenIt\Auditing\Auditable;


    protected $table = 'param_insurance_policies';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

    public function insurancePolicy() {
        return $this->belongsTo('App\Models\ParamInsuranceCompany', 'insurance_company_id','id'); 
    }

}
