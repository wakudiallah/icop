<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiGetvehiclemodel extends Model
{
    use HasFactory;

    // /zurich_api_getvehiclemodels
    protected $table = 'zurich_api_getvehiclemodels';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
