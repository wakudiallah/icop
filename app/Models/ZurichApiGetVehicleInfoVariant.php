<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiGetVehicleInfoVariant extends Model
{
    use HasFactory;

    //zurich_api_get_vehicle_info_variants

    protected $table = 'zurich_api_get_vehicle_info_variants';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
