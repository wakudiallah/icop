<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichProductCode extends Model
{
    use HasFactory;

    //zurich_product_codes
    protected $table = 'zurich_product_codes';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
