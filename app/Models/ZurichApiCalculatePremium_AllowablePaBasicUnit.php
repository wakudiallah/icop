<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiCalculatePremium_AllowablePaBasicUnit extends Model
{
    use HasFactory;

    protected $table = 'zurich_api_calculate_premium__allowable_pa_basic_units';
    //zurich_api_calculate_premium__allowable_pa_basic_units

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


}
