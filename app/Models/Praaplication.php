<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Praaplication extends Model 
{
    //implements Auditable
    use HasFactory;
    //use \OwenIt\Auditing\Auditable;

    protected $table = 'praaplications';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;



    public function pra_ehailing() {
        return $this->belongsTo('App\Models\ParamEhailing', 'ehailing','id'); 
    }

    public function statedesc() {
		return $this->belongsTo('App\Models\Postcode','postcode','postcode');	
		
	}

  public function finance() {
		return $this->belongsTo('App\Models\ApplicationFinancing','uuid','uuid');	
		
	}

  public function status() {
		return $this->belongsTo('App\Models\ParamStatusApplication','app_status','code_status_app');	
		
	}

  public function doc() {
		return $this->belongsTo('App\Models\ApplicationDocument','uuid','uuid');	
		
	}

  public function cif() {
		return $this->belongsTo('App\Models\ApplicationCustomerInfo','uuid','uuid');	
		
	}


}
