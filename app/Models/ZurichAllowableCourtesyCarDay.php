<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichAllowableCourtesyCarDay extends Model
{
    use HasFactory;

    //zurich_allowable_courtesy_car_days

    protected $table = 'zurich_allowable_courtesy_car_days';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
