<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichPerformanceAndAestheticModification extends Model
{
    use HasFactory;

    //zurich_performance_and_aesthetic_modifications

    protected $table = 'zurich_performance_and_aesthetic_modifications';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
