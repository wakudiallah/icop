<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichOccupation extends Model
{
    use HasFactory;

    protected $table = 'zurich_occupations';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
