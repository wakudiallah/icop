<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParamRoadtaxlkm extends Model implements Auditable
{
    use HasFactory;
    //param_roadtaxlkms

    use \OwenIt\Auditing\Auditable;

    protected $table = 'param_roadtaxlkms';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

    public function reg() {
        return $this->belongsTo('App\Models\ParamRegionlkm', 'region','id'); 
    }

    public function owner() {
        return $this->belongsTo('App\Models\ParamOwnershiplkm', 'ownership','id'); 
    }

}
