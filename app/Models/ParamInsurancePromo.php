<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParamInsurancePromo extends Model implements Auditable
{
    use HasFactory;
    //param_insurance_promos

    use \OwenIt\Auditing\Auditable;


    protected $table = 'param_insurance_promos';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

    public function insurance() {
        return $this->belongsTo('App\Models\ParamInsuranceCompany', 'insurance_product_id','id'); 
    }
}
