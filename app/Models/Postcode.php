<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postcode extends Model implements Auditable
{
    use HasFactory;
    //postcodes


    use \OwenIt\Auditing\Auditable;


    protected $table = 'postcodes';

    
    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function statedesc() {
		return $this->belongsTo('App\Models\State','state_code','state_code');	
		
	}

    public function state() {
        return $this->belongsTo('App\Models\State','state_code','state_code');   
        //Postcode dipunyai city
    }


}
