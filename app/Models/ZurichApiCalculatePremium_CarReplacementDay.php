<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZurichApiCalculatePremium_CarReplacementDay extends Model
{
    use HasFactory;

    //zurich_api_calculate_premium__car_replacement_days
    protected $table = 'zurich_api_calculate_premium__car_replacement_days';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

}
