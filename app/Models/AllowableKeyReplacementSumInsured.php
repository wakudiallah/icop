<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllowableKeyReplacementSumInsured extends Model
{
    use HasFactory;

    //allowable_key_replacement_sum_insureds

    protected $table = 'allowable_key_replacement_sum_insureds';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;

}
